# Copyright (C) 2021, University of Porto and Tau Toolbox developers.
#
# This file is part of Tautoolbox package.
#
# Tautoolbox is free software: you can redistribute it and/or modify it
# under the terms of version 3 of the GNU Lesser General Public License as
# published by the Free Software Foundation.
#
# Tau Toolbox is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser
# General Public License for more details.

from tautoolbox.functions import *
import tautoolbox as tau
import re
from copy import deepcopy
from inspect import getsource, signature, Parameter


class equation:
    def __init__(self, form):
        if isinstance(form, tau.equation):
            self.__dict__ = form.__dict__
            return
        elif callable(form):
            self.lhs = form
            self._lhs_st = re.sub(r"(\s{2})+|,\n", "", getsource(form))

            self.rhs = lambda x: x * 0
            self._rhs_st = re.sub(r"(\s{2})+|,\n", "", getsource(self.rhs))
        elif isinstance(form, str):
            (
                self.lhs,
                self.rhs,
                self._lhs_st,
                self._rhs_st,
            ) = tau.equation.fromstr(form)
        elif isinstance(form, list):
            len_form = len(form)
            if len_form == 1:
                if isinstance(form[0], tau.equation):
                    self.__dict__ = form[0].__dict__
                    return
                elif callable(form[0]):
                    self.lhs = form[0]
                    self._lhs_st = re.sub(
                        r"(\s{2})+|,\n", "", getsource(form[0])
                    )

                    self.rhs = lambda x: x * 0
                    self._rhs_st = re.sub(
                        r"(\s{2})+|,\n", "", getsource(self.rhs)
                    )
                elif isinstance(form[0], str):
                    (
                        self.lhs,
                        self.rhs,
                        self._lhs_st,
                        self._rhs_st,
                    ) = tau.equation.fromstr(form[0])

            elif len_form == 2:
                self.lhs, self.rhs = form
                self._lhs_st = re.sub(r"(\s{2})+|,\n", "", getsource(self.lhs))
                if isinstance(self.rhs, tau.Polynomial):
                    self._rhs_st = repr(self.rhs)
                else:
                    self._rhs_st = re.sub(
                        r"(\s{2})+|,\n", "", getsource(self.rhs)
                    )

            else:
                raise ValueError("tautoolbox: wrong list argument")

        if isinstance(self.rhs, list):
            self.varnames = "x y LAMBDA".split()
        elif len(self.lhs.__code__.co_varnames) == 3:
            self.varnames = "x y yo".split()
        else:
            self.varnames = "x y".split()
        self.nvars = len(self.varnames) - 1

    def info(self, options):
        obj = deepcopy(self)

        options = deepcopy(options)
        options.n = 2

        # if len(obj.lhs.__code__.co_varnames)  =2: this was the original
        # check if obj.lhs have only 2 variables with non default arguments
        if (
            len(
                [
                    v.default
                    for k, v in signature(obj.lhs).parameters.items()
                    if v.default == Parameter.empty
                ]
            )
            == 2
        ):
            opts = deepcopy(options)
            opts.nequations = 1

            D = obj.lhs(tau.Polynomial(options=opts), tau.operator(options))
        else:
            D = obj.lhs(
                tau.Polynomial(options=opts),
                tau.operator(options),
                tau.Polynomial(options=options),
            )
        if getattr(D, "isfred1", False):
            res = {"isfred1": D.isfred1, "kernel": D.kernel}
        else:
            res = {
                "height": D.opHeight(),
                "hasIntegral": any(D.hasIntegral),
                "derivOrder": D.odiff,
                "isEig": isinstance(obj.rhs, list),
            }

            if res["isEig"]:
                for i in range(len(obj.rhs)):
                    if obj.rhs[i] is not None:
                        D = obj.rhs[i](
                            tau.Polynomial(options=options),
                            tau.operator(options),
                        )
                        res["height"] = max(res.height, D.opHeight())
        return res

    @staticmethod
    def fromstr(form):
        if isinstance(form, list):
            form = form[0]

        isLinearized = False

        form = form.replace(" ", "")

        k = [m.start() for m in re.finditer("=", form)]

        if len(k) != 1:
            raise ValueError(
                "tautoolbox: equation must have only and " "only one equal sign"
            )

        k = k[0]

        # Heuristic to determin if the equation is linearized

        lhs_st = tau.equation.normalizeOperator(form[:k])

        if form.find("yo") != -1:
            isLinearized = True
            lhs_st = "lambda x, y, yo:" + lhs_st
            lhs = eval(lhs_st)
        else:
            lhs_st = "lambda x, y: " + lhs_st
            lhs = eval(lhs_st)

        if form[k + 1 :].find("LAMBDA") == -1:
            if isLinearized:
                rhs_st = "lambda x, yo: " + form[k + 1 :]

                rhs = eval(rhs_st)
            else:
                rhs_st = "lambda x: " + form[k + 1 :] + "+0*x"

                rhs = eval(rhs_st)

        else:
            # assume eig problem
            # here we will assume that all the terms are on the form of
            # lambda^p*f(x,y)
            rhs = []
            terms = tau.equation.parseeEquation(form[k + 1 :])

            for term in terms:
                k = [m.start() for m in re.finditer("LAM", form)]
                if k == []:
                    raise ValueError(
                        "Tautoolbox: term whitout lambda"
                        " for an eigenvalue problem"
                    )
                if k[0] != 0:
                    raise ValueError(
                        "Tautoolbox:  please place the lambda "
                        "at the begin of a term"
                    )
                if term[6] == "^":
                    start = [m.start() for m in re.finditer("*", term[7:])]
                    pow = float(term[7 : (start[0] + 1 + 6)])
                    start = start[0] + 8
                elif term[6] == "*":
                    pow = 0
                    start = 7
                else:
                    raise ValueError("tautoolbox: unknown eig expression")

                if len(rhs) <= pow or rhs[pow] is None:
                    rhs[pow] = term[start:]
                else:
                    rhs[pow] += " + " + term[start:]

            for k in range(len(rhs)):
                if rhs[k] is not None:
                    rhs[k] = lambda x, y: eval(rhs[k])

        return lhs, rhs, lhs_st, rhs_st

    @staticmethod
    def normalizeOperator(expression):
        # expression = tau.equation.normalizeDiff(expression)

        expression = re.sub(r"y([0-9]+)", r"y[\1]", expression)
        return re.sub(r"y([\)\(\-\+'\*\.,]|$)", r"y[0]\1", expression)

    @staticmethod
    def normalizeDiff(expression):
        diff_args = re.findall(r"diff\((y[0-9]*(?:,[0-9]+)?:*)\)", expression)

        var_diff_order = [da.split(",") for da in diff_args]

        for da in var_diff_order:
            if len(da) == 1:
                expression = expression.replace(
                    f"diff({da[0]})", da[0] + ".diff(1)"
                )
            else:
                expression = expression.replace(
                    f"diff({da[0]},{da[1]})", f"{da[0]}.diff({da[1]})"
                )
        return expression

    @staticmethod
    def parseeEquation(equation):
        if not isinstance(equation, str):
            raise TypeError("Tautoolbox: wrong argument")

        # Remove all spaces
        equation = equation.replace(" ", "")

        # insert a "+" before every "-" for easier parsing
        equation = equation.replace("-", "+-")

        # if the first char is a "+" discard it
        if equation[0] == "+":
            equation = equation[1:]

        # Find the nodes where to break the string, those will be the plus signs
        # outside of parenthesis expressions

        nodes = []
        parenthesis_level = 0

        for k, l in enumerate(equation):
            if l == "(":
                parenthesis_level += 1
            if l == ")":
                parenthesis_level -= 1

            if l == "+" and parenthesis_level == 0:
                nodes += [k]

        nodes += [len(equation)]
        funcell = [None] * len(nodes)

        start = 0
        for k, node in enumerate(nodes):
            funcell[k] = equation[start:node]
            start = node + 1

        return funcell

    def __str__(self):
        st = (
            f"Equation:\n    Left hand side:\n        {self._lhs_st}\n"
            f"     Right hand side:\n        {self._rhs_st}"
        )
        return st

    def __repr__(self):
        return self.__str__()
