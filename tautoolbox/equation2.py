#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jun  1 21:37:51 2022

@author: nilson
"""
import sympy as sp
from sympy.core.function import AppliedUndef


class Equation2:
    def __init__(self, eq):
        if isinstance(eq, str):
            sides = eq.split("=")
            if len(sides) == 1:

                sides = sp.sympify(sides[0])
                self.ind_vars = [str(i) for i in sides.free_symbols]
                self.dep_var = set(
                    [
                        str(i.func)
                        for i in sides.atoms(sp.Function)
                        if isinstance(i, AppliedUndef)
                    ]
                )
                args = sp.Add.make_args(sides)
                rhs = []
                for arg in args:
                    if (
                        len(
                            [
                                f.func
                                for f in arg.atoms(sp.Function)
                                if isinstance(f, AppliedUndef)
                            ]
                        )
                        == 0
                    ):
                        rhs.append(arg)
                        args.remove(arg)
                self.lhs = sum(args)
                self.rhs = -sum(rhs)
                diff_terms = self.lhs.atoms(sp.Derivative)
                self.diff_terms = diff_terms
                self.deriv_order = dict(
                    [(str(vo.args[-1][0]), int(vo.args[-1][1])) for vo in diff_terms]
                )
