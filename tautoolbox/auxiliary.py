#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Mar 12 01:18:17 2022

@author: nilson
"""
# import tautoolbox as tau

import numpy as np
import tautoolbox as tau
from numbers import Number
from collections.abc import Iterable
from scipy.sparse import spdiags, issparse, eye as speye, csr_matrix
from scipy.linalg import qz, toeplitz, hankel
from scipy.special import gamma, loggamma, beta
from scipy.fft import fft, ifft
from scipy.optimize import fminbound


def readiness_check(
    values,
    vscale=0,
    hscale=1,
    state="standard",
    tol=np.spacing(1),
    basis="ChebyshevT",
):
    vscale = max(vscale, np.max(np.abs(values)))
    n = values.shape[-1]

    # The abcissas are the nodes of ChebyshevU basis
    coef = interp_from_values(values, basis=basis)

    if coef.ndim == 1:
        coef = coef.reshape(1, -1)
        values = values.reshape(1, -1)
    m = coef.shape[0]
    if state == "standard":
        vscaleF = np.max(np.abs(values), axis=1)
        if vscaleF.size == 1:
            vscaleF = vscaleF[0]

        tol = tol * np.maximum(hscale, vscale / vscaleF)
        is_ok = np.zeros(m, dtype=bool)
        cutoff = np.zeros(m)
        for i in range(m):
            cutoff[i] = standard_chop(coef[i], tol)
            # Cheak readiness
            is_ok[i] = cutoff[i] < n
            # Exit if any column is not ok
            if not is_ok[i]:
                break
        return np.all(is_ok), np.max(cutoff)


def standard_chop(coef, tol):
    if tol >= 1:
        return 1

    n = len(coef)
    cutoff = n

    if n < 17:
        return n
    b = np.abs(coef)
    m = b[-1] * np.ones(n)

    for i in range(n - 2, -1, -1):
        m[i] = np.maximum(b[i], m[i + 1])

    if m[0] == 0:
        return 1

    envelope = m / m[0]

    for i in range(2, n):
        i2 = round(1.25 * i + 5)
        if i2 > n:
            # There is no plateau
            return cutoff

        e1 = envelope[i - 1]
        e2 = envelope[i2 - 1]
        r = 3 * (1 - np.log(e1) / np.log(tol))
        plateau = (e1 == 0) or (e2 / e1 > r)
        if plateau:
            plateau_point = i - 1
            break

    if envelope[plateau_point - 1] == 0:
        cutoff = plateau_point
    else:
        i3 = np.sum(envelope >= tol ** (7 / 6))
        if i3 < i2:
            i2 = i3 + 1
            envelope[i2 - 1] = tol ** (7 / 6)
        cc = np.log10(envelope[:i2])

        cc += np.linspace(0, (-1 / 3) * np.log10(tol), i2)
        d = np.argmin(cc)
        cutoff = np.maximum(d, 1)
    return cutoff


def interp_from_values(values, abcissas=None, **kwargs):
    r"""
    Get the coefficients of a Polynomial aproximation in a given orthogonal
    basis of a function with the values evaluated at the abcissas.


    Parameters
    ----------
    values : scalar or array_like
        The values of a function evaluated at the abcissas.
    abcissas : scalar or array_like, optional
        The abcissas corresponding to the values.
    **kwargs : dict
        bases : str
            The bases we want the coefficients
        domain : array_like
            The domain of the basis
        alpha : scalar
            This only make sense for Gegenbauer bases
    Returns
    -------
    TYPE
        DESCRIPTION.

    """
    bas = tau.basis(tau.settings(**kwargs))
    n = values.shape[-1]

    # When abcsissas are  not given assumes that the values are evaluated at
    # ChebyshevU nodes
    if abcissas is None:
        abcissas, *_ = tau.basis(
            tau.settings(basis="ChebyshevU", domain=bas.domain)
        ).nodes(n)
    A = bas.polyvalb(abcissas, n=n)

    return np.linalg.solve(A, values.T).T


def get_tol(xx, yy, values, domain=[[-1, 1]] * 2, level=np.finfo(float).eps):
    m, n = xx.shape
    if isinstance(values, Number):
        values = values * np.ones((m, n))

    grid = max(m, n)
    dfdx = 0
    dfdy = 0
    if m > 1 and n > 1:
        # Pad  the arrays to garante df/dx and df/dy have the same size.
        dfdx = np.diff(values[: m - 1, :]) / np.diff(xx[: m - 1, :])
        dfdy = np.diff(values[:, : n - 1], axis=0) / np.diff(
            yy[:, : n - 1], axis=0
        )
    elif m > 1 and n == 1:
        # Constant in the y direction
        dfdy = np.diff(values) / np.diff(yy)
    elif m == 1 and n > 1:
        # constant in the x direction
        dfdx = np.diff(values, axis=0) / np.diff(xx, axis=0)

    # Approximation for the norm of the gradient over the domain
    jac_norm = np.max((np.max(np.abs(dfdx)), np.max(np.abs(dfdy))))

    vscale = np.max(np.abs(values))

    rtol = grid ** (2 / 3) * level
    atol = np.max(np.abs(domain)) * np.max((jac_norm, vscale)) * rtol
    return rtol, atol


def adaptive_cross_approx(V, atol, factor):
    m, n = V.shape
    width = np.min((m, n))
    pivot_vals = []  # Store an unknown number of pivots
    pivot_pos = []  # store the positions of the pivots
    fail = True  # assume we fail

    # Main algorithm
    zero_rows = 0  # Count the number of zero rows
    ind = np.argmax(np.abs(V))
    row, col = np.unravel_index(ind, V.shape)
    inf_norm = np.abs(V[row, col])

    # Bias toward diagonal for square matrices
    if m == n and np.max(np.abs(np.diag(V))) - inf_norm > -atol:
        ind = np.argmax(np.abs(np.diag(V)))
        inf_norm = np.abs(np.diag(V))[ind]
        col = ind
        row = ind

    rows = np.zeros((0, n))
    cols = np.zeros((m, 0))

    while inf_norm > atol and zero_rows < width / factor and zero_rows < width:
        # Extract the row and the column

        rows = np.concatenate([rows, V[[row], :]])
        cols = np.concatenate([cols, V[:, [col]]], axis=1)

        piv_val = V[row, col]
        V -= cols[:, [zero_rows]] @ (rows[[zero_rows], :] / piv_val)

        # Keep track of progress
        zero_rows += 1  # One more row is zero
        pivot_vals.append(piv_val)  # Store the value of the pivot
        pivot_pos.append([row, col])  # Store the position of the pivot

        # Next pivot
        ind = np.argmax(np.abs(V))
        row, col = np.unravel_index(ind, V.shape)
        inf_norm = np.abs(V[row, col])

        if m == n and np.max(np.abs(np.diag(V))) - inf_norm > -atol:
            ind = np.argmax(np.abs(np.diag(V)))
            inf_norm = np.abs(np.diag(V))[ind]

            row = ind
            col = ind

    if inf_norm <= atol:
        fail = False

    if zero_rows >= width / factor:
        fail = True

    return (
        np.array(pivot_vals),
        np.array(pivot_pos, dtype=int),
        rows,
        cols,
        fail,
    )


def refine_grid(n, fun_type=None):
    if fun_type in ["trig", "periodic"]:
        n = 2 ** (np.floor(np.log2(n)) + 1)
    else:
        n = 2 ** (np.floor(np.log2(n)) + 1) + 1

    nest = np.arange(0, n, 2)
    return int(n), nest


def nodes(n, domain=[-1, 1]):
    """


    Parameters
    ----------
    n : Integer
        DESCRIPTION. The Number of nodes
    domain : Iterable, optional
        DESCRIPTION. The default is [-1, 1]. A length 2 iterable of numbers

    Returns
    -------
    Array_like
        DESCRIPTION. An array like object of of numbers which are the nodes

    """

    return tau.basis(tau.settings(basis="ChebyshevU", domain=domain)).nodes(n)[
        0
    ]


def parse_domain(domain):
    if not (
        isinstance(domain, Iterable)
        and len(domain) == 2
        and all(
            [
                isinstance(i, Iterable)
                and len(i) == 2
                and all([isinstance(j, Number) for j in i])
                for i in domain
            ]
        )
    ):
        raise ValueError("Domain must be an 2X2 array_like object of numbers")

    domain = np.array(domain)
    if domain[0, 0] >= domain[0, 1] or domain[1, 0] >= domain[1, 1]:
        raise ValueError(
            "The left hand boundaries of the domains must be "
            "less than the right hand boundaries "
        )
    return domain


def parse_bases(bases):
    if not (
        isinstance(bases, Iterable)
        and len(bases) == 2
        and all([isinstance(b, str) for b in bases])
    ):
        raise ValueError(
            "Polynomial2: bases must be an Iterable with "
            "two string elements e.g., ['ChebyshevT','LegendreP']"
        )
    return bases if isinstance(bases, list) else list(bases)


def chebt2leg_mat(n):
    """
    Construct the conversion matrix from chebyshev of the first kind coeffi-
    cients to Legendre coefficients

    Parameters
    ----------
    n : int
        The Dimension of the conversion matrix.

    Returns
    -------
    res : array_like
        A nxn conversion matrix

    """
    vals = np.zeros(2 * n)
    vals[0] = np.pi ** (1 / 2)
    vals[1] = 2 / vals[0]
    for i in range(2, 2 * n - 1, 2):
        vals[i] = vals[i - 2] * (1 - 1 / i)
        vals[i + 1] = vals[i - 1] * (1 - 1 / (i + 1))

    res = np.zeros((n, n))
    for i in range(n):
        if i + 2 < n:
            res[i, i + 2 : n : 2] = (
                -np.arange(i + 2, n, 2)
                * (i + 0.5)
                * (vals[: n - i - 2 : 2] / (np.arange(i + 2, n, 2) - i))
                * (
                    vals[2 * i + 1 : n + i - 1 : 2]
                    / (np.arange(i + 2, n, 2) + i + 1)
                )
            )
    c = np.sqrt(np.pi) / 2
    res[np.arange(1, n), np.arange(1, n)] = c / vals[2 : 2 * (n - 1) + 2 : 2]
    res[0, 0] = 1
    return res


def leg2chebt_mat(n):
    """
    Conversion matrix from Legendre coefficients to Chebyshev of the first
    kind

    Parameters
    ----------
    n : int
        The Dimension of the conversion matrix

    Returns
    -------
    res : array_like
        A nxn conversion matrix.

    """
    vals = np.zeros(2 * n)
    vals[0] = np.sqrt(np.pi)
    vals[1] = 2 / vals[0]
    for i in range(2, 2 * n - 1, 2):
        vals[i] = vals[i - 2] * (1 - 1 / i)
        vals[i + 1] = vals[i - 1] * (1 - 1 / (i + 1))

    res = np.zeros((n, n))
    for i in range(n):
        res[i, i:n:2] = (
            2 / np.pi * vals[0 : n - i : 2] * vals[2 * i : n + i : 2]
        )

    res[0, :] = 0.5 * res[0, :]
    return res


def chebt2leg(coef):
    """
    Convert a vector of ChebyshevT coefficients to a vector of LegendreP coe-
    fficients. If coef is a bydimensional array applies the conversion to each row.

    Parameters
    ----------
    X : array_like
        One or two dimensional array of numbers.


    Returns
    -------
    array_like
        The coefficients in LegendreP basis.

    """

    if isinstance(coef, Number):
        return coef
    if not isinstance(coef, np.ndarray):
        try:
            coef = np.array(coef)
            if not np.issubdtype(coef.dtype, np.number):
                raise ValueError(
                    "When an array_like it must contains only numbers"
                )
        except:
            raise TypeError("coef,must be a number or array_like or a Number")

    n = coef.shape[-1]
    # Since the coefficients are in the rows
    res = coef @ chebt2leg_mat(n).T
    res[np.abs(res) < np.spacing(1)] = 0
    return res


def leg2chebt(coef):
    """
    Convert a vector of LegendreP coefficients to a vector of ChebyshevT coe-
    fficients. If coef is a bydimensional array applies the conversion to each row.

    Parameters
    ----------
    X : array_like
        One or two dimensional array of numbers.


    Returns
    -------
    array_like
        The coefficients in ChebyshevT basis.

    """

    if isinstance(coef, Number):
        return coef
    if not isinstance(coef, np.ndarray):
        try:
            coef = np.array(coef)
            if not np.issubdtype(coef.dtype, np.number):
                raise ValueError(
                    "When an array_like it must contains only numbers"
                )
        except:
            raise TypeError("coef,must be a number or array_like or a Number")

    n = coef.shape[-1]
    # Since the coefficients are in the rows
    res = coef @ leg2chebt_mat(n).T
    return res


def chebt2ultra(c):
    """
    Convert ChebyshevT coefficients to GegenbauerC with parameter alpha =3/2
    coefficients

    Parameters
    ----------
    coef : array_like
        A matrix or an array representing the coefficients.
        When a matrix aplies the conversion to each row.

    Raises
    ------
    ValueError
        DESCRIPTION.
    TypeError
        DESCRIPTION.

    Returns
    -------
    array_like
        A vector or a matrix in accordance with the input

    """
    if isinstance(c, Number):
        return c
    if not isinstance(c, np.ndarray):
        try:
            c = np.array(c)
            if not np.issubdtype(c.dtype, np.number):
                raise ValueError(
                    "When an array_like it must contains only numbers"
                )
        except:
            raise TypeError("coef,must be a number or array_like or a Number")

    n = c.shape[-1]
    return chebt2leg(c) @ leg2ultra_mat(n).T


def chebt2ultra2(c):
    """
    Convert ChebyshevT coefficients to GegenbauerC with parameter alpha =3/2
    coefficients

    Parameters
    ----------
    coef : array_like
        A matrix or an array representing the coefficients.
        When a matrix aplies the conversion to each row.

    Raises
    ------
    ValueError
        DESCRIPTION.
    TypeError
        DESCRIPTION.

    Returns
    -------
    array_like
        A vector or a matrix in accordance with the input

    """
    if isinstance(c, Number):
        return c
    if not isinstance(c, np.ndarray):
        try:
            c = np.array(c)
            if not np.issubdtype(c.dtype, np.number):
                raise ValueError(
                    "When an array_like it must contains only numbers"
                )
        except:
            raise TypeError("coef,must be a number or array_like or a Number")

    n = c.shape[-1]
    if n == 1:
        return c
    l = chebt2leg(c)
    lam = 1 / 2
    dg = lam / (lam + np.arange(2, n))
    v = np.concatenate(([1, lam / (lam + 1)], dg))

    return l * v + np.concatenate(
        [-dg * l[..., 2:], np.zeros_like(c[..., :2])], axis=-1
    )


def leg2ultra_mat(n):
    lam = 1 / 2
    dg = lam / (lam + np.arange(2, n))
    v = np.concatenate([[1, lam / (lam + 1)], dg])
    w = np.concatenate(([0, 0], -dg))
    return spdiags(np.array([v, w]), [0, 2], n, n)


def ultra1mx2chebt(coef):
    """
    Convert a vector of (1-x**2)^(3/2) coefficients to Chebyshev of the first
    kind. If X is a bydimensional array applies the conversion to each column

    Parameters
    ----------
    X : array_like
        One or two dimensional array of numbers.


    Returns
    -------
    array_like
        The first-kind Chebyshev coefficients.

    """

    if not isinstance(coef, np.ndarray):
        coef = np.array(coef)

    if not np.issubdtype(coef.dtype, np.number):
        raise TypeError(
            "X must be a one or two dimensional  array_like object"
            " of numbers"
        )
    if coef.ndim > 2:
        raise ValueError(
            "X must be a one or two dimensional  array_like object"
            " of numbers"
        )

    # First convert the matrix of (1-x^2)C^(3/2)(x) coefficients to
    # Legendre coefficients and, second convert the Legendre coefficients
    # in to Chebyshev coefficients of the first kind
    return leg2chebt(coef @ ultra1mx2leg_mat(coef.shape[-1]).T)


def ultra1mx2leg_mat(n):
    """
    Conversion matrix from legendre coefficients to C**(3/2).

    Parameters
    ----------
    n : int
        The dimension of the matrix

    Returns
    -------
    m : array_like
        The conversion matrix

    """
    d = np.ones(n)
    S = spdiags(
        np.arange(1, n + 1) * np.arange(2, n + 2) / 2 / np.arange(3 / 2, n + 1),
        0,
        n,
        n,
    )

    return spdiags(np.array([d, -d]), [0, -2], n, n) @ S


def qzsplit(A, C):
    """
    A faster qz factorisation for problems that decouple.

    This is equivalent to standard qz, except we take account of symmetry to
    reduce the computational requirements of the QZ factorisation.
    """

    if issparse(A):
        A = A.A
    if issparse(C):
        C = C.A

    A1 = A[::2, ::2]
    C1 = C[::2, ::2]

    P1, S1, Q1, Z1 = qz(A1, C1)

    A2 = A[1::2, 1::2]
    C2 = C[1::2, 1::2]
    P2, S2, Q2, Z2 = qz(A2, C2)

    # Initialize all the variables.
    hf1 = len(P1)
    n = 2 * hf1 - 1
    P = np.zeros((n, n))
    S = np.zeros((n, n))
    Q = np.zeros((n, n))
    Z = np.zeros((n, n))

    # Push the subproblem back together.

    P[:hf1, :hf1] = P1
    P[hf1:, hf1:] = P2

    S[:hf1, :hf1] = S1
    S[hf1:, hf1:] = S2

    Q[:hf1, ::2] = Q1
    Q[hf1:, 1::2] = Q2

    Z[::2, :hf1] = Z1
    Z[1::2, hf1:] = Z2

    return P, S, Q, Z


def ultraS_convertmat(n, k1, k2):
    """
    Conversion matrix used in the ultraspherical spectral method.
    computes the N-by-N matrix realization of the
    conversion operator between two bases of ultrapherical Polynomials.  The
    matrix S maps N coefficients in a C^{(K1)} basis to N coefficients in a
    C^{(K2 + 1)} basis, where, C^{(K)} denotes ultraspherical Polynomial basis
    with parameter K.  If K2 < K1, S is the N-by-N identity matrix.
    This function is meant for internal use only and does not validate its
    inputs.



    Parameters
    ----------
    n : int
        The dimension of the converion matrix
    k1 :  int
        The parameter in the the first Gegenbauer basis C^{(k1)}

    alpha2 : int
        The parameter in the the first Gegenbauer basis C^{(k2)}

    Returns
    -------
    array:like
        The converision matrix used in the ultraspherical basis.

    Notes
    -----
    Based on chebfun, Copyright 2017 by The University of Oxford and
    The Chebfun Developers

    """
    S = speye(n)

    for s in np.arange(k1, k2 + 1):
        S = ultraS_spconvert(n, s) @ S
    return S


def ultraS_spconvert(n, lam):
    """
    SPCONVERT   Compute sparse representation for conversion operators.
    CONVERMAT(N, LAM) returns the truncation of the operator that transforms
    C^{lam} (Ultraspherical Polynomials) to C^{lam+1}.  The truncation gives
    back a matrix of size n x n.
    Relation is: C_n^(lam) = (lam/(n+lam))(C_n^(lam+1) - C_{n-2}^(lam+1))



    Parameters
    ----------
    n : integer
        The dimension of the matrix
    lam : numeric
        The parameter of Gegenbauer basis in C^{(lam)}



    Returns
    -------
    array_like
        The matrix representing the conversion operator.

    Notes
    -----
    Based on chebfun, Copyright 2017 by The University of Oxford and
    The Chebfun Developers

    """
    if lam == 0:
        return spdiags(
            np.array([[1, 0.5] + [0.5] * (n - 2), [0, 0] + [-0.5] * (n - 2)]),
            [0, 2],
            n,
            n,
        )
    else:
        dg = lam / (lam + np.arange(2, n))
        return spdiags(
            np.array(
                [
                    np.concatenate(([1, lam / (lam + 1)], dg)),
                    np.concatenate(([0, 0], -dg)),
                ]
            ),
            [0, 2],
            n,
            n,
        )


def ultraS_difmat(n, m=1):
    """
    DIFFMAT   Differentiation matrices for ultraspherical spectral method.
    returns the differentiation matrix that takes N Chebyshev
    coefficients and returns N C^{(M)} coefficients that represent the derivative
    of the Chebyshev series. Here, C^{(K)} is the ultraspherical Polynomial basis
    with parameter K.
    Parameters
    ----------
    n : int
        The dimension of the matrix
    m : number, optional
        The parameter of the ultraspherical basis. The default is 1.

    Returns
    -------
    array:likw
        the differentiation matrix

    Notes
    -----
    Based on chebfun, Copyright 2017 by The University of Oxford and
    The Chebfun Developers


    """

    # Create the differentiation matrix
    if m > 0:
        D = spdiags(np.arange(n), 1, n, n)
        for i in range(1, m):
            D = spdiags([2 * i] * (n), 1, n, n) @ D
        return D
    else:
        return speye(n)


def ultraS_multmat(n, p, lam):
    if isinstance(p, tau.Polynomial):
        a = p.coeff.copy()
    elif isinstance(p, np.ndarray):
        a = p.copy()

    elif isinstance(p, Iterable):
        if all([isinstance(el, Number) for el in p]):
            a = np.array(p)
        else:
            raise TypeError(
                "p must be a Polynomial or a vector of "
                "Coefficients representing the Polynomial"
            )
    elif isinstance(p, Number):
        a = np.array(p)
    else:
        raise TypeError(
            "p must be a Polynomial or a vector of "
            "Coefficients representing the Polynomial"
        )

    # Multplying by a scalar is easy
    if a.size == 1:
        return a * speye(n)

    # Prolong or truncate the coefficients
    if a.size < n:
        a = np.concatenate((a, [0] * (n - a.size)))  # Prolong
    else:
        a = a[:n]  # Truncate

    # Multiplication in ChebyshevT Coefficients
    if lam == 0:
        a = a / 2
        M = csr_matrix(toeplitz(np.r_[2 * a[0], a[1:]], np.r_[2 * a[0], a[1:]]))
        H = csr_matrix(hankel(a[1:]))

        M[1 : a.size, : a.size - 1] += H
        return M

    # Multiplication in ChebyshevU coefficients
    elif lam == 1:
        M = (
            csr_matrix(toeplitz(np.r_[2 * a[0], a[1:]], np.r_[2 * a[0], a[1:]]))
            / 2
        )
        M[: a.size - 2, : a.size - 2] -= csr_matrix(hankel(a[2:] / 2))
        return M
    else:
        # Want the C^{lam}C^{lam} Cheb Multiplication matrix.

        # Convert ChebT of a to ChebC^{lam}
        a = ultraS_convertmat(n, 0, lam - 1) @ a
        m = 2 * n
        M0 = speye(m)
        d1 = (
            np.r_[1, 2 * lam : 2 * lam + m - 1]
            / np.r_[1, 2 * np.r_[lam + 1 : lam + m]]
        )
        d2 = np.r_[1 : m + 1] / (2 * np.r_[lam : lam + m])
        B = np.r_["0,2", d2, [0] * m, d1]
        Mx = spdiags(B, [-1, 0, 1], m, m)

        M1 = 2 * lam * Mx

        # Construct the multiplication operator by a three-term recurrence.
        M = a[0] * M0
        M += a[1] * M1
        for i in range(a.size - 2):
            M2 = (
                2 * (i + 1 + lam) / (i + 2) * Mx @ M1
                - (i + 2 * lam) / (i + 2) * M0
            )
            M += a[i + 2] * M2
            M0 = M1
            M1 = M2
            if (np.abs(a[i + 3 :]) < np.spacing(1)).all():
                break
        return M.tocsr()[:n, :n]


def antidiag_ind_flatened(m, n):
    A = np.arange(m * n).reshape(m, -1)
    aux = []
    for col in range(n):
        startcol = col
        startrow = 0

        while startcol >= 0 and startrow < m:
            aux.append(A[startrow][startcol])

            startcol -= 1
            startrow += 1

    # For each row start column is N-1
    for row in range(1, m):
        startrow = row
        startcol = n - 1

        while startrow < m and startcol >= 0:
            aux.append(A[startrow][startcol])

            startcol -= 1
            startrow += 1

    return aux


def ultra2ultra(coef, alpha_in, alpha_out):
    r"""
    This method Convert coefficients in :math:`C^{(\alpha_{in})}` into coeffic
    ients in :math:`C^{(\alpha_{in})}`. This method is based on chebfun method
    of the same name.

    Parameters
    ----------
    coef : array_like
        The coefficients with parameter `alpha_in`.
    alpha_in : scalar
        The parameter of the input coefficients.
    alpha_out : scalar
        The parameter of the output coefficients

    Returns
    -------
    array_like
        The coefficients in C^{(\alpha_{out})}

    References
    ----------
    [1] A. Townsend, M. Webb, and S. Olver, "Fast polynomial transforms
    based on Toeplitz and Hankel matrices", submitted, 2016.
    [2] F.W.J. Olver et al., editors. NIST Handbook of Mathematical Functions.
    Cambridge University Press, New York, NY, 2010.


    """

    n = coef.shape[-1] - 1
    coef = coef / scl(alpha_in, n)

    coef = jac2jac(
        coef, alpha_in - 0.5, alpha_in - 0.5, alpha_out - 0.5, alpha_out - 0.5
    )

    return coef * scl(alpha_out, n)


def scl(alpha, n):
    r"""
    Scale Jacobi polynomials to ultraspherical polynomials.


    Parameters
    ----------
    alpha : scalar
        The Paremeter of the ultraspherical polynomial.
    n : integer
        The degree of polynomial.

    Returns
    -------
    array_like
        The scales for the coefficients

    Notes
    -----
    This conversion is based on the Table. 18.3.1. [1, P.439]

    References
    ----------
    [1] F.W.J. Olver et al., editors. NIST Handbook of Mathematical Functions.
    Cambridge University Press, New York, NY, 2010.
    """
    if alpha == 0:
        nn = np.arange(n)
        return np.r_[1, np.cumprod((nn + 0.5) / (nn + 1))]
    else:
        nn = np.arange(n + 1)
        return (gamma(2 * alpha) / gamma(alpha + 0.5)) * np.exp(
            loggamma(alpha + 0.5 + nn) - loggamma(2 * alpha + nn)
        )


def jac2jac(coef, alpha, beta, gam, delta):
    coef, a, b = jacobiIntegerConversion(coef, alpha, beta, gam, delta)

    if abs(a - gam) > 1e-15:
        coef = jacobiFractionalConversion(coef, a, b, gam)
    if abs(b - delta) > 1e-15:
        # Use reflection formula for jacobi Polynomials
        coef.reshape(-1)[1::2] = -coef.reshape(-1)[1::2]
        coef = jacobiFractionalConversion(coef, b, gam, delta)
        coef.reshape(-1)[1::2] = -coef.reshape(-1)[1::2]

    return coef


def UpJacobiConversion(v, a, b):
    n = v.shape[-1]
    d1 = np.r_[
        1,
        (a + b + 2) / (a + b + 3),
        np.r_[a + b + 3 : a + b + n + 1] / np.r_[a + b + 5 : a + b + 2 * n : 2],
    ]
    d2 = np.r_[a + 1 : a + n] / np.r_[a + b + 3 : a + b + 2 * n : 2]

    c = 0 if v.ndim == 1 else np.zeros((v.shape[0], 1))
    return v * d1 + np.r_["-1", d2 * v[..., 1:], c]


def DownJacobiConversion(v, a, b):
    n = v.shape[-1]
    topRow = np.r_[
        1,
        (a + 1) / (a + b + 2),
        (a + 1)
        / (a + b + 2)
        * np.cumprod(np.r_[a + 2 : a + n] / np.r_[a + b + 3 : a + b + n + 1]),
    ]
    topRow *= (-1) ** np.r_[0:n]
    tmp = v * topRow
    vecsum = np.flip(np.cumsum(np.flip(tmp, axis=-1), axis=-1), axis=-1)

    ratios = (
        np.r_[a + b + 5 : a + b + 2 * n : 2] / np.r_[a + b + 3 : a + b + n + 1]
    ) * (1 / topRow[2:])
    ratios = np.r_[1, -(a + b + 3) / (a + 1), ratios]
    return ratios * vecsum


def RightJacobiConversion(v, a, b):
    v = v.copy()
    v.reshape(-1)[1::2] = -v.reshape(-1)[1::2]
    v = UpJacobiConversion(v, b, a)
    v.reshape(-1)[1::2] = -v.reshape(-1)[1::2]
    return v


def LeftJacobiConversion(v, a, b):
    v = v.copy()
    v.reshape(-1)[1::2] = -v.reshape(-1)[1::2]
    v = DownJacobiConversion(v, b, a)
    v.reshape(-1)[1::2] = -v.reshape(-1)[1::2]
    return v


def jacobiIntegerConversion(coef, alpha, beta, gam, delta):
    while alpha <= gam - 1:
        coef = RightJacobiConversion(coef, alpha, beta)
        alpha += 1

    while alpha >= gam + 1:
        coef = LeftJacobiConversion(coef, alpha - 1, beta)
        alpha -= 1

    while beta <= delta - 1:
        coef = UpJacobiConversion(coef, alpha, beta)
        beta += 1

    while beta >= delta + 1:
        coef = DownJacobiConversion(coef, alpha, beta - 1)
        beta -= 1

    return coef, alpha, beta


def jacobiFractionalConversion(v, alpha, beta, gam):
    lam1 = lambda z: np.exp(
        loggamma(z + alpha + beta + 1) - loggamma(z + gam + beta + 2)
    )
    lam2 = lambda z: np.exp(loggamma(z + alpha - gam) - loggamma(z + 1))
    lam3 = lambda z: np.exp(
        loggamma(z + beta + gam + 1) - loggamma(z + beta + 1)
    )
    lam4 = lambda z: np.exp(
        loggamma(z + beta + 1) - loggamma(z + alpha + beta + 1)
    )
    vect = False
    if v.ndim == 1:
        v = v[np.newaxis]
        vect = True

    m, n = v.shape

    if n == 1:
        return v

    d1 = (2 * np.r_[:n] + gam + beta + 1) * lam3(np.r_[1, 1:n])
    d1[0] = 1

    d2 = 1 / gamma(alpha - gam) * lam4(np.r_[1, 1:n])
    d2[0] = 0
    # Symbol of the Hhankel part
    vals = lam1(np.r_[1, 1 : 2 * n])

    vals[0] = 0

    d = vals[::2]
    pivotValues = []
    c = np.zeros((0, n))
    tol = 1e-14 * np.log(n)
    idx = d.argmax()
    mx = d[idx]

    while mx > tol:
        newRow = vals[idx : idx + n]

        if c.size > 0:
            newRow = newRow - (c[:, idx] * pivotValues) @ c

        pivotValues += [1 / mx]
        c = np.r_["0,2", c, newRow]
        d = d - newRow**2 / mx
        idx = d.argmax()
        mx = d[idx]

    c = (c.T * np.sqrt(pivotValues)).T

    T_row = lam2(np.r_[1, 1:n])
    T_row[0] = gamma(alpha - gam + 1) / (alpha - gam)
    z = np.r_[T_row[0], [0] * (n - 1)]
    a = fft(np.r_[z, T_row[:0:-1]])

    coef = v * d2

    for i in range(m):
        tmp1 = c * coef[i]

        f1 = fft(tmp1, 2 * n - 1)

        tmp2 = f1 * a
        b = ifft(tmp2).real

        coef[i] = d1 * np.sum(c * b[..., :n], axis=0)
    matrow1 = gamma(gam + beta + 2) / gamma(beta + 1) * d2 * T_row * vals[:n]
    coef[..., 0] = v @ matrow1 + v[..., 0]

    if vect:
        return coef.reshape(-1)

    return coef


def jac2chebt(cj, alpha, beta):
    r"""
    Convert coefficients in Jacobi basis with parameters ``alpha`` and ``beta``
    to coefficients in ChebyshevT basis. This method is based in the matlab
    system chebfun of the same name.

    Parameters
    ----------
    cj : array_like
        The coefficients in Jacobi basis.
    alpha : scalar
        The parameter :math:`\alpha` of the Jacobi basis.
    beta : scalar
        The parameter :math:`\beta` of the Jacobi basis.

    Returns
    -------
    array_like
        The coefficients in ChebyshevT basis

    """
    # The case when Jacobi basis match Legendre basis
    if alpha == beta == 0:
        return leg2chebt(cj)

    n = cj.shape[-1]

    # when n is small
    if n < 512:
        return jac2chebt_rec(cj, alpha, beta)

    # Call jac2jac  and then convert jacobi(-.5,-.5) to ChebyshevT
    else:
        # COnvert P_n^(alpha,beta) to P_n^(-.5.-.5).
        cj = jac2jac(cj, alpha, beta, -0.5, -0.5)
        # Convert  P_n^(-.5.-.5) to T_n.
        scl = np.r_[
            1, np.cumprod(np.arange(0.5, 0.5 + n - 1) / np.arange(1, n))
        ]
        return scl * cj


def jac2chebt_rec(cj, a, b):
    r"""
    This function convert Jacobi coefficients with parameters ``a`` and ``b``
    into ChebyshevT Coefficients. here we use the 3 therms recurrence relation
    ro compute the coefficients in ChebyshevT basis.This method is a tranlation
    to python of the method ``jac2cheb_direct`` of matlab system chebfun.

    Parameters
    ----------
    cj : array_like
        The coefficients of the polynomial in Jacobi basis.
    a : scalar
        The parameter :math:`\alpha` of the Jacobi basis.
    b : scalar
        The parameter :math:`\beta` of the Jacobi basis.

    Returns
    -------
    array_like
        The coefficients in ChebyshevT basis

    """
    n = cj.shape[-1]

    # The trivial case
    if n < 2:
        return

    bs = tau.basis()
    x, *_ = bs.nodes(n)  # Nodes of the ChebyshevT

    # Make the Jacobi-Chebyshev Vandermonde matrix
    apb, aa, bb = a + b, a * a, b * b
    P = np.zeros((n, n))
    P[:, 0] = 1
    P[:, 1] = 0.5 * (2 * (a + 1) + (apb + 2) * (x - 1))

    for i in range(2, n):
        i2 = 2 * i
        i2apb = i2 + apb
        q1 = i2 * (i + apb) * (i2apb - 2)
        q2 = (i2apb - 1) * (aa - bb)
        q3 = (i2apb - 2) * (i2apb - 1) * i2apb
        q4 = 2 * (i + a - 1) * (i + b - 1) * i2apb
        P[:, i] = ((q2 + q3 * x) * P[:, i - 1] - q4 * P[:, i - 2]) / q1

    vc = cj @ P.T  # Values on the Chebyshev Grid
    return bs.vals1tocoeffs(vc)


def chebt2chebu(coef):
    r"""
    This method convert coefficients of Chebyshev polynomials of the
    first kind to Chebyshev polynomials of the second kind.

    Parameters
    ----------
    coef : array_like
        The coefficients to convert.

    Returns
    -------
    array_like
        The converted coefficients

    Notes
    -----
    This method is based in the relaction  18.9.9 found in [1, P.446]
    :math:`T_n(x)=1/2(U_n(x)+U_{n-1}(x))`. Where :math:`T_n(x)` are the
    Chebyshev polynomials of the first kind and :math:`U_n(x)` the Cheby
    shev polynomials of the second kind.


    References
    ----------
    [1] F.W.J. Olver et al., editors. NIST Handbook of Mathematical Functions.
    Cambridge University Press, New York, NY, 2010.
    """
    sh = list(coef.shape)
    sh[-1] += 2
    aux = np.zeros(sh)
    aux[..., :-2] = coef
    aux[..., 0] = 2 * coef[..., 0]
    aux = 0.5 * (aux[..., :-2] - aux[..., 2:])
    return aux


def chebt2chebv(coef):
    r"""
    This method convert coefficients of Chebyshev polynomials of the
    first kind to Chebyshev polynomials of the fourth kind.

    Parameters
    ----------
    coef : array_like
        The coefficients to convert.

    Returns
    -------
    array_like
        The converted coefficients

    Notes
    -----
    This method is based in the relaction  18.9.11 found in [1, P.446]
    :math:`T_n(x)=1/2(V_n(x)+V_{n-1}(x))`. Where :math:`T_n(x)` are the Cheby
    shev polynomials of the first kind and :math:`W_n(x)` the Chebyshev polyno
    mials of the fourth kind.


    References
    ----------
    [1] F.W.J. Olver et al., editors. NIST Handbook of Mathematical Functions.
    Cambridge University Press, New York, NY, 2010.

    """

    sh = list(coef.shape)
    sh[-1] += 1
    aux = np.zeros(sh)
    aux[..., :-1] = coef
    aux[..., 0] = 2 * coef[..., 0]
    aux = 0.5 * (aux[..., :-1] + aux[..., 1:])

    return aux


def chebt2chebw(coef):
    r"""
    This method convert coefficients of Chebyshev polynomials of the
    first kind to Chebyshev polynomials of the fourth kind.

    Parameters
    ----------
    coef : array_like
        The coefficients to convert.

    Returns
    -------
    array_like
        The converted coefficients

    Notes
    -----
    This method is based in the relaction  18.9.11 found in [1, P.446]
    :math:`T_n(x)=1/2(W_n(x)-W_{n-1}(x))`. Where :math:`T_n(x)` are the Cheby
    shev polynomials of the first kind and :math:`W_n(x)` the Chebyshev polyno
    mials of the fourth kind.


    References
    ----------
    [1] F.W.J. Olver et al., editors. NIST Handbook of Mathematical Functions.
    Cambridge University Press, New York, NY, 2010.

    """

    sh = list(coef.shape)
    sh[-1] += 1
    aux = np.zeros(sh)
    aux[..., :-1] = coef
    aux[..., 0] = 2 * coef[..., 0]
    aux = 0.5 * (aux[..., :-1] - aux[..., 1:])

    return aux


def chebt2kind(c, kind=2):
    r"""
    Convert Chebyshev of the first kind on another kind.

    Parameters
    ----------
    c : ndarray
        An array with the coefficients of a polynomial in ChebyshevT basis.
    kind : int, optional
        The kind to convert the coefficients. The default is 2.

    Raises
    ------
    ValueError
        When the kind is not 2,3 or 4.

    Returns
    -------
    res : ndarray
        The  coefficients in the ``kind`` basis.

    """
    switch = {2: chebt2chebu(c), 3: chebt2chebv(c), 4: chebt2chebw(c)}
    res = switch.get(kind, None)
    if res is None:
        raise ValueError(
            f"'kind' must be an integer 1,2 or 3, was given {kind} "
        )

    return res


def chebw2chebu(coef):
    r"""
    This method convert coefficients of Chebyshev polynomials of the
    fourth kind to Chebyshev polynomials of the sekond  kind.

    Parameters
    ----------
    coef : array_like
        The coefficients to convert.

    Returns
    -------
    array_like
        The converted coefficients

    Notes
    -----
    This method is based in the relation :math:`W_n(x)=U_n(x)+U_{n-1}(x)`. Whe
    re :math:`W_n(x)` are the Chebyshev polynomials of the fourth kind and
    :math:`U_n(x)` the Chebyshev polynomials of the sekond kind.


    References
    ----------
    [1] Kamal Aghigh, et al. A survey on third and fourth kind of Chebyshev
    polynomials and their applications. Journal of Applied Mathematics and
    Computation, 2008.

    """

    sh = list(coef.shape)
    sh[-1] += 1
    aux = np.zeros(sh)
    aux[..., :-1] = coef

    return aux[..., :-1] + aux[..., 1:]


def chebv2chebu(coef):
    r"""
    This method convert coefficients of Chebyshev polynomials of the
    third kind to Chebyshev polynomials of the sekond  kind.

    Parameters
    ----------
    coef : array_like
        The coefficients to convert.

    Returns
    -------
    array_like
        The converted coefficients

    Notes
    -----
    This method is based in the relation :math:`V_n(x)=U_n(x)-U_{n-1}(x)`. Whe
    re :math:`W_n(x)` are the Chebyshev polynomials of the fourth kind and
    :math:`U_n(x)` the Chebyshev polynomials of the sekond kind.


    References
    ----------
    [1] Kamal Aghigh, et al. A survey on third and fourth kind of Chebyshev
    polynomials and their applications. Journal of Applied Mathematics and
    Computation, 2008.

    """

    sh = list(coef.shape)
    sh[-1] += 1
    aux = np.zeros(sh)
    aux[..., :-1] = coef

    return aux[..., :-1] - aux[..., 1:]


def fastHankelMul(a, b, x=None):
    if x is None:
        x = b.copy()
        n = len(x)
        b = np.r_[a[-1], np.zeros(n - 1)]

    n = len(x)
    return ifft(fft(np.r_[b, 0, a[:-1]]) * fft(np.r_[x[::-1], np.zeros(n)]))[
        :n
    ].real


def fastToeplitzMul(a, b, x=None):
    a = a.copy()
    a[0] *= 2
    if x is None:
        x = b
        b = a

    n = len(a)
    return ifft(fft(np.r_[a, 0, np.flip(b[1:])]) * fft(np.r_[x, np.zeros(n)]))[
        :n
    ].real


def computeWeights(e, n):
    c1 = e + 1
    c2 = 0.5
    c3 = e + c2
    c4 = c1 + c2
    c0 = (2**c3) * beta(c1, c2)

    w = np.zeros(n)

    w[0] = 1

    if n > 1:
        w[1] = c3 / c4

        for i in range(2, n):
            w[i] = (2 * c3 * w[i - 1] + (i - 1 - c4) * w[i - 2]) / (c3 + i)

    return c0 * w


def fredholm1(funs, method="tsve", delta=1e-2, eta=1):
    r"""
    Compute the solution of Fredholm integral  equation of the first kind.



    Parameters
    ----------
    funs : Iterable
        The three functions. The first is a Polynomial2 representing k(s,t),the
        second is a Polynomial representing x(t) and the third is a Polynomial
        representing g(s)
    method : TYPE, optional
        The method to solve the equation. Can be either 'tsve' or 'tr' The default is 'tsve'.
    delta : TYPE, optional
        DESCRIPTION. The default is 1e-2.

    Raises
    ------
    TypeError
        DESCRIPTION.

    Returns
    -------
    None.

    Notes
    -----
    This compute the Fredholm integral equation of the first kind
    .. math::
        \int_{\Omega_1}k(s,t)x(t)dt=g(s), \quad s \in \Omega_2

    with a square integrable kernel k. The :math:`\Omega_i, i=1,2` are subsets
    of :math:`\mathbb{R}`.
    This equation is solved using truncated singular value expansion method
    (tsve) or  Tikhonov regularization method.
    """
    if not (isinstance(funs, Iterable) and len(funs) == 3):
        raise TypeError("funs must be an Iterable with ")
    ke, f, g = funs
    opts = g.options

    # adding continuous noise to g
    noise = tau.Polynomial.randnPol(0.01, options=opts)  # generate noise
    ng = g.norm()  # compute norm of rhs

    noise = delta * noise * ng / noise.norm()  # adjust norm of the noise
    g_delta = g + noise  # add noise to rhs
    nnoise = noise.norm()  # compute norm of noise

    psi, ks, phi = ke.svd()
    rk = len(ks)  # maximal rank of the separable approximation
    if method == "tsve":
        beta = np.zeros([rk, 1])
        for i in range(rk):
            beta[i] = (phi[i] * g_delta).definite_integral() / ks[i]
            xk = psi[0 : i + 1] * beta[0 : i + 1]

            if ((ke.T * xk).sum(axis=1) - g_delta).norm() < eta * nnoise:
                break
        relative_error_TSVE = (xk - f).norm() / f.norm()
        return xk, relative_error_TSVE.item()

    elif method == "tr":

        def errlambda1(lam, sigma, gnoise, psi, ke, rk, eta, e, dd):
            beta = dd * sigma / (sigma**2 + lam**2)
            x = psi * beta.reshape(-1, 1)
            return abs(
                (((ke.T * x).sum(1) - gnoise).norm()) ** 2 - eta**2 * e**2
            )

        dd = (phi * g_delta).sum()

        # Solving minimization problem for lambda
        eta = 1
        lam = fminbound(
            lambda x: errlambda1(x, ks, g_delta, psi, ke, rk, eta, nnoise, dd),
            0,
            2,
            xtol=1e-12,
        )
        beta2 = ks / (ks**2 + lam**2)
        # xtol = 1e-5
        beta = (dd * beta2).reshape(-1, 1)

        # tikonov relative error
        xlam = psi * beta
        rel_error_tikhonov = (xlam - f).norm() / f.norm()
        return xlam, rel_error_tikhonov.item()
    else:
        raise ValueError("Possible methods are 'tsve' and 'tr")
