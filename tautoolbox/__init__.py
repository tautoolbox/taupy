# Copyright (C) 2021, University of Porto and Tau Toolbox developers.
#
# This file is part of Tautoolbox package.
#
# Tautoolbox is free software: you can redistribute it and/or modify it
# under the terms of version 3 of the GNU Lesser General Public License as
# published by the Free Software Foundation.
#
# Tau Toolbox is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser
# General Public License for more details.

"""
tautoolbox: Tau Toolbox for Python
=============================

Provides
  1. Solvers for integro differential problems.
  2. Associated classes to make easy to operate in the Tau Lanczos method.

Documentation
-------------

Each function and class is documented in place. That documentation is part of
the References Guide.

There are examples available that show how these functions can be used together.

Available subpackages
---------------------

There are several packages available but they can all be imported from the
tautoolbox namespace.

The recommended way to use this package is to use ``import tautoolbox as tau``

Utilities
---------
version
    tautoolbox version
"""

# In order to simplify the usage we import the different subpackages in to
# this namespace

from .basis import basis
from .polynomial import Polynomial
from .polynomial import Polynomial as polynomial
from .settings import settings
from . import utils
from . import orthogonal
from .matrix import matrix
from .equation import equation
from .operator import operator
from .condition import condition
from .problem import problem
from . import ode
from .solve import solve
from .polynomial2 import Polynomial2

from .auxiliary import *
from .operator2 import Operator2
from .problem2 import Problem2
from .pde import taupdesolve
from .polynomial3 import Polynomial3

version = (1, 0, 0, "development")
