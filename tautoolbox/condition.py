# Copyright (C) 2021, University of Porto and Tau Toolbox developers.
#
# This file is part of Tautoolbox package.
#
# Tautoolbox is free software: you can redistribute it and/or modify it
# under the terms of version 3 of the GNU Lesser General Public License as
# published by the Free Software Foundation.
#
# Tau Toolbox is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser
# General Public License for more details.

import tautoolbox as tau
from copy import deepcopy
import numpy as np
import re
import numbers


class condition:
    def __init__(self, cond=None, options=None):
        if options is None:
            self.options = tau.settings()
        else:
            self.options = deepcopy(options)

        if cond is None:
            self.coeff = np.array([1])
            self.order = np.array([0])
            self.point = np.array([])
            self.value = 0
            self.xmin = np.nan
            self.xmax = np.nan
            self.nvars = self.options.nequations
            if self.nvars == 1:
                self.var = [1]
            else:
                self.var = []
            self.varname = "y"
            return

        if isinstance(cond, tau.condition):
            self.__dict__ = cond.__dict__

        elif isinstance(cond, str):
            self.__dict__ = tau.condition.fromstr(cond, self.options).__dict__

        elif callable(cond):
            self.__dict__ = cond(tau.condition(options=self.options)).__dict__
        else:
            raise TypeError("Tautoolbox: unknown argument")

    def __call__(self, index):
        obj = deepcopy(self)
        if all([len(obj.var), len(obj.point)]):
            raise Exception("Taupy: forbiden operation")
        if len(obj.var):
            obj.point = np.r_[obj.point, index]
            obj.xmin = obj.xmax = index
        else:
            obj.var = obj.nvars = index

        return obj

    def diff(self, order=None, point=None):
        obj = deepcopy(self)
        if order is None:
            order = 1
        obj.order += order
        if point is not None:
            obj.point = np.array([point])
        return obj

    def __add__(self, rhs):
        obj = deepcopy(self)
        if isinstance(rhs, tau.condition):
            obj.var = np.r_[obj.var, rhs.var]
            obj.coeff = np.r_[obj.coeff, rhs.coeff]
            obj.order = np.r_[obj.order, rhs.order]
            obj.point = np.r_[obj.point, rhs.point]
            obj.value += rhs.value
            obj.xmax = max(obj.xmax, rhs.xmax)
            obj.xmin = max(obj.xmin, rhs.xmin)
            obj.nvars = max(obj.var)
        elif isinstance(rhs, numbers.Number):
            obj.value -= rhs
        else:
            raise ArithmeticError(
                "Taupy: You cannot add a tau.condiction"
                f" with a {type(rhs)} object"
            )
        return obj

    def __radd__(self, lhs):
        return self + lhs

    def __pos__(self):
        return deepcopy(self)

    def __neg__(self):
        obj = deepcopy(self)
        obj.coeff = -obj.coeff
        obj.value = -obj.value
        return obj

    def __sub__(self, rhs):
        return self + (-rhs)

    def __rsub__(self, lhs):
        return -self + lhs

    def __mul__(self, rhs):
        if not isinstance(rhs, numbers.Number):
            raise Exception("nilon")
        obj = deepcopy(self)

        obj.coeff *= rhs
        obj.value *= rhs
        return obj

    def __rmul__(self, lhs):
        return self * lhs

    def __truediv__(self, rhs):
        if not isinstance(rhs, numbers.Number):
            raise Exception("nilon")
        obj = deepcopy(self)

        obj.coeff = obj.coeff / rhs
        obj.value = obj.value / rhs
        return obj

    def isGIVP(self, domain):
        return False if self.xmax > domain[1] else True

    @staticmethod
    def fromstr(condition, options):
        condition = condition.replace(" ", "")
        condition = condition.replace("pi", "np.pi")
        inde = [m.start() for m in re.finditer("=", condition)]
        if len(inde) > 1:
            raise SyntaxError(
                "Tautoolbox: more than an equal sign in condition"
            )
        if len(inde) == 0:
            raise SyntaxError("Tautoolbox: no eequal sign found in condiction")

        rhs = condition[: inde[0]]
        inda = np.array([m.start() for m in re.finditer(r"\(", rhs)])
        indf = np.array([m.start() for m in re.finditer(r"\)", rhs)])
        indv = np.array([m.start() for m in re.finditer(r"y", rhs)])

        if not (len(inda) == len(indf) == len(indv)):
            raise SyntaxError(
                "Tautoolbox: badly formed condition, parenthesis "
                " and variables do not match in number"
            )
        if any(indv > inda) or any(indv > indf) or any(inda > indf):
            raise SyntaxError(
                "Tautoolbox: badly formed condition, the order of "
                "functions and parenthesis is mixed"
            )

        re.findall(r"y([0-9]*)", rhs)
        nvar = np.array(
            [1 if nv == "" else int(nv) for nv in re.findall(r"y([0-9]*)", rhs)]
        )

        orde = np.array([len(o) for o in re.findall(r"y[0-9]*('*)", rhs)])
        # arg = np.array(
        #     [eval(co) for co in re.findall(r"\(([\+\-}?[0-9]+\.?[0-9]*)\)", rhs)]
        # )
        arg = np.array([eval(co) for co in re.findall(r"\(([^()]+)\)", rhs)])

        switcher = {"+": 1, "(": 1, "-": -1, "": 1}
        coeff = re.findall(r"(^|[\+\-]?[0-9]*)y", rhs)

        coeff = np.array(
            [
                switcher[co] if co in switcher.keys() else eval(co)
                for co in coeff
            ]
        )
        obj = tau.condition(options=options)
        obj.var = nvar
        obj.order = orde
        obj.point = arg
        obj.coeff = coeff
        obj.value = eval(condition[inde[0] + 1 :])
        obj.xmin = min(obj.point)
        obj.xmax = max(obj.point)
        obj.nvars = max(obj.var)

        return obj

    def __str__(self):
        st = ""
        if not len(self.point):
            return st

        for k in range(len(self.coeff)):
            if k > 0 and self.coeff[k] > 0:
                st += " + "
            if self.coeff[k] == -1:
                st += "-" if k == 0 else " - "
            elif self.coeff[k] != 1:
                st += f"{self.coeff[k]}"

            vname = f"{self.varname}{self.var[k]}"

            if self.order[k] < 4:
                aux = "'"
                st += f"{vname}{aux* self.order[k]}"
            else:
                st += f"diff({vname}, {self.order[k]})"
            st += f"({self.point[k]})"
        return st + f" = {self.value}"

    def __repr__(self):
        return self.__str__()

    def __getitem__(self, index):
        aux = deepcopy(self)
        if aux.options.nequations == 1:
            if index == 0:
                return aux
            else:
                raise IndexError(
                    "Index out of range - the index must be"
                    f" at most {aux.options.nequations -1}"
                )

        if index < 0 and int(index) != index or index > aux.options.nequations:
            raise ValueError("Index must be a non-negative integer.")
        aux.var = [int(index)]
        return aux
