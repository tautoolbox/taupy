# Copyright (C) 2021, University of Porto and Tau Toolbox developers.
#
# This file is part of Tautoolbox package.
#
# Tautoolbox is free software: you can redistribute it and/or modify it
# under the terms of version 3 of the GNU Lesser General Public License as
# published by the Free Software Foundation.
#
# Tau Toolbox is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser
# General Public License for more details.

import tautoolbox as tau


def solve(problem, **kwargs):
    if not isinstance(problem, tau.problem):
        raise ValueError("Tautoolbox: the imput argument must be a tau.problem")

    if problem.kind in ["ide", "ode"]:
        return tau.ode.solve(problem)
    elif problem.kind == "eig":
        return tau.eig.solve(problem)
    elif problem.kind == "fred1":
        return problem.kernel.fredholm1(problem.rhs, **kwargs)
