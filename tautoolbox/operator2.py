#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jun  9 00:21:52 2022

@author: nilson
"""
import tautoolbox as tau
import numpy as np
from copy import deepcopy
from numbers import Number
from collections.abc import Iterable


class Operator2:
    func = jacobian = domain = None

    def __init__(self, func=None, **kwargs):
        # The enpty operator
        if func == None:
            return
        if isinstance(func, tau.Polynomial2):
            self.func = func
            self.jacobian = np.array([[1]], dtype="O")
            self.domain = func.domain
            self.bases = func.bases
        # In this case the constructor receive the same input a Polynomial2
        # object
        else:
            self.__dict__ = Operator2(tau.Polynomial2(func, **kwargs)).__dict__

    def cos(self):
        aux = deepcopy(self)
        aux.func = aux.func.cos()
        aux.jacobian = -aux.func.sin() * aux.jacobian
        return aux

    def diff(self, order=1):
        aux = deepcopy(self)
        aux.func = aux.func.diff(order)
        # Check if order is a number
        if isinstance(order, Number):
            order = (order, 0)

        if isinstance(order, Iterable):
            if len(order) == 2 and all(
                [
                    isinstance(o, Number) and o >= 0 and int(o) == o
                    for o in order
                ]
            ):
                x_order, y_order = order
            else:
                raise ValueError(
                    "When the order is an iterable it must have "
                    "length 2 and all entries integers"
                )
        else:
            raise TypeError(
                "Order can be only an integer or a length 2 "
                "iterable of integers"
            )
        # Update information of the derivatives
        mj, nj = aux.jacobian.shape
        njac = np.zeros((y_order + mj, x_order + nj), dtype="O")

        # Differentiate in the x direction shift the derivative information
        # to the bottom while differentiate in the y direction shift de deri-
        # vative information to the right
        njac[-mj:, -nj:] = aux.jacobian
        aux.jacobian = njac
        return aux

    def diffx(self, order):
        return self.diff((order, 0))

    def diffy(self, order):
        return self.diff((0, order))

    def gradient(self):
        return np.array([self.diff((1, 0)), self.diff((0, 1))])

    def laplacian(self):
        return self.diff((2, 0)) + self.diff((0, 2))

    def isempty(self):
        return self.func == None

    def __neg__(self):
        aux = deepcopy(self)
        aux.func = -aux.func
        aux.jacobian = -aux.jacobian
        return aux

    def __pos__(self):
        return deepcopy(self)

    def __add__(self, rhs):
        if self.isempty():
            return deepcopy(self)

        aux = deepcopy(self)
        if isinstance(rhs, Operator2):
            # The function is the sum of the of the two Polynomial2
            aux.func = aux.func + rhs.func
            # The resulting derivatives will have the dimensions of the maximum
            # of the dimensions of en each direction of the imput derivatives
            m_lhs, n_lhs = aux.jacobian.shape
            m_rhs, n_rhs = rhs.jacobian.shape
            jac_lhs = np.zeros(
                (max(m_lhs, m_rhs), max(n_lhs, n_rhs)), dtype=object
            )
            jac_rhs = deepcopy(jac_lhs)
            jac_lhs[:m_lhs, :n_lhs] = aux.jacobian
            jac_rhs[:m_rhs, :n_rhs] = rhs.jacobian
            aux.jacobian = jac_lhs + jac_rhs
            return aux
        elif isinstance(rhs, (Number, tau.Polynomial2)):
            aux.func = aux.func + rhs
            return aux
        else:
            raise TypeError(f"Cannot add {type(self)} and {type(rhs)} ")

    def __radd__(self, lhs):
        return self + lhs

    def __sub__(self, rhs):
        return self + (-rhs)

    def __rsub__(self, lhs):
        return -self + lhs

    def __mul__(self, rhs):
        if self.isempty():
            return Operator2()
        aux = deepcopy(self)
        if isinstance(rhs, Number):
            aux.func = rhs * aux.func
            aux.jacobian = aux.jacobian * rhs
            return aux
        elif isinstance(rhs, tau.Polynomial2):
            aux.func = aux.func * rhs

            for i in range(aux.jacobian.shape[0]):
                for j in range(aux.jacobian.shape[1]):
                    if aux.jacobian[i, j] != 0:
                        aux.jacobian[i, j] = aux.jacobian[i, j] * rhs

            return aux
        else:
            raise TypeError(
                "The operation * is not defined for "
                f"types {type(aux)} and {type(rhs)}"
            )

    def __rmul__(self, lhs):
        return self * lhs
