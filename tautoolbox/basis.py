# Copyright (C) 2021, University of Porto and Tau Toolbox developers.
#
# This file is part of Tautoolbox package.
#
# Tautoolbox is free software: you can redistribute it and/or modify it
# under the terms of version 3 of the GNU Lesser General Public License as
# published by the Free Software Foundation.
#
# Tau Toolbox is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser
# General Public License for more details.

import tautoolbox as tau
from . import orthogonal


def basis(options=None):
    """
    Parameters
    ----------
    options : tau.settings, optional
        All the different options used in tautoolbox. The default is None.

    Returns
    -------
    tau.orthogonal
        Orthogonal Polynomials base class.
        All the different classes returned are derived from the base
        class tau.orthogonal.

    """
    if options is None:
        options = tau.settings()
    if isinstance(options, dict):
        options = tau.settings(**options)

    if isinstance(options, str):
        switcher = {
            "supported": orthogonal.supported,
            "experimental": orthogonal.experimental,
            "all": orthogonal.supported + orthogonal.experimental,
        }
        bas = switcher.get(options, None)

        if bas is None:
            raise TypeError("tautoolbox: unknown string option")
        return sorted(bas)

    if not isinstance(options, tau.settings):
        raise TypeError(
            "tautoolbox: the optional argument must be of tau.settings type"
        )

    switcher = {
        "ChebyshevT": orthogonal.ChebyshevT,
        "ChebyshevU": orthogonal.ChebyshevU,
        "ChebyshevV": orthogonal.ChebyshevV,
        "ChebyshevW": orthogonal.ChebyshevW,
        "LegendreP": orthogonal.LegendreP,
        "GegenbauerC": orthogonal.GegenbauerC,
        "BesselY": orthogonal.BesselY,
        "HermiteH": orthogonal.HermiteH,
        "LaguerreL": orthogonal.LaguerreL,
        "PowerX": orthogonal.PowerX,
    }

    bas = switcher.get(options.basis, "nothing")
    if bas == "nothing":
        raise TypeError(f"tautoolbox: Unknown basis {options.basis.name}")
    return bas(options)
