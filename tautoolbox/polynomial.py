# Copyright (C) 2021, University of Porto and Tau Toolbox developers.
#
# This file is part of Tautoolbox package.
#
# Tautoolbox is free software: you can redistribute it and/or modify it
# under the terms of version 3 of the GNU Lesser General Public License as
# published by the Free Software Foundation.
#
# Tau Toolbox is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser
# General Public License for more details.

# implementation under rows is more intuitive

import tautoolbox as tau
import numpy as np
import warnings
from copy import deepcopy
from numbers import Number
import matplotlib.pyplot as plt
from typing import Optional
from collections.abc import Iterable
from numpy.linalg import eigvals, qr
from scipy.sparse import spdiags
from scipy.sparse.linalg import spsolve
from scipy.special import binom
from tautoolbox.auxiliary import (
    leg2chebt,
    chebt2kind,
    jac2chebt,
    jac2jac,
    chebt2leg,
    standard_chop,
    ultra2ultra,
    scl,
)
from tautoolbox.utils import get_shape


class Polynomial:
    __array_priority__ = 1000
    istranspose = False

    exponents = np.zeros(2)

    def __init__(
        self, coeff: Optional[np.ndarray] = None, options=None, **kwargs
    ):
        if options is None:
            self.options = tau.settings()

        if isinstance(options, dict):
            self.options = tau.settings(**options)

        elif isinstance(options, tau.settings):
            self.options = options.copy()

        self.basis = tau.basis(self.options)
        self.domain = self.options.domain

        if not isinstance(self.domain, np.ndarray):
            self.domain = np.array(self.domain)
        self.iter = 0

        if coeff is None:
            # Construct the identity Polynomial
            coeff = self.basis.x1

            # If the number of equations is greater than one repeat the identity
            # Polynomial in the number of rows
            if self.options.nequations > 1:
                coeff = np.array(
                    [self.basis.x1 for i in range(self.options.nequations)]
                )

        # If coef is a number
        elif isinstance(coeff, Number):
            coeff = np.array([coeff])

        # Coef is an array_like
        elif get_shape(coeff) is not None:
            # check if coef is not already a numpy array
            if not isinstance(coeff, np.ndarray):
                coeff = np.array(coeff)

            if self.options.nequations > 1:
                coeff = coeff.reshape(self.options.nequations, -1)
        elif callable(coeff):
            coeff = tau.Polynomial.interp1(self.basis, coeff)
        elif isinstance(coeff, str):
            coeff = tau.Polynomial.interp1(self.basis, lambda x: eval(coeff))

        # in the case where coeff is not an array_like of numbers
        elif isinstance(coeff, list):
            p = tau.Polynomial(coeff[0], options)

            for i in range(1, len(coeff)):
                p = p.append(tau.Polynomial(coeff[i], options))

            self.__dict__ = p.__dict__

            return

        if isinstance(coeff, np.ndarray):
            # Check if coeff has the right dimensions
            if coeff.ndim > 2:
                raise ValueError(
                    "The dimensions of the coefficients must at most be 2"
                )
            vals = kwargs.get("vals", False)
            if vals:
                n = coeff.shape[-1]
                ab, *_ = self.basis.nodes(n)
                self.coeff = self.basis.interp_from_values(
                    coeff, ab, self.options
                )
            else:
                self.coeff = coeff.copy()

        else:
            raise TypeError(
                f"tautoolbox: tau.Polynomial -{type(coeff)} cannot be the 1st argument"
            )

    @property
    def n(self):
        """
        Compute the length of the coefficients array of a tau.Polynomial.

        Returns
        -------
        n : int
            Lenght of the cooefficients array

        Examples
        --------
        Approximate cos(x) on [-1, 0] using Legendre basis:

        >>> options = {"basis": "LegendreP", "domain": [-1, 0]}
        >>> p = tau.Polynomial(np.cos, options)
        >>> p
        Polynomial object with 1 column:
          basis          : LegendreP
          domain         : [-1, 0]
          degree         : 12
          end values     : [0.54, 1.0]
          vertical scale : 1.00
        >>> p.n                             
        13
        
        """
        self._n = self.coeff.shape[-1]
        return self._n

    @property
    def degree(self):
        """
        Compute the polynomial degree.

        Returns
        -------
        degree : int
            The polynomial degree
            
        """        
        return self.n - 1

    @property
    def nequations(self):
        """
        Compute the number of polynomials.

        Returns
        -------
        nequations : int
            Number of polynomials
            
        """          
        if self.coeff.ndim == 1:
            return 1
        return self.coeff.shape[0]

    @property
    def T(self):
        """
        Compute the transpose of a polynomial.

        Returns
        -------
        polynomial
            Transpose of a polynomial
            
        """         
        aux = self.copy()
        aux.istranspose = not aux.istranspose
        return aux

    def append(self, p):
        """
        Append polynomial p to self. Only appends if both are columns or
        rows polynomials.

        Parameters
        ----------
        p : polynomial
            Polynomial to append to ``self``.

        Raises
        ------
        TypeError
            This occurs when ``p`` is not a polynomial. It also happens when 
            both self and p are polynomials, but one is a column polynomial 
            and the other is a row polynomial, or vice versa. It can also occur
            when they are both column or row polynomials but do not share the 
            same options.

        Returns
        -------
        polynomial
            A polynomial with the rows or columns of ``p`` added to it.

        Examples
        ---------
        >>> p = tau.Polynomial(np.cos)
        >>> p.n
        15
        >>> q = tau.Polynomial(np.sin)
        >>> q.n
        14
        >>> # Since the polynomials have different lengths, a zero will 
        >>> # be appended to the end of the shorter polynomial in the 
        >>> # corresponding column (or row).
        >>> pq = p.append(q)
        >>> pq.n
        15
        
        """

        if not isinstance(p, tau.Polynomial):
            raise TypeError("Only two polynomials can be appended.")

        if self.istranspose != p.istranspose:
            raise TypeError(
                "You are trying to append polynomial columns with "
                "polynomials rows."
            )

        if self.options != p.options:
            raise TypeError("TBoth polynomials must share the same options")

        if p.n == self.n:
            coef = np.r_["0,2", self.coeff, p.coeff]
        elif p.n > self.n:
            coef = np.r_["0,2", self.extend(p.n).coeff, p.coeff]
        else:
            coef = np.r_["0,2", self.coeff, p.extend(self.n).coeff]

        return tau.Polynomial(coef, self.options)

    def __repr__(self):
        """
        This is a representation only for summary purpose.

        Returns
        -------
        str
            Representation of the object

        """
        if self.size == 1 and self.coeff.ndim == 2:
            return self[0].__repr__()

        tr = "row" if self.istranspose else "column"
        end_val = self(self.domain).round(2).tolist()
        ver_scal = self.vscale
        dom = self.domain.round(2).tolist()
        if self.size == 1:
            st = (
                f"Polynomial object with 1 {tr}:\n"
                f"  basis          : {self.basis.name}\n"
                f"  domain         : {dom}\n"
            )
            if self.options.basis == "GegenbauerC":
                st += f"  alpha          : {self.options.alpha}\n"
            st += (
                f"  degree         : {self.n-1}\n"
                f"  end values     : {end_val}\n"
                f"  vertical scale : {ver_scal:.2f}\n"
            )
            if (self.exponents).any():
                st += f"  exponents      : {self.exponents.round(2)}\n"
            return st
        st = f"Polynomial object with {self.size} {tr}s:\n"

        for i in range(self.size):
            aux = (
                f"\n  {tr} {i+1}:\n"
                f"    basis          : {self.basis.name}\n"
                f"    domain         : {dom}\n"
            )
            if self.options.basis == "GegenbauerC":
                aux += f"    alpha          : {self.options.alpha}\n"

            aux += (
                f"    degree         : {self.n-1}\n"
                f"    end values     : {end_val[i]}\n"
                f"    vertical scale : {ver_scal[i]:.2f}\n"
            )
            if self.exponents.any():
                aux += f"  exponents      : {self.exponents.round(2)}\n"

            st += aux
        return st

    @staticmethod
    def significantTerms(cn, tol=None):
        """
        Returns the length of the array of coefficients from which
        all the coefficients are less than ``tol`` in absolute value.

        Parameters
        ----------
        cn : ndarray
        tol : float, optional
            Tolerance. When not given, machine eps is used. The default
            is None.

        Returns
        -------
        int
            Number of significant terms
            
        Examples
        --------
        Compute the significant terms of cos(x) with the standard options, 
        using a tolerance of 1e-14

        >>> p = tau.Polynomial(np.cos)
        >>> p.n
        15
        >>> tau.Polynomial.significantTerms(p.coeff, 1e-14)
        13
        
        """
        if tol is None:
            tol = np.spacing(1)
        pos = np.argwhere(abs(cn) > tol)[:, -1]
        return 1 if pos.size == 0 else max(pos) + 1

    def __add__(self, rhs):
        """
        Parameters
        ----------
        rhs : array_like
            can be a list, a tuple or a Tau polynomial object

        Returns
        -------
        tau.Polynomial
            Sum is a Tau polynomial object

        Examples
        --------
        a and b are instances of Tau polynomial:

        >>> a = tau.Polynomial([[1,0,1], [2,0,1], [3,0,1]])
        >>> a.coef
        array([[1,0,1],
               [2,0,1],
               [3,0,1]])
        >>> b = tau.Polynomial([[1,0,1,0], [2,0,1,0], [3,0,1,0]])
        >>> b.coef
        array([[1, 0, 1, 0],
               [2, 0, 1, 0],
               [3, 0, 1, 0]])
        >>> c = a+b
        >>> c.coef
        array([[2,0,2],
               [4,0,2],
               [6,0,2]])

        a is a Tau polynomial and b is a list congruent to a matrix where the
        list represents the coeficients of a Tau polynomial in the same basis:

        >>> b = [[1, 0, 1,0],[2, 0, 1,0],[3,0,1,0]]
        >>> c = a+b
        >>> c.coef
        array([[2, 0, 2],
               [4, 0, 2],
               [6, 0, 2]])

        a is a Tau polynomial and b is a scalar representing the degree 0
        polynomial:

        >>> c = a + 2
        >>> c.coef
        array([[3., 0., 1.],
               [4., 0., 1.],
               [5., 0., 1.]])
        
        """
        aux = self.copy()
        if isinstance(rhs, Number):
            aux.coeff[..., 0] = aux.coeff[..., 0] + rhs
            return aux

        if not isinstance(rhs, tau.Polynomial):
            raise TypeError(
                "tautoolbox: you can only sum a Tau polynomial with an rhs Tau polynomial"
            )
            # the coefficients must be congruent to a matrix or vector

        aux.coeff = aux.basis.add(aux.coeff, rhs.coef)
        return aux.trim()

    def __radd__(self, lhs):
        return self + lhs

    def __pos__(self):
        """
        to enable operations like b = +a

        Returns
        -------
        polynomial


        """
        return self

    def __neg__(self):
        """
        The same as -a

        Returns
        -------
        tau.Polynomial
            -1 times the polynomial

        Examples
        --------
        >>> a = tau.Polynomial([[1,0,1], [2,0,1], [3,0,1]])
        >>> a.coef
        array([[1, 0, 1],
               [2, 0, 1],
               [3, 0, 1]])

        >>> b = -a
        >>> b.coef
        array([[-1,  0, -1],
               [-2,  0, -1],
               [-3,  0, -1]])

        >>> a = tau.Polynomial([1,0, 1])
        >>> a.coef
        array([1, 0, 1])

        >>> b = -a
        >>> b.coef
        array([-1,  0, -1])
        
        """

        aux = self.copy()
        aux.coeff = -aux.coeff
        return aux

    def __sub__(self, rhs):
        """
        Parameters
        ----------
        rhs : array_like
            can be a list, a tuple or a Tau polynomial object

        Returns
        -------
        tau.Polynomial
            Subtraction is a Tau polynomial object

        Examples
        --------
        a and b are instances of Tau polynomial:

        >>> a = tau.Polynomial([[1,0,1], [2,0,1], [3,0,1]])
        >>> b = tau.Polynomial([[1,0,1,0], [2,0,1,0], [3,0,1,0]])
        >>> c = a-b
        >>> c.coef
        array([[0],
               [0],
               [0]])
        >>> c= b-a
        >>> c.coef
        array([[0],
               [0],
               [0]])

        """

        if isinstance(rhs, Iterable):
            rhs = np.array(rhs)

        return self + (-rhs)

    def __rsub__(self, lhs):
        return -self + lhs

    def __mul__(self, rhs):
        """
        Parameters
        ----------
        rhs : array_like
            can be a list, a tuple or a tau.Polynomial object

        Returns
        -------
        tau.Polynomial
            the sum is a tau Polynomial object

        Examples
        --------

        """

        if isinstance(rhs, tau.operator):
            return rhs * self

        # The case when you multiply a polynomial by a constant
        if isinstance(rhs, Number):
            # When rhs is 0 must return a null polynomial
            if rhs == 0:
                aux = self.copy()
                aux.coeff = aux.coeff[..., :1]
                aux.coeff[:] = 0
                return aux
            aux = self.copy()
            aux.coeff *= rhs
            return aux

        # When rhs is an array_like object it must have tha same numbers of
        # rows
        if isinstance(rhs, Iterable):
            sh = get_shape(rhs)

            if sh is None:
                raise ValueError("The iterable must be an array_like object")
            rhs = np.array(rhs)
            if np.prod(sh) == 1:
                return self * rhs.item()

            # Do a broadcasting so the coefficients
            elif len(sh) == 1:
                return tau.Polynomial(
                    self.coeff * rhs[..., np.newaxis], self.options
                )
            else:
                # Because we implements Polynomial in rows
                # what we find here is the transpose of what is suposed in
                # Tautoolbox matlb

                return tau.Polynomial(rhs.T @ self.coeff, self.options)

        if isinstance(rhs, tau.Polynomial2):
            if self.options != rhs.opty:
                raise TypeError(
                    "Polynomial2 options in the y direction must be "
                    "the same as the Polynomial options"
                )

            aux = rhs.copy()
            aux.cols = aux.cols * self
            return aux

        if not isinstance(rhs, tau.Polynomial):
            raise TypeError(
                "tautoolbox: you can only multiply a tau.Polynomial with "
                "a tau.Polynomial, a list, or a tuple."
            )
        if self.istranspose == rhs.istranspose:
            # in the case where is two Polynomials they must have same basis
            # and alpha for the case of Gegenbauer Polynomials

            if self.options != rhs.options:
                raise TypeError(
                    "The two polynomial must share the same options"
                )

            # The case When at leat one of the polynomials are null
            if np.all(self.iszero()):
                return self.copy()

            elif np.all(rhs.iszero()):
                return rhs.copy()

            # The case when at least one the polynomials are constant
            if self.n == 1:
                aux = self.copy()
                aux.coeff = aux.coeff * rhs.coeff
                return aux

            elif rhs.n == 1:
                aux = rhs.copy()
                aux.coeff = aux.coeff * self.coeff
                return aux

            bas = tau.basis(self.options)

            aux = tau.Polynomial(
                bas.productv2(self.coeff, rhs.coeff), self.options
            ).trim()
            aux.istranspose = self.istranspose

            return aux
        elif rhs.istranspose:
            if self.size != rhs.size:
                raise ValueError("Inconsistent size")
            # In this case return a Polynomial2 which cols are self and
            # rows are rhs

            p = tau.Polynomial2()
            p.optx = rhs.options
            p.opty = self.options
            p.cols = self.copy()
            p.rows = rhs.T
            p.diag = np.ones(self.size)
            return p
        else:
            return self.inner_product(rhs)

    def __rmul__(self, lhs):
        return self * lhs

    def __truediv__(self, rhs):
        if rhs == 0:
            raise ZeroDivisionError("division by zero")
        aux = self.copy()
        aux.coeff = aux.coeff / rhs
        return aux

    def __pow__(self, order):
        if order != int(order):
            raise ValueError("order must be an integer.")

        obj = self.copy()
        if order == 0:
            obj.coeff = np.ones_like(obj.coeff[..., :1])

        elif order > 1:
            aux = obj
            for i in range(order - 1):
                obj = obj * aux
        return obj

    @staticmethod
    def interp1(bas, fx):
        """
        Parameters
        ----------
        bas : basis
            a tau basis object from which we want to work.
        fx : a function to interpolate

        Returns
        -------
        cn : tau.Polynomial
            a Tau polynomial approximation of the function in the working basis
            case x is a non polynomial function or an exact representation
            otherwise.

        Examples
        --------

        a) fx is a polynomial function, using the default basis 'ChebyshevT'

        >>> import numpy as np
        >>> np.set_printoptions(precision=3)
        >>> a=tau.Polynomial(lambda x: x**5+ x**3+1)

        b) fx is a non polynomial function

        >>> tau.Polynomial.interp1(tau.basis(), np.cos)
        array([ 7.652e-01, -1.830e-17, -2.298e-01,  1.830e-17,  4.953e-03,
                1.003e-17, -4.188e-05, -4.365e-18,  1.884e-07, -7.972e-18,
               -5.261e-10, -1.070e-17,  1.000e-12,  1.297e-17, -1.371e-15])

        c) fx is a polynomial function, using 'LegendreP':

        >>> options = tau.settings(basis='LegendreP')
        >>> tau.Polynomial.interp1(tau.basis(options), lambda x: x**5+ x**3+1)
        array([1.   , 1.029, 0.   , 0.844, 0.   , 0.127])

        """

        try:
            options = bas.options.copy()
            options.nequations = 1

            x = tau.Polynomial(options=options)
            y = fx(x) + 0 * x

            return y.coeff
        except:
            return tau.Polynomial.interp1f(bas, fx)

    @staticmethod
    def interp1f(bas, fx, tol=None, **kwargs):
        """
        Parameters
        ----------
        bas : tau.basis
        fx : function
            Function to approximate by a polynomial.
        **kwargs : optional arguments
            see tau.basis.interp1f

        Returns
        -------
        array_like
            an array with the coefs of the polynomial approximation of the
            function in the working basis.

        Examples
        --------

        working with the default basis 'ChebyshevT':

        >>> import numpy as np
        >>> np.set_printoptions(precision=3)
        >>> tau.Polynomial.interp1f(tau.basis(),np.cos)
        array([ 7.652e-01, -1.830e-17, -2.298e-01,  1.830e-17,  4.953e-03,
                1.003e-17, -4.188e-05, -4.365e-18,  1.884e-07, -7.972e-18,
               -5.261e-10, -1.070e-17,  1.000e-12,  1.297e-17, -1.371e-15])

        working with 'LegendreP':

        >>> options= tau.settings(basis='LegendreP')
        >>> tau.Polynomial.interp1f(tau.basis(options),lambda x: x**10 +2*x +1)
        array([1.091e+00,  2.000e+00,  3.497e-01, -1.613e-16,  3.357e-01,
                1.090e-16,  1.711e-01,  3.218e-17,  4.711e-02,  1.013e-16,
                5.542e-03])

        >>> a=tau.Polynomial.interp1f(tau.basis(),np.cos)
        >>> a
        array([ 7.652e-01, -1.830e-17, -2.298e-01,  1.830e-17,  4.953e-03,
                1.003e-17, -4.188e-05, -4.365e-18,  1.884e-07, -7.972e-18,
               -5.261e-10, -1.070e-17,  1.000e-12,  1.297e-17, -1.371e-15])

        working with 'PowerX':

        >>> options= tau.settings(basis='PowerX')
        >>> a=tau.Polynomial.interp1f(tau.basis(),np.cos)
        >>> a
        array([ 7.652e-01, -1.830e-17, -2.298e-01,  1.830e-17,  4.953e-03,
                1.003e-17, -4.188e-05, -4.365e-18,  1.884e-07, -7.972e-18,
               -5.261e-10, -1.070e-17,  1.000e-12,  1.297e-17, -1.371e-15])
        
        """
        bas = bas.copy()
        if tol == None:
            tol = tau.settings.interpRelTol
        n = 4
        nd = 8

        while n < bas.options.interpMaxDim and nd >= n:
            n *= 2
            f, *_ = bas.interp1f(fx, n=n, **kwargs)
            # Get the n when the coefficients has hitted a plateau below ot tol
            nd = standard_chop(f, tol)

        if n >= bas.options.interpMaxDim:
            warnings.warn(
                "Tautoolbox: tau.Polynomial.interp1: did not converge."
                "You can change interpRelTol" or "interpMaxDim" 
                "from tau.settings "
            )
        return f[..., :nd]

    def arccos(self):
        """
        Returns
        -------
        obj : tau.Polynomial
            returns a Tau polynomial representation of the composition of the
            arccos function with a Tau polynomial in the working basis, being 
            'ChebyshevT' the default basis.

        Examples
        --------

        The case when p is the identity function:

        >>> import tautoolbox as tau
        >>> import numpy as np
        >>> p=tau.Polynomial()

        In this case the interpolation did not converge with the default options:

        >>> p.arccos().coef[:5]
        array([ 1.571e+00, -1.273e+00, -5.152e-19, -1.415e-01, -1.084e-17])

        """
        return tau.Polynomial(
            self.interp1f(self.basis, np.arccos, poly=self), self.options
        )

    def arccosh(self):
        """
        The function arccosh(x) has domain [1, +oo[ and is not smooth in the
            neighborhood of 1.


        Returns
        -------
        obj : tau.Polynomial
            returns a Tau polynomial representation of the composition of the
            arccosh function with a Tau polynomial in the working basis, being
            'ChebyshevT' the default basis.

        Examples
        --------

        The case when p is the identity using default settings and domain
        [2, 6]:

        >>> import tautoolbox as tau
        >>> p= tau.Polynomial(options=tau.settings(domain=[2,6]))
        >>> p.arccosh().coef
        array([ 1.985e+00,  5.624e-01, -8.271e-02,  1.692e-02, -4.043e-03,
                1.063e-03, -2.990e-04,  8.826e-05, -2.702e-05,  8.508e-06,
               -2.739e-06,  8.975e-07, -2.984e-07,  1.004e-07, -3.412e-08,
                1.169e-08, -4.038e-09,  1.403e-09, -4.905e-10,  1.723e-10,
               -6.079e-11,  2.153e-11, -7.656e-12,  2.731e-12, -9.770e-13,
                3.505e-13, -1.260e-13,  4.545e-14, -1.649e-14,  5.916e-15,
               -2.126e-15,  7.284e-16, -3.140e-16])

        """
        return tau.Polynomial(
            self.interp1f(self.basis, np.arccosh, poly=self), self.options
        )

    def cos(self):
        """
        Returns
        -------
        obj : tau.Polynomial
            returns a Tau Polynomial representation of the composition of the
            cosine function with a Tau polynomial in the working basis, being 
            'ChebyshevT' the default basis.

        Examples
        --------
        >>> a=tau.Polynomial()
        >>> a.cos().coef
        array([ 7.652e-01, -1.830e-17, -2.298e-01,  1.830e-17,  4.953e-03,
                1.003e-17, -4.188e-05, -4.365e-18,  1.884e-07, -7.972e-18,
               -5.261e-10, -1.070e-17,  1.000e-12,  1.297e-17, -1.371e-15])
        """
        return tau.Polynomial(
            self.interp1f(self.basis, np.cos, poly=self), self.options
        )

    def arcsin(self):
        """
        This function arcsin(x) has domain [-1 , 1] ans is not smooth in the
        neighborhood of -1 and 1.

        Returns
        -------
        obj : tau.Polynomial
            returns a Tau polynomial representation of the composition of the
            arcsin function with a Tau polynomial in the working basis, being 
            'ChebyshevT' the default basis.

        Examples
        --------

        Using de default settings  and domain [-0.5,0.5]:


        >>> import tautoolbox as tau
        >>> p=tau.Polynomial(options=tau.settings(domain=[-0.5,0.5]))
        >>> p.arcsin().coef
        array([2.071e-17,  5.173e-01, -1.094e-17,  6.079e-03, -3.224e-18,
                1.952e-04, -2.806e-19,  8.317e-06,  2.140e-18,  4.056e-07,
               -7.573e-18,  2.142e-08, -5.813e-18,  1.192e-09,  7.813e-18,
                6.881e-11, -1.070e-18,  4.084e-12, -1.103e-17,  2.477e-13,
               -2.185e-20,  1.528e-14,  2.848e-18,  9.500e-16])
        """
        return tau.Polynomial(
            self.interp1f(self.basis, np.arcsin, poly=self), self.options
        )

    def cosh(self):
        """
        Returns
        -------
        obj : tau.Polynomial
            returns a Tau polynomial representation of the composition of the
            cosine hyperbolic function with a Tau polynomial in the working basis, 
            being 'ChebyshevT' the default basis.

        Examples
        --------

        The case when p is the identity function in the default settings:

        >>> import tautoolbox as tau
        >>> import numpy as np
        >>> p=tau.Polynomial()
        >>> p.cosh().coef
        array([1.266e+00,  3.396e-17,  2.715e-01, -2.650e-17,  5.474e-03,
                1.093e-17,  4.498e-05, -1.435e-17,  1.992e-07, -1.040e-17,
                5.506e-10,  7.476e-18,  1.039e-12, -3.770e-17,  1.409e-15])

        """
        return tau.Polynomial(
            self.interp1f(self.basis, np.cosh, poly=self), self.options
        )

    def sin(self):
        """
        Returns
        -------
        obj : tau.Polynomial
            returns a Tau polynomial representation of the composition of the
            sine function with a Tau polynomial in the working basis, being 
            'ChebyshevT' the default basis.

        Examples
        --------
        >>> a=tau.Polynomial()
        >>> a.sin().coef
        array([ 3.318e-17,  8.801e-01, -3.443e-17, -3.913e-02,  3.092e-19,
                4.995e-04,  3.899e-19, -3.005e-06, -1.453e-17,  1.050e-08,
                1.967e-17, -2.396e-11,  1.160e-17,  3.852e-14])
        """

        # obj = self.copy()

        return tau.Polynomial(
            self.interp1f(self.basis, np.sin, poly=self), self.options
        )

    def sinh(self):
        """
        Returns
        -------
        obj : tau.Polynomial
            returns a Tau polynomial representation of the composition of the
            sine hyperbolic function with a Tau polynomial in the working basis,
            being 'ChebyshevT' the default basis.

        Examples
        --------
        >>> a=tau.Polynomial()
        >>> a.sinh().coef
        array([5.573e-17,  1.130e+00, -2.454e-17,  4.434e-02, -2.281e-17,
                5.429e-04, -7.121e-18,  3.198e-06, -1.242e-17,  1.104e-08,
                1.394e-17,  2.498e-11, -3.019e-18,  3.996e-14])
        """

        return tau.Polynomial(
            self.interp1f(self.basis, np.sinh, poly=self), self.options
        )

    def exp(self):
        """
        Returns
        -------
        tau.Polynomial
            returns a Tau polynomial representation of the composition of the
            exponential function with a Tau polynomial in the working basis,
            being 'ChebyshevT' the default basis.

        Examples
        --------
        >>> a=tau.Polynomial()
        >>> a.exp().coef
        array([1.266e+00, 1.130e+00, 2.715e-01, 4.434e-02, 5.474e-03, 5.429e-04,
               4.498e-05, 3.198e-06, 1.992e-07, 1.104e-08, 5.506e-10, 2.498e-11,
               1.039e-12, 3.990e-14, 1.398e-15])
        """
        return tau.Polynomial(
            self.interp1f(self.basis, np.exp, poly=self), self.options
        )

    def linspace(self, n=None):
        """
        Parameters
        ----------
        n : int, optional
            number of samples to generate. If n is none the number of point is
            50.

        Returns
        -------
        array
            an array with n samples equally spaced across the domain.

        Examples
        --------
        >>> a=tau.Polynomial()
        >>> a.linspace(21)
        array([-1. , -0.9, -0.8, -0.7, -0.6, -0.5, -0.4, -0.3, -0.2, -0.1,  0. ,
                0.1,  0.2,  0.3,  0.4,  0.5,  0.6,  0.7,  0.8,  0.9,  1. ])
        """
        if n is None:
            return np.linspace(*self.domain, 100)
        return np.linspace(*self.domain, n)

    def polyval(self, x):
        """
        Parameters
        ----------
        x : array_like or a number
            an array, or any list congruent with an array, or a number.

        Returns
        -------
        array_like or a number
            if the input is a number then the output is a number; if the input
            is an array_like then the output is an array of the same dimension.

        Examples
        --------

        x is a number:

        >>> a=tau.Polynomial()
        >>> a.polyval(2)
        array(2.)

        x is a one-dimensional array_like:

        >>> a.polyval([1, 2])
        array([1., 2.])

        x is two-dimensional array_like:

        >>> a.polyval([[1, 2],[1, 3]])
        array([[1., 2.],
               [1., 3.]])
        
        """

        # if self.coef.ndim == 1:
        #     return self.basis.polyval(self.coef, x)

        # return np.array(
        #     [self.basis.polyval(self.coef[i], x) for i in range(self.nequations)]
        # )
        x = np.array(x) # convert input to numerical array
        
        ex = self.exponents

        res = self.basis.polyval(self.coeff, x)

        if not any(ex):
            return res

        # The case when exponents are not null
        dom = self.domain
        # scale x to the domain for prosuct ()
        xc = (2 * x - sum(dom)) / np.diff(dom).item()
        if ex[0]:
            res *= (1 + xc) ** ex[0]
        if ex[1]:
            res *= (1 - xc) ** ex[1]

        return res

    def mldivide(self, T):
        """
        Solve a linear matrix equation, or system of linear scalar equations,
        using Gauss elimination.
        
        Parameters
        ----------
        T : Coefficient matrix

        Returns
        -------
        x : tau.Polynomial
            Solution to the system Tx = self
        
        """
        
        obj = self.copy()
        b = obj.coeff.flatten()
        # options = obj.options

        #if options.solver.lower() == "gaussian":
        obj.iter = 0
        obj.coeff = np.linalg.solve(T, b).reshape(obj.coeff.shape)
        # else:
            # if options.precond == "ilu":
            #     setup = {
            #         opt: getattr(options, opt)
            #         for opt in "milu typeilu droptol udiag thresh".split()
            #     }

            # assert setup
        return obj

    def trim(self, tol=np.spacing(1)):
        aux = self.copy()
        ind = np.where(abs(aux.coeff) > tol)[-1]
        if ind.size == 0:
            aux.coeff = aux.coeff[..., :1] * 0
        else:
            aux.coeff = aux.coeff[..., : max(ind) + 1]

        return aux

    def __call__(self, x):
        """
        Parameters
        ----------
        x : array_like or a number
            an array, or any list congruent with an array, or a number.

        Returns
        -------
        array_like or a number
            if the input is a number then the output is a number. If the input
            is array_like then the ouutput is an array of the same dimension.

        Examples
        --------

        x is a number:

        >>> a=tau.Polynomial()
        >>> a(2)
        array(2.)

        x is one-dimensional  array_like:

        >>> a([1, 2])
        array([1., 2.])

        x is two-dimensional array_like:

        >>> a([[1, 2],[1, 3]])
        array([[1., 2.],
               [1., 3.]])
        
        """

        return self.polyval(x)

    def diff(self, order=1, kind=None):
        """
        Differentiate a polynomial to the given order.

        Parameters
        ----------
        order : Integer, optional
            The default is 1. The order of differentiation.

        Returns
        -------
        tau.Polynomial
            A Tau polynomial which is the result of the derivation

        """        
        # When the order is integer
        if round(order) == order:
            order = round(order)
            aux = self.copy()
            if any(aux.exponents):
                for i in range(order):
                    aux = aux.singDiff()
                return aux
            aux.coeff = aux.coeff @ np.linalg.matrix_power(
                aux.basis.matrixN(aux.n).T, order
            )
            return aux

        # When the order is fractional
        return self.fractionalDerivative(order, kind)

    def fractionalDerivative(self, order, kind=None):
        """
        Fracional derivative of a polynomial to the given order.

        Parameters
        ----------
        order : 
            The order of fractional differentiation.

        Returns
        -------
        tau.FPolynomial
            A Tau fractional polynomial which is the result of the fractional 
            derivation

        """           
        if order == round(order):
            return self.diff(order)

        if kind is None:
            kind = "c"
        elif kind.lower() in ["c", "caputo"]:
            kind = "c"
        elif kind.lower() in ["rl", "r-l", "riemann-liouville"]:
            kind = "r"
        else:
            raise ValueError(
                "kind must be in ['c','caputo'] for Caputo fractional derivati"
                "ve or in ['rl','r-l','Riemann-Liouville'] for Riemann-Liouvil"
                f"le fractional derivative. '{kind}' is given."
            )

        n = order.__ceil__()
        # Caputo fractional Derivative
        if kind == "c":
            aux = self.diff(n)
            return aux.fractionalIntegral(n - order)

        aux = self.fractionalIntegral(n - order)

        return aux.diff(n)

    def singDiff(self):
        opt = self.options.copy()
        opt.domain = [-1, 1]
        t = tau.polynomial(self.coeff, opt)
        u = t.copy()
        s = t.diff()
        s.exponents = self.exponents.copy()

        if self.exponents[0]:
            t = t * self.exponents[0]
            t.exponents = self.exponents.copy()
            t.exponents[0] = t.exponents[0] - 1
            s = s.singAdd(t)

        if self.exponents[1]:
            u = u * (-self.exponents[1])
            u.exponents = self.exponents.copy()
            u.exponents[1] = u.exponents[1] - 1
            s = s.singAdd(u)
        sex = s.exponents
        rescale_factor = 0.5 * np.diff(self.domain)
        s = tau.polynomial(s.coeff / rescale_factor, self.options)
        s.exponents = sex
        return s

    def singAdd(self, q):
        tolex = 1e-12
        p = self.copy()
        opt = p.options.copy()
        opt.domain = [-1, 1]
        if isinstance(q, Number):
            q = tau.polynomial(q, self.options)

        pex = p.exponents
        qex = q.exponents

        if all(abs(pex - qex) < tolex):
            # The functions have the same exponents
            p.coeff = p.basis.add(p.coeff, q.coef)
            return p.trim()
        elif all(abs(np.round(pex - qex) - (pex - qex)) < tolex):
            # The exponents differ by integers
            fP = tau.Polynomial(1, opt)
            fQ = tau.Polynomial(1, opt)
            nEx = np.zeros(2)

            for i in range(2):
                ind = np.argsort([pex[i], qex[i]])
                e = np.sort([pex[i], qex[i]])
                nEx[i] = e[0]

                # The coetient factor is the difference in the exponents
                if i == 0:
                    nF = tau.Polynomial(
                        lambda x: (1 + x) ** np.diff(e).item(), opt
                    )
                else:
                    nF = tau.Polynomial(
                        lambda x: (1 - x) ** np.diff(e).item(), opt
                    )

                # Who had the algebraically smaller exponent? the other one
                # gets the factor

                if ind[0] == 0:
                    fQ = fQ * nF
                else:
                    fP = fP * nF

            s = (
                tau.Polynomial(p.coeff, opt) * fP
                + tau.Polynomial(q.coeff, opt) * fQ
            )

            if s.iszero():
                return tau.Polynomial(0, self.options)

            s = tau.Polynomial(s.coeff, self.options)
            s.exponents = nEx

            return s
        else:
            if p.iszero():
                return q.copy()
            elif q.iszero():
                return p
            raise ValueError(
                "We cannot add two singular functions when the difference "
                "in the exponents are not integer."
            )

    # this function only integrate one level
    # after i will make this function auxiliary to integration for arbitrary levels
    def integrate(self, order=1, k=[]):
        """
        Integrate the Polynomial to the given order where k are the constants
        of integration. The first constant are the constant of the first
        integration, the second constant are the constant of the second inte-
        gration and so on


        Parameters
        ----------
        order : Integer, optional
            The default is 1. The order of integration
        k : Number or list, optional
            DESCRIPTION. The default is []. The constants of integration. The
            first constant corresponds to the first integration constant the
            second constant corresponds to the second integration constant and
            so on.

        Returns
        -------
        Polynomial
            A Polynomial which is the result of the integration

        """

        if order < 0 or int(order) != order:
            raise ValueError(
                "The degree must be a non-negative integer was "
                f"given {order}."
            )
        if isinstance(k, list):
            if k == []:
                k = [0] * order
            else:
                k = k + [0] * (order - len(k))
        if isinstance(k, int):
            k = [k] + [0] * (order - 1)

        if len(k) > order:
            raise ValueError(
                "The Number of integration constants must be "
                "less than the integration order"
            )

        p = self.copy()
        aux = p.coeff
        for i in range(order):
            aux = np.r_["-1", aux, np.zeros_like(aux[..., :1])]
            aux = aux @ self.basis.matrixO(self.n + i + 1).T
            aux[..., 0] = aux[..., 0] - self.basis.polyval(aux, 0) + k[i]

        p.coeff = aux
        return p

    fracDiff = fractionalDerivative

    # def integrate(self, order=1, k=[]):
    #     if order < 0 or int(order) != order:
    #         raise ValueError(
    #             "The degree must be a non-negative integer was " f"given {order}."
    #         )
    #     if len(k) > order:
    #         raise ValueError(
    #             "The Number of integration constants must be "
    #             "less than the integration order"
    #         )
    #     if isinstance(k, list):
    #         if k == []:
    #             k = [0] * order
    #         else:
    #             k = k + [0] * (order - len(k))
    #     elif isinstance(k, int):
    #         k = [k] + [0] * (order - 1)

    #     for i in range(order):
    #         pass

    @property
    def power_coef(self):
        """
        Returns an array_like object where the entries are the coeficients of
        each row in the power basis

        Returns
        -------
        Array_like
            The coeficients in the power basis

        Examples
        --------

        using the basis ChebyshevT  and the domain [-1,1]:

        >>> import tautoolbox as tau
        >>> import numpy as np
        >>> coef=np.eye(3)
        >>> p=tau.Polynomial(coef)
        >>> p.power_coef
        array([[ 1.,  0.,  0.],
               [ 0.,  1.,  0.],
               [-1.,  0.,  2.]])

        now using the basis ChebyshevU and domain [-1,1]:

        >>> p=tau.Polynomial(coef,tau.settings(basis='ChebyshevU'))
        >>> p.power_coef
        array([[ 1.,  0.,  0.],
               [ 0.,  2.,  0.],
               [-1.,  0.,  4.]])

        now using LegendreP basis and domain [-3,0]:

        >>> coef =np.arange(9).reshape(3,3)
        >>> coef
        array([[0, 1, 2],
               [3, 4, 5],
               [6, 7, 8]])
        >>> p=tau.Polynomial(coef,tau.settings(basis='LegendreP',domain=[-3,0]))
        >>> p.power_coef
        array([[ 3.        ,  4.66666667,  1.33333333],
               [12.        , 12.66666667,  3.33333333],
               [21.        , 20.66666667,  5.33333333]])

        """
        return self.coeff @ self.basis.orth2powmatrix(self.n).T

    def plot(self, ax=None, **kwargs):
        """
        Plots a tau.Polynomial
        """
        plt.style.use("ggplot")
        if ax is None:
            ax = plt.gca()
        xx = self.linspace()
        yy = self.polyval(xx)
        if self.size > 1:
            xx = np.repeat(xx.reshape(1, -1), self.size, axis=0)
        ax.plot(xx.T, yy.T, **kwargs)
        return ax

    def __len__(self):
        """
        Computes the length.

        Returns
        -------
        n : int
            Lenght
        
        """

        return self.n

    @property
    def size(self):
        return self.nequations

    def definite_integral(self, bounds=None, fast=True):
        """(
        Computes the definite integral of a tau.Polynomial with the 
        interval set in bounds

        .. math::
            \int_a^bp(x)d(x)

        Parameters
        ----------
        bounds : iterable, optional
            The default is None. A real valued iterable with
            2 elements. When the bounds are not given we assumes the bounds
            as the domain of the polynomial.

        Returns
        -------
        Number or an array of numbers
            The result of the definite integral, can be a
            number of an array of numbers in the case the polynomial has more
            than one row.

        Examples
        --------

        using the default settings:

        >>> import numpy as np
        >>> import tautoolbox as tau
        >>> p=tau.Polynomial(np.arange(6).reshape(2,3))
        >>> p.definite_integral()
        array([-1.33333333,  2.66666667])

        Now considering the ChebyshevU basis, compute the definite integral
        between 0 an 1:

        >>> options =tau.settings(basis='ChebyshevU')
        >>> p=tau.Polynomial(np.arange(6).reshape(2,3),options)
        >>> p.definite_integral((0,1))
        array([1.66666667, 8.66666667])

        """
        if bounds is None:
            bounds = self.domain

        if fast:
            if all(bounds == self.domain):
                # This is based in the fact that \int_{-1}^{1}T_{n}(x)dx=2/(1-n**2)
                # for n even
                if self.options.basis == "ChebyshevT":
                    return (
                        (1 - np.arange(0, self.n, 2, dtype=float) ** 2) ** -1
                        @ self.coeff[..., ::2].T
                        * np.diff(self.domain).item()
                    )

                # This is based in the fact that \int_{-1}^{1}T_{n}(x)dx=2/(n+1)
                # for n even
                if self.options.basis == "ChebyshevU":
                    return (
                        np.arange(1, self.n + 1, 2, dtype=float) ** -1
                        @ self.coeff[..., ::2].T
                        * np.diff(self.domain).item()
                    )

                # This is based in the fact that \int_{-1}^{1}T_{n}(x)dx=2/(n+1)
                # for n even and -2/n for n odd
                if self.options.basis == "ChebyshevV":
                    n = self.n
                    c = np.arange(1, n + 1, 2, dtype=float) ** -1
                    aux = np.zeros(n)
                    aux[::2] = c
                    aux[1::2] = -c[: n // 2]
                    return aux @ self.coeff.T * np.diff(self.domain).item()

                # This is based in the fact that \int_{-1}^{1}T_{n}(x)dx=2/(n+1)
                # for n even and 2/n for n odd
                if self.options.basis == "ChebyshevW":
                    n = self.n
                    aux = np.repeat(
                        np.arange(1, n + 1, 2, dtype=float) ** -1, 2
                    )[:n]

                    return aux @ self.coeff.T * np.diff(self.domain).item()

                # This is based in the fact that \int_{-1}^{1}T_{n}(x)dx=0 if n>0
                # and 2*T_{0}(x) if n=0
                if self.options.basis == "LegendreP":
                    return np.diff(self.domain) @ self.coeff[..., :1].T

                # This is based in the fact that \int_{-1}^{1} C_{n}^{\alpha}(x)dx=
                # \binom{2\alpha+n-2}{n}/(n+1) if n even
                if self.options.basis == "GegenbauerC":
                    al = self.options.alpha
                    n = self.n
                    c = np.arange(0, n, 2)
                    aux = binom(2 * al + c - 2, c) / (c + 1)
                    if np.isnan(aux[0]):
                        aux[0] = 1
                    return (
                        aux
                        @ self.coeff[..., ::2].T
                        * np.diff(self.domain).item()
                    )

        p = self.copy()
        aux = p.coeff
        aux = np.r_["-1", aux, np.zeros_like(aux[..., :1])]
        aux = aux @ self.basis.matrixO(self.n + 1).T
        p.coeff = aux
        return p(bounds[1]) - p(bounds[0])

    def inner_product(self, other, broadcast=False):
        """
         Compute the L2 inner product of two tau.Polynomials

         When broadcast is false does simply the definite integral of self*other
         over the domain, otherwise the result is a matrix whose i,j entry is the
         inner product of the ith row of self with the jth column of other.

         Parameters
         ----------
         other : TYPE
         broadcast : bool
            If broadcast is true then the
            result is a matrix whose i,j entry is the inner product of the ith
            row of self with the jth column of other.

         Returns
         -------        

        """

        self.istranspose = other.istranspose

        return np.array(
            [(self * other[i]).definite_integral() for i in range(other.size)]
        ).reshape(self.size, other.size)

    def inner_product2(self, other):
        x = tau.Polynomial(
            np.tile(self.coeff, (other.size, 1)),
            self.options,
        )
        y = tau.Polynomial(
            np.repeat(other.coeff.reshape(self.size, -1), self.size, axis=0),
            other.options,
        )
        return (x * y).definite_integral()

    def __getitem__(self, index):
        if not isinstance(index, (int, slice, list)):
            raise TypeError("The index must an integer or a slice object")
        opt = self.options

        aux = self.coeff.copy()
        if aux.ndim == 1:
            aux = aux.reshape(1, -1)

        if isinstance(index, (slice, list)):
            if aux.size == 0:
                raise IndexError(
                    "Cannot return a Polynomial with these slice parameters"
                )
            aux = aux[index]

            aux = Polynomial(aux, opt)
            aux.istranspose = self.istranspose
            return aux

        if index < -self.size or index >= self.size:
            raise IndexError(
                "Index out of bound the Polynomial only have "
                f"{self.size} rows"
            )
        aux = Polynomial(aux[index], opt)
        aux.istranspose = self.istranspose
        return aux

    def get_coef(self, n=None):
        if n is None:
            return self.coef
        if not (isinstance(n, Number) and int(n) == n):
            raise TypeError("n must be a positive integer.")
        n = int(n)
        if n < 1:
            raise ValueError("n must be a positive integer.")

        lc = self.n  # the number of coefficients

        if n <= lc:
            return self.coeff[..., :n].copy()
        else:
            pad = [[0, 0]] * self.coeff.ndim
            pad[-1] = [0, n - lc]
            return np.pad(self.coeff, pad)

    def coef_to_val(self, **kwargs):
        # Compute the values of the function on the ChebyshevU points

        kwargs.setdefault("domain", self.domain)
        kwargs.setdefault("basis", "ChebyshevU")

        bas = tau.basis(tau.settings(**kwargs))

        if self.n == 1:
            return self.coeff.copy()
        return self(bas.nodes(self.n)[0])

    def simplify(self, tol=None):
        if tol is None:
            tol = self.options.interpRelTol

        vs = self.vscale
        toli = tol * np.max(vs) / vs
        if self.coeff.ndim == 1:
            m = standard_chop(self.coeff, toli)
        else:
            m = max([standard_chop(p.coeff, t) for t, p in zip(toli, self)])
        return self.extend(m)

    @property
    def vscale(self):
        """
        Returns an estimate of the maximum of the absolute value of self

        Parameters
        ----------
        type : TYPE, optional
            The default is None.

        Returns
        -------
        vs : TYPE

        """

        return np.max(abs(self.coef_to_val()), axis=-1)

    def iszero(self, tol=None):
        if tol is None:
            tol = self.vscale * tau.settings.defaultPrecision

        if isinstance(tol, np.ndarray):
            tol = tol.reshape(-1, 1)

        res = np.all(self(self.domain) <= tol, axis=-1)
        if np.any(res):
            res &= np.all(self.coef == 0, axis=-1)
        return res

    def roots(self, kind=None, tol=None, htol=None, **kwargs):
        """
        Estimates of the roots of a polynomial

        Parameters
        ----------
        kind : str, optional
            Can be 'real' or 'complex' if not is given assumes 'real'
        htol : scalar, optional
            A tolerance on the extremes of the domain
        **kwargs : 

        Returns
        -------
        r : 

        """
        if tol == None:
            tol = tau.settings.interpRelTol
        if kind == None:
            kind = "real"
        kind = kind.lower()

        if htol is None:
            htol = 1e-12
        co = self.coeff

        dom = self.domain
        if self.size == 1:
            if co.ndim == 2:
                co = co.flatten()
            # Find the position in which all coefficients from this position are
            # in the neighborhood of zero
            nzPos = np.argwhere(abs(co) > tol)

            # The trivial case where all coefficients are 0 so the function is zero
            # function so pick a  point in the domain we choose the mid-point of
            # the domain

            n = None if nzPos.size == 0 else np.max(nzPos)

            # The trivial case with zero function. the root can be every point on
            # the domain we choose the mid-point
            if n == None:
                r = np.array([0])

            # The npn-zero constant function
            elif n == 0:
                r = np.zeros(0)

            # the case when f= mx+b

            else:
                r = eigvals(self.basis.companion(co[: n + 1]))
                if kind == "real":
                    r = r[r.imag == 0].real
            # Scale the roots to the interval
            if (dom != [-1, 1]).any():
                r = (np.sum(dom) + np.diff(dom) * r) / 2
            r = r[(r > dom[0] - htol) & (r < dom[1] + htol)]
            r.sort()

            r[abs(r - dom[0]) < htol] = dom[0]
            r[abs(r - dom[1]) < htol] = dom[1]
            return r

        else:
            r_list = [self[i].roots() for i in range(self.size)]
            le = list(map(len, r_list))
            r = np.empty((self.size, max(le)))
            r[:] = np.nan
            for i, el in enumerate(le):
                if el > 0:
                    r[i, :el] = r_list[i]
            return r

    def critical(self):
        """
        The critical points of a tau.Polynomial. These are the roots of the
        first derivative plus the extremes of the domain.

        Returns
        -------
        array_like
            the critical points of the polinomial

        """

        return self.diff().roots()

    def max(self, glob=True):
        """
        Computes the max of a tau.Polynomial

        Parameters
        ----------
        glob :  optional

        Returns
        -------

        """        
        dom = self.domain
        if glob:
            if self.size == 1:
                crit = np.array(list(set(np.r_[dom, self.critical()])))
                vals = self(crit)
                max_pos = vals.argmax()
                return np.array([vals[max_pos], crit[max_pos]])
            else:
                return np.array([self[i].max() for i in range(self.size)])

        else:
            if self.size == 1:
                crit = self.critical()
                vals = self.diff(2)(crit)
                max_pos = np.where(vals < 0)
                vals = self(crit[max_pos])
                crit = crit[max_pos]
                if np.isscalar(vals):
                    vals = np.array([vals])
                ext_vals = self(dom)
                ext_max = np.argmax(ext_vals)
                if (vals < ext_vals[ext_max]).all():
                    vals = np.r_[vals, ext_vals[ext_max]]
                    crit = np.r_[crit, dom[ext_max]]

                sort_ind = crit.argsort()

                return vals[sort_ind], crit[sort_ind]

            else:
                return [self[i].max(glob=False) for i in range(self.size)]

    def min(self, glob=True):
        """
        Computes the min of a tau.Polynomial

        Parameters
        ----------
        glob :  optional

        Returns
        -------

        """
        # The min of a polynomial p are the max of -p
        return (-self).max(glob=glob)

    def sign(self):
        if self.size == 1:
            part = np.array(list(set(self.roots()).union(self.domain)))
            part.sort()

            mid = (part[:-1] + part[1:]) / 2
            vals = self(mid)
            sig = np.ones_like(mid, dtype=int)
            sig[vals < 0] = -1
            return part, sig
        else:
            return [self[i].sign() for i in range(self.size)]

    def norm(self, ord=None):
        """
        Compute the norm of order ``ord``.

        Parameters
        ----------
        ord : integer or str, optional
            A integer or string representing the order of the norm

        Returns
        -------
        scalar or vector
            The norm of each row of the Polynomials

        """
        if isinstance(ord, str):
            ord = ord.lower()

        # the norm   is sqrt(integral(p^2))  in the domain
        if ord in [None, "fro"]:
            ord = 2

        # The norm max(abs(p)). This occur in the critical points or in the ex-
        # tremes of the domain
        if ord in ["inf", np.inf]:
            if self.size == 1:
                return np.max(
                    np.abs(self(np.r_[self.critical(), self.domain]))
                )
            else:
                return np.array(
                    [self[i].norm(ord=np.inf) for i in range(self.size)]
                )

        # the norm is min(abs(p))
        elif ord in ["-inf", -np.inf]:
            if self.size == 1:
                # If the Polynomial has at lest one zero so its norm(-inf)
                # must zero

                if len(self.roots()) > 0:
                    return 0

                # Otherwise it may ocur on the on the critical points or
                # extreme of the domain
                return np.min(
                    np.abs(self(np.r_[self.critical(), self.domain]))
                )
            else:
                return np.array(
                    [self[i].norm(ord=-np.inf) for i in range(self.size)]
                )

        # when ord is n the result is integral(abs(p**n))**(1/n)
        elif isinstance(ord, int):
            # The case where the order is even
            if ord % 2 == 0:
                if ord == 2:
                    return np.sqrt((self * self).sum())
                return (self**ord).sum() ** (1 / ord)

            # When the order is odd
            else:
                if self.size == 1:
                    bounds, sign = self.sign()
                    return sum(
                        [
                            (self**ord).sum(bounds[i : i + 2]) * sign[i]
                            for i in range(len(sign))
                        ]
                    ) ** (1 / 3)
                else:
                    return np.array(
                        [self[i].norm(ord) for i in range(self.size)]
                    )

        else:
            raise ValueError(
                "Possible orders are: None, 1,2,'fro','-inf','inf',"
                "np.inf,-np.inf, or an integer positive."
            )

    def mean(self):
        r"""
        Returns de mean of the function over the domain. This means:

        .. math::
            \frac{1}{b-a}\int_a^b p(x)dx


        Returns
        -------
        scalar or vector
            The mean of the function over the domain

        """

        return self.definite_integral() / np.diff(self.domain)

    def cumsum(self, order=1):
        r"""
        Computes the nth order indefinite integral of a tau.Polynomial in such way that
        this integral evaluated at the left extreme of the domain is zero; i.e.
        Q(a)=0 where que is this integral. This is not to be confused with the
        method integrate where i can specify the constant of integration.

        Parameters
        ----------
        order : optional
            The default is 1.

        Returns
        -------


        """
        if order < 0:
            raise ValueError(
                f"order must be greather than 0 was given {order}."
            )

        if int(order) != order:
            return self.fractionalIntegral(order)

        aux = self.copy()

        if any(aux.exponents):
            for i in range(order):
                aux = aux.singInt()
        else:
            for i in range(order):
                aux = aux.integrate()
                aux = aux - aux(aux.domain[0])

        return aux.trim()

    def simplifyExponents(self):
        aux = self.copy()

        opt = aux.options.copy()
        opt.domain = [-1, 1]
        exps = aux.exponents.copy()
        sm = tau.polynomial(aux.coeff, opt)

        tol = 100 * np.spacing(1) * sm.vscale
        exps[exps < tol] = 0
        aux.exponents[:] = exps

        idx = abs(np.round(exps) - exps) < tol
        exps[idx] = np.round(exps[idx])

        ind = exps >= 1 - tol
        if not any(ind):
            return aux

        # Sort out the new exponents and the order of the boundary roots which
        # need to be absorbed into the smoothpart
        newExps = exps.copy()
        newExps[ind] = exps[ind] - np.floor(exps[ind])
        pw = exps - newExps

        mult = tau.polynomial(
            lambda x: (1 + x) ** pw[0] * (1 - x) ** pw[1], opt
        )

        p_c = (mult * sm).coeff
        p = tau.polynomial(p_c, self.options)

        p.exponents = newExps
        return p

    def singInt(self):
        n = self.n
        al = self.options.alpha
        switch = {
            "LegendreP": leg2chebt(self.coeff),
            "ChebyshevU": jac2chebt(self.coeff / scl(1, n - 1), 0.5, 0.5),
            "GegenbauerC": jac2chebt(
                self.coeff / scl(al, n - 1), al - 0.5, al - 0.5
            ),
            "ChebyshevW": jac2chebt(self.coeff / scl(0, n - 1), 0.5, -0.5),
            "ChebyshevV": jac2chebt(self.coeff / scl(0, n - 1), -0.5, 0.5),
        }

        co = switch.get(self.options.basis, self.coeff)

        aux = tau.polynomial(co)
        flip = False
        # When the singularity is at the extreme right of the interval,flip
        # so that the singularity is at the extreme left of the interval
        if self.exponents[1]:
            aux = aux.flip()
            flip = True

        ex = self.exponents
        xs = aux * tau.polynomial(lambda x: 1 + x)
        a = -ex[0]
        ra = max(round(a), 1)

        # If the size of xs is less than ra+3 we pad xs to size ra+3
        n = xs.n
        oldn = n
        if n < ra + 3:
            n = ra + 3
            xs = xs.extend(n)

        aa = xs.coeff

        # Recurrence relation to solve for the coefficients u', i.e.,c_k.
        c = np.zeros_like(aa[..., :-1])

        c[..., n - 2] = 2 * aa[..., n - 1] / (1 - a / (n - 1))
        c[..., n - 3] = (
            2 * (aa[..., n - 2] - c[..., n - 2]) / (1 - a / (n - 2))
        )
        for i in range(n - 4, ra - 1, -1):
            c[..., i] = (
                2
                * (
                    aa[..., i + 1]
                    - c[..., i + 1]
                    - c[..., i + 2] * 0.5 * (1 + a / (i + 1))
                )
                / (1 - a / (i + 1))
            )

        # Compute Cm
        Cm = 2 ** (ra - 1) * (
            aa[..., ra] - c[..., ra] - c[..., ra + 1] * (1 + a / ra) / 2
        )

        # Compute the polynomial representation of (x+1)**[a] in [-1,1]
        xa = tau.polynomial(lambda x: (1 + x) ** ra)

        # Intermediate result for temporary use
        aa[..., : ra + 1] = aa[..., : ra + 1] - Cm * np.flip(xa.coeff, axis=-1)

        # Compute the rest of the coefficients
        for i in range(ra - 2, -1, -1):
            c[..., i] = (
                2
                * (
                    aa[..., i + 1]
                    - c[..., i + 1]
                    - c[..., i + 2] * 0.5 * (1 + a / (i + 1))
                )
                / (1 - a / (i + 1))
            )

        # Compute the Chebyshev coefficients of u from those of u'
        kk = np.arange(1, n)
        c = 0.5 * c
        dd1 = c / kk
        dd2 = -c[..., 2:] / kk[:-2]

        pad = np.zeros_like(c[..., :1])
        cc = np.r_["-1", pad, dd1 + np.r_["-1", dd2, pad, pad]]

        # Choose first coefficient so that u(-1) =(x+1)*f(-1)=0
        cc[..., 0] = np.sum(cc[..., 1::2], axis=-1) - np.sum(
            cc[..., 2::2], axis=-1
        )

        # Remove the padding we put in
        if n > oldn + 2:
            cc = cc[..., : oldn + 1]

        # Drop the leading zeros in the coefficients
        ind = np.where(cc != 0)[-1]

        if ind.size == 0:
            cc = pad
        else:
            cc = cc[..., : max(ind) + 1]

        u = tau.polynomial(cc)

        tol = np.spacing(1) * aux.vscale

        if abs(ra - a) > tol:
            CM = Cm / (ra - a)

            if u.iszero() and abs(CM) > tol * aux.vscale:
                p = tau.polynomial(lambda x: CM)
                p.exponents = p.exponents + [ra - a, 0]
            elif not u.iszero() and abs(CM) < tol:
                p, rootsLeft, rootsRight = u.extractBoundaryRoots(
                    np.array([1, 0])
                )
                p.exponents = ex + [rootsLeft.item(), rootsRight.item()]
            else:
                # The general case where where both terms are non trivial
                p, rootsLeft, rootsRight = (u + CM * xa).extractBoundaryRoots(
                    np.array([1, 0])
                )
                p.exponents = ex + [rootsLeft.item(), rootsRight.item()]
        elif abs(Cm) < tol:
            # No log term: fractional poles with non-constant smooth part
            p, rootsLeft, rootsRight = u.extractBoundaryRoots(np.array([1, 0]))
            p.exponents = ex + [rootsLeft.item(), rootsRight.item()]
        else:
            # Log term: Integer poles with constant or non constant smooth part:
            # TODO: Construct a representation of log
            raise ValueError(
                "cumsum does not support the case where the singular"
                "indefinite integral has a logarithmic therm."
            )

        if flip:
            p = -p.flip()

        # Ensure p(-1)=0
        if p.exponents[0] >= 0:
            p.coeff[..., 0] = p.coeff[..., 0] - p(-1)

        # scale to the domain
        rescale_factor = 0.5 * np.diff(self.domain)
        p.coeff = rescale_factor * p.coef
        n = p.coeff.shape[-1]

        switch = {
            "LegendreP": chebt2leg(p.coeff),
            "GegenbauerC": jac2jac(
                p.coeff / scl(0, n - 1), -0.5, -0.5, al - 0.5, al - 0.5
            )
            * scl(al, n - 1),
            "ChebyshevU": chebt2kind(p.coeff),
            "ChebyshevV": chebt2kind(p.coeff, 3),
            "ChebyshevW": chebt2kind(p.coeff, 4),
        }

        p.coeff = switch.get(self.options.basis, p.coeff)

        ex = p.exponents
        p = tau.polynomial(p.coeff, self.options)
        p.exponents = ex

        return p

    def extractBoundaryRoots(self, numRoots=None):
        aux = self.copy()
        m = aux.size

        # The multiplicity of the roots
        rootsLeft, rootsRight = np.zeros((2, m))
        # Tolerance for root (we will this with each run of the loop below if)
        # there are multiple roots)
        tol = 1e3 * aux.vscale

        # Values at endpoints
        endValues = abs(aux(aux.domain))

        if np.all(np.min(endValues, axis=-1) > tol):
            return aux, rootsLeft, rootsRight

        # Get the coefficients
        c = aux.coef

        while (
            numRoots is None and np.any(np.min(endValues, axis=-1) <= tol)
        ) or np.any(numRoots > 0):
            if numRoots is None:
                # Roots at the left
                ind = np.where(endValues[..., 0] <= tol)[-1]
                if ind.size > 0:
                    sgn = 1
                    rootsLeft[ind] += 1

                else:
                    sgn = -1
                    ind = np.where(endValues[..., 1] <= tol)[-1]
                    rootsRight[ind] += 1
            else:
                if np.any(numRoots[..., 0]):
                    # Roots at the left
                    ind = endValues[..., 0] <= tol
                    indNumRoots = numRoots[..., 0] > 0
                    if ind == indNumRoots:
                        sgn = 1
                        numRoots[ind, 0] -= 1
                        rootsLeft += 1
                    else:
                        numRoots[..., 0] = 0
                        continue
                elif np.any(numRoots[..., 1]):
                    # Roots at the right
                    ind = endValues[..., 1] <= tol
                    indNumRoots = numRoots[..., 1] > 0
                    if ind == indNumRoots:
                        sgn = -1
                        numRoots[ind, 1] -= 1
                        rootsRight += 1
                    else:
                        numRoots[..., 1] = 0
                        continue
            # Construct the matrix of recurrence

            n = aux.n
            e = np.ones(n - 1)
            d = spdiags(
                [np.r_[1, 0.5 * e[1:]], sgn * e, 0.5 * e],
                [0, 1, 2],
                n - 1,
                n - 1,
                format="csc",
            )

            # Compute the new coefficients

            c[ind, :-1] = sgn * spsolve(d, c[ind, 1:].T).T

            # Pad zero at the highest coefficients

            c[ind, -1] = 0
            endValues = abs(aux(aux.domain))
            # Loosen the tolerance for checking multiple roots
            tol *= 1e2

        return aux.simplify(), rootsLeft, rootsRight

    def fractionalIntegral(self, order):
        r"""
        Compute the fractional integral of order `order`. When `order` is inte
        ger the result is the standard indefinite integral of order `order`.

        Parameters
        ----------
        order : scalar
            The order of the integral.

        Returns
        -------
        Polynomial
            The fractional Integral of order `order`.

        Examples
        --------
        Using ChebyshevT basis in the [-1,1] domain:

        >>> p = tau.polynomial(lambda x: x + 2 * x * 3 * x**2 - x**3)
        array([5.  , 4.75, 0.  , 1.25])
        >>> p.fractionalIntegral(2.6).coef
        array([ 1.1387984 ,  1.67645714,  0.60594962,  0.05632403, -0.00652738,
                0.00543948])
        """
        oi = order.__floor__()  # The integer part of the order
        of = order - oi  # The part fractional of the order

        p = self.cumsum(oi)

        if of == 0:
            return p

        else:
            b = tau.basis(self.options)
            if any(p.exponents):
                p = p.simplifyExponents()

                p.coeff = b.fractionalIntegral(p.coeff, of, p.exponents[0])
                p.exponents = p.exponents + [of, 0]

                if p.exponents[0]:
                    p = p.simplifyExponents()
            else:
                p.coeff = b.fractionalIntegral(p.coeff, of)
                p.exponents = p.exponents + [of, 0]
            return p.trim(np.spacing(1))

    # Alias for fractionalIntegral
    fracInt = fractionalIntegral

    def extend(self, n):
        r"""
        This method return a polynomial that add zeros to the columns from
        self.n to n if n>self.n; otherwise a polynomial that are self trimmed
        at n.

        Parameters
        ----------
        n : int
            The number of coefficients the columns must have

        Returns
        -------
        aux : Polynomial
            A polynomial identical to self but with exactly n coefficients per
            column

        Examples
        --------

        Using ChebyshevT basis in the [-1,1] domain:

        >>> p = tau.polynomial(lambda x: x + 2 * x * 3 * x**2 - x**3 + 5 * x**4)
        >>> p.coef
        array([1.875, 4.75 , 2.5  , 1.25 , 0.625])
        >>> p.extend(7).coef
        array([1.875, 4.75 , 2.5  , 1.25 , 0.625, 0.   , 0.   ])
        >>> p.extend(3).coef
        array([1.875, 4.75 , 2.5  ])

        """
        aux = self.copy()
        # The length of the polynomial
        sn = aux.n
        if n > sn:
            # When n is greather than the length of polynomial pad with zeros
            pad = [[0, 0]] * aux.coeff.ndim
            pad[-1] = [0, n - sn]
            aux.coeff = np.pad(aux.coeff, pad)
        else:
            # Cut the coefficients at n
            aux.coeff = aux.coeff[..., :n]
        return aux

    def qr(self):
        r"""
        QR factorization of array_like Polynomial. where self is Polynomial
        with n rows, produces a Polynomial Q with n orthonormal rows  and an
        n x n upper triangular matrix R such that A = Q*R.

        Returns
        -------
        q : TYPE
            DESCRIPTION.
        r : TYPE
            DESCRIPTION.

        """
        # The trivial case when self has only one row
        if self.size == 1:
            r = np.sqrt(self.inner_product(self))
            if r != 0:
                q = self / r
            else:
                q = 1 / np.sqrt(np.diff(self.domain).item()) + 0 * self

            return q, r
        b_name = self.options.basis
        if b_name == "ChebyshevT":
            # for reason and don't know this nethod only work with ChebyshevT
            q_coef, r = self._qr_builtin()

        else:
            q_coef, r = self._qr_builtin1()

        q = tau.Polynomial(q_coef.T, self.options)
        return q, r

    def _qr_builtin(self):
        aux = self.copy()
        n, m = aux.size, aux.n
        if m < n:
            aux = aux.extend(n)
            m = n
        co = aux.coeff
        # Compute the weighted QR factorization
        opt = self.options.copy()
        opt.domain = [-1, 1]

        b = tau.basis(tau.settings(basis="LegendreP"))
        x, wl, _, th = b.nodes(m)
        # Weighted QR with gauss legendre weighs.
        w = (wl) ** (1 / 2)
        # Undo the weights used for QR
        winv = (wl) ** (-1 / 2)

        converted = (tau.Polynomial(co, opt)(x) * w).T

        q, r = qr(converted)

        s = np.sign(np.diag(r))

        s[s == False] = 1

        q = (q.T * winv).T * s
        # if opt.basis == "GegenbauerC" and opt.alpha == 0.5:
        #     opt.basis = "LegendreP"

        di = {
            "LegendreP": b.idlt(q.T),
            "ChebyshevT": leg2chebt(b.idlt(q.T)),
            "GegenbauerC": ultra2ultra(b.idlt(q.T), opt.alpha, opt.alpha),
        }
        q = di.get(opt.basis, None)
        if q is None:
            raise TypeError(f"QR not yet implemented for basis {opt.basis}")
        # q = leg2chebt(b.interp_from_values(q.T, x, basis=b.name))

        r = (r.T * s).T
        rescale_factor = 0.5 * np.diff(self.domain)
        q = q * rescale_factor ** (-1 / 2)
        r = r * rescale_factor ** (1 / 2)

        return q.T, r

    def _qr_builtin1(self):
        """

        """
        aux = self.copy()
        n, m = aux.size, aux.n
        if m < n:
            aux = aux.extend(n)
            m = n
        co = aux.coeff

        # Compute the weighted QR factorization
        opt = self.options.copy()
        opt.domain = [-1, 1]
        b = tau.basis(opt)

        x, wl, *_ = b.nodes(m)
        # Weighted QR with gauss legendre weighs.
        w = (wl) ** (1 / 2)
        # Undo the weights used for QR
        winv = (wl) ** (-1 / 2)

        converted = (tau.Polynomial(co, opt)(x) * w).T

        q, r = qr(converted)

        s = np.sign(np.diag(r))

        s[s == False] = 1

        q = (q.T * winv).T * s
        # q = b.idlt(q.T)
        q = b.interp_from_values(q.T, x, opt)

        r = (r.T * s).T
        rescale_factor = 0.5 * np.diff(self.domain).item()
        q = q / rescale_factor ** (1 / 2)
        r = r * rescale_factor ** (1 / 2)

        return q.T, r

    def sample(self, n=None):
        if n is None:
            n = self.n

        return self(self.basis.nodes(n)[0])

    @staticmethod
    def randnPol(lam=1, n=1, options=None, trig=False, big=False):
        r"""
        returns a Polynomial with settings given by options with maximum
        frequency <= 2pi/lam and distribution N(0,1). Can be seen as the path
        of a standard caussian process

        Parameters
        ----------
        lam : scalar, optional
            the inverse frequency
        n : integer, optional
            the numbers of columns of the Polynomial
        options : settings, optional
            The settings for the Polynomial
        trig : bool, optional
            In this case  return the coefficients of a trigonometric series
            i.e. fourier series in the domain given by settings
        big : bool, optional
            Normalize the output by dividing it by (lam/2)**(1/2).
            The default is False.

        Returns
        -------
        Polynomial or, array_like

        """

        if options is None:
            options = tau.settings()
        elif isinstance(options, dict):
            options = tau.settings(**options)
        else:
            options = options.copy()

        if trig:
            L = np.diff(options.domain).item()
            m = (L / lam).__floor__()
            c = np.random.randn(2 * m + 1, 2 * n)
            ii = np.concatenate(
                (np.arange(2 * m, -1, -2), np.arange(1, 2 * m, 2))
            )
            c = c[ii, :].T
            c = (c[:n, :] + 1j * c[n : 2 * n, :]) * 2 ** (-1 / 2)
            c = (c + np.flip(c.conjugate(), axis=1)) * 2 ** (-1 / 2)

            if big:
                c = c * L ** (-1 / 2)
            else:
                c = c * (2 * m + 1) ** (-1 / 2)

            return c

        else:
            domain = options.domain
            dom = domain[0] + np.r_[0, 1.2 * np.diff(domain)]
            m = (np.diff(domain).item() / lam).__round__()
            new_opt = options.copy()
            new_opt.domain = dom
            c = tau.Polynomial.randnPol(
                lam=lam, n=n, options=new_opt, trig=True, big=big
            )
            b = tau.basis(options)
            x, *_ = b.nodes(5 * m + 20)
            v = b.trigeval(c, x, dom)
            p = tau.Polynomial(v, options=options, vals=True)

            return p.simplify(1e-13)

    def copy(self):
        return deepcopy(self)

    def flipud(self):
        r"""
        returns a Polynomial P_1 which is the original Polynomial P_0 flipped
        180 degrees in the domain [a,b] of P_0; i.e. P_1(x)=P_0(a+b-x). If P_0
        are row Polynomials, flip the order of the rows.

        Returns
        -------
        Polynomial
            as described above

        """
        if self.istranspose:
            return self.T.fliplr().T
        aux = self.copy()
        aux.coeff[..., 1::2] = -aux.coeff[..., 1::2]
        aux.exponents = np.flip(aux.exponents)
        return aux

    def fliplr(self):
        r"""
        returns a Polynomial P_1 which is the original Polynomial P_0 flipped
        180 degrees in the domain [a,b] of P_0, if P_0 are row Polynomials, i.e.
        P_1(x)=P_0(a+b-x). If P_0 are column Polynomials, flip the order of
        the columns.

        Returns
        -------
        Polynomial
            as described above

        """

        if self.istranspose:
            return self.T.flipud().T
        aux = self.copy()
        if aux.size > 1:
            aux.coeff = np.flip(aux.coeff, axis=0)
        return aux

    def flip(self, axis=None):
        r"""
        Flip acording with the axis 0 meaning flipud and 1 meaning fliplr.

        Parameters
        ----------
        axis : int, optional
            When axis is None flip acordind if the Polynomial are columns or
            rows . The default is None.

        Raises
        ------
        ValueError
            When axis is not in [None,0,1].

        Returns
        -------
        Polynomial
            As described above.

        """
        if axis not in [None, 0, 1]:
            raise ValueError(f"Axis must in [None,0,1] was given {axis}")
        if self.istranspose:
            if axis is None or axis == 1:
                return self.fliplr()
            return self.flipud()
        else:
            if axis is None or axis == 0:
                return self.flipud()
            return self.fliplr()

    # Aliases
    # from here to below will add aliases to some properties and methods

    # concat is an alias to append
    concat = append

    # sum is an alias to definite_integral
    def sum(self, bounds=None, axis=None):
        r"""

        Parameters
        ----------
        bounds : , optional
            A tuple with the limits of integration, when not given we assume
            the boundaries  of the domain. The default is None.
        axis : int, optional
            Either 0 or 1 when not given simple compute the defini-
            te integral over the domain. If self is a polynomial in columns
            axis=0 compute the definite integral over ``bounds`` and axis=1
            compute a polynomial with one columns whose coefficients are the sum
            of the array of coefficients in the axis=1.
            When self is a polynomial in rows it is the opposite.

        Raises
        ------
        ValueError
            When the axis is neither 0 nor 1 or when the axis do not match co-
            lumns/rows

        Returns
        -------
        Polynomial,number, or ndarray
            The definite integral or a Polynomial depending on the axis.

        """
        if axis is None:
            return self.definite_integral(bounds)

        if self.istranspose:
            if axis == 1:
                return self.T.sum(bounds)
            elif axis == 0:
                return self.T.sum(bounds, axis=1).T
            else:
                raise ValueError(
                    f"Axis can be only 0, 1 or None was Given {axis}."
                )

        if axis == 1:
            if bounds is not None:
                raise ValueError("Cannot integrate over this axis")
            aux = self.copy()
            if aux.coeff.ndim > 1:
                aux.coeff = aux.coeff.sum(axis=0)
            return aux
        elif axis == 0:
            return self.definite_integral(bounds)

        else:
            raise ValueError(
                f"Axis can be only 0, 1 or None was Given {axis}."
            )

    @property
    def coef(self):
        """
        Ges the coefficients of a tau.Polynomial

        Parameters
        ----------
        glob :  optional

        Returns
        -------

        """
        return self.coeff

    coeffs = coef
