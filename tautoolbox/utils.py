# Copyright (C) 2021, University of Porto and Tau Toolbox developers.
#
# This file is part of Tautoolbox package.
#
# Tautoolbox is free software: you can redistribute it and/or modify it
# under the terms of version 3 of the GNU Lesser General Public License as
# published by the Free Software Foundation.
#
# Tau Toolbox is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser
# General Public License for more details.

import numpy as np
from numbers import Number
import enum
from difflib import get_close_matches
import matplotlib.pyplot as plt
from collections.abc import Iterable

opList = enum.Enum(
    "oplist",
    "m multiplication n differentiation \
                   o integration c condition d operator t taumatrix \
                       r tauresidual b taurhs v orth2pow w pow2orth",
)


def quadprecision(arr):
    """
    quadprecision - returns a quadruple precision version of the input argument

    input (required):
       x          = argument to convert

    output:
       xp          = argument in quadruple precision

    """
    if isinstance(arr, np.ndarray):
        return np.array(arr, dtype=np.float128)
    else:
        return np.float128(arr)


def chooseOperator(op):

    """
    chooseOperator - returns a cell list of operators where op fits

    input (required):
       op         = operator (string)

    output:
       opSel      = cell arrays of candidates that satisfy condition

    """

    op = op.lower()

    # test exact match
    if op in opList.__members__:
        return op

    # test approximate match
    opSel = get_close_matches(op, opList.__members__, len(op))

    if len(opSel) == 1:
        return opSel[0]

    if len(opSel) == 0:
        raise ValueError(
            "the 1st input argument must be a prescribed "
            "operation ( see help(tau.matrix))"
        )

    raise ValueError(
        f'operator name "{op}" is anbiguous possible matches are: '
        f'{ ", ".join(opList.__members__)}'
    )


def numclass(x):
    """
    numclass - returns the type of numeric class

    input (required):
       x          = argument to determine the class type

    output:
       cls        = type of numeric class: vector or matrix

    """

    if not isinstance(x, np.ndarray):
        raise ValueError("the imput must be of numpy.ndarray type")

    return "vector" if x.ndim == 1 else "matrix"


def get_shape(a):
    """
    Returns the shape of `a`, if `a` has a regular array-like shape.

    Otherwise returns None.
    """

    if isinstance(a, Number):
        return ()

    if not isinstance(a, Iterable) or isinstance(a, str):
        return
    shapes = [get_shape(item) for item in a]
    if len(shapes) == 0:
        return (0,)
    if any([shape is None for shape in shapes]):
        return None
    if not all([shapes[0] == shape for shape in shapes[1:]]):
        return None
    return (len(shapes),) + shapes[0]


def isMatrixOrVectorLike(obj):
    """
    Check if an object is array_like: only returns true if obj is an np.ndarray,
    a list/tuple with all elements being as numbers, or a list/tuple of list/tuple
    with all sublists/subtuples having the same length and with all elements numbers.
    Otherwise returns false.


    Parameters
    ----------
    obj : any


    Returns
    -------
    bool
        returns true if obj is arraylike otherwise false.

    Examples
    --------
    a is a list with all elemnts being numbers:

    >>> a = [1, 2]

    >>> tau.utils.isMatrixOrVectorLike(a)
    True

    a is a list of lists/tuples with all sublists/subtuples having the same
    length with elements being numbers

    >>> a = [[1, 2],[1, 2]]

    >>> tau.utils.isMatrixOrVectorLike(a)
    True

    a is a a tuple of lists/tuples with all sublists/subtuples having the same
    length with elements being numbers:

    >>> a = ([1, 2],[1, 2])

    >>> tau.utils.isMatrixOrVectorLike(a)
    True

    a is a a tuple of lists/tuples with all sublists/subtuples having the same
    length with elements being numbers:

    >>> a = ([1, 2],(1, 2))

    >>> tau.utils.isMatrixOrVectorLike(a)
    True

    a is a  tuple of lists/tuples all sublists/subtuples are sublists/subtuples
    of numbers but not all sublists/subtuples have the same length:

    >>> a=([1,1],[1,2,3])

    >>> tau.utils.isMatrixOrVectorLike(a)
    False
    """
    if isinstance(obj, np.ndarray):
        return True
    if isinstance(obj, (list, tuple)):
        if all([isinstance(el, numbers.Number) for el in obj]):
            return True
        if all([isinstance(el, (list, tuple)) for el in obj]):
            len_ref = len(obj[0])
            for el in obj:
                if len(el) != len_ref:
                    return False

                if not all([isinstance(i, numbers.Number) for i in el]):
                    return False
            return True
        return False
    return False


def spy(T):

    m, n = T.shape
    nnz = np.count_nonzero(T)
    d = 0.1
    fig = plt.figure()
    ax = fig.gca(projection="3d")
    # ax = Axes3D(fig)
    for i in range(m):
        for j in range(n):
            if not T[i, j] == 0:

                ax.plot_surface(
                    np.array(
                        [
                            [j - 0.5 + d, j + 0.5 - d],
                            [j - 0.5 + d, j + 0.5 - d],
                        ]
                    ),
                    np.array(
                        [
                            [i - 0.5 + d, i - 0.5 + d],
                            [i + 0.5 - d, i + 0.5 - d],
                        ]
                    ),
                    np.array([[T[i, j], T[i, j]], [T[i, j], T[i, j]]]),
                    shade=True,
                )

                # ax.bar3d(j+1,i+1, 0, d,d,T[i,j], color='b')
    # ax.xaxis.set_major_locator(MultipleLocator(1))
    # ax.xaxis.set_minor_locator(MultipleLocator(d))
    plt.xlabel(f"nz = {nnz}")
    # plt.xlim((1-d,m +d))
    # plt.xticks(range(1,m+1))
    # plt.ylim((1-d,n +d))
    # plt.yticks(range(1,n+1))
    # plt.colorbar(ax)
    ax.colorbar(shrink=0.5, aspect=5)
    ax.view_init(azim=0, elev=90)
    plt.show()
    print("nilo")


if __name__ == "__main__":
    x = np.random.randn(20, 20)
    x[5, :] = 0.0
    x[:, 12] = 0.0
    Lx, Ly = x.shape
    plt.matshow(x, cmap="PiYG", origin="lower", extent=[0, Lx, 0, Ly])
    plt.colorbar(orientation="vertical")
