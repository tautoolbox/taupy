# Copyright (C) 2021, University of Porto and Tau Toolbox developers.
#
# This file is part of Tautoolbox package.
#
# Tautoolbox is free software: you can redistribute it and/or modify it
# under the terms of version 3 of the GNU Lesser General Public License as
# published by the Free Software Foundation.
#
# Tau Toolbox is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser
# General Public License for more details.

import tautoolbox as tau
import numpy as np
import warnings
from inspect import signature, Parameter


class problem:
    validkinds = "auto eig ide ode".split()

    def __init__(self, equation, domain=None, conditions=None, options=None):
        self.options = tau.settings() if options is None else options.copy()
        self.domain = self.options.domain = (
            [-1, 1] if domain is None else domain
        )

        # process equation
        if callable(equation) or isinstance(equation, str):
            equation = [equation]

        if len(equation) == 2 and all([callable(form) for form in equation]):
            if any(
                [isinstance(form, tau.Polynomial) for form in equation]
            ) or any(
                [len(form.__code__.co_varnames) == 1 for form in equation]
            ):
                equation = [equation]

        self.options.nequations = self.nequations = len(equation)

        hasIntegral = 0
        isEig = np.zeros(self.nequations)

        self.derivOrder = np.zeros(
            (self.nequations, self.nequations), dtype=int
        )
        self.height = [None] * self.nequations
        self.equations = [None] * self.nequations

        self.isLinearized = False

        for k in range(self.nequations):
            self.equations[k] = tau.equation(equation[k])

            # alterei para resolver uma equação
            # if len(self.equations[k].lhs.__code__.co_varnames) = 3: horiginalmente
            # substituted by if the number of empty variables == 3
            # empty variables are variables with non default arguments
            if (
                len(
                    [
                        v.default
                        for k, v in signature(
                            self.equations[k].lhs
                        ).parameters.items()
                        if v.default == Parameter.empty
                    ]
                )
                == 3
            ):
                self.isLinearized = True

            info = self.equations[k].info(self.options)
            if info.get("isfred1", False):
                self.kind = "fred1"
                self.kernel = info["kernel"]
                self.rhs = tau.Polynomial(
                    self.equations[0].rhs, self.kernel.optx
                )
                self.conditions = None
                return

            self.height[k] = info["height"]
            self.derivOrder[k, :] = info["derivOrder"]
            isEig[k] = info["isEig"]
            hasIntegral = hasIntegral or info["hasIntegral"]

        # Process conditions

        if callable(conditions):
            conditions = conditions(tau.condition(options=self.options))

        if isinstance(conditions, (str, tau.condition)) or conditions is None:
            conditions = [conditions]

        self.nconditions = len(conditions)
        self.conditions = [None] * self.nconditions
        self.isGIVP = True

        for k in range(self.nconditions):
            self.conditions[k] = tau.condition(conditions[k], self.options)
            if any(self.conditions[k].point < self.domain[0]) or any(
                self.conditions[k].point > self.domain[1]
            ):
                raise ValueError(
                    f"taupy: error in condiction {k} a point "
                    "is outside the domain of integration"
                )
            if not self.conditions[k].isGIVP(self.domain):
                self.isGIVP = False

        if self.options.kind == "auto":
            if any(isEig):
                self.kind = "eig"
            elif hasIntegral:
                self.kind = "ide"
            else:
                self.kind = "ode"
        else:
            self.kind = self.options.kind

        # Validate problem components
        # If possible recover issuing a warning, if not issue an error
        if self.derivOrder.max(0).sum() != self.nconditions:
            warnings.warn(
                "Tau Toolbox: the Number of conditions does"
                " not fit with order of equations"
            )
        # Assign the number of conditions to be distributed by each equation

        self.condpart = self.derivOrder.max(1)

        # 2) Validate the type of problem
        # If possible recover issuing a warning, if not issue an error

        if self.kind == "eig":
            # Currently eigenproblem only works for 1 equation
            if self.nequations > 1:
                raise ValueError(
                    "tautoolbox: not yet prepared for differential eigenvalues problem"
                )

    def __str__(self):
        if self.kind == "fred1":
            st = (
                "Fredholm integral equation of the first kind:\n\n"
                "kernel:\n"
                f"{self.kernel}\n\n"
                "rhs:\n"
                f"{self.rhs}"
            )
            return st

        st = f"Tau problem of type {self.kind}:\n"
        st += f" * Integration domain: {self.domain}\n"
        if self.nequations == 1:
            st += " * 1 equation:\n"
            st += f"   lhs:  {self.equations[0]._lhs_st}\n\n"
            st += f"   rhs:  {self.equations[0]._rhs_st}\n\n"
        else:
            st += f"System of {self.nequations}:\n"
            for k in range(self.nequations):
                st += f"   Equation #{k}:\n"
                st += f"     lhs:  {self.equations[k]._lhs_st}\n\n"
                st += f"     rhs:  {self.equations[k]._rhs_st}\n\n"
        if self.nconditions == 0:
            st += " * No condition/constraints\n"
        elif self.nconditions == 1:
            st += " * 1 condition/constraints\n"
            st += f"      {self.conditions[0]}"
        else:
            st += f" * {self.nconditions} conditions\\constraints\n"
            for k in range(self.nconditions):
                st += f"    #{k}: {self.conditions[k]}\n\n"
        return st

    def __repr__(self):
        return str(self)

    @staticmethod
    def isKind(kind):
        return kind in tau.problem.validkinds
