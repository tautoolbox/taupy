# Copyright (C) 2021, University of Porto and Tau Toolbox developers.
#
# This file is part of Tautoolbox package.
#
# Tautoolbox is free software: you can redistribute it and/or modify it
# under the terms of version 3 of the GNU Lesser General Public License as
# published by the Free Software Foundation.
#
# Tau Toolbox is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser
# General Public License for more details.

import numpy
from functools import singledispatch
from tautoolbox.polynomial import Polynomial

# define functions that we want to export
__all__ = [
    "cosh",
    "cos",
    "diff",
    "exp",
    "log",
    "fred",
    "integral",
    "linspace",
    "polyval",
    "sinh",
    "sin",
    "volt",
]


# Functions shared between Polynomial and numpy.array
@singledispatch
def cos(x):
    return numpy.cos(x)


@cos.register(Polynomial)
def _(x):
    return x.cos()


@singledispatch
def cosh(x):
    return numpy.cosh(x)


@cosh.register(Polynomial)
def _(x):
    return x.cosh()


@singledispatch
def exp(x):
    return numpy.exp(x)


@exp.register(Polynomial)
def _(x):
    return x.exp()


@singledispatch
def sin(x):
    return numpy.sin(x)


@sin.register(Polynomial)
def _(x):
    return x.sin()


@singledispatch
def sinh(x):
    return numpy.sinh(x)


@sinh.register(Polynomial)
def _(x):
    return x.sinh()


# Integro-differential operator/functions
@singledispatch
def diff(y, *a, **b):
    return y.diff(*a, **b)


@diff.register(numpy.ndarray)
def _(y, *a, **b):
    return numpy.diff(y, *a, **b)


# @diff.register(condition.condition)
# def _(y, *n):
#     print(*n)
#     return y.diff(*n)


@singledispatch
def integral(y, *a):
    return y.integral(*a)


@singledispatch
def fred(y, *a, **b):
    return y.fred(*a, **b)


@singledispatch
def fred1(y, *a, **b):
    return y.fred(*a, **b)


@singledispatch
def volt(y, *a, **b):
    return y.volt(*a, **b)


@singledispatch
def polyval(obj, x):
    return obj.polyval(x)


@singledispatch
def linspace(
    start, stop, num=100, endpoint=True, retstep=False, dtype=None, axis=0
):
    return numpy.linspace(start, stop, num, endpoint, retstep, dtype, axis)


@linspace.register(Polynomial)
def _(y, n=100):
    return y.linspace(n)


@singledispatch
def log(x):
    return numpy.log(x)


@log.register(Polynomial)
def _(x):
    return x.log()
