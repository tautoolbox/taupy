# Copyright (C) 2021, University of Porto and Tau Toolbox developers.
#
# This file is part of Tautoolbox package.
#
# Tautoolbox is free software: you can redistribute it and/or modify it
# under the terms of version 3 of the GNU Lesser General Public License as
# published by the Free Software Foundation.
#
# Tau Toolbox is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser
# General Public License for more details.

import numpy as np
from numbers import Number
from copy import deepcopy
from tautoolbox.auxiliary import *
import tautoolbox as tau
from numpy.linalg import svd
from matplotlib import pyplot as plt
from warnings import warn
from collections.abc import Iterable
from scipy.sparse import spdiags
from tautoolbox.utils import get_shape
from scipy.optimize import fminbound
import warnings

warnings.filterwarnings("error", category=np.exceptions.VisibleDeprecationWarning)
np.seterr(divide="ignore")

class Polynomial2:
    r"""
    A class used to represent a polynomial in 2D. Implemented using
    Low rank approximation of bivariate functions in the vein of Chebfun.
    Reference: Townsend and L. N. Trefethen, "An extension of Chebfun to two 
    dimensions", SIAM Journal on Scientific Computing, 35 (2013), C495-C518.

    Attributes
    ----------
    coef : array_like
        A bidimensional array like representing the coefficients of the poly-
        nomial in the given bases and domain
    bases : list or tutle
        A list with the names of the basis for each independent variable must
        have length 2. The default is ["ChebyshevT"] * 2.
    domain : array_like, optional
        A 2 by2 array_like object where ach row is the domain of an independent
        variable in the order given by bases. The default is [[-1, 1]] * 2.

    Notes
    -----
    This is the form of representing a Polynomial in a Polynomial in two
    dimensios when each variable are represented in one orthogonal basis.
    The basis for each variable can be the same of different. Suppose we
    have two bases of lentgh :math:`m+1` and :math:`n+1` respectively
    :math:`\mathcal{P_m}(x)=[P_0(x),\ \dots,\ P_m(x)],\ x \in [a,b]` and
    :math:`\mathcal{Q_n}(y) =[Q_0(y),\ \dots,\ Q_n(y)]\ y\in [c,d]`,
    then a Polynomial can be be writed in the those bases as:

    .. math::
        \Xi (x,y) =a_{0,0}P_0(x)Q_0 + a_{0,1}P_1Q_0 + ... + a_{n,m}P_mQ_n.
        \quad (x,y) \in [a,b]\times [c,d]

    Therefore the coefficients are represented by :math:`[a_{i,j}]_{n\times m}`



    """

    __array_priority__ = 1000
    cols = None
    rows = None
    pivots = None
    __vscale = None

    def __init__(
        self,
        coef=None,
        options=None,
        **kwargs,
    ):
        r"""


        The constructor. When ``coef`` is callable and have no Polynomial
        structure it uses interpolation to construct it

        Parameters
        ----------
        coef : array_like, scalar or callable
            Can be the coefficients of a Polynomial, an anonymous function
            to approximate by a Polynomial or a scalar representing a constant
            function.
        bases : Iterable
            An iterable with two string elemnts representing the basis
            for each variable. The Default is ["ChebyshevT"] * 2.
        domain : array_like, optional
            Each row is the domain of an independent variable in the order
            given by bases. The default is [[-1, 1]] * 2.

        Other Parameters
        ----------------
        method : str
            The method to use for interpolation when needed. If not specified
            the default is 'aca'.
        grid_shape : int or iterable
            Used to specify a fixed grid dimension. When `int` means a square
            grid of dimension nxn. When not specified we assumes an adaptive
            grid
        Raises
        ------
        ValueError
            When ``coef`` is an array with dimension not equal to 2, when do-
            main is not a 2x2 array_like object, when bases is not a length 2
            ierable of strings.

        TypeError
            When ``coef`` is a ragged nested sequence, or when ``coef`` is not
            a:  scalar, array_like, callable.

        Returns
        -------
        None.

        Notes
        -----
        This is the form of representing a Polynomial in a Polynomial in two
        dimensios when each variable are represented in one orthogonal basis.
        The basis for each variable can be the same of different. Suppose we
        have two bases of lentgh :math:`m` and :math:`n` respectively
        :math:`\mathcal{P_m}(x)=[P_0(x),\ \dots,\ P_m(x)],\ x \in [a,b]` and
        :math:`\mathcal{Q_n}(y) =[Q_0(y),\ \dots,\ Q_n(y)]\ y\in [c,d]`,
        then a Polynomial can be be writed in the those bases as:

        .. math::
            \Xi (x,y) =a_{0,0}P_0(x)Q_0 + a_{0,1}P_1Q_0 + ... + a_{n,m}P_mQ_n.
            \quad (x,y) \in [a,b]\times [c,d]

        Therefore the coefficients are represented by :math:`[a_{i,j}]_{n\times m}`

        """
        # Returns an empty Polynomial2
        if coef is None:
            return

        # If coef is a Polynomial2D the the new Polynomial2 is a copy of coef
        if isinstance(coef, Polynomial2):
            self.__dict__ = coef.__dict__.copy()
            return

        # Processing  x and y options
        if options is None:
            optx = tau.settings()
            opty = tau.settings()
        elif isinstance(options, tau.settings):
            optx = options.copy()
            opty = options.copy()
        elif isinstance(options, dict):
            optx = tau.settings(**options)
            opty = optx.copy()
        elif isinstance(options, Iterable):
            if options[0] is None:
                optx = tau.settings()
            elif isinstance(options[0], tau.settings):
                optx = options[0].copy()
            elif isinstance(options[0], dict):
                optx = tau.settings(**options[0])
            else:
                raise TypeError(
                    "The first element of options must be a settings or s dict"
                )
            if options[1] is None:
                opty = tau.settings()

            elif isinstance(options[1], tau.settings):
                opty = options[1].copy()
            elif isinstance(options[1], dict):
                opty = tau.settings(**options[1])
            else:
                raise TypeError(
                    "The second element of options must be a settings or s dict"
                )
        else:
            raise TypeError("Unrecognizable options")

        self.optx = optx
        self.opty = opty

        # Parse coef
        # When coef is array like
        coef_shape = get_shape(coef)
        if coef_shape is not None:
            if coef_shape == ():
                coef = np.array([[coef]])
            elif len(coef_shape) == 1:
                coef = np.array(coef)[np.newaxis]
            elif len(coef_shape) == 2:
                if not isinstance(coef, np.ndarray):
                    coef = np.array(coef)
            elif len(coef_shape) > 2:
                raise ValueError(
                    "When the coefficients must be a dimension 2 array_like sequence"
                )

        if isinstance(coef, np.ndarray):
            self.cols, self.diag, self.rows = self.set_coef(coef)
        elif callable(coef):
            # The default way of creating a Polynomial2 from a callable

            if kwargs == {}:
                # For the case coef is an anonimous fucntion representing
                # a by-dimensional Polynomial
                try:
                    x = Polynomial2(
                        tau.Polynomial(options=optx).coeff.reshape(1, -1),
                        options=(optx, opty),
                    )
                    y = Polynomial2(
                        tau.Polynomial(options=opty).coeff.reshape(-1, 1),
                        options=(optx, opty),
                    )

                    self.__dict__ = coef(x, y).__dict__
                    return
                except:
                    # If the above way fail  use a low rank approximation
                    # The default method is  'aca' adaptive

                    self.__dict__ = tau.Polynomial2.interp2d(
                        coef, optx, opty
                    ).__dict__

            else:
                (
                    self.cols,
                    self.diag,
                    self.rows,
                    self.pivot_locations,
                    self.__vscale,
                ) = tau.Polynomial2.interp2d(coef, optx, opty, **kwargs)

        else:
            raise TypeError(
                "Coef must be a number, a two varriable callable, a Polynomial2"
                " object or a bydiemnsional array_like object representing the coefficients"
            )

        self._bas = [
            tau.basis(self.rows.options),
            tau.basis(self.cols.options),
        ]

    @property
    def domain(self):
        if self.cols is not None and self.rows is not None:
            return np.array([self.rows.domain, self.cols.domain])

    @property
    def bases(self):
        if self.cols is not None and self.rows is not None:
            return [self.rows.options.basis, self.cols.options.basis]

    @property
    def alpha(self):
        if self.cols is not None and self.rows is not None:
            return [
                o.alpha if o.basis == "GegenbauerC" else None
                for o in [self.rows.options, self.cols.options]
            ]

    @property
    def coef(self):
        if not self.isempty():
            aux = (self.cols.coeff.T * self.diag) @ self.rows.coeff
            aux[np.abs(aux) < 1e-15] = 0
            return aux

    def set_coef(self, co):
        u, s, vh = np.linalg.svd(co)

        rk = np.sum(s > 1e-15)
        if rk == 0:
            rk += 1

        cols = tau.Polynomial(u[:, :rk].T, self.opty)
        rows = tau.Polynomial(vh[:rk, :], self.optx)
        diag = s[:rk]
        return cols.trim(1e-15), diag, rows.trim(1e-15)

    def isempty(self):
        return self.rows is None or self.cols is None

    def __call__(self, x, y):
        return self.polyval(x, y)

    def copy(self):
        return deepcopy(self)

    @property
    def T(self):
        aux = self.copy()
        aux.cols = self.rows.copy()
        aux.rows = self.cols.copy()
        aux.optx = self.opty.copy()
        aux.opty = self.optx.copy()
        return aux

    def __repr__(self):
        if self.isempty():
            return "empty Polynomial2"

        dom = self.domain.round(2).tolist()
        alpha = self.alpha
        vscale = self.vscale

        cv = (
            self(*np.meshgrid(self.domain[0], self.domain[1]))
            .round(2)
            .flatten()
            .tolist()
        )

        st = (
            "Polynomial2 object:\n"
            f"  bases          :  {self.bases}\n"
            f"  domain         :  {dom[0]} x {dom[1]}\n"
        )

        if any(self.alpha):
            st += f"  alpha          :  {alpha}\n"
        st += (
            f"  rank           :  {len(self)}\n"
            f"  shape          :  {self.cols.n} x {self.rows.n}\n"
            f"  corner values  :  {cv}\n"
            f"  vertical scale :  {vscale:.2f}"
        )

        return st

    @staticmethod
    def sum_coef(c1, c2):
        aux1 = c1.copy()
        aux2 = c2.copy()
        aux1_shape, aux2_shape = aux1.shape, aux2.shape
        coef = np.zeros(np.maximum(aux1_shape, aux2_shape))
        coef[: aux1_shape[0], : aux1_shape[1]] = aux1
        coef[: aux2_shape[0], : aux2_shape[1]] += aux2
        return coef

    def __add__(self, rhs):
        aux = self.copy()
        aux.__vscale = None
        # In the case self is an empty Polynomial2
        if aux.isempty():
            return rhs.copy()

        # For the case the sum is a Polynomial and a scalar
        if isinstance(rhs, Number):
            coef = aux.coef
            coef[0, 0] += rhs
            aux.cols, aux.diag, aux.rows = aux.set_coef(coef)
        # The case of two Polynomial2
        elif isinstance(rhs, Polynomial2):
            # Check if the Polynomials2d  has the same bases and domain
            if aux.optx != rhs.optx or aux.opty != rhs.opty:
                raise ValueError("The Polynomials2 must have the same options")
            coef = tau.Polynomial2.sum_coef(aux.coef, rhs.coef)

            aux.cols, aux.diag, aux.rows = aux.set_coef(coef)

        # In the case rhs is a Polynomial2 object check if they have the same
        # bases and domain

        elif isinstance(rhs, tau.Operator2):
            return rhs + self
        else:
            raise TypeError(
                "You can only operate a Polynomial2 with a "
                "Number or a Polynomial2"
            )
        return aux

    def __radd__(self, lhs):
        return self + lhs

    def __pos__(self):
        return self.copy()

    def __neg__(self):
        aux = self.copy()
        aux.cols = -aux.cols
        return aux

    def __sub__(self, rhs):
        return -(rhs + (-self))

    def __rsub__(self, lhs):
        return -self + lhs

    # def __mul__(self, rhs):
    #     aux = self.copy()

    #     # In the case self is an empty Polynomial2
    #     if aux.isempty():
    #         return rhs.copy()

    #     # For the case the sum is a Polynomial and a scalar
    #     if isinstance(rhs, Number):
    #         aux.coef = aux.diag * rhs

    #     # The case of two Polynomial2
    #     elif isinstance(rhs, Polynomial2):

    #         # Check if the Polynomials2d  has the same bases and domain
    #         if (aux.domain != rhs.domain).any() or aux.bases != rhs.bases:
    #             raise ValueError(
    #                 "The corresponding variable in each polynomi"
    #                 "als must have the same basis and domain "
    #             )
    #         aux1, aux2 = aux.coef, rhs.coef
    #         aux.coef = Polynomial2.product(aux1, aux2, self.bases, self.domain)
    #     return aux

    # def __mul__(self, rhs):
    #     # In the case self is an empty Polynomial2
    #     if self.isempty():
    #         return rhs.copy()
    #     if isinstance(rhs, tau.Operator2):
    #         return rhs * self

    #     # For the case the sum is a Polynomial and a scalar
    #     if isinstance(rhs, Number):
    #         if rhs == 0:
    #             return tau.Polynomial2(0, (self.optx, self.opty))
    #         aux = self.copy()
    #         aux.diag = aux.diag * rhs

    #     # The case of two Polynomial2
    #     elif isinstance(rhs, Polynomial2):
    #         # Check if the Polynomials2d  has the same bases and domain
    #         if [self.optx, self.opty] != [rhs.optx, rhs.opty]:
    #             raise ValueError(
    #                 "The corresponding variable in each polynomi nust have"
    #                 " the same options."
    #             )
    #         if self.iszero():
    #             return self.copy()
    #         elif rhs.iszero():
    #             return rhs.copy()

    #         if len(self) == 1:
    #             cols, diag, rows = self.cols, self.diag, self.rows
    #             aux = rhs.copy()
    #             cols_ones = np.sqrt(np.abs(diag)) * np.ones(len(aux))
    #             rows_ones = np.sign(diag) * cols_ones
    #             p_cols = tau.Polynomial((cols.coef.T * cols_ones).T, self.opty)

    #             aux.cols = p_cols * rhs.cols

    #             p_rows = tau.Polynomial((rows.coef.T * rows_ones).T, self.optx)

    #             aux.rows = p_rows * rhs.rows

    #         elif len(rhs) == 1:
    #             return rhs * self
    #         else:
    #             aux = Polynomial2(
    #                 lambda x, y: self(x, y) * rhs(x, y),
    #                 options=(self.optx, self.opty),
    #             )
    #     elif isinstance(rhs, tau.Polynomial):
    #         if self.optx != rhs.options:
    #             raise TypeError(
    #                 "Polynomial2 options in the x direction must be "
    #                 "the same of the Polynomial options"
    #             )

    #         aux = self.copy()
    #         aux.rows = aux.rows * rhs
    #         return aux

    #     else:
    #         raise TypeError(
    #             "You can only multiply a Polynomial2 and a Number "
    #             "or two Polynomial2"
    #         )

    #     return aux

    # def __mul__(self, rhs):
    #     # In the case self is an empty Polynomial2
    #     if self.isempty():
    #         return rhs.copy()
    #     if isinstance(rhs, tau.Operator2):
    #         return rhs * self

    #     # For the case the sum is a Polynomial and a scalar
    #     if isinstance(rhs, Number):
    #         if rhs == 0:
    #             return tau.Polynomial2(0, (self.optx, self.opty))
    #         aux = self.copy()
    #         aux.diag = aux.diag * rhs

    #     # The case of two Polynomial2
    #     elif isinstance(rhs, Polynomial2):
    #         # Check if the Polynomials2d  has the same bases and domain
    #         if [self.optx, self.opty] != [rhs.optx, rhs.opty]:
    #             raise ValueError(
    #                 "The corresponding variable in each polynomi nust have"
    #                 " the same options."
    #             )
    #         if self.iszero():
    #             return self.copy()
    #         elif rhs.iszero():
    #             return rhs.copy()

    #         if len(self) <= len(rhs):
    #             cols, diag, rows = self.cols, self.diag, self.rows
    #             aux = rhs.copy()
    #             col_el = np.sqrt(np.abs(diag[0]))
    #             row_el = np.sign(diag[0]) * col_el
    #             p_cols = tau.Polynomial(cols.coeff[0, :] * col_el, self.opty)

    #             aux.cols = p_cols * rhs.cols

    #             p_rows = tau.Polynomial(rows.coeff[0, :] * row_el, self.optx)

    #             aux.rows = p_rows * rhs.rows
    #             aux.__vscale = None

    #             for i in range(1, len(diag)):
    #                 aux2 = rhs.copy()
    #                 col_el = np.sqrt(np.abs(diag[i]))
    #                 row_el = np.sign(diag[i]) * col_el
    #                 p_cols = tau.Polynomial(
    #                     cols.coeff[i, :] * col_el, self.opty
    #                 )

    #                 aux2.cols = p_cols * rhs.cols

    #                 p_rows = tau.Polynomial(
    #                     rows.coeff[i, :] * row_el, self.optx
    #                 )
    #                 aux2.rows = p_rows * rhs.rows

    #                 aux = aux + aux2

    #         else:
    #             return rhs * self
    #     elif isinstance(rhs, tau.Polynomial):
    #         if self.optx != rhs.options:
    #             raise TypeError(
    #                 "Polynomial2 options in the x direction must be "
    #                 "the same of the Polynomial options"
    #             )

    #         aux = self.copy()
    #         aux.rows = aux.rows * rhs
    #         return aux

    #     else:
    #         raise TypeError(
    #             "You can only multiply a Polynomial2 and a Number "
    #             "or two Polynomial2"
    #         )
    #     return aux

    def __mul__(self, rhs):
        r"""
        Parameters
        ----------
        rhs : Number, Polynomial,Operator2 or Polynomial2.
            The right hand side of the operator.

        Raises
        ------
        ValueError
            When rhs is a Polynomial2 and its options are not equal  to
            self options or when rhs is a Polynomial and its options is not
            equal to the options of self in the x direction.
        TypeError
            When the rhs is None of the above types

        Returns
        -------
        Polynomial2 or Operator2
            DESCRIPTION.

        """
        # In the case self is an empty Polynomial2
        if self.isempty():
            return rhs.copy()
        if isinstance(rhs, tau.Operator2):
            return rhs * self

        # For the case the sum is a Polynomial and a scalar
        if isinstance(rhs, Number):
            if rhs == 0:
                return tau.Polynomial2(0, (self.optx, self.opty))
            aux = self.copy()
            aux.diag = aux.diag * rhs

        # The case of two Polynomial2
        elif isinstance(rhs, Polynomial2):
            # Check if the Polynomials2d  has the same bases and domain
            if [self.optx, self.opty] != [rhs.optx, rhs.opty]:
                raise ValueError(
                    "The corresponding variable in each polynomial nust have"
                    " the same options."
                )
            if self.iszero():
                return self.copy()
            elif rhs.iszero():
                return rhs.copy()

            if len(self) <= len(rhs):
                bx, by = tau.basis(self.optx), tau.basis(self.opty)
                cols, diag, rows = self.cols, self.diag, self.rows
                aux = rhs.copy()
                col_el = np.sqrt(np.abs(diag[0]))
                row_el = np.sign(diag[0]) * col_el
                co = by.productv2(cols.coeff[0, :] * col_el, rhs.cols.coeff)
                ro = bx.productv2(rows.coeff[0, :] * row_el, rhs.rows.coeff)
                coef = (co.T * rhs.diag) @ ro

                for i in range(1, len(diag)):
                    col_el = np.sqrt(np.abs(diag[i]))
                    row_el = np.sign(diag[i]) * col_el

                    co = by.productv2(cols.coeff[i, :] * col_el, rhs.cols.coeff)

                    ro = bx.productv2(rows.coeff[i, :] * row_el, rhs.rows.coeff)

                    coef = coef + (co.T * rhs.diag) @ ro

                aux.cols, aux.diag, aux.rows = aux.set_coef(coef)

            else:
                return rhs * self
        elif isinstance(rhs, tau.Polynomial):
            if self.optx != rhs.options:
                raise ValueError(
                    "Polynomial2 options in the x direction must be "
                    "the same of the Polynomial options"
                )

            aux = self.copy()
            aux.rows = aux.rows * rhs
            return aux

        else:
            raise TypeError(
                "You can only multiply a Polynomial2 and a Number "
                "or two Polynomial2"
            )
        return aux

    def __rmul__(self, lhs):
        r"""
        When self is in the right hand side of the operator

        Parameters
        ----------
        lhs : Number, Polynomial,Operator2 or Polynomial2
            The left hand side of the operator

        Returns
        -------
        TYPE
            Polynomial2 or Operator2

        """

        return self * lhs

    def __pow__(self, n):
        r"""
        Compute the power of ``self``with exponent ``n``.
        Parameters
        ----------
        n : Integer
            an integer exponent of ``self``

        Raises
        ------
        ValueError
            When ``n`` is not an integer.

        Returns
        -------
        Polynomial2
            A polynomial2 which is self**n

        """
        if self.iszero():
            return self.copy()
        if n < 0 or n != int(n):
            raise ValueError(
                "Polynomial2: n must be a non-negative integer"
                f"but was given {n}"
            )
        if n == 0:
            return Polynomial2([[1]], self.optx, self.opty)

        aux = self.copy()
        if n == 1:
            return aux

        for i in range(1, n):
            aux *= self

        return aux

    def polyval(self, x, y):
        r"""
        Evaluate the Polynomial in the abcissas ``x`` and ordinates ``y``

        Parameters
        ----------
        x : Number or array_Like
            The the value of the first variable. Can be a Number or an
            array_like structure of numbers.
        y : Number or array_Like
            The the value of the first variable. Can be a Number or an
            array_like structure of numbers

        Returns
        -------
        Number or array_like
            The value or values of the Polynomials in the point or points x, y

        Examples
        --------
        >>> a=np.arange(9).reshape(3,3)
        >>> v=Polynomial2(a)
        >>> v.polyval(a,a)
        array([[0.00000e+00, 3.60000e+01, 6.40000e+02],
               [3.10800e+03, 9.50400e+03, 2.26600e+04],
               [4.61760e+04, 8.44200e+04, 1.42528e+05]])

        >>> v.polyval(np.arange(6),np.arange(2,8))
        array([  -60.,   300.,  2388.,  8268., 20772., 43500.])

        >>> v.polyval(0,2)
        array([-60.])
        """

        if isinstance(x, Iterable) and not isinstance(x, np.ndarray):
            try:
                x = np.stack(x)
                if not np.issubdtype(x.dtype, np.number):
                    raise ValueError(
                        "When the first argument is an iterable all "
                        " sub-datatype  must be numbers"
                    )

            except:
                raise ValueError(
                    "When the first argument is an iterable, "
                    "the iterable must be an array_like structure"
                )
        if isinstance(y, Iterable) and not isinstance(y, np.ndarray):
            try:
                y = np.stack(y)
                if not np.issubdtype(y.dtype, np.number):
                    raise ValueError(
                        "When the second argument is an iterable all "
                        " sub-datatype  must be numbers"
                    )
            except:
                raise ValueError(
                    "When the first argument is an iterable, "
                    "the iterable must be an array_like structure"
                )
        m = n = None
        if (
            isinstance(x, np.ndarray)
            and isinstance(y, np.ndarray)
            and x.size != y.size
        ):
            m, n = x.size, y.size
            x = np.repeat(x, y.size)
            y = np.tile(y, (x.size, 1)).flatten()

        if isinstance(x, (int, float)):
            x = np.array(x)
        if isinstance(y, (int, float)):
            y = np.array(y)

        if x is None and y is None:
            raise ValueError("Both arguments cannot be None at the same time ")

        if not (x is None or isinstance(x, np.ndarray)) or not (
            y is None or isinstance(y, np.ndarray)
        ):
            raise TypeError(
                "The possible arguments are None, numbers or array_like objects"
            )

        if y is None:
            x = x.flatten()
            opty = self.opty.copy()
            aux = ((self.cols.coeff.T * self.diag) @ self.rows(x)).T
            if aux.shape[0] == 1:
                aux = aux.flatten()

            return tau.Polynomial(aux, opty)

        if x is None:
            y = y.flatten()
            optx = self.optx.copy()
            aux = (self.cols(y).T * self.diag) @ self.rows.coeff
            if aux.shape[0] == 1:
                aux = aux.flatten()
            return tau.Polynomial(aux, optx)

        if x.ndim == 2:
            # int this case x and y are aa meshgrid
            if (x == x[0]).all() and (y.T == y.T[0]).all():
                return self.evalm(x[0], y[:, 0])

            return np.einsum(
                "i,ijk->jk", self.diag, (self.cols(y) * self.rows(x))
            )
        else:
            aux = self.cols(y) * self.rows(x)
            res = (
                self.diag.item() * aux if np.isscalar(aux) else self.diag @ aux
            )
            if m != None:
                return res.reshape(m, n).T
            return res

    @staticmethod
    def interporth2(Fxy, n, options=None):
        """
        A function that interpolate a 2-dimensional function in  given two bases,
        one for each variable and a domain in a grid mxn

        Parameters
        ----------
        Fxt : callable
            A 2-dimanesional anonymous function.
        n : int or iterable
            The dimension of the grid. When n is an int means a square grid of
            dimension n. When an iterable n must be length 2 where where the
            first element is the number of pooints in x direction and the se-
            cond element is the  the number of points in the y direction.
            number of points in the
        options : settings,dict, tuple or list
            The options for the first variable and the second variable. The def
            ault is None

        Returns
        -------
        f : array_like
            A nxm array_like object with the coefficients of the interpolation
            Polynomial.
        esys : float
            The residual.
        eeval : flaot
            An estimation of the absolute error.
        edom : tuple
            A tuple with the meshgrid and the difference betwen the function
            and the interpolation Polynomial evaluated at the meshgrid.

        """

        if isinstance(n, int):
            n = (n,) * 2
        if options is None:
            options = tau.settings(), tau.settings()
        elif isinstance(options, dict):
            options = tau.settings(**options), tau.settings(**options)
        elif isinstance(options, tau.settings):
            options = options, options

        bas = [tau.basis(opt) for opt in options]

        # Creating the points of choosen orthogonal Polynomial basis
        x_points, *_ = bas[0].nodes(n[1])
        y_points, *_ = bas[1].nodes(n[0])

        # Memory allocation for Px, Pt, X and T
        Px = bas[0].polyvalb(x_points, n=n[1])
        Py = bas[1].polyvalb(y_points, n=n[0])
        X, Y = np.meshgrid(x_points, y_points, indexing="ij")

        # Obtaining the function Fxt and evaluating it at the b vector
        b = Fxy(X.reshape(np.prod(n)), Y.reshape(np.prod(n)))
        A = np.kron(Px, Py)
        # Solving the system and changing the shape.

        solution = np.linalg.solve(A, b)

        f = solution.reshape(*n, order="F")
        esys = np.max(np.abs(A @ solution - b))
        eeval, X, Y, F = tau.Polynomial2.interporth2_error(
            Fxy, solution, n, options
        )
        edom = [X, Y, F.reshape(*n, order="F")]

        return f, esys, eeval, edom

    @staticmethod
    def interporth2_error(Fxt, solution, n, options):
        r"""
        Compute the error of the the method interporth2 in the sense of the
        infinite norm error meaning max(abs(...))

        Parameters
        ----------
        Fxt : callable
            A lambda function representing the function we want to approximate
            by ``solution``.
        solution : array_like
            A bydimensional array with the coefficients of the approximate solu-
            tion
        n : Integer,tuple or list
            the dimension of the grid we want to compute the error
        options: settings,dict,tuple or list
            The options settings for the first and second independent variables.


        Returns
        -------
        f : Number
            The infinite norm error of the soluction meaning the max(abs(...))
        X : array_like
            The componente x of the meshgrid used to compute the error
        T : array_like
            The componente y of the meshgrid used to compute the error
        F : array_like
            The value of the meshgrid used to compute the error

        """
        bas = [tau.basis(opt) for opt in options]

        x_points = np.linspace(*options[0].domain, n[1])
        t_points = np.linspace(*options[1].domain, n[0])
        Px = bas[0].polyvalb(x_points, n=n[1])
        Pt = bas[1].polyvalb(t_points, n=n[0])
        X, T = np.meshgrid(x_points, t_points, indexing="ij")

        F = np.kron(Px, Pt) @ solution - Fxt(
            X.reshape(np.prod(n)), T.reshape(np.prod(n))
        )
        f = np.max(np.abs(F))

        return f, X, T, F

    @staticmethod
    def eval_from_coef(coef, x, y, options=None):
        r"""
        A method the receives the coefficients ``coef`` of a 2-Dimensional
        Polynomial in the bases ``bases`` and evaluate it on the abcissas
        ``x`` and ordinates ``y``. The bases can be any of the bases implemen-
        ted in tau.basis and can be two different bases



        Parameters
        ----------
        coef : scaler or array_like
            The coefficients of the Polynomial to evaluate.
        x : scalar or array_like
            The abcissas to evaluate. when an array it can be at most by-di-
            mensional.
        y : scalar or array_like
            The ordinates  to evaluate. when an array it can be at most by-di-
            mensional.
        options : settings,dict,list or tuple
            The options for the first and second independent variables.The
            default is None

        Raises
        ------
        TypeError
            When `coef` is a ragged nested sequence.
            When `x` is a ragged nested sequence.
        ValueError
            When `coef` has more than 2 dimensions.
            When `x` has more than two dimensions.

        Returns
        -------
        scalar or array_like
            The result of the evaluations

        Notes
        -----
        We have two bases :math:`\mathcal{P}=[P_0(x),\ P_1(x),\ \dots]` and
        :math:`\mathcal{Q}=[Q_0(y),\ Q_1(y),\ \dots]`. Those  bases can be used
        to write Polynomial in 2D as :

        .. math:: \Xi (x,y)=[a_{i,j}]_{n,m}\cdot (\mathcal{P}_m \otimes \mathcal{Q}_n^T),

        where :math:`\cdot` represents the  Hadamard  product and :math:`\otimes`
        means the Kronecker product. Here the bases are truncated in the
        dimension m and n respectively.


        """
        if options is None or isinstance(options, (dict, tau.setting)):
            options = [options] * 2

        # Check if coef are well posed
        coef_shape = tau.utils.get_shape(coef)
        if coef_shape is None:
            raise TypeError(
                "Coef must be an by-dimensional array_like "
                "structure or a scalar"
            )
        elif len(coef_shape) == 0:
            m = n = 1
        elif len(coef_shape) == 2:
            m, n = coef_shape
        else:
            raise ValueError(
                "Coef must be an by-dimensional array_like "
                "structure or a scalar"
            )
        bas = [tau.basis(opt) for opt in options]

        x_sha = tau.utils.get_shape(x)
        if x_sha is None:
            raise TypeError("x must be a scalar or a vector")

        if len(x_sha) == 0:
            xval = bas[0].polyvalb(x, n=n)
            yval = np.reshape(bas[1].polyvalb(y, n=m), (-1, 1))

            return np.sum(coef * yval * xval)
        elif len(x_sha) == 1:
            xval = bas[0].polyvalb(x, n=n)
            yval = bas[1].polyvalb(y, n=m)

            return np.einsum(
                "ij,kij->k", coef, np.einsum("ij,ik->ikj", xval, yval)
            )
        elif len(x_sha) == 2:
            xval = bas[0].polyvalb(x, n=n)
            yval = bas[1].polyvalb(y, n=m)
            # Here we use Einstein tensor sumation notation because is faster
            return np.einsum(
                "ij,klij->kl", coef, np.einsum("ijk,ijl->ijlk", xval, yval)
            )

        else:
            ValueError("Not yet implemented when x has dimension 3 or more")

    def trim(self, tol=0):
        """
        Trim the Polynomial in the position i,j from which the absolute value
        are less than or equal to tol.

        Parameters
        ----------
        tol : Number, optional
            The tol to compare the coefficients of the Polynomial2.

        Returns
        -------
        aux : Polynomial2
            A copy of the Polynomial2 trimmed.

        Examples
        --------
        >>> # Example where some coefficients are non zero

        >>> a=np.zeros((5,5))
        >>> a[(1,2,1),(1,3,3)]=[1,2,4]
        >>> a
        array([[0., 0., 0., 0., 0.],
               [0., 1., 0., 4., 0.],
               [0., 0., 0., 2., 0.],
               [0., 0., 0., 0., 0.],
               [0., 0., 0., 0., 0.]])

        >>> p=Polynomial2(a)
        >>> p2=p.trim()
        >>> p2.coef
        array([[0., 0., 0., 0.],
               [0., 1., 0., 4.],
               [0., 0., 0., 2.]])

        >>> # Example where the coefficients are all zeros

        >>> a=np.zeros((5,5))
        >>> a
        array([[0., 0., 0., 0., 0.],
               [0., 0., 0., 0., 0.],
               [0., 0., 0., 0., 0.],
               [0., 0., 0., 0., 0.],
               [0., 0., 0., 0., 0.]])

        >>> p=Polynomial2(a)
        >>> p2=p.trim()
        >>> p2.coef
        array([[0]])

        """

        # Check the positions where the abs of the value is greater than tol
        aux = self.copy()
        gt_abs_tol_pos = np.argwhere(np.abs(aux.coef) > tol)
        if len(gt_abs_tol_pos) == 0:
            aux.coef = np.array([[0]])
            aux.degree = np.array(aux.coef.shape) - 1
            return aux
        # trim the coef array in the max of the columns and max of the rows
        # the positions positions have values greater than tol
        aux.coef = aux.coef[
            : np.max(gt_abs_tol_pos[:, 0]) + 1,
            : np.max(gt_abs_tol_pos[:, 1]) + 1,
        ]
        aux.degree = np.array(aux.coef.shape) - 1

        return aux

    @property
    def power_coef(self):
        """
        Returns  an array with the coeficients of this Polynomial converted to
        power basis in all variables

        Returns
        -------
        ndarray
            An array which are the coeficients of the Polynomial with each
            variables in power basis, means PowerX.

        Examples
        --------
        >>> a=np.arange(12).reshape(3,4)
        >>> a
        array([[ 0,  1,  2,  3],
               [ 4,  5,  6,  7],
               [ 8,  9, 10, 11]])

        >>> Polynomial2(a).power_coef
        array([[  0.,  16., -16., -32.],
               [ -2., -16.,  12.,  28.],
               [ -4., -48.,  40.,  88.]])

        """
        m, n = self.shape
        res = (
            self._bas[1].orth2powmatrix(m)
            @ self.coef
            @ self._bas[0].orth2powmatrix(n).T
        )
        res[np.abs(res) < 1e-15] = 0
        return res

    # this is an alias
    coeff = coef

    @staticmethod
    def from_power_coef(coef, options=None):
        r"""
        Returns a Polynomial2 where the  new coefficients are converted from
        the given coefficients in power bases to a Polynomial2 with the options
        given by ``options``


        Parameters
        ----------
        coef : array_like
            A bidimensional array with the coeficients of the polynamials in
            the basis where the polinomial have the form c_{0,0}P_0Q_0 +
            c_{1,0}P_1Q_0 + ... + c_{m,n}P_mQ_n. Therefore coef=[c_{i,j}]_{m,n}
        options: settings,dict,tuple or list
            The options in the first and second variable to construct the Poly
            nomial2

        Returns
        -------
        Polynomial2
            A Polynomial2 in the given basis and domain where the coeficients
            are converted from power basis to the given basis.

        See Also
        --------
        power_coef

        Examples
        --------

        Converting from power coefficients to Chebyshev in all variables in the
        standard domain:

        >>> a= [[  0.,  16., -16., -32.],
                [ -2., -16.,  12.,  28.],
                [ -4., -48.,  40.,  88.]]

        >>> p=Polynomial2.from_power_coef(a)
        >>> p.coef
        array([[ 0.,  1.,  2.,  3.],
               [ 4.,  5.,  6.,  7.],
               [ 8.,  9., 10., 11.]])


        """
        if options is None or isinstance(options, (dict, tau.settings)):
            options = [options] * 2

        # Parse coef
        # When coef is array like
        if not (
            isinstance(coef, Iterable)
            and all([isinstance(co, Iterable) for co in coef])
            and all([len(co) == len(coef[0]) for co in coef])
            and all([all([isinstance(c, Number) for c in co]) for co in coef])
        ):
            raise ValueError(
                "Polynomial2: coef must be a m x n array_like object"
            )

        power_coef = coef if isinstance(coef, np.ndarray) else np.array(coef)

        bases_ = [tau.basis(opt) for opt in options]
        m, n = power_coef.shape
        new_coef = (
            bases_[0].pow2orthmatrix(m)
            @ power_coef
            @ bases_[1].pow2orthmatrix(n).T
        )

        return Polynomial2(new_coef, options)

    def diff(self, order=1):
        """
        Returns the derivative of the Polynomial2 object where order is a
        tuple or list with two elements indicating the order of the derivative
        in each variable.

        Parameters
        ----------
        order : tuple or list
            A tuple or list of numbers indicating the order of the derivatives
            in each variable

        Returns
        -------
        Polynomial2
            The Polynomial2 which is the partial derivative with the order
            given by the order parameter.

        Examples
        --------
        >>> a =np.arange(15).reshape(3,5)
        >>> a
        array([[ 0,  1,  2,  3,  4],
               [ 5,  6,  7,  8,  9],
               [10, 11, 12, 13, 14]])

        >>> p=Polynomial2(a)

        >>> # Is more easy to see the correctness of the result in the power series
        >>> # bases

        >>> p.power_coef
        array([[ -10.,   20.,   60.,  -40.,  -80.],
               [   7.,  -18.,  -58.,   32.,   72.],
               [  24.,  -56., -176.,  104.,  224.]])
        >>>
        >>> # To compute de d^3/(dx^2 dy)
        >>> p_deriv =p.diff((2,1))
        >>> p_deriv.power_coef
        array([[-116.,  192.,  864.,    0.,    0.],
               [-704., 1248., 5376.,    0.,    0.],
               [   0.,    0.,    0.,    0.,    0.]])



        """
        # Check if order is a number
        if isinstance(order, int):
            order = (order, 0)

        if isinstance(order, Iterable):
            if len(order) == 2 and all(
                [np.isscalar(o) and o >= 0 and int(o) == o for o in order]
            ):
                x_order, y_order = order

            else:
                raise ValueError(
                    "When the order is an iterable it must have "
                    "length 2 and all entries integers"
                )
        else:
            raise TypeError(
                "Order can be only an integer or a length 2 "
                "iterable of integers"
            )

        aux = self.copy()

        if y_order >= self.cols.n or x_order >= self.rows.n:
            return Polynomial2(
                np.zeros((self.cols.n, self.rows.n)),
                (self.rows.options, self.cols.options),
            )

        aux.cols = aux.cols.diff(y_order)
        aux.rows = aux.rows.diff(x_order)

        return aux

    @staticmethod
    def _power_mul(lhs, rhs):
        r"""
        This method multiply two arrays of coefficients when the two variables
        are in power series.

        Parameters
        ----------
        lhs : array_like
            A bydimesional array of coefficients representing a Polynomial in
            two variables in power basis

        rhs : array_like
            A bydimesional array of coefficients representing a Polynomial in
            two variables in power  basis

        Returns
        -------
        aux : array_like
            A bidimensional array_like which coefficients are the result of the
            product of  two polynomials in two variables which each variable are
            in power basis

        """
        lhs_shape = lhs.shape
        rhs_shape = rhs.shape

        aux = np.zeros(
            (
                np.prod(lhs_shape),
                *[sum(i) - 1 for i in zip(lhs_shape, rhs_shape)],
            )
        )
        for i, j in zip(*np.nonzero(lhs)):
            aux[
                i * lhs.shape[1] + j,
                i : i + rhs_shape[0],
                j : j + rhs_shape[1],
            ] = (
                lhs[i, j] * rhs
            )

        aux = np.sum(aux, axis=0)

        return aux

    @staticmethod
    def product(lhs, rhs, options=None):
        """
        When we have two arrays which are the coeficients of two Polynomial2
        in the same bases and domain, the result are the coeficnets  of the
        product of these two Polynomial2

        Parameters
        ----------
        lhs : array_like
            The coeficients of a Polynomial2.
        rhs : array_like
            The coefficients of a Polynomial2.
        options : dict, settings,list or tuple
            The options for the independents variables. The default is None.

        Returns
        -------
        array_like
            The coefficients of a Polynomial in two variables.

        Examples
        --------
        >>> import numpy as np

        Using the default basis and domain for each variables. Two arrays that
        are supposed to be the the coeficients of two Polynomial2 object with
        the same bases and domain:

        >>> a=np.arange(10).reshape(5,2)
        >>> a
        array([[0, 1],
               [2, 3],
               [4, 5],
               [6, 7],
               [8, 9]])

        >>> b=a.reshape(2,5)
        >>> b
        array([[0, 1, 2, 3, 4],
               [5, 6, 7, 8, 9]])

        >>> tau.Polynomial2.product(a,b)
        array([[ 10.  ,  19.75,  19.5 ,  23.  ,  16.5 ,   8.75],
               [ 22.  ,  46.75,  48.5 ,  59.  ,  44.5 ,  21.75],
               [ 37.5 ,  75.5 ,  81.  ,  99.  ,  79.5 ,  32.5 ],
               [ 54.5 , 108.5 , 117.  , 143.  , 116.5 ,  45.5 ],
               [ 30.  ,  64.75,  79.5 , 103.  ,  86.5 ,  33.75],
               [ 33.5 ,  62.25,  59.5 ,  68.  ,  54.  ,  20.25]])

        ---

        """
        if options is None or isinstance(options, (dict, tau.settings)):
            options = [options] * 2

        m1, n1 = lhs.shape
        m2, n2 = rhs.shape
        m = m1 + m2 - 1

        optx, opty = options
        aux1 = np.zeros(1, dtype="O")
        aux2 = np.zeros(m2, dtype="O")
        aux1[0] = tau.Polynomial(lhs, optx)
        aux2[:] = tau.Polynomial(rhs, optx)
        res1 = aux1 * aux2
        res2 = np.zeros_like(res1)
        res2[:] = [tau.Polynomial(i.coeff.T, opty) for i in res1]
        res3 = np.zeros_like(aux2)
        res3[:] = tau.Polynomial(np.eye(m2), opty)

        return sum([i.get_coef(m).T for i in res2 * res3])

    def plot(
        self,
        x_lim=None,
        y_lim=None,
        n=None,
        ax=None,
        *args,
        **kwargs,
    ):
        r"""
        Plot the polynomial inside the limits given by x_lim and y_lim.
        The density of

        Parameters
        ----------
        x_lim : array_like, optional
            The endpoints of values in the x-axis. The default is None.
        y_lim : array_like, optional
            The endpoints values in y-direction. The default is None.
        n : int , optional
            The number of points in each direction to construct the meshgrid.
            The default is None.
        ax : AxesSubplot
            to put the figure in a particular axes. The default is None.
        *args : TYPE
            These are the args of matplotlib plot_surface
        **kwargs : TYPE
            Those are the kwargs ofmatplotlib plot_surface
         : TYPE
            DESCRIPTION.

        Returns
        -------
        ax : AxesSubplot
            The axessubplot where these plot are drawed

        Examples
        --------
        Using the function f(x,y)=x**2+y**2 with the default basis and domain:

        >>> p = tau.Polynomial2(lambda x, y:x**2 + y**2)
        >>> p.p.contour()
        """
        kwargs.setdefault("cmap", "winter_r")
        kwargs.setdefault("cstride", 1)
        kwargs.setdefault("rstride", 1)
        kwargs.setdefault("antialiased", False)

        if ax is None:
            ax = plt.axes(projection="3d")

        if x_lim is None:
            x_lim = self.domain[0]
        if y_lim is None:
            y_lim = self.domain[1]
        if n is None:
            n = 100
        plt.style.use("ggplot")
        ax.set_facecolor("w")
        x = np.linspace(*x_lim, n)
        y = np.linspace(*y_lim, n)
        X, Y = np.meshgrid(x, y)
        ax.plot_surface(X, Y, self.evalm(x, y), *args, **kwargs)
        return ax

    def contour(
        self,
        plot_points=200,
        pivots=False,
        ax=None,
        **kwargs,
    ):
        r"""
        Draw the contour plot of a Polynomial2

        Parameters
        ----------
        plot_points : Integer, optional
            The Number of points to create the meshgrid. so the grid will have
            dimension ``plot_points``x``plot_points``. The default is 200.
        pivots : Boolean, optional
            Plot the pivot values. This only make sense when the Polynomial2 are
            generated using ACA. The default is False.
        ax : AxesSubplot, optional
            If the plot must be drawed in a specific axes. The default is None.
        **kwargs : TYPE
            These are the others parameters that the function contourf from
            ``matplotlib`` can acept.

        Returns
        -------
        ax : AxesSubplot
            Return the plot as an AxesSubplot object

        Examples
        --------
        >>> import numpy as np

        Using the function f(x,y)=x**2+y**2 with the default basis and domain:

        >>> p = tau.Polynomial2(lambda x, y:x**2 + y**2)
        >>> p.p.contour()

        Using the function f(x,y)=cos(x**2+y**2) with the default LegendreP
        basis in all variables and the default domain:

        >>> p = tau.Polynomial2(lambda x,y:np.cos(x**2+y**2),{'basis':'LegendreP'})
        >>> p.contour(pivots=True)

        """
        if ax == None:
            ax = plt.gca()
        x = np.linspace(*self.domain[0], plot_points)
        y = np.linspace(*self.domain[1], plot_points)
        xx, yy = np.meshgrid(x, y)
        zz = self.evalm(x, y)
        plt.style.use("ggplot")

        # Default colormap
        kwargs.setdefault("cmap", "ocean")

        ax.contourf(xx, yy, zz, **kwargs)
        if pivots:
            ax.plot(
                self.pivot_locations[:, 0],
                self.pivot_locations[:, 1],
                ".",
                color="red",
            )

        return ax

    @staticmethod
    def nodes2d(
        m=32,
        n=32,
        optx=None,
        opty=None,
    ):
        if optx is None:
            optx = tau.settings(basis="ChebyshevU")
        if opty is None:
            opty = tau.settings(basis="ChebyshevU")
        if isinstance(optx, dict):
            optx = tau.settings(**optx)
        if isinstance(opty, dict):
            opty = tau.settings(**opty)

        nodes_x, *_ = tau.basis(optx).nodes(n)
        nodes_y, *_ = tau.basis(opty).nodes(m)
        return np.meshgrid(nodes_x, nodes_y)

    @staticmethod
    def interp2d(
        f,
        optx=None,
        opty=None,
        method=None,
        grid_shape=None,
    ):
        r"""
        Interpolate the bivariate function ``f`` with ``optx`` as the options
        for the first independent variable and ``opty`` as the options for the
        second independent variable.

        Parameters
        ----------
        f : callable
            A callable which we want to interpolate
        optx : settings, optional
            The options for the first independent variable. When not given we
            assume the default settings. The default is None.
        opty : settings, optional
            The options for the second independent variable. When not given we
            assume the default settings. The default is None.
        method : str, optional
            The method used to Interpolate. When not given we use ACA. The de
            fault is None.
        grid_shape : tuple or list, optional
            The dimension of the grid. This is only specified if we want a
            fixed grid. When not given we assume an adaptive grid. The default
            is None.

        Raises
        ------
        ValueError
            When the method is not  ACA we need to specify a grid_shape because
            for methods other than ACA until now we only works with fixed grid.

        Returns
        -------
        Polynomial2 or the components of a Polynomial2
            The result of the interpolation.

        """
        if optx is None:
            optx = tau.settings()
        if opty is None:
            opty = tau.settings()
        if isinstance(optx, dict):
            optx = tau.settings(**optx)
        if isinstance(opty, dict):
            opty = tau.settings(**opty)

        method = "aca" if method is None else method.lower()

        if grid_shape == None and method != "aca":
            raise ValueError(
                "When the method is not 'aca' you need to specify a grid_shape"
            )

        if method == "aca":
            if grid_shape == None:
                return Polynomial2.interp2d_aca_adaptive_grid(
                    f, optx=optx, opty=opty
                )

            else:
                (
                    col_vals,
                    pivot_vals,
                    row_vals,
                    piv_pos,
                    vscale,
                    rtol,
                ) = Polynomial2.interp2d_aca_fixed_grid(
                    f, optx, opty, grid_shape=grid_shape
                )

        elif method == "svd":
            (
                col_vals,
                pivot_vals,
                row_vals,
                vscale,
                rtol,
            ) = Polynomial2.interp2d_svd(f, optx, opty, grid_shape=grid_shape)

        else:
            raise ValueError("Methods implemented yet are 'aca' and 'svd'.")

        # Transform the n columns in n one dimensional Polynomials
        # To be consistent with x in vertical and y in horizontal i transpose
        # i interchange the variable  x with y

        if (pivot_vals != 0).any():
            p_rows = tau.orthogonal.basis.interp_from_values(
                row_vals, options=optx
            )

            # pivot_vals = pivot_vals[: min(p_cols.shape)]

            # Transform the n rows in n onedimnesional Polynomials
            p_cols = tau.orthogonal.basis.interp_from_values(
                col_vals.T, options=opty
            )
        else:
            p_cols, p_rows = np.array([[0]]), np.array([[0]])

        p_cols = tau.Polynomial(p_cols, opty)
        p_rows = tau.Polynomial(p_rows, optx)

        # When using adaptive grid trim  the coefficients at rtol
        if grid_shape is None:
            p_cols = p_cols.trim(rtol)
            p_rows = p_rows.trim(rtol)

        if method == "svd":
            piv_pos = None

        return p_cols, pivot_vals, p_rows, piv_pos, vscale

    @staticmethod
    def interp2d_aca_fixed_grid(f, optx=None, opty=None, grid_shape=(16, 16)):
        # When optx or opty are not given we assume working with
        # Chebyshev Polynomials of the first kind in [-1,1]
        if optx is None:
            optx = tau.settings()
        if opty is None:
            opty = tau.settings()
        if isinstance(optx, dict):
            optx = tau.settings(**optx)
        if isinstance(opty, dict):
            opty = tau.settings(**opty)

        level = np.spacing(1)  # The same as np.finfo(float).eps

        domain = np.array([optx.domain, opty.domain])

        # Options for a chebyshev grid
        new_optx = {"basis": "ChebyshevU", "domain": optx.domain}
        new_opty = {"basis": "ChebyshevU", "domain": opty.domain}

        # Construct a ChebyshevU Grid
        xx, yy = Polynomial2.nodes2d(*grid_shape, new_optx, new_opty)

        values = f(xx, yy)
        # For the case the function passed is a constant
        if isinstance(values, Number):
            values = values * np.ones(xx.shape)

        # The maximum of the abs of the elements of vales

        # Scale of V
        vmax = np.max(np.abs(values))
        # Raise an exception if the max is
        if vmax == np.inf:
            raise Exception(
                "The function encountered infininte when evaluating"
            )
        if np.any(np.isnan(values)):
            raise Exception(
                "The function encountered not a number when evaluatinh"
            )

        rtol, atol = get_tol(xx, yy, values, domain, level)

        (
            pivot_vals,
            pivot_pos,
            row_vals,
            col_vals,
            fail,
        ) = adaptive_cross_approx(values, atol, np.float64(0))
        piv_pos = np.stack(
            [xx[0, pivot_pos[:, 1]], yy[pivot_pos[:, 0], 0]], axis=1
        )
        return col_vals, 1 / pivot_vals, row_vals, piv_pos, vmax, rtol

    @staticmethod
    def interp2d_aca_adaptive_grid(f, optx=None, opty=None):
        r"""


        Parameters
        ----------
        f : callable
            A callable in two independent variables which we want to interpolate
            in two variables
        optx : settings or dict, optional
            The options for the first independent variable. When not given we
            assume the default settings.The default is None.
         opty : settings or dict, optional
             The options for the second independent variable. When not given we
             assume the default settings.The default is None.

        Raises
        ------
        ValueError
            DESCRIPTION.
        Exception
            DESCRIPTION.

        Returns
        -------
        Polynomial2
            A Polynomial2 result of the interpolation

        """

        # When optx or opty are not given we assume working with
        # Chebyshev Polynomials of the first kind in [-1,1]
        if optx is None:
            optx = tau.settings()
        if opty is None:
            opty = tau.settings()
        if isinstance(optx, dict):
            optx = tau.settings(**optx)
        if isinstance(opty, dict):
            opty = tau.settings(**opty)
        domain = np.array([optx.domain, opty.domain])
        min_samples = (
            np.array([2, 2]) ** 4 + 1
        )  # The max matrix to operate a 128 x 128 matrix
        max_samples = np.array([2, 2]) ** 16 + 1
        max_rank = np.array([513, 513])
        # where to start
        level = min(optx.interpRelTol, opty.interpRelTol)
        factor = 4
        enough = 0
        failure = 0
        rtol = None
        atol = None
        vscale = None
        p = tau.Polynomial2()
        new_optx = {"basis": "ChebyshevU", "domain": optx.domain}
        new_opty = {"basis": "ChebyshevU", "domain": opty.domain}
        while not enough and not failure:
            grid = min_samples
            # The grid points and their values to interpolate
            xx, yy = Polynomial2.nodes2d(*grid, new_optx, new_opty)

            values = f(xx, yy)
            # For the case the function passed is a constant
            if isinstance(values, Number):
                values = values * np.ones(xx.shape)
            if np.isnan(values).any() or np.isinf(values).any():
                raise ValueError(
                    "Encountered NaN or inf when evaluating the function."
                )

            # The maximum of the abs of the elements of vales
            # Chek if all values is 0
            if (values == 0).all():
                return np.array([[0]]), np.array([0]), np.array([[0]]), 0

            # Scale of V
            vmax = np.max(abs(values))
            vscale = vmax
            # Raise an exception if the max is
            if vmax == np.inf:
                raise Exception(
                    "The function encountered infininte when evaluating"
                )
            if np.any(np.isnan(values)):
                raise Exception(
                    "The function encountered not a number when evaluatinh"
                )

            rtol, atol = get_tol(xx, yy, values, domain, level)

            ##### Step one #####

            (
                pivot_vals,
                pivot_pos,
                row_vals,
                col_vals,
                fail,
            ) = adaptive_cross_approx(values, atol, factor)
            strike = 1

            while (
                fail
                and np.all(grid <= factor * (max_rank - 1) + 1)
                and strike < 3
            ):
                # Refine the grid  and do a resampling and repeat the above process
                grid[:] = [refine_grid(el)[0] for el in grid]
                xx, yy = Polynomial2.nodes2d(*grid, new_optx, new_opty)
                values = f(xx, yy)
                vmax = np.max(abs(values))
                vscale = vmax
                rtol, atol = get_tol(xx, yy, values, domain, level)
                optx.interpRelTol = opty.interpRelTol = rtol

                (
                    pivot_vals,
                    pivot_pos,
                    row_vals,
                    col_vals,
                    fail,
                ) = adaptive_cross_approx(values, atol, factor)
                # Stop if the function is 0 + noise
                if np.abs(pivot_vals[0]) < 1e4 * vmax * rtol:
                    strike += 1
            # The function algorithm stop if the rank of the function is
            # greater than max_rank

            if (grid > factor * (max_rank - 1) + 1).any():
                warn("Polynomial2: The functions is not a low-rank function")
                failure = 1

            col_hscale = np.linalg.norm(domain[1], np.inf)
            row_hscale = np.linalg.norm(domain[0], np.inf)

            resolved_cols, *_ = readiness_check(
                np.sum(col_vals, axis=1),
                vscale=vmax,
                hscale=col_hscale,
                tol=opty.interpRelTol,
            )

            resolved_rows, *_ = readiness_check(
                np.sum(row_vals, axis=0),
                vscale=vmax,
                hscale=row_hscale,
                tol=optx.interpRelTol,
            )

            enough = resolved_cols and resolved_rows

            piv_pos = np.stack(
                (xx[0, pivot_pos[:, 1]], yy[pivot_pos[:, 0], 0]), axis=1
            )
            pp = pivot_pos

            ###### Phase 2 ########

            m, n = grid

            while not enough and not failure:
                if not resolved_cols:
                    # Double the sampling along columns
                    n, nest = refine_grid(n)
                    # Find the location of pivots on new grid
                    pp[:, 0] = nest[pp[:, 0]]

                xx, yy = np.meshgrid(piv_pos[:, 0], nodes(n, domain[1]))
                col_vals = f(xx, yy)

                if not resolved_rows:
                    m, nest = refine_grid(m)
                    # Find the location of pivots in the new grid
                    pp[:, 1] = nest[pp[:, 1]]

                xx, yy = np.meshgrid(nodes(m, domain[0]), piv_pos[:, 1])
                row_vals = f(xx, yy)

                # Do Gaussian Elimination on the skeleton to update slices
                nn = pivot_vals.size

                for i in range(nn - 1):
                    col_vals[:, i + 1 :] -= (
                        col_vals[:, [i]]
                        @ row_vals[i : i + 1, pp[i + 1 : nn, 1]]
                        / pivot_vals[i]
                    )
                    row_vals[i + 1 :, :] -= (
                        col_vals[pp[i + 1 : nn, [0]], i]
                        @ row_vals[[i], :]
                        / pivot_vals[i]
                    )
                if not resolved_cols:
                    resolved_cols, *_ = tau.readiness_check(
                        np.sum(col_vals, axis=1),
                        vscale=vmax,
                        hscale=col_hscale,
                    )
                if not resolved_rows:
                    resolved_rows, *_ = tau.readiness_check(
                        np.sum(row_vals, axis=0),
                        vscale=vmax,
                        hscale=row_hscale,
                    )
                enough = resolved_rows and resolved_cols

                if not (np.array((m, n)) < np.array(max_samples)).all():
                    warn(
                        "Polynomial2: Unresolved with respect to maximum "
                        f"length: {max_samples}"
                    )
                    failure = 1

            p_rows = tau.orthogonal.basis.interp_from_values(
                row_vals, options=optx
            )

            p_cols = tau.orthogonal.basis.interp_from_values(
                col_vals.T, options=opty
            )

            # Create an empty Polynomial2
            p = tau.Polynomial2()
            p.optx = optx
            p.opty = opty

            # The coluns of the Polynomial2
            p.cols = tau.Polynomial(p_cols, opty)

            # The rows of the Polynomial2
            p.rows = tau.Polynomial(p_rows, optx)

            # The diagonal
            p.diag = 1 / pivot_vals

            # The locations of the pivots
            p.pivot_locations = piv_pos

            # Construct a grid to compare with the original function
            # to see if we have a sufficientely small error

            enough = p.sampletest(f, atol)
            # If the error is not  suficientely small duplicate the grid
            # dimensiosns
            if not enough:
                min_samples = (
                    refine_grid(min_samples[0])[0],
                    refine_grid(min_samples[1])[0],
                )
        min_tol = min(optx.interpRelTol, opty.interpRelTol)

        p.cols = p.cols.trim(min_tol)
        p.rows = p.rows.trim(min_tol)
        p.__vscale = vscale
        return p

    @staticmethod
    def interp2d_svd(f, optx=None, opty=None, grid_shape=(60, 60)):
        r"""


        Parameters
        ----------
        f : callable
            A callable in two independent variables which we want to interpolate
            in two variables
        optx : settings or dict, optional
            The options for the first independent variable. When not given we
            assume the default settings.The default is None.
         opty : settings or dict, optional
             The options for the second independent variable. When not given we
             assume the default settings.The default is None.
        grid_shape : integer,tuple or list, optional
            The dimension of the grid. The default is (60, 60).

        Raises
        ------
        Exception
            DESCRIPTION.

        Returns
        -------
        TYPE
            DESCRIPTION.
        TYPE
            DESCRIPTION.
        TYPE
            DESCRIPTION.
        rtol : TYPE
            DESCRIPTION.

        """
        # When optx or opty are not given we assume working with
        # Chebyshev Polynomials of the first kind in [-1,1]
        if optx is None:
            optx = tau.settings()
        if opty is None:
            opty = tau.settings()
        if isinstance(optx, dict):
            optx = tau.settings(**optx)
        if isinstance(opty, dict):
            opty = tau.settings(**opty)

        if isinstance(grid_shape, int):
            grid_shape = (grid_shape,) * 2

        level = np.spacing(1)  # The same as np.finfo(float).eps

        domain = np.array([optx.domain, opty.domain])

        # Options for a chebyshev grid
        new_optx = {"basis": "ChebyshevU", "domain": optx.domain}
        new_opty = {"basis": "ChebyshevU", "domain": opty.domain}

        # Construct a ChebyshevU Grid
        xx, yy = Polynomial2.nodes2d(*grid_shape, new_optx, new_opty)

        values = f(xx, yy)
        # For the case the function passed is a constant
        if isinstance(values, Number):
            values = values * np.ones(xx.shape)

        # The maximum of the abs of the elements of vales

        # Scale of V
        vmax = np.max(abs(values))
        vscale = vmax
        # Raise an exception if the max is
        if vmax == np.inf:
            raise Exception(
                "The function encountered infininte when evaluating"
            )
        if np.any(np.isnan(values)):
            raise Exception(
                "The function encountered not a number when evaluating"
            )

        rtol, atol = get_tol(xx, yy, values, domain, level)
        u, s, vh = np.linalg.svd(values)

        # The rank of values
        k = sum(s > rtol)
        return u[:, :k], s[:k], vh[:k, :], vscale, rtol

    @staticmethod
    def vals_to_coef(vals, optx=None, opty=None, **kwargs):
        r'''
        """
        Converts a matrix of values to orthogonal coeficients. The matrix
        are the values of the evaluation of a meshgrid in xy indexing
        of second kind Chebyshev points.

        Parameters
        ----------
        vals : Array_like or Iterable.
            DESCRIPTION. A matrix representing the values of evaluation
            or an length 3 Iterable with a low rank representaion (u,s,vh) of
            the evaluation  such that (u*s)@vh is the matrix of the evaluation.
        optx : settings, optional
            The options in the variable x. When not given we assume the default
            basis and domain i.e. ChebyshevT in [-1,1]. The default is None.
        opty : settings, optional
            The options in the variable y. When not given we assume the default
            basis and domain i.e. ChebyshevT in [-1,1]. The default is None.
        **kwargs : TYPE
            kind1x: bool
                If the values are the images of ChebyshevT nodes. This only ma-
                tter when the basis in x is ChebyshevT. When False if the basis
                is ChebyshevT the nodes that are considered is the nodes of
                ChebyshevU
            kind1y: bool
                If the values are the images of ChebyshevT nodes. This only ma-
                tter when the basis in y is ChebyshevT. When False if the basis
                is ChebyshevT the nodes that are considered is the nodes of
                ChebyshevU
        Returns
        -------
        TYPE
            DESCRIPTION.

        '''
        # When optx or opty are not given we assume working with
        # Chebyshev Polynomials of the first kind in [-1,1]
        if optx is None:
            optx = tau.settings()
        if opty is None:
            opty = tau.settings()
        if isinstance(optx, dict):
            optx = tau.settings(**optx)
        if isinstance(opty, dict):
            opty = tau.settings(**opty)

        kind1x = kwargs.get("kind1x", False)
        kind1y = kwargs.get("kind1y", False)

        u, s, vh = np.linalg.svd(vals)

        rk = np.sum(s > 100 * np.spacing(1))
        u = u[:, :rk]
        vh = vh[:rk, :]
        s = s[:rk]

        p_rows = tau.orthogonal.basis.interp_from_values(
            vh, options=optx, kind1=kind1x
        )

        p_cols = tau.orthogonal.basis.interp_from_values(
            u.T, options=opty, kind1=kind1y
        )

        return (p_cols.T * s) @ p_rows

    def iszero(self):
        r"""
        Return true if ``self`` is zero, false otherwise.

        Returns
        -------
        bool
            Whether ``self`` is zero or not.

        Examples
        --------
        >>> a =np.arange(15).reshape(3,5)
        >>> a
        array([[ 0,  1,  2,  3,  4],
               [ 5,  6,  7,  8,  9],
               [10, 11, 12, 13, 14]])
        >>>
        >>> p=Polynomial2(a)
        False
        >>>
        >>> # Now doing the partial derivative in x direction of order 5.
        >>> p.diff((5, 0)).iszero()
        True
        """
        diag = self.diag
        domain = self.domain

        if np.linalg.norm(diag, np.inf) == 0:
            return True

        # Quick check over a small grid
        x = np.linspace(*domain[0], 10)
        y = np.linspace(*domain[1], 10)

        vals = self(x, y)

        if np.linalg.norm(vals, np.inf) > 0:
            return False

        # Ohterwise check if either columns or rows are 0

        return np.all(self.cols.iszero()) or np.all(self.rows.iszero())

    def __len__(self):
        r"""
        The rank of the Polynomial2

        Returns
        -------
        int
            The rank of the Polynomial2

        """
        return len(self.diag)

    @property
    def shape(self):
        r"""
        Gives a pair representing the length of the columns and the rows.

        Returns
        -------
        int
            The length of the columns of ``self``.
        int
            The length of the rows of ``self``.

        """
        return self.cols.n, self.rows.n

    def interpf(self, fun):
        r"""
        This method compose ``fun`` with ``self``.

        Parameters
        ----------
        fun : callable
            A callable we want to compose in the form fun(self).

        Returns
        -------
        Polynomial2
            A polynomial2 which is the approximation of fun(self).

        Examples
        --------

        Using the the function f(x,y)=cos(x**2 + y**2) in the standard domain
        and basis i.e. ChebyshevT in [-1, 1]:

        >>> f = lambda x, y: x**2 + y**2
        >>> g = lambda x, y: np.cos(f(x, y))
        >>> p = tau.Polynomial2(f)
        >>> ep = p.interpf(np.cos)

        Asserting that the error in the norm at infinity sense is less than
        1e-14:

        >>> ep.sampletest(g, 1e-14)
        True
        """
        return Polynomial2(lambda x, y: fun(self(x, y)), (self.optx, self.opty))

    def get_coef(self, dims=None, fact=False):
        r"""
        This method  when ``fact``  is False returns a bydimensional array of
        coefficients of dimensions ``dims``=(m1,n1), considering that ``self``
        has shape(m,n) if m1>m the position from m are filled with zeros, the
        happen if n1>m. When ``fact=True`` the process is similar but return the
        low rank factorization of the array.

        Parameters
        ----------
        dims : int tuple or list, optional
            The dimensions of the array of coefficients
        fact : bool, optional
            If we want the array in a low rank factorization. If so returns
            the components of the factorization.

        Raises
        ------
        ValueError
            When ``dims`` is neither an integer nor a tuple.

        Returns
        -------
        array or tuple
            An array or a low rank factorization of the array.

        Examples
        --------

        Using the the function f(x,y)=cos(x**2 + y**2) in the standard domain
        and basis i.e. ChebyshevT in [-1, 1]:

        >>> f = lambda x, y: x**2 + y**2
        >>> # This is the same as p.coef
        >>> p.coef
        array([[0.5 , 0.75, 0.  , 0.25],
               [0.  , 0.  , 0.  , 0.  ],
               [0.5 , 0.  , 0.  , 0.  ]])
        >>>
        >>> p.get_coef()
        array([[0.5 , 0.75, 0.  , 0.25],
               [0.  , 0.  , 0.  , 0.  ],
               [0.5 , 0.  , 0.  , 0.  ]])
        >>>
        >>> # geting an array of coefficients with shape (4, 3), this will trun-
        >>> # cate in x direction and fill with zeros in y direction.
        >>> p.get_coef((4, 3))
        array([[0.5 , 0.75, 0.  ],
               [0.  , 0.  , 0.  ],
               [0.5 , 0.  , 0.  ],
               [0.  , 0.  , 0.  ]])
        >>>
        >>> # Doing the same but now getting the components.
        >>> p.get_coef((4, 3), fact=True)
        (array([[ 0.94362832,  0.        ,  0.33100694,  0.        ],
                [-0.33100694,  0.        ,  0.94362832,  0.        ]]),
         array([0.98117035, 0.40287062]),
         array([[ 0.6495484 ,  0.72130312, -0.        ],
                [ 0.76032025, -0.61621571,  0.        ]]))
        """

        if dims is None or isinstance(dims, int):
            dims = (dims,) * 2
        if not isinstance(dims, (list, tuple)):
            raise ValueError(
                "'dims' must be an integer when we want square array of coeffi"
                "cients or a tuple or list with the dimensions in y and x axis."
            )

        d = self.diag
        m, n = dims
        c, r = self.cols.get_coef(m), self.rows.get_coef(n)

        if fact is False:
            aux = (c.T * d) @ r
            aux[np.abs(aux) < tau.settings.defaultPrecision] = 0
            return aux
        return c, d, r

    def cos(self):
        r"""
        This compute the cossine of a Polynomial2

        Returns
        -------
        Polynomial2
            The cossine approximation of a Polynomial2

        Examples
        --------

        Using the the function f(x,y)=cos(x**2 + y**2) in the standard domain
        and basis i.e. ChebyshevT in [-1, 1]:

        >>> f = lambda x, y: x**2 + y**2
        >>> g = lambda x, y: np.cos(f(x, y))
        >>> p = tau.Polynomial2(f)
        >>> ep = p.cos()

        Asserting that the error in the norm at infinity sense is less than
        1e-14:

        >>> ep.sampletest(g, 1e-14)
        True
        """

        return self.interpf(np.cos)

    def sin(self):
        r"""
        This compute the sinema of a Polynomial2

        Returns
        -------
        Polynomial2
            The cossine approximation of a Polynomial2

        Examples
        --------

        Using the the function f(x,y)=sin(x**2 + y**2) in the standard domain
        and basis i.e. ChebyshevT in [-1, 1]:

        >>> f = lambda x, y: x**2 + y**2
        >>> g = lambda x, y: np.sin(f(x, y))
        >>> p = tau.Polynomial2(f)
        >>> ep = p.sin()

        Asserting that the error in the norm at infinity sense is less than
        1e-14:

        >>> ep.sampletest(g, 1e-14)
        True
        """
        return self.interpf(np.sin)

    def exp(self):
        r"""
        This compute the exponential of a Polynomial2

        Returns
        -------
        Polynomial2
            The cossine approximation of a Polynomial2

        Examples
        --------

        Using the the function f(x,y)=exp(x**2 + y**2) in the standard domain
        and basis i.e. ChebyshevT in [-1, 1]:

        >>> f = lambda x, y: x**2 + y**2
        >>> g = lambda x, y: np.exp(f(x, y))
        >>> p = tau.Polynomial2(f)
        >>> ep = p.exp()

        Asserting that the error in the norm at infinity sense is less than
        1e-14:

        >>> ep.sampletest(g, 1e-14)
        True
        """
        return self.interpf(np.exp)

    def cosh(self):
        r"""
        This compute the cossine hyperbolic of a Polynomial2

        Returns
        -------
        Polynomial2
            The cossine approximation of a Polynomial2

        Examples
        --------

        Using the the function f(x,y)=cosh(x**2 + y**2) in the standard domain
        and basis i.e. ChebyshevT in [-1, 1]:

        >>> f = lambda x, y: x**2 + y**2
        >>> g = lambda x, y: np.cosh(f(x, y))
        >>> p = tau.Polynomial2(f)
        >>> ep = p.cosh()

        Asserting that the error in the norm at infinity sense is less than
        1e-14:

        >>> ep.sampletest(g, 1e-14)
        True
        """

        return self.interpf(np.cosh)

    def sinh(self):
        r"""
        This compute the sine hyperbolic of a Polynomial2

        Returns
        -------
        Polynomial2
            The cossine approximation of a Polynomial2

        Examples
        --------

        Using the the function f(x,y)=sinh(x**2 + y**2) in the standard domain
        and basis i.e. ChebyshevT in [-1, 1]:

        >>> f = lambda x, y: x**2 + y**2
        >>> g = lambda x, y: np.sinh(f(x, y))
        >>> p = tau.Polynomial2(f)
        >>> ep = p.sinh()

        Asserting that the error in the norm at infinity sense is less than
        1e-14:

        >>> ep.sampletest(g, 1e-14)
        True
        """

        return self.interpf(np.sinh)

    def laplacian(self):
        r"""
        This apply the Laplacian operator to ``self`` i.e. :math:`\nabla^2p`.

        Returns
        -------
        Polynomial2
            A Polynomial2 which is the result of applying the laplacian operator
            to ``self``.

        Examples
        --------

        Using the the function f(x,y)=x**3+y**2 in the standard domain
        and basis i.e. ChebyshevT in [-1, 1]:

        >>> f = lambda x, y: x**3 + y**2
        >>> # The laplacian of f is g(x,y) =6x+2
        >>> g = lambda x, y: 6 * x + 2
        >>> p = tau.Polynomial2(f)
        >>> lp = p.laplacian()

        Asserting that the error in the norm at infinity sense is less than
        1e-14:

        >>> lp.sampletest(g, 1e-14)
        True


        """
        return self.diff((2, 0)) + self.diff((0, 2))

    def poisson(self, bc=None, grid_shape=None, method=None):
        r"""
        Fast solver for Poisson equation. Let :math:`p` be a Polynomial2
        approximation of a function :math:`f` in two independent variables and
        :math:`u` an unknown function in the same variables, this method is a
        fast approximation of the poisson equation:

        .. math::
            \nabla^2u=f

        Parameters
        ----------
        bc : scalar,Polynomial2,dict, optional
            The boundary conditions. When not given we assume Dirichlet zero
            boundary conditions, The default is None.
        grid_shape : integer, tuple or list, optional
            The size of the polynomial in y and x direction. When not given
            we assume an adaptive grid. The default is None.
        method : str, optional
            The method to be used to solve the problem. When not given it chooses
            the best suitable method between 'adi' and 'fadi'
            The default is None.
        Raises
        ------
        ValueError
            DESCRIPTION.
        TypeError
            DESCRIPTION.

        Returns
        -------
        Polynomial2
            The approximate solution of the problem.

        """

        domain = self.domain
        bases = self.bases
        optx = self.optx.copy()
        opty = self.opty.copy()
        x_scale = (2 / (domain[0, 1] - domain[0, 0])) ** 2
        y_scale = (2 / (domain[1, 1] - domain[1, 0])) ** 2

        if bc is None:
            N = tau.Problem2._setup_laplace(optx, opty)
            N.bc = 0
            N.rhs = self.copy()
            return N.solve(*grid_shape)
        elif grid_shape is None:
            # if callable(bc):
            #     if bc.__code__.co_argcount != 2:
            #         raise ValueError(
            #             "When the boundary condition is a callable it must ha"
            #             "ve two arguments representing the dependent variables"
            #         )
            #         optx = tau.settings(domain=domain[0], basis=self.bases[0])
            #         opty = tau.settings(domain=domain[1], basis=self.bases[1])
            #         N.lbc = tau.Polynomial(lambda y: bc(domain[0, 0], y), opty)
            #         N.rbc = tau.Polynomial(lambda y: bc(domain[0, 1], y), opty)
            #         N.dbc = tau.Polynomial(lambda x: bc(x, domain[1, 0]), optx)
            #         N.ubc = tau.Polynomial(lambda x: bc(x, domain[1, 1]), optx)
            #         return N.solve()

            N = tau.Problem2._setup_laplace(domain)
            N.bc = bc
            N.rhs = self.copy()
            return N.solve()

        if np.isscalar(grid_shape):
            # In this case imply a NxN discretization grid
            m = grid_shape
            if m < 1 or int(m) != m:
                raise ValueError(
                    "grid_shape must be an array_like object with "
                    " two positive positive integers or a positive integer"
                )
            m = n = int(m)

        elif isinstance(grid_shape, Iterable):
            if len(grid_shape) == 2 and all(
                [np.isscalar(el) for el in grid_shape]
            ):
                m, n = grid_shape
                if m < 1 or n < 1 or int(n) != n or int(m) != m:
                    raise ValueError(
                        "grid_shape must be an array_like object with "
                        " two positive positive integers or a positive integer"
                    )
                m, n = int(m), int(n)
            else:
                raise ValueError(
                    "grid_shape must be an array_like object with "
                    " two positive positive integers or a positive integer"
                )
        else:
            raise ValueError(
                "grid_shape must be an array_like object with "
                " two positive positive integers or a positive integer"
            )

        # Compute the coeficients of the right hand side
        tol = tau.settings.defaultPrecision
        Cp, Dp, Rp = self.get_coef(dims=(m, n), fact=True)

        if isinstance(bc, Number) or callable(bc):
            bc = Polynomial2(bc, optx=optx, opty=opty)

        if not isinstance(bc, Polynomial2):
            raise TypeError(
                "The boundary condition must be a Number or a function"
            )

        if bc.optx != self.optx or bc.opty != self.opty:
            raise ValueError(
                "The boundary condition and the function must have "
                "the same domain"
            )

        lap_bc = bc.laplacian()

        if not lap_bc.iszero():
            Cbc, Dbc, Rbc = lap_bc.get_coef(dims=(m, n), fact=True)
            Cp = np.r_[Cp, Cbc]
            Dp = np.r_[Dp, -Dbc]
            Rp = np.r_[Rp, Rbc]

        # Convert the rhs to C**(3/2) coeficients

        Cp = chebt2ultra(Cp)
        Rp = chebt2ultra(Rp)
        # Construct M, the multiplication matrix for (1-x**2) in the basis
        # C**(3/2)
        jj = np.r_[0:n]
        dsub = (
            -1 / (2 * (jj + 3 / 2)) * (jj + 1) * (jj + 2) * 1 / 2 / (5 / 2 + jj)
        )
        dsup = (
            -1 / (2 * (jj + 3 / 2)) * (jj + 1) * (jj + 2) * 1 / 2 / (1 / 2 + jj)
        )
        d = -dsub - dsup
        Mn = spdiags(np.r_["0,2", dsub, d, dsup], [-2, 0, 2], n, n)
        # Construct D^{-1}, which undoes the scaling from the Laplacian identity
        InvDn = spdiags(-1 / (jj * (jj + 3) + 2), 0, n, n)

        # Construct T= Dn^(-1)*Mn
        Tn = y_scale * InvDn @ Mn

        jj = np.r_[0:m]
        dsub = (
            -1 / (2 * (jj + 3 / 2)) * (jj + 1) * (jj + 2) * 1 / 2 / (5 / 2 + jj)
        )
        dsup = (
            -1 / (2 * (jj + 3 / 2)) * (jj + 1) * (jj + 2) * 1 / 2 / (1 / 2 + jj)
        )
        d = -dsub - dsup
        Mm = spdiags(np.r_["0,2", dsub, d, dsup], [-2, 0, 2], m, m)

        # Construct D^{-1}, which undoes the scaling from the Laplacian identity
        InvDm = spdiags(-1 / (jj * (jj + 3) + 2), 0, m, m)

        # Construct T= Dm^(-1)*Mm
        Tm = x_scale * InvDm @ Mm
        A = tau.basis(opty).matrixN(m) @ Cp.T
        B = tau.basis(opty).matrixN(n) @ Rp.T
        Cp = InvDm @ Cp.T
        Rp = InvDn @ Rp.T

        if isinstance(method, str):
            method = method.lower()

        if method == "bartelsstewart":
            ######################################
            ######  Bartels-Stwart method  #######
            ######################################
            # Solve Tm@X + X@Tn.T = F using bartels-Stwart, which requires O(n**3)
            # operations
            pass
        elif method is None or method in ("adi", "fadi"):
            ######################################################
            ######  Alternating direction implicit method  #######
            ######################################################
            # Solve Tm@X + XTn = F using bartels-Stwart, which requires
            # O(n**2log(n)log(1/eps)) operations:

            # An ADI method will be used (either given by the user, or selected
            # by us.)

            # Compute Adi shifts
            a = -4 / np.pi**2 * y_scale
            b = -39 * n**-4 * y_scale
            c = 39 * m**-4 * x_scale
            d = 4 / np.pi**2 * x_scale

            [p, q] = tau.Problem2.adiShifts(a, b, c, d, tol)

            if method is None:
                # Try to pick a good method to use
                # Teste if we should use ADI or FADI
                rho = Cp.shape[1]  # Rank of rhs
                adi_test = min(m, n) < rho * p.size / 2  # Worth doing fadi
                method = "adi" if adi_test else "fadi"

            if method == "adi":
                # Use the ADI method
                X = tau.Problem2.adi(Tm, -Tn.T, (Cp * Dp) @ Rp.T, p, q)
                # Convert back to ChebyshevT
                X = ultra1mx2chebt(ultra1mx2chebt(X).T).T

                u = tau.Polynomial2(X, (self.optx, self.opty))

            else:
                # Use the FADI method

                UX, DX, VX = tau.Problem2.fadi(Tm, -Tn, Cp * Dp, Rp, p, q)

                UX, VX = ultra1mx2chebt(UX.T), ultra1mx2chebt(VX.T)
                u = tau.Polynomial2()
                u.optx = optx
                u.opty = opty
                u.cols = tau.Polynomial(UX, opty)
                u.rows = tau.Polynomial(VX, optx)
                u.diag = DX

        else:
            raise ValueError("Invalid method suplied.")

        return u + bc

    def cdr(self):
        r"""
        Give the low rank components of the aproximation:
        Let :math:`k` be the rank of the aproximation and :math:`\mathbf{c}=
        (c_0(y),\dots,c_{k-1}(y)),\ \mathbf{d}=(d_0,\dots,d_{k-1}),\ \mathbf{r}=
        (r_0(x),\dots,r_{k-1}(x))` be a vector of polynomials in the variable
        :math:`y` a vector of scalar and vector of polynomials in the variable
        :math:`x` respectively, this method returns the components of :math:`p`:
        :math:`\mathbf{c, \ d,\ r}` such that:

        .. math::
            p(x,y)=\sum_{i=0}^{k-1}r_{i}(x)d_{i}c_i(y)

        Returns
        -------
        Polynomial
            The separable component in the y direction.
        vector
            A vector which is the diagonal elements of the diagonal matrix
            of the low rank aproximation.
        Polynomial
            The separable component in the x direction.

        """
        return self.cols, self.diag, self.rows

    def sample(self, grid_shape=None, components=False):
        r"""
        Evaluate the function at an n x m ChebyshevU grid. If not given
        we assumes the length in x direction and in y direction

        Parameters
        ----------
        grid_shape : int or Iterable, optional
            An Iterable with two integer elements reprsenting the shape of
            the grid.
        components : bool
            If you want the values in the cols c(y) and the values in the diago
            nal d and values in the rows r(x) of self such (c(y).T * d) @ r(x)
            equals to self(meshgrid(x,y))

        Raises
        ------
        TypeError
            When grid shape is not int oor an array_like object with 2
            elements

        Returns
        -------
        TYPE : array_like or tuple
            The values to return acording to components

        Examples
        --------

        Using the standard domain and basis i.e. ChebyshevT in [-1, 1]:

        >>> f = lambda x, y: x**2 + y**2
        >>> domain = [[-1, 1]] * 2
        >>> p = tau.Polynomial2(f)
        >>> sample = p.sample((3, 4))
        >>> sample
        array([[1.60355339, 0.89644661, 0.89644661, 1.60355339],
               [0.85355339, 0.14644661, 0.14644661, 0.85355339],
               [1.60355339, 0.89644661, 0.89644661, 1.60355339]])

        Now giving the components:

        >>> c, d, r = p.sample((3, 4), components=True)
        >>> all((c.T * d) @ r == sample)
        True
        """

        if grid_shape is None:
            m, n = self.shape
        elif isinstance(grid_shape, Iterable):
            m, n, *_ = grid_shape
        else:
            m = n = grid_shape

        if not (isinstance(m, int) and isinstance(n, int)):
            raise TypeError(
                "Polynomial2: grid_shape must be a integer representing "
                "the dimension of a square grid or an iterable with two"
                "integers when the grid is rectangular"
            )

        c, d, r = self.cdr()
        cv, rv = c.sample(m), r.sample(n)
        if components:
            return cv, d, rv

        return (cv.T * d) @ rv

    @property
    def vscale(self):
        r"""
        This is an estimation of the max(abs(self))

        Returns
        -------
        float
            the estimation of the maximum of the absolute value of the
            Polynomial2

        """
        if self.__vscale is None:
            m, n = self.shape

            # If m if of low degree then oversample
            m = min(max(m, 9), 2000)
            n = min(max(n, 9), 2000)  # Cannot afford to go over 2000x2000
            self.__vscale = np.max(np.abs(self.sample((m, n))))
        return self.__vscale

    def truncate(self, shape):
        r"""
        This method truncates the polynomial wo the given shape

        Parameters
        ----------
        shape : int, tuple or list
            The shape of the new Polynomial2

        Returns
        -------
        aux : Polynomial2
            A polynomial2 truncated with the given shape

        Examples
        --------
        >>> f = lambda x, y: np.cos(10 * x * (1 + y**2))

        >>> domain = [[0, 1], [-1, 1]]
        >>> optx = {"basis": "ChebyshevU", "domain": [0, 1]}
        >>> opty = {"basis": "LegendreP", "domain": [-1, 1]}
        >>> opt = (optx, opty)

        >>> fp = tau.Polynomial2(f, options=opt)
        >>> fp
        Polynomial2 object:
          bases          :  ['ChebyshevU', 'LegendreP']
          domain         :  [0, 1] x [-1, 1]
          rank           :  16
          shape          :  49 x 34
          corner values  :  [1.0, 0.41, 1.0, 0.41]
          vertical scale :  1.00

        Truncate the polynomial with a new (20, 30) shape:

        >>> fp.truncate((20,30))
        Polynomial2 object:
          bases          :  ['ChebyshevU', 'LegendreP']
          domain         :  [0, 1] x [-1, 1]
          rank           :  16
          shape          :  20 x 30
          corner values  :  [1.0, 0.4, 1.0, 0.4]
          vertical scale :  1.00
        """
        aux = self.copy()
        if isinstance(shape, int):
            shape = [shape] * 2

        m, n = shape
        aux.cols = aux.cols.extend(m)
        aux.rows = aux.rows.extend(n)
        return aux

    def definite_integral(self, bounds=None):
        r"""
        Computes the definite integral with the given bounds.

        Parameters
        ----------
        bounds : tuple or list, optional
            The intervals in each variable which we want to compute the definite
            integral. When not hiven we assume the extremes fo the domains of
            each variable.

        Returns
        -------
        float
            The definite integral over ``bounds``.

        Examples
        --------
        >>> f = lambda x, y: np.cos(10 * x * (1 + y**2))

        >>> domain = [[0, 1], [-1, 1]]
        >>> optx = {"basis": "ChebyshevU", "domain": [0, 1]}
        >>> opty = {"basis": "LegendreP", "domain": [-1, 1]}
        >>> opt = (optx, opty)

        >>> fp = tau.Polynomial2(f, options=opt)
        >>> fp
        Polynomial2 object:
          bases          :  ['ChebyshevU', 'LegendreP']
          domain         :  [0, 1] x [-1, 1]
          rank           :  16
          shape          :  49 x 34
          corner values  :  [1.0, 0.41, 1.0, 0.41]
          vertical scale :  1.00

        Computing the definite integral over [0, 1] x [-1, 1]:

        >>> fp.definite_integral()
        -0.0563153056630981

        Computing the definite integral over [0, .5] x [-1, 0]:

        >>>> fp.definite_integral(((0, .5),(-1, 0)))
        -0.01715587996634457

        """
        if bounds is None:
            return (
                self.rows.definite_integral().T
                * self.diag
                @ self.cols.definite_integral()
            )
        else:
            return (
                self.rows.definite_integral(bounds[0]).T
                * self.diag
                @ self.cols.definite_integral(bounds[1])
            )

    def sum(self, axis=None):
        r"""
        Compute the definite integral over one or both axis of a Polynomial2.
        If axis=None integrate in booth direstions, if axis=0 integrate in the
        variable y if axis=1 integrate in the variable x.

        Parameters
        ----------
        axis : scalar, optional
            Can be either None, 0 or 1. The default is None.

        Raises
        ------
        ValueError
            When the axis are neither None,0 or 1

        Returns
        -------
        Polynomial or Polynomial2
            If ``axis`` is None returns a Polynomial2, if axis=0 return a Poly
            nomial in the variable x and if axis=1 return a Polynomial in the
            variable y.

        Examples
        --------
        >>> f = lambda x, y: np.cos(10 * x * (1 + y**2))

        >>> domain = [[0, 1], [-1, 1]]
        >>> optx = {"basis": "ChebyshevU", "domain": [0, 1]}
        >>> opty = {"basis": "LegendreP", "domain": [-1, 1]}
        >>> opt = (optx, opty)

        >>> fp = tau.Polynomial2(f, options=opt)
        >>> fp
        Polynomial2 object:
          bases          :  ['ChebyshevU', 'LegendreP']
          domain         :  [0, 1] x [-1, 1]
          rank           :  16
          shape          :  49 x 34
          corner values  :  [1.0, 0.41, 1.0, 0.41]
          vertical scale :  1.00

        Integrating in the y variable we get a Polynomial in the variable x:

        >>> fp.sum(axis=0)
        Polynomial object with 1 column:
          basis          : ChebyshevU
          domain         : [0, 1]
          degree         : 33
          end values     : [2.0, -0.03]
          vertical scale : 2.00

        Integrating in the x variable we get a Polynomial in the variable y:

        >>> fp.sum(axis=1)
        Polynomial object with 1 column:
          basis          : LegendreP
          domain         : [-1, 1]
          degree         : 48
          end values     : [0.05, 0.05]
          vertical scale : 0.09

        Integrating booth directions:

        >>> fp.sum()
        -0.0563153056630981

        """
        if axis is None:
            return self.definite_integral()
        elif axis == 0:
            c, d, r = self.cdr()
            opt = r.options.copy()
            opt.nequations = 1
            coef = np.einsum("ij,i->j", r.coeff, (c.sum().T * d).flatten())
            return tau.Polynomial(coef, opt)
        elif axis == 1:
            c, d, r = self.cdr()
            opt = c.options.copy()
            opt.nequations = 1

            coef = np.einsum("ij,i->j", c.coeff, (r.sum().T * d).flatten())
            return tau.Polynomial(coef, opt)
        else:
            raise ValueError(
                "axis can be: 0, meaning integrating only over y "
                "1, meaning integrating only over x and None meaning "
                "all varriables"
            )

    def qr(self):
        c, d, r = self.cdr()

        # Balance out the scaling
        sign = np.sign(d)
        c = c * sign * abs(d) ** (1 / 2)
        r = r * abs(d) ** (1 / 2)

        # QR of the columns
        q, rc = c.qr()

        # Form R
        r = r * rc.T
        return q, r.T

    def svd(self):
        c, d, r = self.cdr()

        qle, rle = c.qr()
        qri, rri = r.qr()
        u, s, v = svd((rle * d) @ rri.T)
        u = qle * u
        v = qri * v.T
        return u, s, v

    def evalm(self, x, y):
        r"""
        This is the same as evaluate self at a meshgrig(x,y).
        this function is useful because is a fast way of evaluate a
        meshgrid since it evaluate x over the rows of ``self`` and
        y over the columns of self and the values of the meshgrid(x,y) is the
        product broadcasting of the y values and the x values.
        This function is used to make the plot grid and for estimating the error
        in the norm at infinity sense.

        Parameters
        ----------
        x : array_like
            A set o points in the x-axis
        y : array_like
            A set of points in the y-axis

        Returns
        -------
        array_like
            The same as self(meshgrid(x,y))

        Examples
        --------

        Using the standard domain and basis i.e. ChebyshevT in [-1, 1]:

        >>> f = lambda x, y: x**2 + y**2
        >>> domain = [[-1, 1]] * 2
        >>> p = tau.Polynomial2(f)
        >>> x, y = [np.linspace(*domain[0])] * 2
        >>> xx, yy = np.meshgrid(x, y)
        >>> np.max(abs(p.evalm(x, y) - f(xx, yy)))
        6.661338147750939e-16
        """

        return (self.cols(y).T * self.diag) @ self.rows(x)

    def sampletest(self, fun, atol):
        r"""
        This method is a fast way to test the accuracy of the Approximation

        Parameters
        ----------
        fun : callable
            A function from which to approximate
        atol : scalar
            The absolute tollerance

        Returns
        -------
        bool
            wheter the aproximation cumply with the tolerance or not

        Examples
        --------

        Testing if the error of the approximation of cos(x**2 + y**2) in the
        standard domain and basis i.e. ChebyshevT in [-1, 1] is less than 1e-14:

        >>> f = lambda x, y: np.cos(x**2 + y**2)
        >>> domain = [[-1, 1]] * 2
        >>> p = tau.Polynomial2(f)
        >>> x, y = [np.linspace(*domain[0])] * 2
        >>> xx, yy = np.meshgrid(x, y)
        >>> p.sampletest(f, 1e-14)
        True
        """
        n = 100
        x = np.linspace(*self.domain[0], n)
        y = np.linspace(*self.domain[1], n)
        xx, yy = np.meshgrid(x, y)

        return np.max(abs(self.evalm(x, y) - fun(xx, yy))) < atol

    def fredholm1(self, rhs, method="tsve", alpha=0, eta=1, xtol=1e-05, maxfun=500):
        r"""
        Compute the solution of Fredholm integral  equation of the first kind
        that have ``self`` as kernel.


        Parameters
        ----------
        rhs : Polynomial.
            The right hand side of the first equation (see notes).
        method : str, optional
            The method for solving the equation. The default is "tsve".
        alpha : scalar, optional
            The noise level. The default is 0.
        eta : scalar, optional
            Related with the discrepancy principle, eta>=1 (see notes). The de
            fault is 1.
        xtol : float, optional
            The convergence tolerance (fminbound function). 
            The default value is 1e-5.
        maxfun : int, optional
            Maximum number of function evaluations allowed (fminbound function). 
            The default value is 500.

        Returns
        -------
        Polynomial
            The aproximate solution.


        Notes
        -----
        This compute the Fredholm integral equation of the first kind

        .. math::
            \int_{\Omega_1}k(s,t)x(t)dt=g(s), \quad s \in \Omega_2
        with a square integrable kernel k. The :math:`\Omega_i, i=1,2` are subsets
        of :math:`\mathbb{R}`.
        This equation is solved using truncated singular value expansion method
        (tsve) or Tikhonov regularization method. The parameters ``eta`` and 
        ``alpha``, which interferes on the computation of ``delta'', 
        are for the discrepancy principle:

        .. math::
            \left\|\int_{\Omega_1}k(s,t)x(t)-g^{\delta}(s)\right\|\le\eta\delta

        """
        g = rhs
        if not isinstance(g, tau.Polynomial):
            g = tau.Polynomial(g, self.optx)
        opt = g.options

        # adding continuous noise to g
        noise = tau.Polynomial.randnPol(0.01, options=opt)  # generate noise
        ng = g.norm()  # compute norm of rhs

        noise = alpha * noise * ng / noise.norm()  # adjust norm of the noise
        g_delta = g + noise  # add noise to rhs
        delta = noise.norm()  # compute norm of noise

        psi, ks, phi = self.svd()
        rk = len(ks)  # maximal rank of the separable approximation
        
        if method == "tsve":
            beta = np.zeros([rk, 1])
            for i in range(rk):
                beta[i] = (phi[i] * g_delta).definite_integral() / ks[i]
                xk = psi[0 : i + 1] * beta[0 : i + 1]

                if ((self.T * xk).sum(axis=1) - g_delta).norm() < eta * delta:
                    break

            return xk[0]

        elif method == "tr":

            def errlambda(lam, sigma, gnoise, psi, ke, rk, eta, e, dd):
                beta = dd * sigma / (sigma**2 + lam**2)
                x = psi * beta.reshape(-1, 1)
                return abs(
                    (((ke.T * x).sum(1) - gnoise).norm()) ** 2
                    - eta**2 * e**2
                )

            dd = (phi * g_delta).sum()

            # Solving minimization problem for lambda
            eta = 1
            lam = fminbound( 
                    lambda x: errlambda(x, ks, g_delta, psi, self, rk, eta, delta, dd),
                  0, 2, xtol=xtol, maxfun=maxfun)
            beta2 = ks / (ks**2 + lam**2)
            beta = (dd * beta2).reshape(-1, 1)

            # tikonov relative error
            xlam = psi * beta

            return xlam[0]
        else:
            raise ValueError("Possible methods are 'tsve' and 'tr")
