# Copyright (C) 2021, University of Porto and Tau Toolbox developers.
#
# This file is part of Tautoolbox package.
#
# Tautoolbox is free software: you can redistribute it and/or modify it
# under the terms of version 3 of the GNU Lesser General Public License as
# published by the Free Software Foundation.
#
# Tau Toolbox is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser
# General Public License for more details.

import tautoolbox as tau
import numpy as np
from numpy import linalg as lin
from copy import deepcopy
from scipy.linalg import lu
from warnings import warn


def matrix(op, problem, h):
    op = tau.utils.chooseOperator(op)
    if not isinstance(problem, tau.problem):
        raise TypeError("Tautoolbox: the 2nd argument must be a tau.problem")

    # set definitions
    equations = problem.equations
    conditions = problem.conditions
    options = deepcopy(problem.options)

    # define for conveniency (shorter to type and read)
    nequations = problem.options.nequations
    n = problem.options.n

    nu = problem.nconditions
    h = np.max(problem.height)

    he = np.zeros(problem.nequations, dtype=int)
    for i in range(problem.nequations):
        he[i] = max(problem.height[i])
    he += 1
    cols = np.arange(n).reshape(1, -1) + (n + nu + h) * np.arange(
        nequations
    ).reshape(-1, 1)

    options.n = n + nu + h
    basis = tau.basis(problem.options)

    if op in "condition c".split():
        return basis.conditions(conditions)[0]

    elif op in ["operator", "d"]:
        out = np.zeros((0, np.prod(cols.shape)))
        for neq in range(nequations):
            D = (
                problem.equations[neq]
                .lhs(tau.Polynomial(options=options), tau.operator(options))
                .mat
            )
            rows = np.arange(n - problem.condpart[neq]).reshape(-1, 1)
            out = np.r_[out, D[rows, cols.reshape(1, -1)]]
        return out

    elif op in ["taumatrix", "t"]:
        out = basis.conditions(conditions)[0]
        for neq in range(nequations):
            D = (
                problem.equations[neq]
                .lhs(tau.Polynomial(options=options), tau.operator(options))
                .mat
            )
            rows = np.arange(n - problem.condpart[neq]).reshape(-1, 1)
            out = np.r_[out, D[rows, cols.reshape(1, -1)]]
        return out

    elif op in ["taurhs", "b"]:
        out = basis.conditions(conditions)[1]
        for neq in range(nequations):
            basis = tau.basis(options)
            rows = np.arange(n - problem.condpart[neq])
            out = np.r_[out, basis.interp1(equations[neq].rhs, rows[-1] + 1)]
        return out.reshape(-1, 1)
    elif op in ["tauresidual", "r"]:
        out = np.zeros((0, np.prod(cols.shape)))
        for neq in range(nequations):
            D = (
                problem.equations[neq]
                .lhs(tau.Polynomial(options=options), tau.operator(options))
                .mat
            )
            rows = np.arange(n, n + 1 + he[neq]).reshape(-1, 1)
            out = np.r_[out, D[rows, cols.reshape(1, -1)]]
        return out
    elif op in ["multiplication", "m"]:
        return basis.matrixM(n + h)
    elif op in ["differentiation", "n"]:
        return basis.matrixN(n + h)
    elif op in ["integration", "o"]:
        return basis.matrixO(n + h)


def solve(problem):
    if not isinstance(problem, tau.problem):
        raise TypeError("Tautoolbox: the input argument must be a tau.problem")

    if problem.nequations == 1:
        if problem.isLinearized:
            return tau.ode.nonlinear(problem)
        else:
            return linear(problem)
    else:
        if problem.isLinearized:
            return tau.ode.nonlinearSystem(problem)
        else:
            return linearSystem(problem)


def linear(problem):
    if not isinstance(problem, tau.problem):
        raise TypeError("Tautoolbox: the input argument must be a tau.problem")

    equation = problem.equations[0]
    options = deepcopy(problem.options)
    n = options.n
    nu = problem.nconditions
    h = problem.height[0][0]

    T = np.zeros((n + nu + n, n))
    if options.quadPrecision:
        T = tau.utils.quadprecision(T)

    basis = tau.basis(options)
    C, bs = basis.conditions(problem.conditions)

    options.n = n + h
    D = equation.lhs(
        tau.Polynomial(options=options), tau.operator(options)
    ).mat
    T = np.r_[C, D[:, :n]]

    bf = basis.interp1(equation.rhs, n - nu)
    b = tau.Polynomial(np.r_[bs, bf], problem.options)
    # yn = tau.Polynomial(np.linalg.solve(T[:n, :], b), problem.options)

    yn = b.mldivide(T[:n])

    residual_v = T[:n] @ yn.coeff - b.coeff
    tauresidual_v = T[n:] @ yn.coeff
    info = {
        "algorithm": options.solver,
        "iteration": yn.iter,
        "cond": lin.cond(T[:n, :n]),
        "residual": lin.norm(residual_v),
        "tauresidual": lin.norm(tauresidual_v),
    }

    return (
        yn,
        info,
        tau.Polynomial(residual_v, problem.options),
        tau.Polynomial(
            np.r_[np.zeros(n - nu), tauresidual_v], problem.options
        ),
    )


def Schur(problem):
    if not isinstance(problem, tau.problem):
        raise TypeError("Tautoolbox: the input argument must be a tau.problem")

    problem = deepcopy(problem)

    equation = problem.equations[0]
    #  domain = problem.domain
    conditions = problem.conditions

    options = problem.options
    options.n = options.schurMaxDim
    n = options.schurInitialLU
    m = options.schurSizeBlock
    nu = problem.nconditions
    T = np.zeros((options.n, options.n))
    b = np.zeros(options.n)
    basis = tau.basis(options)

    C, b[:nu] = basis.conditions(conditions)
    D = equation.lhs(tau.Polynomial(options=options), tau.operator(options))

    T[:nu] = C
    T[nu:] = D.mat[:-nu]
    b[nu:n] = basis.interp1(equation.rhs, n - nu)
    P, L, U = lu(T[:n, :n])
    a = lin.solve(U, lin.solve(L, P @ b[:n]))
    tau_res = lin.norm(T[n : n + 1 + m, :n] @ a)

    while tau_res > options.schurPrecision and n + m <= options.schurMaxDim:
        n = n + m
        a = solve(T[:n, :n], b[:n])
        tau_res = lin.norm(T[n : n + m, :n] @ a)

    options.n = n

    if tau_res > options.schurPrecision:
        warn(
            "Tautoolbox: Required accuracy not achieved. you may increase "
            "parameter schurMaxDim"
        )

    return tau.Polynomial(a, options)


def linearSystem(problem):
    if not isinstance(problem, tau.problem):
        raise TypeError("Tautoolbox: the input argument must be a tau.problem")

    problem = deepcopy(problem)

    equations = problem.equations
    #  domain = problem.domain
    conditions = problem.conditions
    options = deepcopy(problem.options)

    nequations = problem.options.nequations
    n = problem.options.n
    nu = problem.nconditions
    basis = tau.basis(problem.options)

    # incorporate the conditions in the system matrix and independent vector
    C, bs = basis.conditions(conditions)

    T = C

    b = bs

    h = np.max(problem.height)
    options.n = n + nu + h

    cols = np.arange(n).reshape(1, -1) + (n + nu + h) * np.arange(
        nequations
    ).reshape(-1, 1)

    # print(b.shape)
    for neq in range(nequations):
        opts = deepcopy(options)
        opts.nequations = 1
        D = (
            problem.equations[neq]
            .lhs(tau.Polynomial([0, 1], options=opts), tau.operator(options))
            .mat
        )

        rows = np.arange(n - problem.condpart[neq]).reshape(-1, 1)
        T = np.r_[T, D[rows, cols.reshape(1, -1)]]
        # print(rows[-1, 0] + 1)

        b = np.r_[
            b, basis.interp1(equations[neq].rhs, rows[-1, 0] + 1)
        ]  # rows[-1, 0] + 1

    # Solving the associated linear system

    b = tau.Polynomial(b, problem.options)

    return b.mldivide(T)
