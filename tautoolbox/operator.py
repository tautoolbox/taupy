# Copyright (C) 2021, University of Porto and Tau Toolbox developers.
#
# This file is part of Tautoolbox package.
#
# Tautoolbox is free software: you can redistribute it and/or modify it
# under the terms of version 3 of the GNU Lesser General Public License as
# published by the Free Software Foundation.
#
# Tau Toolbox is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser
# General Public License for more details.

import tautoolbox as tau
import numpy as np
from copy import deepcopy
import numbers


class operator:
    def __init__(self, options=None):
        if options is None:
            options = tau.settings()
        if not isinstance(options, tau.settings):
            raise TypeError(
                "tautoolbox: The optional argument must be of class tau.settings "
            )

        self.options = options

        self.basis = tau.basis(options)
        if options.quadPrecision:
            self.domain = tau.utils.quadprecision(options.domain)
        else:
            self.domain = options.domain

        self.n = options.n
        if options.nequations == 1:
            self.activeVar = [1]
            self.opMat = [
                tau.utils.quadprecision(np.eye(options.n))
                if options.quadPrecision
                else np.eye(options.n)
            ]

        else:
            self.activeVar = [0] * options.nequations
            self.opMat = [None] * options.nequations

        self.hdiff = [None] * options.nequations
        self.odiff = [None] * options.nequations
        self.hasIntegral = [False] * options.nequations

        for k in range(options.nequations):
            self.hdiff[k] = np.zeros((1, 2), dtype=int)
            self.odiff[k] = 0

        self.N = self.basis.matrixN()

    def diff(self, order=1):
        obj = deepcopy(self)

        for k in range(obj.options.nequations):
            if obj.activeVar[k] == 0:
                continue
            obj.odiff[k] += order
            for i in range(obj.hdiff[k].shape[0]):
                obj.hdiff[k][i, 0] += order

            obj.opMat[k] = np.linalg.matrix_power(obj.N, order) @ obj.opMat[k]
        return obj

    def integral(self, order=1):
        obj = deepcopy(self)

        matO = self.basis.matrixO()
        for k in range(obj.options.nequations):
            if obj.activeVar[k] == 0:
                continue
            obj.odiff[k] -= order
            for i in range(obj.hdiff[k].shape[0]):
                obj.hdiff[k][i, 0] -= order
            obj.opMat[k] = np.linalg.matrix_power(matO, order) @ obj.opMat[k]
            obj.hasIntegral[k] = True
        return obj

    def fred(self, K=1):
        obj = deepcopy(self)
        outer = obj.options.outer if hasattr(obj, "outer") else obj.domain

        if callable(K):
            # K = obj.interporth2inc(K)[0].T
            K = tau.Polynomial2(
                K, ({"basis": self.basis.name, "domain": self.domain},) * 2
            ).coef

        Pa, Pb = obj.basis.polyvalb(outer)
        for k in range(obj.options.nequations):
            if self.activeVar[k] == 0:
                continue
            obj.odiff[k] -= 1
            for i in range(obj.hdiff[k].shape[0]):
                obj.hdiff[k][i, 0] -= 1
                obj.hdiff[k][i, 1] += K.shape[0]

            # Compute the fredholm terms
            opmat = np.zeros((obj.n, obj.n))

            if obj.options.quadPrecision:
                opmat = tau.utils.quadprecision(opmat)
            matO = obj.basis.matrixO()
            for i in range(K.shape[1]):
                for j in range(K.shape[0]):
                    coeff = np.zeros(j + 1)
                    coeff[j] = 1
                    PjM = obj.basis.polyvalmM(coeff)
                    opmat += K[j, i] * (
                        tau.operator.epow(i, obj.n) @ [Pb - Pa] @ matO @ PjM
                    )
            obj.opMat[k] = opmat @ obj.opMat[k]
            obj.hasIntegral[k] = True
        return obj

    def volt(self, K=1):
        obj = deepcopy(self)
        outer = obj.options.outer if hasattr(obj, "outer") else obj.domain

        if callable(K):
            # K = obj.interporth2inc(K)[0].T
            K = tau.Polynomial2(
                K, ({"basis": self.basis.name, "domain": self.domain},) * 2
            ).coef

        Pa = obj.basis.polyvalb(outer[0])
        for k in range(obj.options.nequations):
            if self.activeVar[k] == 0:
                continue
            obj.odiff[k] -= 1
            for i in range(obj.hdiff[k].shape[0]):
                obj.hdiff[k][i, 0] -= 1
                obj.hdiff[k][i, 1] += K.shape[0]

            # Compute the fredholm terms
            matO = obj.basis.matrixO()
            firstrow = np.zeros(obj.n)
            for j in range(obj.n - 1):
                firstrow[j] = obj.basis.polyval(
                    np.r_[0, matO[1 : j + 2, j]], outer[0]
                )

            matO[0, :] = firstrow
            opmat = np.zeros((obj.n, obj.n))
            for i in range(K.shape[1]):
                coeff = np.zeros(i + 1)
                coeff[i] = 1
                PiM = obj.basis.polyvalmM(coeff)
                for j in range(K.shape[0]):
                    coeff = np.zeros(j + 1)
                    coeff[j] = 1
                    PjM = obj.basis.polyvalmM(coeff)
                    opmat += K[j, i] * (
                        (PiM - tau.operator.epow(i, obj.n) @ Pa) @ matO @ PjM
                    )
            obj.opMat[k] = opmat @ obj.opMat[k]
            obj.hasIntegral[k] = True
        return obj

    def fred1(self, k, options):
        aux = deepcopy(self)
        # the parameters defaulsts
        if options is None:
            options = (tau.settings(), tau.settings())

        aux.isfred1 = True
        aux.kernel = tau.Polynomial2(k, options)
        return aux

    def __add__(self, rhs):
        obj = deepcopy(self)

        if all([isinstance(obj, tau.operator), isinstance(rhs, tau.operator)]):
            for k in range(obj.options.nequations):
                if obj.activeVar[k] and rhs.activeVar[k]:
                    obj.hdiff[k] = np.r_[obj.hdiff[k], rhs.hdiff[k]]
                    obj.odiff[k] = max(obj.odiff[k], rhs.odiff[k])
                    obj.opMat[k] = obj.opMat[k] + rhs.opMat[k]
                    obj.hasIntegral[k] = (
                        obj.hasIntegral[k] or rhs.hasIntegral[k]
                    )
                elif rhs.activeVar[k]:
                    obj.activeVar[k] = 1
                    obj.hdiff[k] = rhs.hdiff[k]
                    obj.odiff[k] = rhs.odiff[k]
                    obj.opMat[k] = rhs.opMat[k]
                    obj.hasIntegral[k] = rhs.hasIntegral[k]
        elif isinstance(rhs, tau.Polynomial):
            return obj
        else:
            raise TypeError("tautoolbox: you can only sum two operator objects")

        return obj

    def __pos__(self):
        return deepcopy(self)

    def __neg__(self):
        obj = deepcopy(self)
        for k in range(obj.options.nequations):
            if obj.activeVar[k]:
                obj.opMat[k] = -obj.opMat[k]
        return obj

    def __sub__(self, rhs):
        return self + (-rhs)

    def __mul__(self, rhs):
        obj = deepcopy(self)
        if isinstance(rhs, tau.operator):
            raise ValueError(
                "Tautoolbox: support for non linear terms "
                " is not yet automatic. Please linearize the equation "
            )

        if isinstance(rhs, numbers.Number):
            for k in range(obj.options.nequations):
                if obj.activeVar[k]:
                    obj.opMat[k] *= rhs

        elif isinstance(rhs, tau.Polynomial):
            for k in range(obj.options.nequations):
                if obj.activeVar[k]:
                    obj.hdiff[k][-1, 1] += len(rhs.coeff)
                    obj.opMat[k] = obj.basis.polyvalmM(rhs.coeff) @ obj.opMat[k]

        else:
            raise TypeError(
                "tautoolbox: you can only multyply a tau.operator by a "
                "number or a tau.Polynomial"
            )
        return obj

    def __rmul__(self, lhs):
        return deepcopy(self) * lhs

    def __truediv__(self, rhs):
        if not isinstance(rhs, numbers.Number):
            raise TypeError(
                "tautoolbox: you can only divide a tau.operator for a scalar "
            )

        obj = deepcopy(self)
        for k in range(obj.options.nequations):
            if obj.activeVar[k]:
                obj.opMat[k] = obj.opMat[k] / rhs
        return obj

    def opHeight(self):
        return [max(max(hd[:, 1] - hd[:, 0]), 0) for hd in self.hdiff]

    def interporth2inc(self, Fxt):
        inc = 3
        tool = 1e-14

        k = 5
        cont = 1
        warn = 0
        err = np.array([[1, np.inf]])

        while all(err[-1] > tool):
            f, esys, eeval, edom = self.interporth2(Fxt, k)
            err = np.r_[err, [[k, eeval]]]
            if (err[cont, 1] > err[cont - 1, 1]) and (
                np.abs(err[cont, 1] - err[cont - 1, 1]) < 1e-10
            ):
                warn += 1

            if warn == 5:
                idxm = np.argmin(err[:, 1])
                f, esys, eeval, edom = self.interporth2(Fxt, int(err[idxm, 0]))
                break
            cont += 1
            k += inc

        return f, esys, eeval, edom

    def interporth2(self, Fxt, n):
        obj = deepcopy(self)

        obj.options.n = n
        basis = tau.basis(obj.options)

        # Creating the points of choosen orthogonal Polynomial basis
        x_points = basis.nodes()
        t_points = basis.nodes()

        # Memory allocation for Px, Pt, X and T
        Px = np.zeros((n, n))
        Pt = np.zeros((n, n))
        X = np.zeros((n, n))
        T = np.zeros((n, n))

        # Evaluate Px and Pt with polyval and allocate X and T
        coeff = np.zeros(n)
        for j in range(n):
            coeff[j] = 1
            Px[j, :] = basis.polyval(coeff, x_points)
            Pt[j, :] = basis.polyval(coeff, x_points)
            X[j, :] = x_points[j]
            T[:, j] = t_points[j]
            coeff[j] = 0

        # Allocate the elements of Ax = Px(X) and Ay = Pt(T).
        Ax, At = np.zeros((n**2, n**2)), np.zeros((n**2, n**2))
        for i in range(n):
            for j in range(n):
                Ax[i * n : (i + 1) * n, j * n : (j + 1) * n] = Px[j, i]
                At[i * n : (i + 1) * n, j * n : (j + 1) * n] = Pt.T

        # Obtaining the function Fxt and evaluating it at the b vector
        b = Fxt(X.reshape(n**2), T.reshape(n**2))
        # Solving the system and changing the shape.
        solution = np.linalg.lstsq(Ax * At, b, rcond=None)[0]

        f = solution.reshape((n, n)).T
        esys = np.max(np.abs(Ax * At @ solution - b))
        eeval, X, T, F = tau.operator.interporth2_error(obj, solution, Fxt)
        edom = [X, T, F.reshape((n, n))]

        return f, esys, eeval, edom

    @staticmethod
    def interporth2v2(Fxt, n, bases=["ChebyshevT"] * 2, domain=[[-1, 1]] * 2):
        """
        A function that interpolate a 2-dimensional function in  given two bases,
        one for each variable and a domain in a grid mxn

        Parameters
        ----------
        Fxt : callable
            A 2-dimanesional anonymous function.
        n : int or iterable
            The dimension of the grid. When n is an int means a square grid of
            dimension n. When an iterable n must be length 2 where where the
            first element is the number of pooints in x direction and the se-
            cond element is the  the number of points in the y direction.
            number of points in the
        bases : Iterable, optional
            The bases for each variable, must be a length 2 iterable of strings.
            The default is ["ChebyshevT"] * 2.
        domain : array_like, optional
            The domain of interpolation. Must be a 2x2 array_like structure
            . The default is [[-1, 1]] * 2.

        Returns
        -------
        f : array_like
            A nxm array_like object with the coefficients of the interpolation
            Polynomial.
        esys : float
            The residual.
        eeval : flaot
            An estimation of the absolute error.
        edom : tuple
            A tuple with the meshgrid and the difference betwen the function
            and the interpolation Polynomial evaluated at the meshgrid.

        """

        if isinstance(n, int):
            n = [n] * 2

        bas = [
            tau.basis(
                tau.settings(degree=n[i] - 1, basis=bases[i], domain=domain[i])
            )
            for i in [0, 1]
        ]

        # Creating the points of choosen orthogonal Polynomial basis
        x_points = bas[0].nodes()
        t_points = bas[1].nodes()

        # Memory allocation for Px, Pt, X and T
        Px = bas[0].polyvalv2(np.eye(n[0]), x_points).T
        Pt = bas[1].polyvalv2(np.eye(n[1]), t_points).T
        X, T = np.meshgrid(x_points, t_points, indexing="ij")

        # Obtaining the function Fxt and evaluating it at the b vector
        b = Fxt(X.reshape(np.prod(n)), T.reshape(np.prod(n)))
        A = np.kron(Px, Pt)
        # Solving the system and changing the shape.

        solution = np.linalg.solve(A, b)

        f = solution.reshape((n[1], n[0]), order="F")
        esys = np.max(np.abs(A @ solution - b))
        eeval, X, T, F = tau.operator.interporth2_errorv2(
            solution, Fxt, n=n, bases=bases, domain=domain
        )
        edom = [X, T, F.reshape((n[1], n[0]), order="F")]

        return f, esys, eeval, edom

    @staticmethod
    def interporth2_error(obj, solution, Fxt):
        n, domain = obj.options.n, obj.options.domain

        basis = tau.basis(obj.options)

        Px, Pt, X, T = [np.zeros((n, n)) for i in range(4)]
        x_points = t_points = np.linspace(*domain, n)

        for j in range(n):
            coeff = np.zeros(j + 1)
            coeff[j] = 1
            Px[j, :] = basis.polyval(coeff, x_points)
            Pt[j, :] = basis.polyval(coeff, t_points)
            X[j, :n] = x_points[j]
            T[:n, j] = t_points[j]

        Ax, At = np.zeros((n**2, n**2)), np.zeros((n**2, n**2))
        for i in range(n):
            for j in range(n):
                Ax[i * n : (i + 1) * n, j * n : (j + 1) * n] = Px[j, i]
                At[i * n : (i + 1) * n, j * n : (j + 1) * n] = Pt.T

        F = Ax * At @ solution - Fxt(X.reshape(n**2), T.reshape(n**2))
        f = np.max(np.abs(F))

        return f, X, T, F

    @staticmethod
    def interporth2_errorv2(
        solution, Fxt, n, bases=["ChebyshevT"] * 2, domain=[[-1, 1]] * 2
    ):
        if isinstance(n, int):
            n = [n] * 2

        bas = [
            tau.basis(
                tau.settings(degree=n[i], basis=bases[i], domain=domain[i])
            )
            for i in [0, 1]
        ]

        x_points = np.linspace(*domain[0], n[0])
        t_points = np.linspace(*domain[1], n[1])
        Px = bas[0].polyvalv2(np.eye(n[0]), x_points).T
        Pt = bas[1].polyvalv2(np.eye(n[1]), t_points).T
        X, T = np.meshgrid(x_points, t_points, indexing="ij")

        F = np.kron(Px, Pt) @ solution - Fxt(
            X.reshape(np.prod(n)), T.reshape(np.prod(n))
        )
        f = np.max(np.abs(F))

        return f, X, T, F

    @staticmethod
    def epow(i, n):
        f = np.zeros((n, 1))
        if i >= 0 and i < n:
            f[i, 0] = 1
        return f

    def __getitem__(self, index):
        obj = deepcopy(self)
        # if isinstance(index, slice):
        #     start = 0 if index.start == None else index.start
        #     stop = self._size if index.stop == None else index.stop
        #     step = 1 if index.step == None else index.step

        #     if type(step) == type(stop) == type(step) == int:
        #         tmp = LinkedList()
        #         f
        #     else:
        #         raise TypeError("All slice atributes must be integer or None")

        if obj.options.nequations == 1:
            if index == 0:
                return obj
            else:
                raise IndexError(
                    "Index out of range - the index must be"
                    f" at most {obj.options.nequations -1}"
                )

        if any(obj.activeVar):
            raise IndexError("Tautoolbox: operation already defined")

        if index <= obj.options.nequations:
            obj.activeVar[index - 1] = 1
            obj.opMat[index - 1] = np.eye(obj.n)
            if obj.options.quadPrecision:
                obj.opMat[index - 1] = tau.utils.quadprecision(
                    obj.opMat[index - 1]
                )
        else:
            raise IndexError(
                "Index out of range - the index must be"
                f" at most {obj.options.nequations -1}"
            )

        return obj

    @property
    def mat(self):
        m = np.zeros((self.n, self.options.nequations * self.n))
        if self.options.quadPrecision:
            m = tau.utils.quadprecision(m)

        for k in range(self.options.nequations):
            if self.activeVar[k]:
                m[:, k * self.n : (k + 1) * self.n] = self.opMat[k]
        return m
