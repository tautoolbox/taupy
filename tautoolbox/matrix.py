# Copyright (C) 2021, University of Porto and Tau Toolbox developers.
#
# This file is part of Tautoolbox package.
#
# Tautoolbox is free software: you can redistribute it and/or modify it
# under the terms of version 3 of the GNU Lesser General Public License as
# published by the Free Software Foundation.
#
# Tau Toolbox is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser
# General Public License for more details.

import tautoolbox as tau


def matrix(op, problem=None, h=None):
    op = tau.utils.chooseOperator(op)

    if problem is None:
        problem = tau.settings()

    if isinstance(problem, tau.problem):
        if h is None:
            h = problem.height

        if problem.kind in ["ide", "ode"]:
            return tau.ode.matrix(op, problem, h)
        elif problem.kind == "eig":
            return tau.eig.matrix(op, problem, h)
    elif isinstance(problem, tau.settings):

        basis = tau.basis(problem)

        if op in ["m", "multiplication"]:
            return basis.matrixM()
        elif op in ["n", "differentiation"]:
            return basis.matrixN()
        elif op in ["o", "integration"]:
            return basis.matrixO()
        elif op in ["v", "orth2pow"]:
            return basis.orth2powmatrix()
        elif op in ["w", "pow2orth"]:
            return basis.pow2orthmatrix()

    else:
        raise TypeError(
            f"Tau Toolbox: the operator {op} requires a "
            "tau.problem  as second argument"
        )
