# Copyright (C) 2021, University of Porto and Tau Toolbox developers.
#
# This file is part of Tautoolbox package.
#
# Tautoolbox is free software: you can redistribute it and/or modify it
# under the terms of version 3 of the GNU Lesser General Public License as
# published by the Free Software Foundation.
#
# Tau Toolbox is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser
# General Public License for more details.

import numpy as np
from typing import Sequence
from copy import deepcopy

import tautoolbox as tau

eps = np.spacing(1)  # the same  sys.float_info.epsilon
Basis = (
    "ChebyshevT ChebyshevU LegendreP GegenbauerC "
    "PowerX BesselY ChebyshevV ChebyshevW HermiteH LaguerreL"
).split()
Solver = (
    "Gaussian gmres bicgstab bicg qmr tfqmr qr" "bicgstabl minres lsqr".split()
)
Precond = "none ilu ndiag".split()
Milu = "off col row".split()
Typeilu = "nofill crout ilutp".split()
Color = "blue black red green yellow magenta cyan white".split()


# @dataclass(init=False)
class settings:
    basis: str = "ChebyshevT"
    degree: int = 31
    domain: Sequence = np.array([-1, 1])
    kind: str = "auto"
    alpha: float = 0.5
    defaultPrecision: float = eps
    quadPrecision: bool = False
    pieces: int or list = 1
    nequations: int = 1
    iterativeTol: float = 1e-9
    steps: int = 100
    interpRelTol: float = eps
    interpMaxDim: int = 2**16
    solver: str = "Gaussian"
    precond: str = "none"
    milu: str = "off"
    typeilu: str = "nofill"
    droptol: float = 0.1
    thresh: int = 1
    udiag: int = 0
    numbd: int = 1
    nonlinTol: float = 1e-10
    nonlinMaxIt: int = 100
    schurInitialLU: int = 32
    schurMaxDim: int = 256
    schurPrecision: float = 10 * eps
    schurSizeBlock: int = 10
    lineColor: str = "blue"
    lineStyle: str = "-"
    lineWidth: float = 2.0
    _session = None
    _defaults = None

    def __init__(self, **kwargs):
        self.__dict__ = {
            key: tau.settings.__dict__[key]
            for key in tau.settings.__annotations__.keys()
        }

        di = set(kwargs.keys()) - set(self.__dict__.keys())
        if di != set():
            raise ValueError(f"Tautoolbox: {di} is not recognized parameters")

        if "basis" in kwargs.keys():
            if kwargs["basis"] not in Basis:
                raise ValueError(f"Tautoolbox: possible basis are in {Basis}")
        if "precond" in kwargs.keys():
            if kwargs["precond"] not in Precond:
                raise ValueError(
                    f"Tautoolbox: possible preconds are in {Precond}"
                )
        if "solver" in kwargs.keys():
            if kwargs["solver"] not in Solver:
                raise ValueError(
                    f"Tautoolbox: possible solvers are in {Solver}"
                )
        if "milu" in kwargs.keys():
            if kwargs["milu"] not in Milu:
                raise ValueError(f"Tautoolbox: possible milu are in {Milu}")
        if "typeilu" in kwargs.keys():
            if kwargs["typeilu"] not in Typeilu:
                raise ValueError(
                    f"Tautoolbox: possible typeilu are in {Typeilu}"
                )
        if "color" in kwargs.keys():
            if kwargs["color"] not in Color:
                raise ValueError(f"Tautoolbox: possible colors are in {Color}")

        self.__dict__.update(kwargs)

        if self.alpha is None:
            self.alpha = 0.5
        # if self.basis == "LaguerreL" and kwargs.get("domain", None) is None:
        #     self.domain = np.array([0, np.inf])

    @property
    def npieces(self):
        return self.pieces

    @property
    def n(self):
        return self.degree + 1

    @n.setter
    def n(self, value):
        self.degree = value - 1

    # set the settings for the current session

    @classmethod
    def session_(cls, **kwargs):
        session_fields = set(kwargs.keys())
        possible_fields = set(cls.__annotations__.keys())
        if (session_fields & possible_fields) != session_fields:
            raise TypeError(
                f"Tautoolbox: possible input fields are {possible_fields}"
            )

        cls.reset()

        cls._defaults = {
            key: cls.__dict__[key] for key in cls.__annotations__.keys()
        }

        for key, value in zip(kwargs.keys(), kwargs.values()):
            setattr(tau.settings, key, value)

    # reset the object settings

    @classmethod
    def reset(cls):
        if cls._defaults is None:
            return
        for key, value in cls._defaults.items():
            setattr(tau.settings, key, value)
        cls._defaults = None

    @classmethod
    def defaults(cls):
        if cls._defaults is not None:
            return cls._defaults
        return {key: cls.__dict__[key] for key in cls.__annotations__.keys()}

    def __getdict__(self):
        return {key: value for key, value in self.__dict__.items()}

    def _dict_vertical_disp(self, di):
        aux_str = ""
        for key, value in di.items():
            aux_str = aux_str + f"{key} : {value} \n"
        return aux_str

    def __str__(self):
        return self._dict_vertical_disp(self.__dict__)

    def __repr__(self):
        return self.__str__()

    def copy(self):
        return deepcopy(self)

    def __eq__(self, rhs):
        if isinstance(rhs, tau.settings):
            if (
                self.basis == rhs.basis
                and (np.array(self.domain) == np.array(rhs.domain)).all()
            ):
                if self.basis == "GegenbauerC" and self.alpha != rhs.alpha:
                    return False
                return True
            return False
        return False
