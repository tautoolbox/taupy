# Copyright (C) 2021, University of Porto and Tau Toolbox developers.
#
# This file is part of Tautoolbox package.
#
# Tautoolbox is free software: you can redistribute it and/or modify it
# under the terms of version 3 of the GNU Lesser General Public License as
# published by the Free Software Foundation.
#
# Tau Toolbox is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser
# General Public License for more details.

"""
A module for efficiently deal with orthogonal Polynomial basis.

Within the documentation for this module, a "finite power series,"
i.e., a Polynomial (also referred to simply as a "series") is represented
by a 1-D numpy array of the Polynomial's coefficients, ordered from lowest
order term to highest.  For example, array([1,2,3]) represents
``P_0 + 2*P_1 + 3*P_2``, where P_n is the n-th order basis Polynomial
applicable to the specific module in question, e.g., `Polynomial` (which
"wraps" the "standard" basis) or `chebyshev`.  For optimal performance,
all operations on Polynomials, including evaluation at an argument, are
implemented as operations on the coefficients.  Additional (module-specific)
information can be found in the docstring for the module of interest.
"""

import numpy as np
import tautoolbox as tau
from numbers import Number
from abc import abstractmethod
from copy import deepcopy
from numpy.polynomial import legendre
from numpy.linalg import eig
from scipy.fft import fft, ifft
from scipy.sparse import spdiags
from scipy.sparse.linalg import spsolve
from scipy.special import gamma, beta

from tautoolbox.auxiliary import (
    chebt2leg,
    chebt2kind,
    jac2chebt,
    jac2jac,
    scl,
)

supported = "ChebyshevT ChebyshevU LegendreP GegenbauerC PowerX".split()

experimental = "BesselY HermiteH LaguerreL ChebyshevV ChebyshevW".split()


class basis:
    def __init__(self, options=None):
        if options is None:
            options = tau.settings()
        if not isinstance(options, tau.settings):
            raise ValueError(
                "a basis requires a tau.settings arguments (if none the default is used)"
            )

        self.options = options

        if self.options.quadPrecision:
            self.domain = tau.utils.quadprecision(self.options.domain)
        else:
            self.domain = self.options.domain

        self.p1 = np.array([-self.beta(0) / self.alpha(0), 1 / self.alpha(0)])

        self.support = np.array([-1, 1])
        self.c1 = 2 / np.diff(self.domain).item()
        self.c0 = -sum(self.domain) / np.diff(self.domain).item()

        A = np.array(
            [[1, self.p1[1] * self.c0 + self.p1[0]], [0, self.p1[1] * self.c1]]
        )

        self.x1 = np.linalg.solve(A, [0, 1])

    @abstractmethod
    def alpha(self, n):
        pass

    @abstractmethod
    def beta(self, n):
        pass

    @abstractmethod
    def gamma(self, n):
        pass

    @abstractmethod
    def eta(self, i, j):
        pass

    @abstractmethod
    def theta(self, n):
        pass

    def matrixM(self, n=None):
        r"""
        If C is an orthogonal series then this method give the matrix product
        of C and x such that :math:`Cx=MC`.

        Parameters
        ----------
        n : int, optional
            The dimesnion of the matrix,when not given we take the default di-
            mension of the basis options. The default is None.

        Returns
        -------
        M : array_like
            The ``n``x`ǹ`` product matrix.

        See Also
        --------

        Examples
        --------
        Using ChebyshevT basis in the default domain [-1,1]:

        >>> import tautoolbox as tau
        >>> import numpy as np
        >>> b = tau.basis()
        >>> c = np.arange(5)
        >>> c
        array([0, 1, 2, 3, 4])

        >>> m = b.matrixM(6)
        >>> m
        array([[0. , 0.5, 0. , 0. , 0. , 0. ],
               [1. , 0. , 0.5, 0. , 0. , 0. ],
               [0. , 0.5, 0. , 0.5, 0. , 0. ],
               [0. , 0. , 0.5, 0. , 0.5, 0. ],
               [0. , 0. , 0. , 0.5, 0. , 0.5],
               [0. , 0. , 0. , 0. , 0.5, 0. ]])

        >>> m @ np.r_[c, 0]
        array([0.5, 1. , 2. , 3. , 1.5, 2. ])
        """
        if n is None:
            n = self.options.n

        f2 = 1 / self.c1
        f3 = -self.c0 / self.c1

        e0 = np.arange(n)
        e1 = np.arange(n - 1)
        M = (
            np.diag(f2 * self.alpha(e1) + 0 * e1, -1)
            + np.diag(f2 * self.beta(e0) + f3 + 0 * e0)
            + np.diag(f2 * self.gamma(e1 + 1) + 0 * e1, 1)
        )

        return M

    def matrixN(self, n=None):
        r"""
        Gives the derivative matrix in the basis. This method is based in the
        paper 'Avoinding Similarity Transformations in the tau method - 2015
        -(José Matos et al.), and builds the M matrix in the basis used. Here,
        we also implemented the orthogonal shift.

        Parameters
        ----------
        n : int, optional
            The dimension of the matrix, When not given we take the default
            dimension in the basis options:The default is None.

        Returns
        -------
        N : array_like
            The ``n`` x ``n`` derivative matrix.

        See Also
        --------
        matrixO: The integration matrix
        matrixM: The product by x matrix in the basis

        Examples
        --------
        >>> import tautoolbox as tau
        >>> import numpy as np
        >>> opt={'basis':'LegendreP'}
        >>> b = tau.basis(options=opt)
        >>> c = np.arange(5)
        >>> c
        array([0, 1, 2, 3, 4])

        >>> n = b.matrixN(5)

        >>> # The differentiation matrix
        >>> n
        array([[0., 1., 0., 1., 0.],
               [0., 0., 3., 0., 3.],
               [0., 0., 0., 5., 0.],
               [0., 0., 0., 0., 7.],
               [0., 0., 0., 0., 0.]])

        >>> # The derative of the series in the Legendre basis
        >>> n @ c
        array([ 4., 18., 15., 28.,  0.])
        """

        if n is None:
            n = self.options.n

        N = np.zeros((n, n))
        if self.options.quadPrecision:
            N = tau.utils.quadprecision(N)

        for j in range(1, n):
            N[:j, j] = self.c1 * self.eta(np.arange(j), j + 1)

        return N

    def matrixO(self, n=None):
        r"""
        Matrix related with integration. This function makes use of paper:
        Avoinding Similarity Transformations in the tau method - 2015 -(José
        Matos et al.), and consists in to build the O matrix in the basis used.
        Here, we also implemented the orthogonal shift.

        Parameters
        ----------
        n : int, optional
            The dimension of the matrix. When not given we take the default
            dimesnion of the basis options.The default is None.

        Returns
        -------
        arrayLike
            The ``n`` x ``n`` matrix of integration.

        See Also
        --------
        matrixN
        matrixM


        """

        f2 = 1 / self.c1

        if n is None:
            n = self.options.n
        if self.options.quadPrecision:
            n = tau.utils.quadprecision(n)

        v = f2 * self.theta(n)
        return (
            np.diag(v[0, 0:-1], -1) + np.diag(v[1, :]) + np.diag(v[2, 1:], 1)
        )

    def orth2powmatrix(self, n=None):
        r"""
        Returns the V matrix such that :math:`XV = P`, where
        :math:`X = [x^0, x^1, ...]` is the power series and
        :math:`P = [P_1, P_2, ...]` is the orthogonal basis.

        Parameters
        ----------
        n : int, optional
            The dimnesion of the matrix. When n is None we take the default
            of the basis options.The default is None.

        Returns
        -------
        V : array_like
            The matrix of converion from orthogonal basis to poer basis

        See Also
        --------
        pow2orthmatrix: The matrix of converion from power basis to orthogonal
        basis.

        Example
        -------

        Using ChebyshevT basis in the domain [-1,1]:

        >>> import tautoolbox as tau
        >>> p = tau.basis()
        >>> p.orth2powmatrix(5)
        array([[ 1.,  0., -1., -0.,  1.],
               [ 0.,  1.,  0., -3., -0.],
               [ 0.,  0.,  2.,  0., -8.],
               [ 0.,  0.,  0.,  4.,  0.],
               [ 0.,  0.,  0.,  0.,  8.]])

        Using LegendreP basis in the domain [0,1]:

        >>> opt = {"basis": "LegendreP", "domain": [0, 1]}
        >>> p = tau.basis(options=opt)
        >>> p.orth2powmatrix(5)
        array([[   1.,   -1.,    1.,   -1.,    1.],
               [   0.,    2.,   -6.,   12.,  -20.],
               [   0.,    0.,    6.,  -30.,   90.],
               [   0.,    0.,    0.,   20., -140.],
               [   0.,    0.,    0.,    0.,   70.]])
        """
        if n is None:
            n = self.options.n

        V = np.zeros((n, n))

        if self.options.quadPrecision:
            V = tau.utils.quadprecision(V)

        V[0, 0] = 1
        if n == 1:
            return V
        V[0, 1] = (self.c0 - self.beta(0)) / self.alpha(0)
        V[1, 1] = self.c1 / self.alpha(0)
        if n == 2:
            return V

        for m in range(2, n):
            V[0, m] = (
                (self.c0 - self.beta(m - 1)) * V[0, m - 1]
                - self.gamma(m - 1) * V[0, m - 2]
            ) / self.alpha(m - 1)

            V[1 : m + 1, m] = (
                self.c1 * V[0:m, m - 1]
                + (self.c0 - self.beta(m - 1)) * V[1 : m + 1, m - 1]
                - self.gamma(m - 1) * V[1 : m + 1, m - 2]
            ) / self.alpha(m - 1)

        return V

    def pow2orthmatrix(self, n=None):
        r"""
        Returns the inverse of matrix V, where V is such that
        :math:`XV = P, X = [x^0, x^1, ...]` is the power series and
        :math:`P = [P_1, P_2, ...]` is the orthogonal basis. This is more
        stable than to compute :math:`V^-1, inv(V) or I\V`.
        Parameters
        ----------
        n : int, optional
            The dimension of the matrix. When ``n`` is None we take the de-
            fault that is in the basis options. The default is None.

        Returns
        -------
        W : array_like
            The matrix of conversion from power basis to orthogonal basis.

        See Also
        --------
        orth2powmatrix: The matrix of converion from orthogonal basis to power
        basis

        Examples
        --------

        Using ChebyshevU in the domain [0,2]:

        >>> opt = {"basis": "ChebyshevU", "domain": [0, 2]}
        >>> b = tau.basis(options=opt)
        >>> b.pow2orthmatrix(5)
        array([[1.    , 1.    , 1.25  , 1.75  , 2.625 ],
               [0.    , 0.5   , 1.    , 1.75  , 3.    ],
               [0.    , 0.    , 0.25  , 0.75  , 1.6875],
               [0.    , 0.    , 0.    , 0.125 , 0.5   ],
               [0.    , 0.    , 0.    , 0.    , 0.0625]])
        """

        if n is None:
            n = self.options.n

        W = np.zeros((n, n))

        if self.options.quadPrecision:
            W = tau.utils.quadprecision(W)

        M = self.matrixM(n)
        W[0, 0] = 1

        for i in range(1, n):
            W[:, i] = M @ W[:, i - 1]

        return W

    def polyval(self, coef, x, n=None):
        r"""
        Does the same as the method polyval but now ``coef`` can be a matrix
        with more than one row with each row representing the coefficients
        of a Polynomial in the same basis and domain.

        Parameters
        ----------
        coef : TYPE
            The coefficients of a Polynomial
        x : scalar or array_like
            The abcissas to evaluate
        n : int, optional
            Trim ``coef`` at the column ``n+1``

        Returns
        -------
        scalar or array_like
            sum(a_iP_i(xx)), i = 0:len(a)

        See Also
        --------
        polyval : The non vectorized version of this method

        Examples
        --------
        In the case x is a number:

        >>> coef = np.arange(1, 5)
        >>> bas=tau.basis()
        >>> bas.polyval(coef,1)
        10.0

        In the case x is a one-dimensional array_like object:

        >>> x = np.arange(1,5)
        >>> bas.polyval(coef,x)
        array([  10.,  130.,  454., 1078.])

        In the case x is a by-dimensional array_like object:

        >>> x= np.arange(9).reshape(3,3)
        >>> bas.polyvalv3(coef,x)
        array([[-2.000e+00,  1.000e+01,  1.300e+02],
               [ 4.540e+02,  1.078e+03,  2.098e+03],
               [ 3.610e+03,  5.710e+03,  8.494e+03]])

        """
        if isinstance(coef, Number):
            coef = np.array([coef])
        m = coef.shape[-1]
        if n is None:
            n = m - 1

        if n < 0 or not isinstance(n, int) or n > m - 1:
            raise ValueError(
                "n must be an integer greater or equal to zero and lesser or "
                f" equal to the degree of the polynomial was given{n}"
            )
        P0 = np.ones(np.shape(x))

        v = coef[..., :1][:, np.newaxis] * P0

        if n > 0:
            # Linear transformation from the domain
            z = self.c1 * x + self.c0

            # we are taking into account the linear transformation
            # or else the code would be simply: P1 = p1[1]*x + p1[0];
            P1 = self.p1[1] * z + self.p1[0]
            v = v + coef[..., 1:2][:, np.newaxis] * P1

            for k in range(1, n):
                P2 = (
                    (z - self.beta(k)) * P1 - self.gamma(k) * P0
                ) / self.alpha(k)
                v = v + coef[..., k + 1 : k + 2][:, np.newaxis] * P2
                P0 = P1
                P1 = P2
        return v.reshape(np.shape(coef[..., 0]) + np.shape(x))

    def polyvalm(self, a, x, n=None):
        """
        Orthogonal evaluation for matrices. f = polyvalm(x, a, M) returns the
        value of a Polynomial evaluated (orthogonally) at M: f = sum(a_iP_i).

        Parameters
        ----------
        a : array_like
            The coeficients of a Polynomial in the self basis
        x : scalar or array_like
            Input for evaluation must be a square matrix

        n : int, optional
            Where to stop when n is not given we assumes n=len(co)-1.
            The default is None.

        Raises
        ------
        ValueError
            DESCRIPTION.

        Returns
        -------
        scalar or array_like
            The sum(co_iP_i(x)), i in range(n)

        See Also
        --------
        polyval and polyvalb

        Examples
        --------

        """
        if isinstance(x, Number):
            x = np.array([[x]])
        if isinstance(x, list):
            x = np.array(x)

        p, q = x.shape

        if p != q:
            raise ValueError("x argument must be a square matrix")

        if isinstance(a, list) and all(
            isinstance(el, (int, float)) for el in a
        ):
            a = np.array(a)

        if not (isinstance(a, np.ndarray) and a.ndim == 1):
            raise ValueError(
                "a must be a list with numeric entries or a one "
                "dimensional numpy array"
            )

        if n is None:
            n = len(a) - 1

        iden = np.eye(x.shape[0])

        P0 = iden
        v = a[0] * P0

        if n == 0:
            return v

        # linear transformation from domain
        z = self.c1 * x + self.c0 * iden

        # we are taking into account the linear transformation
        # or else the code would be simply: P1 = p1(1)*x + p1(2)* P0
        P1 = self.p1[1] * z + self.p1[0] * iden
        v = v + a[1] * P1
        if n == 1:
            return v

        for k in range(1, n):
            P2 = (
                (z - self.beta(k) * iden) @ P1 - self.gamma(k) * P0
            ) / self.alpha(k)
            v += a[k + 1] * P2
            P0 = P1
            P1 = P2

        return v.item() if v.size == 1 else v

    def polyvalmM(self, a):
        """
        Evaluation for matrices M.
        v = polyvalmM(x, a) returns the value of
        a Polynomial evaluated (orthogonally) at matrix M.
        M is the matrix associated with the multiplication
        operator in the Tau method: v = sum(a_iP_i).
        This function exists because it is possible to use specialized
        formulas that take into account the errors that happen
        due to the discretization of an infinite operator.
        The idea of this function is similar to the relation between
        functions log and log1p where the later evaluates log more
        accurately in the neighborhood of zero.


        Parameters
        ----------
        co : array_like
            The coeficients of a Polynomial in the self basis

        Returns
        -------
        array_like
            The sum(co_iP_i(M)), i in range(len(co))

        """

        return self.polyvalm(a, self.matrixM())

    def polyvalb(self, x, dmin=0, dmax=0, n=None):
        r"""

        Evaluates the first n orthogonal Polynomials at point x.
        Returns a vector of values such that if c is the coefficients of a
        Polynomial in the basis ``self`` so self.polyvalb(x)* c is the value of
        a Polynomial evaluated at x. With other optional arguments it returns
        not only the function but but also the values of derivatives of the
        basis Polynomials at point x. This function is useful, for instance, to
        build the C  block for the initial/boundary conditions.

        Parameters
        ----------
        x : scalar or vector
            The values we want to compute their images at the basis Polynomials
        dmin : int, optional
            The minimum order of the derivatives. The default is 0.
        dmax : int, optional
            The maximum order of the derivatives. The default is 0.
        n : int, optional
            The first n basis Polynomials to evaluate. When not given we assume
            the length of tge basis. The default is None.

        Raises
        ------
        TypeError
            DESCRIPTION.

        Returns
        -------
        TYPE
            DESCRIPTION.

        See Also
        --------
        polyvalm
        polyval

        """

        if isinstance(x, Number):
            x = np.array([x])
        else:
            x = np.array(x)

        if x.dtype == np.dtype("O"):
            raise TypeError(
                "tautoolbox: x argument should be either a scalar or a vector"
                " or a numeric plain list"
            )

        if n is None:
            n = self.options.n

        if dmax < dmin:
            return np.zeros((0, n))

        x_shape = x.shape
        v = np.zeros((*x_shape, n))

        P0 = np.ones(x.shape)
        v[..., 0] = P0  # Polynomial with 1 term - degree 0

        if n > 1:
            # Transformation from the domain
            z = self.c1 * x + self.c0

            # We are taking into account the linear transformation or else
            # the code would be simply: P1 = p1[1] * x + p1[0]
            P1 = self.p1[1] * z + self.p1[0]
            v[..., 1] = P1  # Polynomial with 2 terms - degree 1

        for k in range(1, n - 1):
            v[..., k + 1] = (
                (z - self.beta(k)) * v[..., k] - self.gamma(k) * v[..., k - 1]
            ) / self.alpha(k)

        # Evaluate derivatives now

        # for k in range(dmin):
        #     v = v @ self.matrixN(n)
        if dmin > 0:
            v = v @ np.linalg.matrix_power(self.matrixN(n), dmin)
        if dmin == dmax:
            return v
        V = v.copy()
        for k in range(dmin + 1, dmax + 1):
            v = v @ self.matrixN(n)
            V = np.concatenate((V, v), axis=x.ndim - 1)

        return V

    # Points where to evaluate the functions (ideally those shoud be the
    # zeros of the orthogonal Polynomial
    def nodes(self, n=None):
        if n is None:
            n = self.options.n
        if n > 0:
            return self._rec(n)
        return [None] * 4

    def _rec(self, n):
        r"""
        Nodes for legendre basis based on the Greg von Winckel algorithm.
        compute the quadrature points, quadrature weights, baricentric weights
        and the angles for the Legendre basis.

        Parameters
        ----------
        n : int
            The number of nodes.

        Returns
        -------
        Tuple
            the quadrature points, quadrature weights, baricentric weights
            and the angle

        """

        dom = self.domain
        if n == 1:
            # Nodes, quadrature Weights, baricentric weights, angles
            x, w, b, a = sum(dom) / 2, np.diff(dom).item(), 1, np.pi / 2
            return x, w, b, a

        n = n - 1
        N1 = n + 1
        N2 = n + 2
        xu = np.linspace(-1, 1, N1)

        # Initial guess
        L = np.zeros((N1, N2))
        if L.size == 0:
            return L

        y = np.cos((2 * np.arange(n + 1) + 1) * np.pi / (2 * n + 2)) + (
            0.27 / N1
        ) * np.sin(np.pi * xu * n / N2)

        # Legendre-Gauss Vandermonde Matrix

        # Compute the zeros of the N+1 Legendre Polynomial using the recursion
        # relation and the Newton-Raphson method
        y0 = 2

        # Iterate until new points are uniformly within epsilon of old points
        while np.max(np.abs(y - y0)) > np.spacing(1):
            L[:, 0] = 1
            L[:, 1] = y

            for k in range(2, N1 + 1):
                L[:, k] = (
                    (2 * k - 1) * y * L[:, k - 1] - (k - 1) * L[:, k - 2]
                ) / k

            # Derivatives of L
            Ld = N2 * (L[:, N1 - 1] - y * L[:, N2 - 1]) / (1 - y**2)

            # Update
            y0 = y

            # Newton-Rapson iteration
            y = y0 - L[:, N2 - 1] / Ld

        # Shift from [-1, 1] to [a, b]

        x = np.sort((sum(dom) + np.diff(dom) * y) / 2)

        w = np.diff(dom).item() / ((1 - y**2) * Ld**2) * (N2 / N1) ** 2
        v = 1 / abs(Ld)
        v = v / max(v)
        v[1::2] = -v[1::2]

        return (x, w, v, np.arccos(np.sort(y)))

    def _gw_alg(self, n):
        r"""
        Based in Golub and Welsh

        Parameters
        ----------
        n : TYPE, optional
            DESCRIPTION. The default is None.

        Returns
        -------
        Tuple
            The quadrature points, quadrature weights, baricentric weights
            and the angle

        """

        dom = self.domain
        if n == 1:
            # Nodes, quadrature Weights, baricentric weights, angles
            x, w, b, a = sum(dom) / 2, np.diff(dom).item(), 1, np.pi / 2
            return x, w, b, a

        ni = np.arange(1, n)
        # Tree term recurrence relation for the coefficients
        beta = ni / np.sqrt(((2 * ni) ** 2 - 1))
        # Jacoby matrix
        J = np.diag(beta, -1) + np.diag(beta, 1)
        # The nodes are the eigenvalues of J
        x, vects = eig(J)
        # Indeces of eigenvalues sorted
        ix = np.argsort(x)
        x = x[ix]
        w = 2 * vects[0, ix] ** 2
        b = np.sqrt(1 - x**2) * abs(vects[0, ix])

        # Enforce simetry
        si = np.r_[0 : n // 2]
        x = x[si]
        w = w[si]
        b_m = b[n // 2]
        b = b[si]

        if n % 2:
            # n is odd
            x = np.r_[x, 0, -x[-1::-1]]
            w = np.r_[w, 2 - sum(2 * w), w[-1::-1]]
            b = np.r_[b, b_m, b[-1::-1]]

        else:
            # n is even
            x = np.r_[x, -x[-1::-1]]
            w = np.r_[w, w[-1::-1]]
            b = np.r_[b, b[-1::-1]]

        # Normailize the baricentric weights
        b = abs(b)
        b = b / max(b)
        b[1::2] = -b[1::2]

        # The angles
        a = np.arccos(x)

        # Shift and scale the nodes from [-1,1] to [a,b]
        x = (sum(dom) + np.diff(dom) * x) / 2
        # Scale the quadrature Weights
        w = np.diff(dom) * w / 2

        return x, w, b, a

    def productv2(self, p, q):
        r"""
        Does the product of two ortogonal Polynomials where ``p`` and que are the
        coefficients of those Polynomials in the ``self`` basis.

        Parameters
        ----------
        p : array_like
            The coeficients of the first polynnomial with shape (m, ).
        q : array_like
            The coefficients of the second Polynomial with shape(n, )

        Returns
        -------
        ndarray
            The coefficients of the Polynomial which are the product of the po-
            lynomials with coefficients ``p`` and ``q``respectively.

        Notes
        -----
        Compute the coefficients of product p*q, where
        p and q are both the vector of coefficients in orthogonal basis, i.e.

        .. math::
            P(x) = & p_0P_0(x) + ... + p_m*P_m(x),\\
            Q(t) = & q_0P_0(x) + ... + q_nP_n(x).

        Then, the result will be the vector of coefficients y such that

        .. math::
            P(x)Q(x) = f_0*P_0(x) + ... + f_{m+n}P_{m+n}(x).
            
        Examples
        --------
        Using the basis ``ChebyshevU`` :
            
        >>> import tautoolbox as tau
        >>> import numpy as np
        >>> p = np.arange(4) 
        >>> q=np.arange(3)
        >>> p
        array([0, 1, 2, 3])
        >>> q
        array([0, 1, 2])
        >>> b = tau.basis(tau.settings(basis='ChebyshevU'))
        >>> b.product(p,q)
        array([ 5., 10.,  8., 10.,  7.,  6.])
        
        Using the basis ``LegendreP`` :
            
        >>> b = tau.basis(tau.settings(basis='LegendreP'))
        >>> b.product(p,q)
        array([1.133, 3.143, 3.095, 4.   , 3.771, 2.857])
        """

        m = np.min([len(p), len(q)]) - 1
        n = np.max([len(p), len(q)]) - 1
        p = np.concatenate((p, np.zeros(n)))
        p = p[: n + 1]
        q = np.concatenate((q, np.zeros(n)))
        q = q[: n + 1]
        a = self.alpha(np.arange(2 * n + 1)) + np.zeros(2 * n + 1)
        b = self.beta(np.arange(2 * n + 1)) + np.zeros(2 * n + 1)
        c = self.gamma(np.arange(2 * n + 1)) + np.zeros(2 * n + 1)
        L = np.zeros((2 * n + 1, 2 * n + 1, 2 * n + 1))
        for k in range(2 * n + 1):
            L[k, 0, k] = 1
            L[0, k, k] = 1
            L[k, 1, k] = (b[k] - b[0]) / a[0]
            L[1, k, k] = L[k, 1, k]

        for k in range(2 * n):
            L[k + 1, 1, k] = c[k + 1] / a[0]
            L[1, k + 1, k] = L[k + 1, 1, k]
            L[k, 1, k + 1] = a[k] / a[0]
            L[1, k, k + 1] = L[k, 1, k + 1]

        for j in range(1, 2 * n):
            for i in range(j + 1, 2 * n):
                L[i, j + 1, :] = (
                    c[i] * L[i - 1, j, :]
                    - c[j] * L[i, j - 1, :]
                    + (b[i] - b[j]) * L[i, j, :]
                    + a[i] * L[i + 1, j, :]
                ) / a[j]

            for i in range(j + 2, 2 * n):
                L[j + 1, i, :] = L[i, j + 1, :]

        # y = p*q
        y = np.zeros(2 * n + 1)

        for i in range(n + 1):
            # P_i *P_i
            for k in range(2 * i + 1):
                y[k] = y[k] + p[i] * q[i] * L[i, i, k]
            for j in range(i):  # P_i*P_{j}
                for k in range(i + j + 1):
                    y[k] = y[k] + (p[i] * q[j] + p[j] * q[i]) * L[i, j, k]

        return y[: m + n + 1]

    def _prodx(self, p):
        r"""
        Multiply the polynomial that have p as coefficients by x and return
        the coefficients of the product of the polynomial by x in the [-1,1]
        domain

        Parameters
        ----------
        p : array_like
            The coefficients of the polynomial multyplied by x

        Returns
        -------
        array_like
            The coefficients of a series in this basis multiplied by x.

        """
        # this is needed when you have more than one row
        fi = 0 if p.ndim == 1 else np.zeros((len(p), 1))
        p = np.r_["-1", p, fi]
        n = p.shape[-1]

        e0 = np.arange(n)

        M = spdiags(
            [
                self.alpha(e0) + 0 * e0,
                self.beta(e0) + 0 * e0,
                self.gamma(e0) + 0 * e0,
            ],
            [-1, 0, 1],
            n,
            n,
        )

        return (M @ p.T).T

    def _ultraProduct(self, p, q, alpha=None):
        # Check if we only n rows x n rows, 1 row x n rows, or n rows x 1 row
        # inputs
        if p.ndim > 2 or q.ndim > 2:
            raise ValueError("p and q must have at most 2 dimensions.")

        # Shape of the arrays
        shp = p.shape
        shq = q.shape
        if p.ndim == q.ndim == 2:
            if shq[0] > 1 and shq[0] > 1 and shq[0] != shq[0]:
                raise ValueError(
                    "You can only add two array of coefficients "
                    "when one of them have one row or when they "
                    "have the same number of rows"
                )

        # Product by a constant
        if shp[-1] == 1:
            return p * q
        elif shq[-1] == 1:
            return q * p

        # Other cases
        if shp[-1] > shq[-1]:
            c = q.copy()
            xs = p.copy()
        else:
            c = p.copy()
            xs = q.copy()

        if alpha is None:
            alpha = self.options.alpha

        if c.shape[-1] == 2:
            c0 = c[..., :1] * xs
            c1 = c[..., 1:2] * xs
        else:
            nd = c.shape[-1]
            c0 = c[..., -2:-1] * xs
            c1 = c[..., -1:] * xs

            for i in range(3, c.shape[-1] + 1):
                tmp = c0
                nd -= 1
                c0 = self.sub(
                    c[..., -i : -i + 1] * xs, c1 * ((nd + 2 * alpha - 2) / nd)
                )
                c1 = self.add(
                    tmp, self._prodx(c1) * (2 * (nd - 1 + alpha) / nd)
                )

        return self.add(c0, self._prodx(c1) * (2 * alpha))

    def _chebThreeTermsRecurrence(self, p, q):
        # Check if we only n rows x n rows, 1 row x n rows, or n rows x 1 row
        # inputs
        if p.ndim > 2 or q.ndim > 2:
            raise ValueError("p and q must have at most 2 dimensions.")

        # Shape of the arrays
        shp = p.shape
        shq = q.shape
        if p.ndim == q.ndim == 2:
            if shq[0] > 1 and shq[0] > 1 and shq[0] != shq[0]:
                raise ValueError(
                    "You can only add two array of coefficients "
                    "when one of them have one row or when they "
                    "have the same number of rows"
                )

        # Product by a constant
        if shp[-1] == 1:
            return p * q
        elif shq[-1] == 1:
            return q * p

        # Other cases
        if shp[-1] > shq[-1]:
            c = q.copy()
            xs = p.copy()
        else:
            c = p.copy()
            xs = q.copy()

        if c.shape[-1] == 2:
            c0 = c[..., :1] * xs
            c1 = c[..., 1:2] * xs
        else:
            c0 = c[..., -2:-1] * xs
            c1 = c[..., -1:] * xs

            for i in range(3, c.shape[-1] + 1):
                tmp = c0
                c0 = self.sub(c[..., -i : -i + 1] * xs, c1)
                c1 = self.add(tmp, self._prodx(c1) * 2)

        return c0, c1

    def add(self, c1, c2):
        r"""
        Add the coefficients ``c1`` and ``c2``.

        Parameters
        ----------
        c1 : ndarray
            A one dimensional or bydimensional array.
        c2 : ndarray
            A one dimensional or bydimensional array.

        Raises
        ------
        ValueError
            When ``c1`` and ``c2`` are bydimensional arrays with more than one
            row and don't have the same number of rows.

        Returns
        -------
        TYPE
            an array of coefficients which are the result of the sum of ``c1``
            and ``c2``.

        """
        if c1.ndim == 1 and c2.ndim == 1:
            m, n = len(c1), len(c2)
            if m >= n:
                res = c1.copy()
                res[:n] += c2
                return res
            else:
                return self.add(c2, c1)

        if c1.ndim == 1:
            c1 = c1.reshape(1, -1).copy()

        if c2.ndim == 1:
            c2 = c2.reshape(1, -1).copy()

        sh1 = c1.shape
        sh2 = c2.shape
        if len(sh1) == len(sh1) == 2:
            if sh1[0] > 1 and sh2[0] > 1 and sh1[0] != sh2[0]:
                raise ValueError(
                    "You can only add two array of coefficients "
                    "when one of them have one row or when they "
                    "have the same number of rows"
                )

            if sh1[1] >= sh2[1]:
                res = np.zeros((max(sh1[0], sh2[0]), max(sh1[1], sh2[1])))
                res[...] = c1
                res[..., : sh2[1]] += c2
                return res

            else:
                return self.add(c2, c1)
        else:
            raise ValueError("Coefficients must have at most dimension 2.")

    def sub(self, c1, c2):
        r"""
        subtract the coefficients ``c1`` and ``c2``.

        Parameters
        ----------
        c1 : ndarray
            A one dimensional or bydimensional array.
        c2 : ndarray
            A one dimensional or bydimensional array.

        Raises
        ------
        ValueError
            When ``c1`` and ``c2`` are bydimensional arrays with more than one
            row and don't have the same number of rows.

        Returns
        -------
        TYPE
            an array of coefficients which are the result of the subtraction of
            ``c1`` and ``c2``.

        """

        return self.add(c1, -c2)

    def interp1f(self, fx, poly=None, n=None):
        r"""
        Compute the coefficients of the Polynomial which are the approximation
        of fx(poly) in the basis ``self`` with ``n`` therms.

        Parameters
        ----------
        fx : TYPE
            DESCRIPTION.
        poly : TYPE, optional
            DESCRIPTION. The default is None.
        n : TYPE, optional
            DESCRIPTION. The default is None.

        Raises
        ------
        TypeError
            DESCRIPTION.
        ValueError
            DESCRIPTION.

        Returns
        -------
        TYPE
            DESCRIPTION.

        """
        """
        interp1f - Orthogonal Polynomial interpolation.
           f = interp1f(basis, fx) interpolates an expression

        inputs:
           self  = tau orthogonal basis (tau.basis object).
           fx = function to interpolate (function handle).

        inputs(optional):
           poly  = Polynomial where to evaluate a composed function fx(poly(..))
           n     = order to approximate

        output:
           f     = interpolation coefficients (double vector).
           esys  = error in the linear system (double vector).
           eeval = (max.) error in the evaluation (double scalar).
           edom  = error in the full domain (double vector).

        See also interporthinc, interporth2 and interporth2inc.
        """

        if not callable(fx):
            raise TypeError(
                "tautoolbox: the 1st argument must be callable in one variable."
            )

        if poly is None:
            poly = lambda x: x

        if not callable(poly):
            raise TypeError(
                "tautoolbox: poly must be a callable with one argument."
            )

        if n is None:
            n = self.options.n

        # The abcissas for the interpolations
        nods, *_ = self.nodes(n)

        b = fx(poly(nods)).T

        if any(np.isnan(b)) or any(np.isinf(b)):
            raise ValueError("Encountered inf or NaN when evaluating.")

        if self.name == "ChebyshevT":
            return (self.vals1tocoeffs(b.T),)
        if self.name == "ChebyshevU":
            return (chebt2kind(self.vals2tocoeffs(b.T)),)

        A = self.polyvalb(nods, n=n)
        f = np.linalg.solve(A, b)

        esys = np.max(np.abs(A @ f - b).T, axis=-1)

        return f.T, esys

    def interp1(self, fx, n=None):
        obj = self.copy()

        if n is None:
            n = obj.options.n

        try:
            options = obj.options
            options.nequations = 1

            x = tau.Polynomial(options=options)
            y = fx(x) + 0 * x
            f = y.coeff
            if len(f) < n:
                f = np.r_[f, np.zeros(n - len(f))]

            if y.n <= n:
                return f
            return f[:n]

        except:
            return obj.interp1f(fx, n=n)[0]

    def system(self, cnd, n=None):
        if n is None:
            n = self.options.n

        C = np.zeros((len(cnd.coeff), n * self.options.nequations))

        for j in range(len(cnd.coeff)):
            if not (self.domain[0] <= cnd.point[j] <= self.domain[1]):
                continue

            idx = (cnd.var[j] - 1) * n + np.arange(n)

            C[j, idx] = cnd.coeff[j] * self.polyvalb(
                cnd.point[j], cnd.order[j], cnd.order[j], n
            )

        return sum(C)

    def conditions(self, conds, n=None):
        nu = len(conds)
        if nu == 0:
            return np.array([]), np.array([])

        if n is None:
            C = np.zeros((nu, self.options.n * self.options.nequations))
        else:
            C = np.zeros((nu, n * self.options.nequations))

        b = np.zeros(nu)

        for k in range(nu):
            C[k] = self.system(conds[k], n)
            b[k] = conds[k].value
        return C, b

    @staticmethod
    def idlt(f=None):
        r"""
        Returns the inverse discretete legendre transform.


        Parameters
        ----------
        f : TYPE, optional
            DESCRIPTION. The default is None.

        Returns
        -------
        TYPE
            DESCRIPTION.

        """

        if f is None:
            return np.zeros(0)

        if np.isscalar(f):
            f = np.array([f])

        N = f.shape[-1]
        if N == 1:
            return 1 + 0 * f
        x, w, *_ = tau.basis(tau.settings(basis="LegendreP")).nodes(N)
        c = w * f

        pm1 = 1 + 0 * x
        p = x
        v = np.zeros_like(c)
        v[..., 0] = np.sum(c, axis=-1)
        v[..., 1] = x @ c.T

        for i in range(N - 2):
            pp1 = (2 - 1 / (i + 2)) * (p * x) - (1 - 1 / (i + 2)) * pm1
            pm1 = p
            p = pp1
            v[..., i + 2] = p @ c.T

        return (np.arange(N) + 0.5) * v

    @staticmethod
    def idct(f):
        """
        Inverse Fast Chebyshev Transform
        Discrete Chebyshev Transform Coefficients.
        Function values evaluted at Chebyshev Gauss Lobatto nodes
        with nodes ordered increasingly x_i=[-1,...,1}
        for i=1,2...,N

        Based in  Allan P. Engsig-Karup, apek@imm.dtu.dk.

        Parameters
        ----------
        f : TYPE
            DESCRIPTION.

        Returns
        -------
        TYPE
            DESCRIPTION.

        """
        N = f.size
        u = fft(
            np.r_[
                f[0],
                np.r_[f[1 : N - 1], f[N - 1] * 2, f[N - 2 : 0 : -1]] * 0.5,
            ]
        )
        # Reverse order due to fft
        u = u[N - 1 :: -1]
        return u.real

    @staticmethod
    def interp_from_values(values, abcissas=None, options=None, **kwargs):
        kind1 = kwargs.get("kind1", False)

        if options is None:
            bas = tau.basis(tau.settings())
        elif isinstance(options, dict):
            bas = tau.basis(tau.settings(**options))
        else:
            bas = tau.basis(options)

        if kind1 and bas.name == "ChebyshevT" and abcissas is None:
            return bas.vals1tocoeffs(values)
        elif bas.name == "ChebyshevT" and abcissas is None:
            return bas.vals2tocoeffs(values)

        if abcissas is None and bas.name == "ChebyshevU":
            return chebt2kind(bas.vals2tocoeffs(values))

        n = values.shape[-1]
        if abcissas is None:
            abcissas, *_ = tau.basis(
                tau.settings(basis="ChebyshevU", domain=bas.domain)
            ).nodes(n)
        A = bas.polyvalb(abcissas, n=n)

        return np.linalg.solve(A, values.T).T

    @staticmethod
    def trigeval(c, x, domain=None):
        if domain is not None:
            x = (2 * x - sum(domain)) / np.diff(domain).item()
        N = c.shape[-1]
        nrows = 1 if c.ndim == 1 else c.shape[0]
        n = ((N - 1) / 2).__ceil__()
        c = c[..., n::-1]
        a = c.real
        b = c.imag
        b.flags["WRITEABLE"] = True

        e = np.ones(x.size)
        if N % 2 == 0:
            a[..., n] = a[..., n] / 2
            b[..., n] = 0

        if N == 1:
            return c * e

        u = np.tile(np.cos(np.pi * x), (nrows, 1))
        v = np.tile(np.sin(np.pi * x), (nrows, 1))

        co = a[..., n : n + 1] * e
        si = b[..., n : n + 1] * e

        for i in range(n - 1, 0, -1):
            temp = a[..., i : i + 1] * e + u * co + v * si

            si = b[..., i : i + 1] * e + u * si - v * co
            co = temp

        res = a[..., :1] * e + 2 * (u * co + v * si)

        if nrows == 1:
            res = res.reshape(x.shape)
        return res

    @staticmethod
    def vals1tocoeffs(vals):
        r"""
        This methods give the coefficients of the polynomial expansion in
        the Chebyshev of the first kind basis where the coefficients are given
        by interpolation  over the Nodes of Chebyshev of the first kind poly-
        nomials

        Parameters
        ----------
        vals : array_like
            A vector (m,) or a matrix (n,m) where the values ineach row is the
            interpolate separately over the nodes.

        Returns
        -------
        coeffs : array_like
            A vector or matrix where each row represents the coefficients of
            a polynomial.

        Notes
        -----
        In input is an array  :math:`C^{n\times n}'  where n is the number of
        rows such that:
            .. math::
                P_i(x) = C_{i,0}T_0(x) +\cdots + C_{i,m-1}T_{m-1}(x),
                i=1,\cdots, n


        """
        n = vals.shape[-1]
        isEven = np.max(abs(vals - np.flip(vals, axis=-1)), axis=-1) == 0
        isOdd = np.max(abs(vals + np.flip(vals, axis=-1)), axis=-1) == 0
        w = 2 * np.exp(1j * np.arange(n) * np.pi / (2 * n))
        tmp = np.r_["-1", vals[..., ::-1], vals]
        coeffs = ifft(tmp)
        # truncate, flip
        coeffs = w * coeffs[..., :n]

        # scale the coefficinets
        coeffs[..., 0] = coeffs[..., 0] / 2
        if np.all(np.isreal(vals)):
            coeffs = coeffs.real
        elif np.all(np.isreal(1j * vals)):
            coeffs = 1j * coeffs.imag

        # adjust for simetry
        coeffs[isEven, 1::2] = 0
        coeffs[isOdd, ::2] = 0

        return coeffs

    @staticmethod
    def vals2tocoeffs(vals):
        r"""
        This methods give the coefficients of the polynomial expansion in
        the Chebyshev of the first kind basis where the coefficients are given
        by interpolation over the Nodes of Chebyshev of the second kind poly-
        nomials

        Parameters
        ----------
        vals : array_like
            A vector (m,) or a matrix (n,m) where the values ineach row is the
            interpolate separately over the nodes.

        Returns
        -------
        coeffs : array_like
            A vector or matrix where each row represents the coefficients of
            a polynomial.

        Notes
        -----
        In input is an array  :math:`C^{n\times n}'  where n is the number of
        rows such that:
            .. math::
                P_i(x) = C_{i,0}T_0(x) +\cdots + C_{i,m-1}T_{m-1}(x),
                i=1,\cdots, n


        """
        n = vals.shape[-1]

        # Check for simetry
        isEven = np.max(abs(vals - np.flip(vals, axis=-1)), axis=-1) == 0
        isOdd = np.max(abs(vals + np.flip(vals, axis=-1)), axis=-1) == 0

        # Mirror the values
        tmp = np.r_["-1", vals[..., :0:-1], vals[..., : n - 1]]

        if np.all(np.isreal(vals)):
            # The case when vals are real
            coef = ifft(tmp).real
        elif np.all(np.isreal(1j * vals)):
            # For the case when vals are imagenary
            coef = 1j * ifft(tmp.imag).real
        else:
            # General Case
            coef = ifft(tmp)

        # Truncate the coefficients
        coef = coef[..., :n]

        # Scale the inner coefficients
        coef[..., 1 : n - 1] *= 2

        # Adjust the coefficients for simmetry
        coef[isEven, 1::2] = 0
        coef[isOdd, ::2] = 0

        return coef

    def copy(self):
        return deepcopy(self)

    def __repr__(self):
        b = {"basis": self.name, "domain": self.domain}
        if self.name == "GegenbauerC":
            b["alpha"] = self.options.alpha

        return f"basis(options={b})"

    def fractionalIntegral(self, c, order, b=0):
        r"""
        Frational Integral for GegenbauerC and ChebyshevU bases. Here ``c`` are
        the coefficients of a polynomial in Legendre basis. We compute here the
        Riemmann-Liouville Integral based on the formula:

        .. math::
            I^\alpha f(x)=\frac{1}{\Gamma(\alpha)}\int_a^x f(t)(x-t)^{\alpha
            -1}dt

        Parameters
        ----------
        c : array_like
            The coefficients of a polynomial in above  bases.
        order : scalar
            A sclar between ]0,1[ meaning the order of the fractional integral.

        Returns
        -------
        array_like
            The fractional integral of order `order` coefficients.

        Notes
        -----
        here when :math:`\alpha=0.5` we use the relation:

        .. math::
            \left(n+\frac{1}{2}\right)(1+x)^\frac{1}{2}\int_{-1}^x (x-t)
            ^\frac{-1}{2}P_n(t)dt = T_n(x) +T_{n+1}(x),
        where :math:`P_n` are the Legendre polynomials and :math:`T_n` the Cheb
        yshev polynomials of the first kind.

        When :math:`\alpha \neq 0.5` we use the relation:

        .. math::
            \frac{(1-x)^{\alpha + \mu}P_n^{(\alpha+\mu,\ \beta -\mu)}(x)}
            {\Gamma(\alpha +\mu+n+1)}=\int_x^1 \frac{(1-y)^\alpha
            P_n^{(\alpha,\ \beta)}(y)\ (y-x)^{\mu -1}}{\Gamma(\alpha +n+1)
            \Gamma(\mu)}dy,\quad \mu >0, -1 <x<1,
        where :math:`P_n^{(\alpha,\ \beta)}` are the Jacoby polynomials with
        parameters :math:`\alpha` and :math:`\beta` respectively. These formu
        las can be found in [1, p.456 ].

        References
        ----------
        [1] F.W.J. Olver et al., editors. NIST Handbook of Mathematical Functions.
        Cambridge University Press, New York, NY, 2010.

        """
        # Conversion to Legendre basis
        al = self.options.alpha
        if self.name == "ChebyshevU":
            al = 1

        n = c.shape[-1]
        sc = (np.diff(self.domain) / 2) ** order

        # For Legendre the scale factor is 0
        cj1 = jac2jac(c / scl(al, n - 1), al - 0.5, al - 0.5, 0, b)
        cj2 = (cj1 * beta(np.arange(n) + b + 1, order)) / gamma(order)
        cout = jac2jac(cj2, -order, b + order, al - 0.5, al - 0.5) * scl(
            al, n - 1
        )
        return cout * sc


class BesselY(basis):
    def __init__(self, options=None):
        super().__init__(options)

        self.name = "BesselY"

    def alpha(self, n):
        return 1 / (2 * n + 1)

    def beta(self, n):
        return -1 * (n == 0)

    def gamma(self, n):
        return -1 / (2 * n + 1)

    def eta(self, i, j):
        return (i - j + 1) * (i + j) * (i + 1 / 2) * (-1) ** (-1 + i + j)

    def theta(self, n):
        return np.array(
            [
                1 / (np.arange(1, n + 1) * np.arange(1, 2 * n, 2)),
                np.concatenate(
                    ([0], 1 / (np.arange(1, n) * np.arange(2, n + 1)))
                ),
                np.concatenate(
                    ([0] * 2, 1 / (np.arange(2, n) * np.arange(5, 2 * n, 2)))
                ),
            ]
        )


class ChebyshevT(basis):
    def __init__(self, options=None):
        super().__init__(options)
        self.name = "ChebyshevT"

    def alpha(self, n):
        return 1 / 2 + 1 / 2 * (n == 0)

    def beta(self, n):
        return 0

    def gamma(self, n):
        return 1 / 2

    def eta(self, i, j):
        return 2 * ((i + j - 1) % 2) * (j - 1) / (1 + (i == 0))

    def theta(self, n):
        return np.array(
            [
                np.concatenate(([1], 1 / np.arange(4, 2 * n + 1, 2))),
                np.zeros(n),
                np.concatenate((np.zeros(2), -1 / np.arange(2, 2 * n - 3, 2))),
            ]
        )

    def nodes(self, n=None):
        if n is None:
            n = self.options.n
        dom = self.domain
        if n > 0:
            if n == 1:
                x = 0
                w = 2
                v = 1
                a = 0.5 * np.pi
            else:
                # Computing the quadrature points
                x = np.cos((np.arange(n, 0, -1) - 1 / 2) * np.pi / n)
                # Computing the quadrature weight
                m = 2 / np.r_[1, 1 - np.r_[2:n:2] ** 2]

                # Mirror the vector for use of ifft
                # n is odd
                if n % 2:
                    c = np.r_[m, -m[(n - 1) // 2 : 0 : -1]]
                else:
                    c = np.r_[m, 0, -m[n // 2 - 1 : 0 : -1]]

                c = c * np.exp(1j * np.r_[0:n] * np.pi / n)
                w = np.fft.ifft(c).real

                # Computing the quadrature points
                l = np.r_[0:n]
                m = np.minimum(l, n - l)
                r = 2 / (1 - 4 * m**2)
                s = np.sign(n / 2 - l)
                v = s * r * np.exp(1j * np.pi / n * l)
                w = ifft(v).real

                # Computing  the baricientric weights
                v = np.sin((np.r_[n - 1 : -1 : -1] + 0.5) * np.pi / n)
                v[: n // 2] = v[: n - n // 2 - 1 : -1]
                v[-2::-2] = -v[-2::-2]

                # The angles
                a = np.r_[n - 0.5 : -0.5 : -1] * np.pi / n

            # Scaling and shifting
            x = (sum(dom) + np.diff(dom).item() * x) / 2
            w = (np.diff(dom).item() / 2) * w
            v = v / np.max(abs(v))

            return x, w, v, a

        return [None] * 4

    def nodes2(self, n):
        pass

    def _gw_alg(self, n):
        dom = self.domain
        # Tree term recurrence relation for the coefficients
        beta = np.sqrt(np.r_[1 / 2, [1 / 4] * (n - 2)])
        # Jacoby matrix
        J = np.diag(beta, -1) + np.diag(beta, 1)
        # The nodes are the eigenvalues of J
        x, vects = eig(J)
        # Indeces of eigenvalues sorted

        w = 2 * vects[0, :] ** 2
        b = np.sqrt(1 - x**2) * abs(vects[0, :])

        # Enforce simetry
        si = np.r_[0 : n // 2]
        x = x[si]
        w = w[si]
        b_m = b[n // 2]
        b = b[si]

        if n % 2:
            # n is odd
            x = np.r_[x, 0, -x[-1::-1]]
            w = np.r_[w, 2 - sum(2 * w), w[-1::-1]]
            b = np.r_[b, b_m, b[-1::-1]]

        else:
            # n is even
            x = np.r_[x, -x[-1::-1]]
            w = np.r_[w, w[-1::-1]]
            b = np.r_[b, b[-1::-1]]

        # Normailize the baricentric weights
        b = abs(b)
        b = b / max(b)
        b[1::2] = -b[1::2]

        # The angles
        a = np.arccos(x)

        # Shift and scale the nodes from [-1,1] to [a,b]
        x = (sum(dom) + np.diff(dom) * x) / 2
        # Scale the quadrature Weights
        w = np.diff(dom) * w / 2

        return x, w, b, a

    def product(self, p, q):
        r"""
        Does the product of two ortogonal Polynomials where ``p`` and que are the
        coefficients of those Polynomials in the cChesbyshev of the first kind
        basis.

        Parameters
        ----------
        p : array_like
            The coeficients of the first polynnomial with shape (m, ).
        q : array_like
            The coefficients of the second Polynomial with shape(n, )

        Returns
        -------
        ndarray
            The coefficients of the Polynomial which are the product of the po-
            lynomials with coefficients ``p`` and ``q``respectively.

        Notes
        -----
        Compute the coefficients of product p*q, where
        p and q are both the vector of coefficients in orthogonal basis, i.e.

        .. math::
            P(x) = & p_0P_0(x) + ... + p_m*P_m(x),\\
            Q(t) = & q_0P_0(x) + ... + q_nP_n(x).

        Then, the result will be the vector of coefficients y such that

        .. math::
            P(x)Q(x) = f_0*P_0(x) + ... + f_{m+n}P_{m+n}(x).
            
        Examples
        --------

        """
        if len(p) == 1:
            return p[0] * q
        if len(q) == 1:
            return q[0] * p

        m = min([len(p), len(q)]) - 1
        n = max([len(p), len(q)]) - 1
        p = np.concatenate((p, np.zeros(n)))
        p = p[: n + 1]
        q = np.concatenate((q, np.zeros(n)))
        q = q[: n + 1]

        # y = p*q
        f = np.zeros(2 * n + 1)
        f[0] = p[0] * q[0] + p[1 : n + 1] @ q[1 : n + 1] / 2

        for k in range(n):
            # y_k
            f[k + 1] = (
                p[: n - k] @ q[k + 1 : n + 1]
                + p[k + 1 : n + 1] @ q[: n - k]
                + p[: k // 2 + 1] @ q[k + 1 : k - k // 2 : -1]
                + p[k + 1 : k - k // 2 : -1] @ q[: k // 2 + 1]
            ) / 2

        for k in range(n, 2 * n - 1):
            # y_k

            f[k + 1] = (
                p[k - n + 1 : k // 2 + 1] @ q[n : k - k // 2 : -1]
                + p[n : k - k // 2 : -1] @ q[k - n + 1 : k // 2 + 1]
            ) / 2

        f[2 : 2 * n - 1 : 2] = f[2 : 2 * n - 1 : 2] + p[1:n] * q[1:n] / 2
        f[2 * n] = p[n] * q[n] / 2
        f = f[: m + n + 1]

        return f

    def productv3(self, p, q):
        r"""
        Does the product for the basis Chebyshev of the first kind
        ``ChebyshevT`` . Here ``p`` and que are the coefficients of two  poly-
        nomials in this basis with the same domain.

        Parameters
        ----------
        p : array_like
            The coeficients of the first polynnomial with shape (m, ).
        q : array_like
            The coefficients of the second Polynomial with shape(n, )

        Returns
        -------
        ndarray
            The coefficients of the Polynomial which are the product of the po-
            lynomials with coefficients ``p`` and ``q``respectively.

        Notes
        -----
        Compute the coefficients of product p*q, where
        p and q are both the vector of coefficients in orthogonal basis, i.e.

        .. math::
            P(x) = & p_0P_0(x) + ... + p_m*P_m(x),\\
            Q(t) = & q_0P_0(x) + ... + q_nP_n(x).

        Then, the result will be the vector of coefficients y such that

        .. math::
            P(x)Q(x) = f_0*P_0(x) + ... + f_{m+n}P_{m+n}(x).
            
        Examples
        --------
        Using the basis ``ChebyshevU`` :
        """
        lp = 1 if p.ndim == 1 else len(p)
        lq = 1 if q.ndim == 1 else len(q)

        if lp > lq:
            return self.productv2(q, p)

        vector = False
        if p.ndim == 1 and q.ndim == 1:
            vector = True
            p = p.reshape(1, -1)
            q = q.reshape(1, -1)
        elif p.ndim == 1:
            p = p.reshape(1, -1)
        elif q.ndim == 1:
            q = q.reshape(1, -1)

        if lp > 1 and lq > 1 and lp != lq:
            raise ValueError(
                "You can only multiply an matrix with the same "
                "Numbers of rows or one with one row by another "
                "with one or more rows"
            )

        l = max(lp, lq)
        if p.shape[1] == 1:
            res = p[:, 0] * q
            if vector:
                return res[0]
            return res
        if q.shape[1] == 1:
            res = q[:, 0] * p

            if vector:
                return res[0]
            return res

        m = min(p.shape[1], q.shape[1]) - 1
        n = max(p.shape[1], q.shape[1]) - 1

        p = np.c_[p, np.zeros((lp, n + 1 - p.shape[1]))]

        q = np.c_[q, np.zeros((lq, n + 1 - q.shape[1]))]

        f = np.zeros((l, 2 * n + 1))

        # Case at least one Polynomial have only one column
        if lp == 1:
            f[:, 0] = (
                p[:, 0] * q[:, 0] + p[:, 1 : n + 1] @ q[:, 1 : n + 1].T / 2
            )

            for k in range(n):
                # y_k
                f[:, k + 1] = (
                    p[:, : n - k] @ q[:, k + 1 : n + 1].T
                    + p[:, k + 1 : n + 1] @ q[:, : n - k].T
                    + p[:, : k // 2 + 1] @ q[:, k + 1 : k - k // 2 : -1].T
                    + p[:, k + 1 : k - k // 2 : -1] @ q[:, : k // 2 + 1].T
                ) / 2

            for k in range(n, 2 * n - 1):
                # y_k
                f[:, k + 1] = (
                    p[:, k - n + 1 : k // 2 + 1] @ q[:, n : k - k // 2 : -1].T
                    + p[:, n : k - k // 2 : -1]
                    @ q[:, k - n + 1 : k // 2 + 1].T
                ) / 2

        # The case where all Polynomials the same number of columns and the
        # number of columns are more than one
        else:
            f[:, 0] = (
                p[:, 0] * q[:, 0]
                + np.einsum("ij,ij->i", p[:, 1 : n + 1], q[:, 1 : n + 1]) / 2
            )

            for k in range(n):
                # y_k
                f[:, k + 1] = (
                    np.einsum("ij,ij->i", p[:, : n - k], q[:, k + 1 : n + 1])
                    + np.einsum("ij,ij->i", p[:, k + 1 : n + 1], q[:, : n - k])
                    + np.einsum(
                        "ij,ij->i",
                        p[:, : k // 2 + 1],
                        q[:, k + 1 : k - k // 2 : -1],
                    )
                    + np.einsum(
                        "ij,ij->i",
                        p[:, k + 1 : k - k // 2 : -1],
                        q[:, : k // 2 + 1],
                    )
                ) / 2

            for k in range(n, 2 * n - 1):
                # y_k
                f[:, k + 1] = (
                    np.einsum(
                        "ij,ij->i",
                        p[:, k - n + 1 : k // 2 + 1],
                        q[:, n : k - k // 2 : -1],
                    )
                    + np.einsum(
                        "ij,ij->i",
                        p[:, n : k - k // 2 : -1],
                        q[:, k - n + 1 : k // 2 + 1],
                    )
                ) / 2

        f[:, 2 : 2 * n - 1 : 2] = (
            f[:, 2 : 2 * n - 1 : 2] + p[:, 1:n] * q[:, 1:n] / 2
        )
        f[:, 2 * n] = p[:, n] * q[:, n] / 2

        if vector:
            return f[0, : m + n + 1]

        return f[:, : m + n + 1]

    def productv2(self, p, q):
        r"""
        Does the product for the basis Chebyshev of the first kind
        ``ChebyshevU`` . Here ``p`` and que are the coefficients of two  poly-
        nomials in this basis with the same domain.

        Parameters
        ----------
        p : array_like
            The coeficients of the first polynnomial with shape (m, ).
        q : array_like
            The coefficients of the second Polynomial with shape(n, )

        Returns
        -------
        ndarray
            The coefficients of the Polynomial which are the product of the po-
            lynomials with coefficients ``p`` and ``q``respectively.

        Notes
        -----
        Compute the coefficients of product p*q, where
        p and q are both the vector of coefficients in orthogonal basis, i.e.

        .. math::
            P(x) = & p_0P_0(x) + ... + p_m*P_m(x),\\
            Q(t) = & q_0P_0(x) + ... + q_nP_n(x).

        Then, the result will be the vector of coefficients y such that

        .. math::
            P(x)Q(x) = f_0*P_0(x) + ... + f_{m+n}P_{m+n}(x).
            
        Examples
        --------
        Using the basis ``ChebyshevU`` :
        """
        lp = 1 if p.ndim == 1 else len(p)
        lq = 1 if q.ndim == 1 else len(q)

        if lp > lq:
            return self.productv2(q, p)

        if lp > 1 and lq > 1 and lp != lq:
            raise ValueError(
                "You can only multiply an matrix with the same "
                "Numbers of rows or one with one row by another "
                "with one or more rows"
            )

        m = p.shape[-1]
        n = q.shape[-1]
        p_pad = [(0, 0)] * p.ndim
        p_pad[-1] = (0, n - 1)
        q_pad = [(0, 0)] * q.ndim
        q_pad[-1] = (0, m - 1)

        p = np.pad(p, p_pad, mode="constant", constant_values=0)
        q = np.pad(q, q_pad, mode="constant", constant_values=0)

        t = np.concatenate((2 * p[..., :1], p[..., 1:]), axis=-1)
        x = np.concatenate((2 * q[..., :1], q[..., 1:]), axis=-1)

        tp = fft(np.concatenate((t, t[..., :0:-1]), axis=-1))
        xp = fft(np.concatenate((x, x[..., :0:-1]), axis=-1))

        aux = ifft(tp * xp).real

        aux = 0.25 * np.concatenate(
            (aux[..., :1], aux[..., 1:] + aux[..., :0:-1]), axis=-1
        )
        return aux[..., : m + n - 1]

    def companion(self, c):
        """Return the scaled companion matrix of c.
        The basis Polynomials are scaled so that the companion matrix is
        symmetric when `c` is a Chebyshev basis Polynomial. This provides
        better eigenvalue estimates than the unscaled case and for basis
        Polynomials the eigenvalues are guaranteed to be real if
        `numpy.linalg.eigvalsh` is used to obtain them.

        Parameters
        ----------
        c : array_like
            An 1-D array of chebyshev coefficients ordeered from the low to high
            degree

        Raises
        ------
        ValueError
            DESCRIPTION.

        Returns
        -------
        array_like
            a 2-D scalled companion matrix of dimension (len(c)-1, len(c)-1)

        References
        ----------

        """

        # c is a trimmed copy

        if len(c) < 2:
            raise ValueError("Series must have maximum degree of at least 1.")
        if len(c) == 2:
            return np.array([[-c[0] / c[1]]])

        n = len(c) - 1
        mat = np.zeros((n, n))
        scl = np.array([1.0] + [np.sqrt(0.5)] * (n - 1))
        top = mat.reshape(-1)[1 :: n + 1]
        bot = mat.reshape(-1)[n :: n + 1]
        top[0] = np.sqrt(0.5)
        top[1:] = 1 / 2
        bot[...] = top
        mat[:, -1] -= (c[:-1] / c[-1]) * (scl / scl[-1]) * 0.5

        return mat

    def fractionalIntegral(self, c, order, b=0):
        r"""
        Frational Integral for ChebyshevT basis. Here ``c`` are the coeffi
        cients of a polynomial in Legendre basis. We compute here the Riemmann-
        Liouville Integral based on the formula:

        .. math::
            I^\alpha f(x)=\frac{1}{\Gamma(\alpha)}\int_a^x f(t)(x-t)^{\alpha
            -1}dt

        Parameters
        ----------
        c : array_like
            The coefficients of a polynomial in ChebyshevT basis.
        order : scalar
            A sclar between ]0,1[ meaning the order of the fractional integral.

        Returns
        -------
        array_like
            The fractional integral of order `order` coefficients.

        Notes
        -----
        here when :math:`\alpha=0.5` we use the relation:

        .. math::
            \left(n+\frac{1}{2}\right)(1+x)^\frac{1}{2}\int_{-1}^x (x-t)
            ^\frac{-1}{2}P_n(t)dt = T_n(x) +T_{n+1}(x),
        where :math:`P_n` are the Legendre polynomials and :math:`T_n` the Cheb
        yshev polynomials of the first kind.

        When :math:`\alpha \neq 0.5` we use the relation:

        .. math::
            \frac{(1-x)^{\alpha + \mu}P_n^{(\alpha+\mu,\ \beta -\mu)}(x)}
            {\Gamma(\alpha +\mu+n+1)}=\int_x^1 \frac{(1-y)^\alpha
            P_n^{(\alpha,\ \beta)}(y)\ (y-x)^{\mu -1}}{\Gamma(\alpha +n+1)
            \Gamma(\mu)}dy,\quad \mu >0, -1 <x<1,
        where :math:`P_n^{(\alpha,\ \beta)}` are the Jacoby polynomials with
        parameters :math:`\alpha` and :math:`\beta` respectively.

        References
        ----------
        [1] F.W.J. Olver et al., editors. NIST Handbook of Mathematical Functions.
        Cambridge University Press, New York, NY, 2010.

        """

        sc = (np.diff(self.domain) / 2) ** order
        n = c.shape[-1]

        if b == 0:
            # Conversion to Legendre basis
            cl = chebt2leg(c)

            # Scale to the domain

            # Coefficients of  half integral
            if order == 0.5:
                c_sc = cl / ((np.arange(n) + 0.5) * gamma(0.5))

                # The new coefficients
                cn = np.concatenate(
                    (
                        c_sc[..., :1],
                        c_sc[..., 1:] + c_sc[..., :-1],
                        c_sc[..., -1:],
                    ),
                    axis=-1,
                )

                # For consistency with  order != .5, we must divide by (1+x).
                e = np.ones(n)
                d = spdiags(
                    np.array(
                        [np.concatenate(((1,), 0.5 * e[1:])), e, 0.5 * e]
                    ),
                    [0, 1, 2],
                    n,
                    n,
                    format="csc",
                )
                cout = np.concatenate(
                    (spsolve(d, cn[..., 1:].T).T, np.zeros_like(cn[..., :1])),
                    axis=-1,
                )
            # Coefficients of fractional integral
            else:
                # The coefficients in Jacoby basis
                cj = (cl * beta(np.arange(1, n + 1), order)) / gamma(order)
                cout = jac2chebt(cj, -order, order)

        else:
            cj1 = jac2jac(c / scl(0, n - 1), -0.5, -0.5, 0, b)
            cj2 = (cj1 * beta(np.arange(n) + b + 1, order)) / gamma(order)

            cout = jac2chebt(cj2, -order, b + order)

        return cout * sc


class ChebyshevU(basis):
    def __init__(self, options=None):
        super().__init__(options)
        self.name = "ChebyshevU"

    def alpha(self, n):
        return 1 / 2

    def beta(self, n):
        return 0

    def gamma(self, n):
        return 1 / 2

    def eta(self, i, j):
        return 2 * ((i + j - 1) % 2) * (i + 1)

    def theta(self, n):
        return np.array(
            [
                1 / np.arange(2, 2 * n + 1, 2),
                np.zeros(n),
                np.concatenate((np.zeros(2), -1 / np.arange(6, 2 * n + 1, 2))),
            ]
        )

    def nodes(self, n=None):
        r"""
        Quadrature  points and weights for second kind Chebyshev Polynomials. Those are
        the `ǹ``  points and weights for the Clenshaw-Curtis quadrature.


        Parameters
        ----------
        n : TYPE, optional
            DESCRIPTION. The default is None.

        Returns
        -------
        TYPE
            DESCRIPTION.
        TYPE
            DESCRIPTION.

        """

        dom = self.domain

        if n is None:
            n = self.options.n
        if n > 0:
            if n == 1:
                x = 0
                w = 2

            else:
                x = np.concatenate(
                    (
                        [-1],
                        np.cos(np.arange(n - 2, 0, -1) * np.pi / (n - 1)),
                        [1],
                    )
                )
                c = 2 / np.concatenate(([1], 1 - np.arange(2, n, 2) ** 2))
                c = np.concatenate((c, c[n // 2 - 1 : 0 : -1]))
                w = ifft(c)
                w = np.concatenate((w[:1] / 2, w[1:], w[:1] / 2)).real

            # Scale and shifting of the Nodes
            x = (sum(dom) + np.diff(dom).item() * x) / 2
            # Scale the weights
            w = (np.diff(dom).item() / 2) * w
            return x, w
        return None, None

    def productv2(self, p, q):
        r"""
        Does the product of ``p`` and ``q`` where ``p``and ``q`` are coeffi-
        cients of two polynomials P and Q in legendre basis and the output is
        the coefficients of PxQ.


        Parameters
        ----------
        p : array_like
            The coefficients of a polynomial in Legendre basis.
        q : array_like
            The coefficients of a polynomial in Legendre basis.

        Raises
        ------
        ValueError
            When we do not have: 1 row x n rows, n rows x 1 row, or n rows x
            n rows.

        Returns
        -------
        array_like
            The coefficients of PxQ which.

        Examples
        --------
        >>> import tautoolbox as tau
        >>> import numpy as np
        >>> b = tau.basis(options={'basis':'LegendreP'})
        >>> p = np.arange(4)
        >>> q = np.arange(3)
        >>> b.productv2(p,q)
        array([1.1333, 3.1429, 3.0952, 4.    , 3.7714, 2.8571])
        """
        return self._ultraProduct(p, q, 1)


class ChebyshevV(basis):
    def __init__(self, options=None):
        super().__init__(options)
        self.name = "ChebyshevV"

    def alpha(self, n):
        return 1 / 2

    def beta(self, n):
        return 1 / 2 * (n == 0)

    def gamma(self, n):
        return 1 / 2

    def eta(self, i, j):
        return 2 * ((i + j - 1) % 2) * (i + 1 / 2) + j - i - 1

    def theta(self, n):
        return np.array(
            [
                np.concatenate(([0.5], 1 / np.arange(4, 2 * n + 1, 2))),
                np.concatenate(
                    ([0], -0.5 / (np.arange(1, n) * np.arange(2, n + 1)))
                ),
                np.concatenate((np.zeros(2), -1 / np.arange(4, 2 * n - 1, 2))),
            ]
        )

    # def nodes(self, n=None):
    #     if n is None:
    #         n = self.options.n
    #     x = np.cos((np.arange(n, 0, -1) - 1 / 2) * np.pi / (n + 1 / 2))
    #     return ((self.domain[0] * (1 - x) + self.domain[1] * (1 + x)) / 2,)

    def fractionalIntegral(self, c, order, b=0):
        r"""
        Frational Integral for ChebyshevV basis. Here ``c`` are
        the coefficients of a polynomial in ChebyshevV basis. We compute here the
        Riemmann-Liouville Integral based on the formula:

        .. math::
            I^\alpha f(x)=\frac{1}{\Gamma(\alpha)}\int_a^x f(t)(x-t)^{\alpha
            -1}dt

        Parameters
        ----------
        c : array_like
            The coefficients of a polynomial in above  bases.
        order : scalar
            A sclar between ]0,1[ meaning the order of the fractional integral.

        Returns
        -------
        array_like
            The fractional integral of order `order` coefficients.

        Notes
        -----
        here we use the relaction:

        .. math::
            V_n=\frac{n!^2 2^{2n}}{(2n)!}P_n^{(\frac{1}{2},\ \frac{-1}{2})}
        where :math:`P_n` are the Jacoby polynomials and :math:`V_n`  are the
        Chebyshev polynomials of the third kind.

        This make possible the conversion to jacoby basis and  next we use the
        relation below to compute the integral.

        .. math::
            \frac{(1-x)^{\alpha + \mu}P_n^{(\alpha+\mu,\ \beta -\mu)}(x)}
            {\Gamma(\alpha +\mu+n+1)}=\int_x^1 \frac{(1-y)^\alpha
            P_n^{(\alpha,\ \beta)}(y)\ (y-x)^{\mu -1}}{\Gamma(\alpha +n+1)
            \Gamma(\mu)}dy,\quad \mu >0, -1 <x<1,
        where :math:`P_n^{(\alpha,\ \beta)}` are the Jacoby polynomials with
        parameters :math:`\alpha` and :math:`\beta` respectively. These formu
        las can be found in [1, p.456 ].

        References
        ----------
        [1] F.W.J. Olver et al., editors. NIST Handbook of Mathematical Functions.
        Cambridge University Press, New York, NY, 2010.
        [2] Kamal Aghigh, et al. A survey on third and fourth kind of Chebyshev
        polynomials and their applications. Journal of Applied Mathematics and
        Computation, 2008.


        """
        # Conversion to Legendre basis

        n = c.shape[-1]

        # Scale factor of conversion from w_n to P_n(1/2,1/2)
        # au = np.arange(n)
        # scl = factorial(au) ** 2 * 2 ** (2 * au) / factorial(2 * au)

        # The coefficients in Legendre basis.
        cl = jac2jac(c / scl(0, n - 1), -0.5, 0.5, 0, b)

        # Scale to the domain
        sc = (np.diff(self.domain) / 2) ** order

        # The coefficients in Jacoby basis
        cj = (cl * beta(np.arange(1, n + 1) + b, order)) / gamma(order)

        cout = jac2jac(cj, -order, b + order, -0.5, 0.5) * scl(0, n - 1)

        return cout * sc

    def productv2(self, p, q):
        r"""
        Does the product of ``p`` and ``q`` where ``p``and ``q`` are coeffi-
        cients of two polynomials P and Q in ChebyshevW basis and the output is
        the coefficients of PxQ.


        Parameters
        ----------
        p : array_like
            The coefficients of a polynomial in Legendre basis.
        q : array_like
            The coefficients of a polynomial in Legendre basis.

        Raises
        ------
        ValueError
            When we do not have: 1 row x n rows, n rows x 1 row, or n rows x
            n rows.

        Returns
        -------
        array_like
            The coefficients of PxQ which.

        Examples
        --------

        Using Legendre basis in the  default domain [-1,1]:

        >>> import tautoolbox as tau
        >>> import numpy as np
        >>> b = tau.basis(options={'basis':'LegendreP'})
        >>> p = np.arange(4)
        >>> p
        array([0, 1, 2, 3])

        >>> q = np.arange(3)
        >>> q
        array([0, 1, 2])

        >>> b.productv2(p,q)
        array([1.1333, 3.1429, 3.0952, 4.    , 3.7714, 2.8571])
        """

        res = self._chebThreeTermsRecurrence(p, q)
        if isinstance(res, tuple):
            c0, c1 = res
            return self.add(c0, self.add(-c1, self._prodx(c1) * 2))
        return res


class ChebyshevW(basis):
    def __init__(self, options=None):
        super().__init__(options)
        self.name = "ChebyshevW"

    def alpha(self, n):
        return 1 / 2

    def beta(self, n):
        return -1 / 2 * (n == 0)

    def gamma(self, n):
        return 1 / 2

    def eta(self, i, j):
        return (2 * ((i + j - 1) % 2) * (i + 1 / 2) + j - 1 - i) * (-1) ** (
            i + j
        )

    def theta(self, n):
        return np.array(
            [
                1 / np.arange(2, 2 * n + 1, 2),
                np.r_[0, 1 / 2 * 1 / (np.arange(1, n) * np.arange(2, n + 1))],
                np.r_[0, 0, -1 / np.arange(4, 2 * n - 1, 2)],
            ]
        )

    # def nodes(self, n=None):
    #     if n is None:
    #         n = self.options.n
    #     x = np.cos(np.arange(n, 0, -1) * np.pi / (n + 1 / 2))
    #     return ((self.domain[0] * (1 - x) + self.domain[1] * (1 + x)) / 2,)

    def productv2(self, p, q):
        r"""
        Does the product of ``p`` and ``q`` where ``p``and ``q`` are coeffi-
        cients of two polynomials P and Q in ChebyshevW basis and the output is
        the coefficients of PxQ.


        Parameters
        ----------
        p : array_like
            The coefficients of a polynomial in Legendre basis.
        q : array_like
            The coefficients of a polynomial in Legendre basis.

        Raises
        ------
        ValueError
            When we do not have: 1 row x n rows, n rows x 1 row, or n rows x
            n rows.

        Returns
        -------
        array_like
            The coefficients of PxQ which.

        Examples
        --------

        Using Legendre basis in the  default domain [-1,1]:

        >>> import tautoolbox as tau
        >>> import numpy as np
        >>> b = tau.basis(options={'basis':'LegendreP'})
        >>> p = np.arange(4)
        >>> p
        array([0, 1, 2, 3])

        >>> q = np.arange(3)
        >>> q
        array([0, 1, 2])

        >>> b.productv2(p,q)
        array([1.1333, 3.1429, 3.0952, 4.    , 3.7714, 2.8571])
        """

        res = self._chebThreeTermsRecurrence(p, q)
        if isinstance(res, tuple):
            c0, c1 = res
            return self.add(c0, self.add(c1, self._prodx(c1) * 2))
        return res

    def fractionalIntegral(self, c, order, b=0):
        r"""
        Frational Integral for ChebyshevW. Here ``c`` are
        the coefficients of a polynomial in ChebyshevW basis. We compute here the
        Riemmann-Liouville Integral based on the formula:

        .. math::
            I^\alpha f(x)=\frac{1}{\Gamma(\alpha)}\int_a^x f(t)(x-t)^{\alpha
            -1}dt

        Parameters
        ----------
        c : array_like
            The coefficients of a polynomial in above  bases.
        order : scalar
            A sclar between ]0,1[ meaning the order of the fractional integral.

        Returns
        -------
        array_like
            The fractional integral of order `order` coefficients.

        Notes
        -----
        here we use the relaction:

        .. math::
            W_n=\frac{n!^2 2^{2n}}{(2n)!}P_n^{(\frac{-1}{2},\ \frac{1}{2})}
        where :math:`P_n` are the Jacoby polynomials and :math:`W_n`  are the
        Chebyshev polynomials of the fourth kind.

        This make possible the conversion to jacoby basis and  next we use the
        relation below to compute the integral.

        .. math::
            \frac{(1-x)^{\alpha + \mu}P_n^{(\alpha+\mu,\ \beta -\mu)}(x)}
            {\Gamma(\alpha +\mu+n+1)}=\int_x^1 \frac{(1-y)^\alpha
            P_n^{(\alpha,\ \beta)}(y)\ (y-x)^{\mu -1}}{\Gamma(\alpha +n+1)
            \Gamma(\mu)}dy,\quad \mu >0, -1 <x<1,
        where :math:`P_n^{(\alpha,\ \beta)}` are the Jacoby polynomials with
        parameters :math:`\alpha` and :math:`\beta` respectively. These formu
        las can be found in [1, p.456 ].

        References
        ----------
        [1] F.W.J. Olver et al., editors. NIST Handbook of Mathematical Functions.
        Cambridge University Press, New York, NY, 2010.
        [2] Kamal Aghigh, et al. A survey on third and fourth kind of Chebyshev
        polynomials and their applications. Journal of Applied Mathematics and
        Computation, 2008.


        """
        # Conversion to Legendre basis

        n = c.shape[-1]

        # Conversion based in [2, p.7]
        # The coefficients in Legendre basis.
        cj1 = jac2jac(c / scl(0, n - 1), 0.5, -0.5, 0, b)

        # Scale to the domain
        sc = (np.diff(self.domain) / 2) ** order

        # The coefficients in Jacoby basis
        cj2 = (cj1 * beta(np.arange(1, n + 1) + b, order)) / gamma(order)

        cout = jac2jac(cj2, -order, b + order, 0.5, -0.5) * scl(0, n - 1)

        return cout * sc


class GegenbauerC(basis):
    def __init__(self, options=None):
        super().__init__(options)
        self.name = "GegenbauerC"

    def alpha(self, n):
        return (n + 1) / (n + self.options.alpha) / 2

    def beta(self, n):
        return 0

    def gamma(self, n):
        return 1 - self.alpha(n)

    # why we have  (i+j+1)%2 while in the other bases we have (i+j-1)%2
    def eta(self, i, j):
        return 2 * ((i + j + 1) % 2) * (i + self.options.alpha)

    def theta(self, n):
        return np.array(
            [
                1 / (2 * self.options.alpha + np.arange(0, 2 * n - 1, 2)),
                np.zeros(n),
                np.r_[
                    0,
                    0,
                    -1 / (2 * self.options.alpha + np.arange(4, 2 * n - 1, 2)),
                ],
            ]
        )

    def productv2(self, p, q):
        r"""
        Does the product of ``p`` and ``q`` where ``p``and ``q`` are coeffi-
        cients of two polynomials P and Q in legendre basis and the output is
        the coefficients of PxQ.


        Parameters
        ----------
        p : array_like
            The coefficients of a polynomial in Legendre basis.
        q : array_like
            The coefficients of a polynomial in Legendre basis.

        Raises
        ------
        ValueError
            When we do not have: 1 row x n rows, n rows x 1 row, or n rows x
            n rows.

        Returns
        -------
        array_like
            The coefficients of PxQ which.

        Examples
        --------
        >>> import tautoolbox as tau
        >>> import numpy as np
        >>> b = tau.basis(options={'basis':'LegendreP'})
        >>> p = np.arange(4)
        >>> q = np.arange(3)
        >>> b.productv2(p,q)
        array([1.1333, 3.1429, 3.0952, 4.    , 3.7714, 2.8571])
        """
        return self._ultraProduct(p, q)


class HermiteH(basis):
    def __init__(self, options):
        super().__init__(options)
        self.c1 = 1
        self.c0 = 0
        self.support = np.array([np.inf, np.inf])
        self.name = "HermiteH"

    def alpha(self, n):
        return 1 / 2

    def beta(self, n):
        return 0

    def gamma(self, n):
        return 2 * n

    def eta(self, i, j):
        return 2 * (j - 1) * ((j - 2) == i)

    def theta(self, n):
        return np.array(
            [1 / np.arange(2, 2 * n + 1, 2), np.zeros(n), np.zeros(n)]
        )


class LaguerreL(basis):
    def __init__(self, options=None):
        super().__init__(options)
        self.support = np.array([0, np.inf])
        self.c1 = 1
        self.c0 = -self.domain[0]
        self.x1 = np.array([1 + self.domain[0], -1])
        self.name = "LaguerreL"

    def alpha(self, n):
        return -n - 1

    def beta(self, n):
        return 2 * n + 1

    def gamma(self, n):
        return -n

    def eta(self, i, j):
        return -1

    def theta(self, n):
        return np.array([-np.ones(n), np.r_[0, np.ones(n - 1)], np.zeros(n)])

    # def nodes(self,n=None):
    #     if n is None:
    #         n=self.options.n
    #     if n>0:

    def _gw_alg(self, n):
        # In the future alpha must be a parameter for generalized Laguerre
        # polynomials
        alpha = 0
        a = np.arange(1, 2 * n, 2) + alpha
        b = np.sqrt(np.arange(1, n) * (np.arange(1, n) + alpha))
        A = np.diag(b, -1) + np.diag(a) + np.diag(b, 1)
        w, v = eig(A)
        ind = np.argsort(w)

        return w[ind], v[0, ind] ** 2


class LegendreP(basis):
    def __init__(self, options=None):
        super().__init__(options)
        self.name = "LegendreP"

    def alpha(self, n):
        return (n + 1) / (2 * n + 1)

    def beta(self, n):
        return 0

    def gamma(self, n):
        return n / (2 * n + 1)

    def eta(self, i, j):
        return 2 * ((i + j + 1) % 2) * (i + 1 / 2)

    def theta(self, n):
        return np.array(
            [
                1 / np.arange(1, 2 * n, 2),
                np.zeros(n),
                np.concatenate(([0] * 2, -1 / np.arange(5, 2 * n, 2))),
            ]
        )

    def product(self, p, q):
        return legendre.legmul(p, q)

    def productv2(self, p, q):
        r"""
        Does the product of ``p`` and ``q`` where ``p``and ``q`` are coeffi-
        cients of two polynomials P and Q in legendre basis and the output is
        the coefficients of PxQ.


        Parameters
        ----------
        p : array_like
            The coefficients of a polynomial in Legendre basis.
        q : array_like
            The coefficients of a polynomial in Legendre basis.

        Raises
        ------
        ValueError
            When we do not have: 1 row x n rows, n rows x 1 row, or n rows x
            n rows.

        Returns
        -------
        array_like
            The coefficients of PxQ which.

        Examples
        --------
        >>> import tautoolbox as tau
        >>> import numpy as np
        >>> b = tau.basis(options={'basis':'LegendreP'})
        >>> p = np.arange(4)
        >>> q = np.arange(3)
        >>> b.productv2(p,q)
        array([1.1333, 3.1429, 3.0952, 4.    , 3.7714, 2.8571])
        """
        return self._ultraProduct(p, q, 0.5)

    def companion(self, c):
        """Return the scaled companion matrix of c.


        The basis Polynomials are scaled so that the companion matrix is
        symmetric when `c` is an Legendre basis Polynomial. This provides
        better eigenvalue estimates than the unscaled case and for basis
        Polynomials the eigenvalues are guaranteed to be real if
        `numpy.linalg.eigvalsh` is used to obtain them.
        Parameters
        ----------
        c : array_like
            1-D array of Legendre series coefficients ordered from low to high
            degree.
        Returns
        -------
        mat : ndarray
            Scaled companion matrix of dimensions (len(c)-1, len(c)-1).

        """
        # c is a trimmed copy
        c = c.copy()
        if len(c) < 2:
            raise ValueError("Series must have maximum degree of at least 1.")
        if len(c) == 2:
            return np.array([[-c[0] / c[1]]])

        n = len(c) - 1
        mat = np.zeros((n, n))
        scl = 1.0 / np.sqrt(2 * np.arange(n) + 1)
        top = mat.reshape(-1)[1 :: n + 1]
        bot = mat.reshape(-1)[n :: n + 1]
        top[...] = np.arange(1, n) * scl[: n - 1] * scl[1:n]
        bot[...] = top
        mat[:, -1] -= (c[:-1] / c[-1]) * (scl / scl[-1]) * (n / (2 * n - 1))
        return mat

    def quad_weights(self, n=None):
        r"""
        Compute the ``n``  Gauss-Legendre quadrature weights points.

        Parameters
        ----------
        n : TYPE, optional
            DESCRIPTION. The default is None.

        Returns
        -------
        TYPE
            DESCRIPTION.

        Notes
        -----
        This implementation is based on the formulas given by the website:
            https://pomax.github.io/bezierinfo/legendre-gauss.html

        """
        if n is None:
            n = self.options.n
        if n > 0:
            opt = tau.settings(basis="LegendreP")
            # get the abcisses x_i for the quadrature
            xi, *_ = tau.basis(opt).nodes(n)
            # Get  P'_n(x_i)
            bi = tau.Polynomial([0] * n + [1], opt).diff()(xi)
            w = 2 / ((1 - xi**2) * bi**2)
            # scale to domain
            return (np.diff(self.domain).item() / 2) * w

    def bary_weights(self, n=None):
        if n is None:
            n = self.options.n
        if n > 0:
            opt = tau.settings(basis="LegendreP")
            # get the abcisses x_i for the quadrature
            xi, *_ = tau.basis(opt).nodes(n)
            # Get  P'_n(x_i)
            bi = tau.Polynomial([0] * n + [1], opt).diff()(xi)
            v = 1 / abs(bi)
            v = v / max(v)
            v[1::2] = -v[1::2]
            return v

    def fractionalIntegral(self, c, order, b=0):
        r"""
        Frational Integral for LegendreP basis. Here ``c`` are the coefficients
        of a polynomial in Legendre basis. We compute here the Riemmann-
        Liouville Integral based on the formula:

        .. math::
            I^\alpha f(x)=\frac{1}{\Gamma(\alpha)}\int_a^x f(t)(x-t)^{\alpha
            -1}dt

        Parameters
        ----------
        c : array_like
            The coefficients of a polynomial in Legendre basis.
        order : scalar
            A sclar between ]0,1[ meaning the order of the fractional integral.

        Returns
        -------
        array_like
            The fractional integral of order `order` coefficients.

        Notes
        -----
        here when :math:`\alpha=0.5` we use the relation:

        .. math::
            \left(n+\frac{1}{2}\right)(1+x)^\frac{1}{2}\int_{-1}^x (x-t)
            ^\frac{-1}{2}P_n(t)dt = T_n(x) +T_{n+1}(x),
        where :math:`P_n` are the Legendre polynomials and :math:`T_n` the Cheb
        yshev polynomials of the first kind.

        When :math:`\alpha \neq 0.5` we use the relation:

        .. math::
            \frac{(1-x)^{\alpha + \mu}P_n^{(\alpha+\mu,\ \beta -\mu)}(x)}
            {\Gamma(\alpha +\mu+n+1)}=\int_x^1 \frac{(1-y)^\alpha
            P_n^{(\alpha,\ \beta)}(y)\ (y-x)^{\mu -1}}{\Gamma(\alpha +n+1)
            \Gamma(\mu)}dy,\quad \mu >0, -1 <x<1,
        where :math:`P_n^{(\alpha,\ \beta)}` are the Jacoby polynomials with
        parameters :math:`\alpha` and :math:`\beta` respectively.

        References
        ----------
        [1] F.W.J. Olver et al., editors. NIST Handbook of Mathematical Functions.
        Cambridge University Press, New York, NY, 2010.

        """

        n = c.shape[-1]
        sc = (np.diff(self.domain) / 2) ** order
        if b == 0:
            # Coefficients of  half integral in Chebyshev basis
            if order == 0.5:
                c_sc = c / ((np.arange(n) + 0.5) * gamma(0.5))

                # The new coefficients
                cn = np.r_[
                    "-1",
                    c_sc[..., :1],
                    c_sc[..., 1:] + c_sc[..., :-1],
                    c_sc[..., -1:],
                ]

                # For consistency with  order != .5, we must divide by (1+x).
                e = np.ones(n)
                d = spdiags(
                    np.r_["0,2", np.r_[1, 0.5 * e[1:]], e, 0.5 * e],
                    [0, 1, 2],
                    n,
                    n,
                    format="csc",
                )
                ct = np.r_[
                    "-1",
                    spsolve(d, cn[..., 1:].T).T,
                    np.zeros_like(cn[..., :1]),
                ]
                return chebt2leg(ct) * sc
            # Coefficients of fractional integral
            else:
                # The coefficients in Jacoby basis
                cj = (c * beta(np.arange(1, n + 1), order)) / gamma(order)
                return jac2jac(cj, -order, order, 0, 0) * sc

        else:
            # For Legendre the scale factor is 0
            cj1 = jac2jac(c, 0, 0, 0, b)
            cj2 = (cj1 * beta(np.arange(n) + b + 1, order)) / gamma(order)
            cout = jac2jac(cj2, -order, b + order, 0, 0)
        return cout * sc


class PowerX(basis):
    def __init__(self, options=None):
        super().__init__(options)
        self.name = "PowerX"

    def alpha(self, n):
        return 1

    def beta(self, n):
        return 0

    def gamma(self, n):
        return 0

    def eta(self, i, j):
        return (j - 1) * ((j - 2) == i)

    def theta(self, n):
        return np.array([1 / np.arange(n) + 1, np.zeros(n), np.zeros(n)])
