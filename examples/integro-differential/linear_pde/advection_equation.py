import numpy as np
from matplotlib import pyplot as plt
import tautoolbox as tau

plt.style.use("ggplot")

optx = {"domain": [-3, 3]}
opty = {"domain": [0, 6]}
opt = (optx, opty)
Pr = tau.Problem2(
    lambda u: u.diff((0, 1)) - 0.1 * u.diff((2, 0)) - u.diff((1, 0)),
    options=opt,
)
Pr.dbc = lambda x: np.sin(np.pi * x) * (x / 6 + 1 / 2) ** 2
Pr.rbc = 0
Pr.lbc = "neumann"  # or  lambda y, u: u.diff()
u = Pr.solve()
fig = plt.figure(figsize=(6, 6))
ax = u.plot()
ax.view_init(elev=20, azim=-130)
ax.set_xlabel("$x$")
ax.set_ylabel("$y$")
ax.set_zlabel("$u$")
ax.locator_params(nbins=5)
plt.show()
