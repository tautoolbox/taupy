import numpy as np
from matplotlib import pyplot as plt
import tautoolbox as tau

plt.style.use("ggplot")

#  Poisson equation with zero Dirichlet conditions: u(+-1,y)=u(x,+-1)=0
#  lap(u) -1 =0,  in [[-1, 1],[-1, 1]]                      #

dom = [[-1, 1]] * 2
Pr = tau.Problem2(lambda u: u.laplacian() - 1)
Pr.bc = 0
u = Pr.solve()

# Plot
x, y = np.linspace(*dom[0], 100), np.linspace(*dom[1], 100)
fig = plt.figure()
ax = u.plot(cmap="ocean")
ax.set_xlabel("$x$")
ax.set_ylabel("$y$")
ax.set_zlabel("$u$")

plt.show()

