import numpy as np
from matplotlib import pyplot as plt
import tautoolbox as tau

plt.style.use("ggplot")

# lap(u) =0 in [[-1,1]]*2 
# exact soluction is f(x,y)= exp(x-y)cos(x+y)  

f = lambda x, y: np.exp(x - y) * np.cos(x + y)

domain = [[-1, 2], [-1, 1]] * 2
optx = {"domain": domain[0]}
opty = {"domain": domain[1]}
Pr = tau.Problem2(lambda u: u.laplacian(), options=(optx, opty))
Pr.bc = f

u = Pr.solve()

x = np.linspace(*domain[0])
y = np.linspace(*domain[1])
xx, yy = np.meshgrid(x, y)

ex = f(xx, yy)  # Exact values evaluated on the grid
ap = u.evalm(x, y)
err = ex - ap

##plots
# Eexact soluction
fig = plt.figure(figsize=(12, 6))
ax = fig.add_subplot(121, projection="3d")
u.plot(ax=ax, cmap="ocean")
ax.set_xlabel("$x$")
ax.set_ylabel("$y$")
ax.set_zlabel("$u$")
ax.set_title("$u=P(x,y)$")
ax.locator_params(nbins=5)

# Eerror
ax = fig.add_subplot(122, projection="3d")
ax.plot_surface(xx, yy, err, cmap="ocean")
ax.set_xlabel("$x$")
ax.set_ylabel("$y$")
ax.set_zlabel("$u$")
ax.set_title("$u=P(x,y) -f(x,y)$")
ax.locator_params(nbins=5)
ax.set_facecolor("w")
plt.show()
