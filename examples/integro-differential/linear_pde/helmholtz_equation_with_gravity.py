import numpy as np
from matplotlib import pyplot as plt
import tautoolbox as tau

plt.style.use("ggplot")

# Helmholtz equation with gravity
#  laplacian(u) - 10*y**2*u =0, in [[-1,1],[-3,0]]
#  u(+-1,y)=u(x,-3)=u(x,0)=1

optx = {"basis": "ChebyshevT", "domain": [-1, 1]}
opty = {"basis": "ChebyshevT", "domain": [-3, 0]}
dom = np.array([optx["domain"], opty["domain"]])
Pr = tau.Problem2(
    lambda x, y, u: u.laplacian() - 10 * y**2 * u, options=(optx, opty)
)
Pr.bc = 1
u = Pr.solve()
x, y = np.linspace(*dom[0], 100), np.linspace(*dom[1], 100)

# Plots
plt.figure()
ax = u.plot()
ax.set_xlabel("$x$")
ax.set_ylabel("$y$")
ax.set_zlabel("$u$")

plt.locator_params(nbins=5)
plt.show()
