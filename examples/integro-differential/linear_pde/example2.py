import numpy as np
from matplotlib import pyplot as plt
import tautoolbox as tau

plt.style.use("ggplot")

# 2*d^2u/dx^2+d^2u/dy^2 - 10*x**2*u =0, in [[-1,1],[-3,0]]
# u(+-1,y)=u(x,-3)=u(x,0)=1

optx = {"basis": "ChebyshevT", "domain": [-1, 1]}
opty = {"basis": "ChebyshevT", "domain": [-3, 0]}
dom = np.array([optx["domain"], opty["domain"]])
Pr = tau.Problem2(
    lambda x, y, u: 2 * u.diff((2, 0)) + u.diff((0, 2)) - 10 * x**2 * u,
    options=(optx, opty),
)
Pr.bc = 1
u = Pr.solve()

# Plots
plt.figure()
ax = u.plot(cmap="ocean")
plt.locator_params(nbins=5)
ax.set_xlabel("$x$")
ax.set_ylabel("$y$")
ax.set_zlabel("$u$")

plt.show()
