
import tautoolbox as tau
from tautoolbox.functions import *
import matplotlib.pyplot as plt
import numpy as np

ode = [
    lambda x, y: diff(y) - y - fred(y, lambda x, t: 1 / (x + exp(t))),
    lambda x: -log((x + exp(1)) / (x + 1)),
]
condition = [lambda y: y(0) - 1]
options = tau.settings(basis="ChebyshevU", domain=[0, 1], degree=20)
domain = [0, 1]
problem = tau.problem(ode, domain, condition, options)
yn = tau.solve(problem)[0]

x = linspace(yn)
plt.semilogy(x, abs(exp(x) - yn(x)))
print(max(abs(exp(x) - yn(x))))
