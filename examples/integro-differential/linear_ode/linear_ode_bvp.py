import tautoolbox as tau
import numpy as np
from tautoolbox.functions import diff, linspace
import matplotlib.pyplot as plt

# Example of a differential equation with conditions given at the boundary
# equation = "diff(y, 2) + y=0"
equation = lambda x, y: diff(y, 2) + y
domain = [0, 2 * np.pi]

conditions = lambda y: [y(0) - 1, y.diff(1, 2 * np.pi)]
options = tau.settings(degree=10, basis="ChebyshevU", domain=domain)
problem = tau.problem(equation, domain, conditions, options)
[yn, info, residual, tauresidual] = tau.solve(problem)

x = linspace(yn, 100)
y_p, residual_p, tauresidual_p = (yn(x), residual(x), tauresidual(x))

fig = plt.figure(figsize=plt.figaspect(0.5))
ax = fig.add_subplot(1, 3, 1, title="Solution")
ax.plot(x, y_p)
ax = fig.add_subplot(1, 3, 2, title="residual")
ax.plot(x, residual_p)
ax = fig.add_subplot(1, 3, 3, title="tauresidual")
ax.plot(x, tauresidual_p)
plt.show()
