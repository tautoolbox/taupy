
import tautoolbox as tau
from tautoolbox.functions import diff, linspace, polyval
import matplotlib.pyplot as plt
import numpy as np

a = 0.4
equation = lambda x, y: (
    (a ** 4 - 4 * a ** 3 * x + 4 * a ** 2 * x ** 2 + 2 * a ** 2 - 4 * a * x + 1)
    * diff(y, 2)
    - a * (-2 * a * x + a ** 2 + 1) * diff(y)
    - 2 * a ** 2 * y
)
conditions = ["y(-1)=1/(1+0.4)", "y(1)=1/(1-0.4)"]
domain = [-1, 1]
options = tau.settings(degree=50)
problem = tau.problem(equation, domain, conditions, options)
yn = tau.solve(problem)[0]

x = linspace(yn)
plt.semilogy(x, np.abs(polyval(yn, x) - (1 - 2 * a * x + a ** 2) ** -0.5))
