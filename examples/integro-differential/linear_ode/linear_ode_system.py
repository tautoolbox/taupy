import tautoolbox as tau
import matplotlib.pyplot as plt
from tautoolbox.functions import diff

# para ser consistente com o python começo o indice das variaveis em 0
# Describe the problem to be solved

# equations = [
#     "y1-1*diff(y0)=0",
#     "y2-1*diff(y1)=0",
#     "(x**2+1)*diff(y2)-(x**2+3*x)*diff(y1)+5*x*y1-5*y0=60*x**2-10",
# ]
equations = [
    lambda x, y: y[2] - diff(y[1]),
    lambda x, y: y[3] - diff(y[2]),
    [
        lambda x, y: (
            (x**2 + 1) * diff(y[3])
            - (x**2 + 3 * x) * diff(y[2])
            + 5 * x * y[2]
            - 5 * y[1]
        ),
        lambda x: 60 * x**2 - 10,
    ],
]

domain = [-1, 1]
conditions = ["y1(-1)=4", "y2(1)=2", "y3(0)=0"]
options = tau.settings(degree=10)
problem = tau.problem(equations, domain, conditions, options)
yn = tau.solve(problem)
xx = yn.linspace(100)
yy = yn(xx)
ye = tau.Polynomial(
    [
        lambda x: x**5 - 3 * x + 2,
        lambda x: 5 * x**4 - 3,
        lambda x: 20 * x**3,
    ],
    options,
)


error = yn - ye
y_error = error(xx)
fig = plt.figure(figsize=plt.figaspect(0.5))
ax = fig.add_subplot(1, 2, 1, title="Solution")
ax.plot(
    xx,
    yy[0],
    label="$y_1$",
    color="red",
)
ax.plot(xx, yy[1], label="$y_2$", color="green")
ax.plot(xx, yy[2], label="$y_3$", color="blue")
# show legend
ax.legend()
ax = fig.add_subplot(1, 2, 2, title="Error")
ax.plot(
    xx,
    y_error[0],
    label="$y_1$ error",
    color="red",
)
ax.plot(xx, y_error[1], label="$y_2$ error", color="green")
ax.plot(xx, y_error[2], label="$y_3$ error", color="blue")
# show legend
ax.legend(loc="lower right")


# show graph
plt.show()
