import numpy as np
from matplotlib import pyplot as plt
import tautoolbox as tau
from scipy.special import airy

plt.style.use("ggplot")

# f(x,y) = exp(-100*(x**2 - x*y + 2*y**2 – 1/2)**2)
f_str = "$real(airy(5 * (x + y^2)) * airy(-5 * (x^2 + y^2)))$"

optx = {"basis": "LegendreP", "alpha": 0.5}

f = lambda x, y: np.real(
    airy(5 * (x + y**2))[0] * airy(-5 * (x**2 + y**2))[0]
)

p = tau.Polynomial2(f)

# Plot of the Polynomial2 approximation
fig = plt.figure()
ax1 = p.plot(cmap="ocean")
ax1.locator_params(axis="z", nbins=3)
ax1.locator_params(axis="x", nbins=3)
ax1.locator_params(axis="y", nbins=3)
plt.show()

# Contour plot of the aproximation
fig = plt.figure()
ax2 = p.contour(pivots=True)

ax2.locator_params(axis="x", nbins=5)
ax2.locator_params(axis="y", nbins=5)
ax2.set_aspect("equal", adjustable="box")
plt.show()
