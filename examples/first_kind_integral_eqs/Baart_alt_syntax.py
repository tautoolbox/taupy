import tautoolbox as tau
import matplotlib.pyplot as plt
import numpy as np

# baart solved as a problem

# set options and the problem
opts = {"domain": [0, np.pi / 2]}
optt = {"domain": [0, np.pi]}

# set the problem
ke = lambda s, t: np.exp(s * np.cos(t))
g = lambda s: 2 * np.sinh(s) / s
# here t is the dependent variable and y is the operator
eq = [lambda t, y: y.fred1(ke, options=(opts, optt)), g]
problem = tau.problem(eq)
f = tau.Polynomial(lambda t: np.sin(t), optt)

# solve using TSVE
p1 = tau.solve(problem, alpha=0.01)
rel_error_TSVE = (p1 - f).norm() / f.norm()

# solve using Tikhonov regularization
p2 = tau.solve(problem, method="tr", alpha=0.01)
rel_error_tikhonov = (p2 - f).norm() / f.norm()

# plots
plt.figure()
f.plot(), p1.plot(), p2.plot()
plt.legend(["exact", "TSVE", "Tikhonov"])
plt.title("Baart problem")
plt.show()