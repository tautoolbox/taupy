import tautoolbox as tau
import matplotlib.pyplot as plt

# Fox-Goodwin problem

# set options
opts = {"domain": [0, 1]}

# set the problem
ke = tau.Polynomial2(lambda s, t: (s**2 + t**2) ** (1 / 2), options=opts)
g = tau.Polynomial(lambda s: 1 / 3 * ((1 + s**2) ** (3 / 2) - s**3), opts)
f = tau.Polynomial(options=opts)

# solve with TSVE and compute the relative error
p1 = ke.fredholm1(g, alpha=0.01)
rel_error_TSVE = (p1 - f).norm() / f.norm()

# solve using Tikhonov regularization and compute the relative error
p2 = ke.fredholm1(g, method="tr", alpha=0.01)
rel_error_tikhonov = (p2 - f).norm() / f.norm()

# plots
plt.figure()
f.plot(), p1.plot(), p2.plot()
plt.legend(["exact", "TSVE", "Tikhonov"])
plt.title("Fox-Goodwin problem")
plt.show()