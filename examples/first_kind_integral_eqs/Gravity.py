import tautoolbox as tau
import numpy as np
import matplotlib.pyplot as plt

# Gravity problem

# set options and the problem
opts = {"domain": [0, 1]}

# set the problem
d = 0.25
ke = tau.Polynomial2(lambda s, t: d * (d**2 + (s - t) ** 2) ** (-3 / 2), options=opts)
f = tau.Polynomial(lambda t: np.sin(np.pi * t) + 0.5 * np.sin(2 * np.pi * t), opts)
g = (ke * f).sum(axis=1)

alpha = 1e-2 # noise rhs

# solve with TSVE and compute the relative error
p1 = ke.fredholm1(g, alpha=alpha); 
rel_error_TSVE = (p1 - f).norm() / f.norm()
    
# solve using Tikhonov regularization and compute the relative error
p2 = ke.fredholm1(g, method="tr", alpha=alpha); 
rel_error_tikhonov = (p2 - f).norm() / f.norm()

# plots
plt.figure()
f.plot(), p1.plot(), p2.plot()
plt.legend(["exact", "TSVE", "Tikhonov"])
plt.title("Gravity problem")
plt.show()
    