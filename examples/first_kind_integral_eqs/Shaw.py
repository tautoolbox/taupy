import tautoolbox as tau
import numpy as np
import matplotlib.pyplot as plt

# Shawn problem

# set options and the problem
opts = {"domain": [-np.pi / 2, np.pi / 2]}

# set the problem
a1, a2, c1, c2, t1, t2 = 2, 1, 6, 2, 0.8, -0.5
f = tau.Polynomial(
    lambda t: a1 * np.exp(-c1 * (t - t1) ** 2) + a2 * np.exp(-c2 * (t - t2) ** 2),
    options = opts
)
u  = tau.Polynomial2(lambda s, t: np.pi * (np.sin(s) + np.sin(t)), opts)
q  = tau.Polynomial (lambda x:    (np.sin(x) / x) ** 2, {"domain": [-2, 2]})
qu = tau.Polynomial2(lambda s, t: q(u(s, t)), opts)
cs = tau.Polynomial2(lambda s, t: np.cos(s) + np.cos(t), opts)
ke = cs * qu
g = (ke.T * f).sum(axis=1)

alpha = 0.0 # no noise on the rhs

# solve with TSVE and compute the relative error 
p1 = ke.fredholm1(g, alpha=alpha); 
rel_error_TSVE = (p1 - f).norm() / f.norm()
rel_error_TSVE
    
# solve using Tikhonov regularization and compute the relative error
p2 = ke.fredholm1(g, method="tr", alpha=alpha); 
rel_error_tikhonov = (p2 - f).norm() / f.norm()
rel_error_tikhonov

# plots
plt.figure()
f.plot(), p1.plot(), p2.plot()
plt.legend(["exact", "TSVE", "Tikhonov"])
plt.title("Shaw problem")
plt.show()
