import tautoolbox as tau
import numpy as np

# Wing problem

# set options and the problem
opts = {"domain": [0, 1]}

# set the problem
ke = tau.Polynomial2(lambda s, t: t * np.exp(-s * t**2), options=opts)
t1, t2 = 1/3, 2/3
g  = lambda s: (np.exp(-s * t1**2) - np.exp(-s * t2**2)) / (2 * s)
f  = lambda t: np.logical_and(t1 < t, t < t2) * 1
x  = np.linspace(*opts["domain"], 100)

alpha = 1e-2 # noise rhs

# solve with TSVE and compute the relative error
p1 = ke.fredholm1(g, alpha=alpha); 
rel_error_TSVE = np.linalg.norm(p1(x) - f(x)) / np.linalg.norm(f(x))
    
# solve using Tikhonov regularization and compute the relative error
p2 = ke.fredholm1(g, method="tr", alpha=alpha);  
rel_error_tikhonov = np.linalg.norm(p2(x) - f(x)) / np.linalg.norm(f(x))
