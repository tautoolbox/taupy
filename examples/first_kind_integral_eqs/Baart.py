import tautoolbox as tau
import matplotlib.pyplot as plt
import numpy as np

# baart problem

# set options and the problem
opts = {"basis": "ChebyshevT", "domain": [0, np.pi / 2]}
optt = {"basis": "ChebyshevT", "domain": [0, np.pi]}

# set the problem
ke = tau.Polynomial2(lambda s, t: np.exp(s * np.cos(t)), options=(opts, optt))
g  = tau.Polynomial (lambda s   : 2 * np.sinh(s) / s, opts)
f  = tau.Polynomial (lambda t   : np.sin(t), optt)

# solve with TSVE and compute the relative error
p1 = ke.fredholm1(g, alpha=0.01)
rel_error_TSVE = (p1 - f).norm() / f.norm()

# solve using Tikhonov regularization and compute the relative error
p2 = ke.fredholm1(g, method="tr", alpha=0.01)
rel_error_tikhonov = (p2 - f).norm() / f.norm()

# plot
plt.figure()
f.plot(), p1.plot(), p2.plot()
plt.legend(["exact", "TSVE", "Tikhonov"])
plt.title("Baart problem")
plt.show()