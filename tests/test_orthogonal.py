import pytest
import tautoolbox as tau
import numpy as np

bases = [
    "ChebyshevT",
    "ChebyshevU",
    "LegendreP",
    "GegenbauerC",
    "PowerX",
    "BesselY",
    "ChebyshevV",
    "ChebyshevW",
    "HermiteH",
    "LaguerreL",
]

# The sets of domains to test
domains = [[-1, 1], [0, 1]]
# The sets of coefficients to test
coeffs = [np.arange(1, 5), np.arange(12).reshape(3, 4)]
# The sets of values to test
values = [1, np.arange(4), np.arange(9).reshape(3, 3)]

#%% testing basis 'ChebyshevT' in the domian [-1,1]
bas = tau.basis(tau.settings(basis=bases[0], domain=domains[0]))

#%%% Test the method polyvalv2
# Test in the case the coefficients which are a vector that represents a poly-
# nomial in this basis
assert bas.polyvalv2(coeffs[0], values[0]) == 10
assert (
    bas.polyvalv2(coeffs[0], values[1]) == np.array([-2.0, 10.0, 130.0, 454.0])
).all()

assert (
    bas.polyvalv2(coeffs[0], values[2])
    == np.array(
        [
            [-2.000e00, 1.000e01, 1.300e02],
            [4.540e02, 1.078e03, 2.098e03],
            [3.610e03, 5.710e03, 8.494e03],
        ]
    )
).all()

# Test for the case coefficients  are a matrix in which each rows represents
# one Polynomial
assert (bas.polyvalv2(coeffs[1], values[0]) == np.array([6.0, 22.0, 38.0])).all()
assert (
    bas.polyvalv2(coeffs[1], values[1])
    == np.array(
        [
            [-2.0, 6.0, 94.0, 334.0],
            [-2.0, 22.0, 238.0, 814.0],
            [-2.0, 38.0, 382.0, 1294.0],
        ]
    )
).all()
assert (
    bas.polyvalv2(coeffs[1], values[2])
    == np.array(
        [
            [
                [-2.0000e00, 6.0000e00, 9.4000e01],
                [3.3400e02, 7.9800e02, 1.5580e03],
                [2.6860e03, 4.2540e03, 6.3340e03],
            ],
            [
                [-2.0000e00, 2.2000e01, 2.3800e02],
                [8.1400e02, 1.9180e03, 3.7180e03],
                [6.3820e03, 1.0078e04, 1.4974e04],
            ],
            [
                [-2.0000e00, 3.8000e01, 3.8200e02],
                [1.2940e03, 3.0380e03, 5.8780e03],
                [1.0078e04, 1.5902e04, 2.3614e04],
            ],
        ]
    )
).all()

#%%% Testing the method polyval
assert bas.polyval(coeffs[0], values[0]) == 10
assert (bas.polyval(coeffs[0], values[1]) == np.array([-2.0, 10.0, 130.0, 454.0])).all()

assert (
    bas.polyval(coeffs[0], values[2])
    == np.array(
        [
            [-2.000e00, 1.000e01, 1.300e02],
            [4.540e02, 1.078e03, 2.098e03],
            [3.610e03, 5.710e03, 8.494e03],
        ]
    )
).all()
