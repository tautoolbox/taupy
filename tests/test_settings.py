# Copyright (C) 2021, University of Porto and Tau Toolbox developers.
#
# This file is part of Tautoolbox package.
#
# Tautoolbox is free software: you can redistribute it and/or modify it
# under the terms of version 3 of the GNU Lesser General Public License as
# published by the Free Software Foundation.
#
# Tau Toolbox is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser
# General Public License for more details.

import pytest
import tautoolbox as tau

__author__ = "José Matos"
__copyright__ = "José Matos"
__license__ = "lgpl3"


def test_settings():
    options = tau.settings()
    assert options.degree == 31
    assert options.degree + 1 == options.n
