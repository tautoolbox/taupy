#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Apr  4 19:37:24 2023

@author: nilson
"""

import tautoolbox as tau
import numpy as np

####################### The construction of Polynomial2 #######################

##### Using adaptive grid #####

# %% Testing f(x,y) = exp(-100*(x**2 - x*y + 2*y**2 – 1/2)**2)  in the stand-%%#
#   dard domain i.e. ChebyshevT basis in the domain [-1,1]

f = lambda x, y: np.exp(-100 * (x**2 - x * y + 2 * y**2 - 1 / 2) ** 2)
p = tau.Polynomial2(f)

assert p.sampletest(f, 1e-11)

# %%%%%%%%%%%%%% 2) f(x,y) = cos(10*x*(1+y**2)) %%%%%%%%%%%%%%#

domain = [[0, 1]] * 2
opt = {"domain": domain[0]}
f = lambda x, y: np.cos(10 * x * (1 + y**2))
p = tau.Polynomial2(f, options=opt)

assert p.sampletest(f, 1e-13)


# %%%%%%%%%%%%%% 2) f(x,y) = cos(10*x*(1+y**2)) %%%%%%%%%%%%%%#
domain = [[0, 1], [-1, 1]]
optx = {"basis": "ChebyshevU", "domain": domain[0]}
opty = {"basis": "LegendreP", "domain": domain[1]}
opt = (optx, opty)

f = lambda x, y: np.cos(10 * x * (1 + y**2))
p = tau.Polynomial2(f, options=opt)

assert p.sampletest(f, 1e-13)

# %% f(x,y)= x**2+2*x*y**2 + 5*x**3*y +x*y
domain = [[0, 1], [-1, 1]]
optx = {"basis": "GegenbauerC", "domain": domain[0], "alpha": 0.6}
opty = {"basis": "GegenbauerC", "domain": domain[1], "alpha": 1.2}

f = lambda x, y: x**2 + 2 * x * y**2 + 5 * x**3 * y + x * y

p = tau.Polynomial2(f, options=(optx, opty))

assert p.sampletest(f, 1e-13)

# %%%%%%%%%%%%%% 2) f(x,y) = cos(10*x*(1+y**2)) %%%%%%%%%%%%%%#
domain = [[0, 1], [-1, 1]]
optx = {"basis": "ChebyshevU", "domain": domain[0]}
opty = {"basis": "LegendreP", "domain": domain[1]}

f = lambda x, y: np.cos(10 * x * (1 + y**2))
p = tau.Polynomial2(f, options=(optx, opty))

assert p.sampletest(f, 1e-13)


###### Using fixed grid #######

# %%%%%%%%%%%% f(x,y) = exp(-100*(x**2 - x*y + 2*y**2 – 1/2)**2) %%%%%%%%%%%%%%#
# Using aca with a fixed grid

f_str = "f(x,y) = e^{-100(x^2 - xy + 2y^2 – 1/2)^2}"
optx = {"basis": "LegendreP", "alpha": 0.5}
domain = [[-1, 1]] * 2
f = lambda x, y: np.exp(-100 * (x**2 - x * y + 2 * y**2 - 1 / 2) ** 2)


p = tau.Polynomial2(f, optx, method="aca", grid_shape=(200, 100))

assert p.sampletest(f, 1e-7)

# %%%%%%%%%%%% f(x,y) = exp(-100*(x**2 - x*y + 2*y**2 – 1/2)**2) %%%%%%%%%%%%%%#
# Using aca with a fixed grid
f_str = "f(x,y) = e^{-100(x^2 - xy + 2y^2 – 1/2)^2}"
optx = {"basis": "LegendreP", "alpha": 0.5}
domain = [[-1, 1]] * 2
f = lambda x, y: np.exp(-100 * (x**2 - x * y + 2 * y**2 - 1 / 2) ** 2)


p = tau.Polynomial2(f, options=optx, method="svd", grid_shape=(200, 100))

assert p.sampletest(f, 1e-7)


######################### Operation on Polynomial2   # ########################

# %%%%%%% Testing the product

# %%%%%% f(x,y) = cos(10*x*(1+y**2))  and g(x,y) = (1+10*(x+2*y)**2)**-1 %%%%%%%
f = lambda x, y: np.cos(10 * x * (1 + y**2))
g = lambda x, y: (1 + 10 * (x + 2 * y) ** 2) ** -1
domain = [[0, 1], [-1, 1]]
optx = {"basis": "ChebyshevU", "domain": domain[0]}
opty = {"basis": "LegendreP", "domain": domain[1]}
opt = (optx, opty)

fp = tau.Polynomial2(f, options=opt)
gp = tau.Polynomial2(g, options=opt)

fm = lambda x, y: f(x, y) * g(x, y)
pm = fp * gp

assert pm.sampletest(fm, 1e-12)


# %%%%%%%%% testing the addition

# %%%%%% f(x,y) = cos(10*x*(1+y**2))  and g(x,y) = (1+10*(x+2*y)**2)**-1 %%%%%%%
f = lambda x, y: np.cos(10 * x * (1 + y**2))
g = lambda x, y: (1 + 10 * (x + 2 * y) ** 2) ** -1
domain = [[0, 1], [-1, 1]]
optx = {"basis": "ChebyshevU", "domain": domain[0]}
opty = {"basis": "LegendreP", "domain": domain[1]}
opt = (optx, opty)
fp = tau.Polynomial2(f, options=opt)
gp = tau.Polynomial2(g, options=opt)

fm = lambda x, y: f(x, y) + g(x, y)
pm = fp + gp
assert pm.sampletest(fm, 1e-11)

# %%%%% Testing the static method interporth2 %%%%%%

# Using the function f(x,y)=cos(x+y) in the standard domain and a grid 20x20
_, res, *_ = tau.Polynomial2.interporth2(lambda x, y: np.cos(x + y), 20)


assert res < 1e-14
