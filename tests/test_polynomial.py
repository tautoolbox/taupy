#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Apr 11 21:08:48 2023

@author: nilson
"""
import tautoolbox as tau
import numpy as np

# Test values for the derivative of order 2.7 in the caputo sense in [-1,1]
test1c = np.array(
    [
        -0.0,
        -0.52217039,
        -0.53002519,
        -0.45295069,
        -0.32149584,
        -0.15239877,
        0.04069979,
        0.24508131,
        0.44854762,
        0.63956442,
    ]
)

# Test values for the derivative of order 2.7 in the caputo sense in [0,1]
test2c = np.array(
    [
        -0.0,
        0.04918294,
        0.1205123,
        0.20249086,
        0.29096917,
        0.38317776,
        0.47690665,
        0.5702447,
        0.66147549,
        0.74903217,
    ]
)


# Test values for the derivative of order 2.7 in the Riemann-Liouville sense in
# [-1,1]
test1r = np.array(
    [
        np.inf,
        8.89366861,
        0.28927861,
        -0.44282233,
        -0.46278117,
        -0.32306298,
        -0.12885984,
        0.08480793,
        0.29923724,
        0.50086021,
    ]
)

# Test values for the derivative of order 2.7 in the Riemann-Liouville sense in
# [0,1]
test2r = np.array(
    [
        np.inf,
        148.49692047,
        22.24711374,
        7.20582884,
        3.25382682,
        1.82359763,
        1.22168559,
        0.95571661,
        0.84518799,
        0.81254399,
    ]
)


########################### Polynomial construction ##########################

# %%%%%%%%%%%%%%%% Using ChebyshevT basis in the domain [-1,1] %%%%%%%%%%%%%%%%#
p = tau.Polynomial(np.cos)

assert ((p(p.linspace()) - np.cos(p.linspace())) < 1e-15).all()

# %%%%%%%%%%%%%%% Using ChebyshevU basis in the domain [-1,1] %%%%%%%%%%%%%%%%%#

p = tau.Polynomial(np.cos, {"basis": "ChebyshevU"})

assert ((p(p.linspace()) - np.cos(p.linspace())) < 1e-15).all()

# %%%%%%%%%%%%%%% Using LegendreP basis in the domain [-1,1] %%%%%%%%%%%%%%%%%#

p = tau.Polynomial(np.cos, {"basis": "LegendreP"})

assert ((p(p.linspace()) - np.cos(p.linspace())) < 1e-15).all()

# %%%%%%%%%%%%%%% Using GegenbauerC basis with alpha = 0.5 in the domain [-1,1] %%%%%%%%%%%%%%%%%#

p = tau.Polynomial(np.cos, {"basis": "GegenbauerC"})

assert ((p(p.linspace()) - np.cos(p.linspace())) < 1e-15).all()


# %%%%%%%%%%%%%%% Using GegenbauerC basis with alpha = 0.7 in the domain [-1,1] %%%%%%%%%%%%%%%%%#

p = tau.Polynomial(np.cos, {"basis": "GegenbauerC", "alpha": 0.7})

assert ((p(p.linspace()) - np.cos(p.linspace())) < 1e-15).all()


############################### Polynomial Operation #########################

############# Product #############
#### ChebyshevT basis #####
# %%%%%%%%%%%%%%%% Using ChebyshevT basis in the domain [-1,1] %%%%%%%%%%%%%%%%#
p = tau.Polynomial(np.cos)

q = tau.Polynomial(np.sin)
ptq = p * q
f = lambda x: np.cos(x) * np.sin(x)

assert np.max(abs(ptq(p.linspace()) - f(p.linspace()))) < 1e-15

# %%%%%%%%%%%%%%%% Using ChebyshevT basis in the domain [0,1] %%%%%%%%%%%%%%%%#
p = tau.Polynomial(np.cos, {"domain": [0, 1]})

q = tau.Polynomial(np.sin, {"domain": [0, 1]})
ptq = p * q
f = lambda x: np.cos(x) * np.sin(x)
assert np.max(abs(ptq(p.linspace()) - f(p.linspace()))) < 1e-15

#### LegendreP basis #####
# %%%%%%%%%%%%%%%% Using LegendreP basis in the domain [-1,1] %%%%%%%%%%%%%%%%#
p = tau.Polynomial(np.cos, {"basis": "LegendreP"})

q = tau.Polynomial(np.sin, {"basis": "LegendreP"})
ptq = p * q
f = lambda x: np.cos(x) * np.sin(x)
assert np.max(abs(ptq(p.linspace()) - f(p.linspace()))) < 1e-15

# %%%%%%%%%%%%%%%% Using LegendreP basis in the domain [0,1] %%%%%%%%%%%%%%%%#
p = tau.Polynomial(np.cos, {"basis": "LegendreP", "domain": [0, 1]})

q = tau.Polynomial(np.sin, {"basis": "LegendreP", "domain": [0, 1]})
ptq = p * q
f = lambda x: np.cos(x) * np.sin(x)
assert np.max(abs(ptq(p.linspace()) - f(p.linspace()))) < 1e-15

#### ChebyshevU basis #####
# %%%%%%%%%%%%%%%% Using LegendreP basis in the domain [-1,1] %%%%%%%%%%%%%%%%#
p = tau.Polynomial(np.cos, {"basis": "ChebyshevU"})

q = tau.Polynomial(np.sin, {"basis": "ChebyshevU"})
ptq = p * q
f = lambda x: np.cos(x) * np.sin(x)
assert np.max(abs(ptq(p.linspace()) - f(p.linspace()))) < 1e-15

# %%%%%%%%%%%%%%%% Using LegendreP basis in the domain [0,1] %%%%%%%%%%%%%%%%#
p = tau.Polynomial(np.cos, {"basis": "ChebyshevU", "domain": [0, 1]})

q = tau.Polynomial(np.sin, {"basis": "ChebyshevU", "domain": [0, 1]})
ptq = p * q
f = lambda x: np.cos(x) * np.sin(x)
assert np.max(abs(ptq(p.linspace()) - f(p.linspace()))) < 1e-15

#### GegenbauerC basis with alpha=0.6 #####
# %%%%%%%%%%%%%%%% Using LegendreP basis in the domain [-1,1] %%%%%%%%%%%%%%%%#
p = tau.Polynomial(np.cos, {"basis": "GegenbauerC", "alpha": 0.6})

q = tau.Polynomial(np.sin, {"basis": "GegenbauerC", "alpha": 0.6})
ptq = p * q
f = lambda x: np.cos(x) * np.sin(x)
assert np.max(abs(ptq(p.linspace()) - f(p.linspace()))) < 1e-15

# %%%%%%%%%%%%%%%% Using LegendreP basis in the domain [0,1] %%%%%%%%%%%%%%%%#
p = tau.Polynomial(
    np.cos, {"basis": "GegenbauerC", "alpha": 0.6, "domain": [0, 1]}
)

q = tau.Polynomial(
    np.sin, {"basis": "GegenbauerC", "alpha": 0.6, "domain": [0, 1]}
)
ptq = p * q
f = lambda x: np.cos(x) * np.sin(x)
assert np.max(abs(ptq(p.linspace()) - f(p.linspace()))) < 1e-15


##############  Adiction ##########

#### ChebyshevT basis #####
# %%%%%%%%%%%%%%%% Using ChebyshevT basis in the domain [-1,1] %%%%%%%%%%%%%%%%#
p = tau.Polynomial(np.cos)

q = tau.Polynomial(np.sin)
paq = p + q
f = lambda x: np.cos(x) + np.sin(x)

assert np.max(abs(paq(p.linspace()) - f(p.linspace()))) < 1e-15

# %%%%%%%%%%%%%%%% Using ChebyshevT basis in the domain [0,1] %%%%%%%%%%%%%%%%#
p = tau.Polynomial(np.cos, {"domain": [0, 1]})

q = tau.Polynomial(np.sin, {"domain": [0, 1]})
paq = p + q
f = lambda x: np.cos(x) + np.sin(x)
assert np.max(abs(paq(p.linspace()) - f(p.linspace()))) < 1e-15

#### LegendreP basis #####
# %%%%%%%%%%%%%%%% Using LegendreP basis in the domain [-1,1] %%%%%%%%%%%%%%%%#
p = tau.Polynomial(np.cos, {"basis": "LegendreP"})

q = tau.Polynomial(np.sin, {"basis": "LegendreP"})
paq = p + q
f = lambda x: np.cos(x) + np.sin(x)
assert np.max(abs(paq(p.linspace()) - f(p.linspace()))) < 1e-15

# %%%%%%%%%%%%%%%% Using LegendreP basis in the domain [0,1] %%%%%%%%%%%%%%%%#
p = tau.Polynomial(np.cos, {"basis": "LegendreP", "domain": [0, 1]})

q = tau.Polynomial(np.sin, {"basis": "LegendreP", "domain": [0, 1]})
paq = p + q
f = lambda x: np.cos(x) + np.sin(x)
assert np.max(abs(paq(p.linspace()) - f(p.linspace()))) < 1e-14


#### ChebyshevU basis #####
# %%%%%%%%%%%%%%%% Using ChebyshevU basis in the domain [-1,1] %%%%%%%%%%%%%%%%#
p = tau.Polynomial(np.cos, {"basis": "ChebyshevU"})

q = tau.Polynomial(np.sin, {"basis": "ChebyshevU"})
paq = p + q
f = lambda x: np.cos(x) + np.sin(x)
assert np.max(abs(paq(p.linspace()) - f(p.linspace()))) < 1e-15

# %%%%%%%%%%%%%%%% Using ChebyshevU basis in the domain [0,1] %%%%%%%%%%%%%%%%#
p = tau.Polynomial(np.cos, {"basis": "ChebyshevU", "domain": [0, 1]})

q = tau.Polynomial(np.sin, {"basis": "ChebyshevU", "domain": [0, 1]})
paq = p + q
f = lambda x: np.cos(x) + np.sin(x)
assert np.max(abs(paq(p.linspace()) - f(p.linspace()))) < 1e-14

#### GegenbauerC basis with alpha=0.6 #####
# %%%%%%%%%%%%%%%% Using GegenbauerC in the domain [-1,1] %%%%%%%%%%%%%%%%#
p = tau.Polynomial(np.cos, {"basis": "GegenbauerC", "alpha": 0.6})

q = tau.Polynomial(np.sin, {"basis": "GegenbauerC", "alpha": 0.6})
paq = p + q
f = lambda x: np.cos(x) + np.sin(x)
assert np.max(abs(paq(p.linspace()) - f(p.linspace()))) < 1e-15

# %%%%%%%%%%%%%%%% Using GegenbauerC basis in the domain [0,1] %%%%%%%%%%%%%%%%#
opt = {"basis": "GegenbauerC", "alpha": 0.6, "domain": [0, 1]}
p = tau.Polynomial(np.cos, opt)
q = tau.Polynomial(np.sin, opt)
ptq = p + q
f = lambda x: np.cos(x) + np.sin(x)
assert np.max(abs(ptq(p.linspace()) - f(p.linspace()))) < 1e-15


##############  Power ##########

#### ChebyshevT basis #####
# %%%%%%%%%%%%%%%% Using ChebyshevT basis in the domain [-1,1] %%%%%%%%%%%%%%%%#
p = tau.Polynomial(np.cos)

pp = p**3
f = lambda x: np.cos(x) ** 3

assert np.max(abs(pp(p.linspace()) - f(p.linspace()))) < 1e-14

# %%%%%%%%%%%%%%%% Using ChebyshevT basis in the domain [0,1] %%%%%%%%%%%%%%%%#
p = tau.Polynomial(np.cos, {"domain": [0, 1]})

pp = p**3
f = lambda x: np.cos(x) ** 3
assert np.max(abs(pp(p.linspace()) - f(p.linspace()))) < 1e-15

#### LegendreP basis #####
# %%%%%%%%%%%%%%%% Using LegendreP basis in the domain [-1,1] %%%%%%%%%%%%%%%%#
p = tau.Polynomial(np.cos, {"basis": "LegendreP"})

pp = p**3
f = lambda x: np.cos(x) ** 3
assert np.max(abs(pp(p.linspace()) - f(p.linspace()))) < 1e-15

# %%%%%%%%%%%%%%%% Using LegendreP basis in the domain [0,1] %%%%%%%%%%%%%%%%#
p = tau.Polynomial(np.cos, {"basis": "LegendreP", "domain": [0, 1]})

pp = p**3
f = lambda x: np.cos(x) ** 3
assert np.max(abs(pp(p.linspace()) - f(p.linspace()))) < 1e-14

#### ChebyshevU basis #####
# %%%%%%%%%%%%%%%% Using ChebyshevU basis in the domain [-1,1] %%%%%%%%%%%%%%%%#
p = tau.Polynomial(np.cos, {"basis": "ChebyshevU"})

pp = p**3
f = lambda x: np.cos(x) ** 3
assert np.max(abs(pp(p.linspace()) - f(p.linspace()))) < 1e-15

# %%%%%%%%%%%%%%%% Using ChebyshevU basis in the domain [0,1] %%%%%%%%%%%%%%%%#
p = tau.Polynomial(np.cos, {"basis": "ChebyshevU", "domain": [0, 1]})

pp = p**3
f = lambda x: np.cos(x) ** 3
assert np.max(abs(pp(p.linspace()) - f(p.linspace()))) < 1e-14

#### GegenbauerC basis with alpha=0.6 #####
# %%%%%%%%%%%%%%%% Using GegenbauerC in the domain [-1,1] %%%%%%%%%%%%%%%%#
p = tau.Polynomial(np.cos, {"basis": "GegenbauerC", "alpha": 0.6})


pp = p**3
f = lambda x: np.cos(x) ** 3
assert np.max(abs(pp(p.linspace()) - f(p.linspace()))) < 1e-15

# %%%%%%%%%%%%%%%% Using GegenbauerC basis in the domain [0,1] %%%%%%%%%%%%%%%%#
opt = {"basis": "GegenbauerC", "alpha": 0.6, "domain": [0, 1]}
p = tau.Polynomial(np.cos, opt)

pp = p**3
f = lambda x: np.cos(x) ** 3
assert np.max(abs(pp(p.linspace()) - f(p.linspace()))) < 1e-15

##############  derivative ##########

#### ChebyshevT basis #####
# %%%%%%%%%%%%%%%% Using ChebyshevT basis in the domain [-1,1] %%%%%%%%%%%%%%%%#
p = tau.Polynomial(np.cos)

# Derivative of integer order
pd = p.diff(2)
f = lambda x: -np.cos(x)

assert np.max(abs(pd(p.linspace()) - f(p.linspace()))) < 1e-12

# %%%%%%%%%%%%%%%% Using ChebyshevT basis in the domain [-1,1] %%%%%%%%%%%%%%%%#
p = tau.Polynomial(np.cos)

# Derivative of fractional order in the Caputo sense
pd = p.diff(2.7)
pdev = pd(p.linspace(10))

assert np.allclose(pdev, test1c)

# %%%%%%%%%%%%%%%% Using ChebyshevT basis in the domain [-1,1] %%%%%%%%%%%%%%%%#
p = tau.Polynomial(np.cos)

# Derivative of fractional order in the Riemann-Liuville sense
pd = p.diff(2.7, "rl")
pdev = pd(p.linspace(10))

assert np.allclose(pdev, test1r)


# %%%%%%%%%%%%%%%% Using ChebyshevT basis in the domain [0,1] %%%%%%%%%%%%%%%%#
p = tau.Polynomial(np.cos, {"domain": [0, 1]})

# Derivative of integer order
pd = p.diff(2)
f = lambda x: -np.cos(x)

assert np.max(abs(pd(p.linspace()) - f(p.linspace()))) < 1e-12

# %%%%%%%%%%%%%%%% Using ChebyshevT basis in the domain [0,1] %%%%%%%%%%%%%%%%#
p = tau.Polynomial(np.cos, {"domain": [0, 1]})

# Derivative of fractional order in the Caputo sense
pd = p.diff(2.7)
pdev = pd(p.linspace(10))

assert np.allclose(pdev, test2c)

# %%%%%%%%%%%%%%%% Using ChebyshevT basis in the domain [0,1] %%%%%%%%%%%%%%%%#
p = tau.Polynomial(np.cos, {"domain": [0, 1]})

# Derivative of fractional order in the Riemann-Liuville sense
pd = p.diff(2.7, "rl")
pdev = pd(p.linspace(10))

assert np.allclose(pdev, test2r)


#### LegendreP basis #####
# %%%%%%%%%%%%%%%% Using LegendreP basis in the domain [-1,1] %%%%%%%%%%%%%%%%#
p = tau.Polynomial(np.cos, {"basis": "LegendreP"})

# Derivative of integer order
pd = p.diff(2)
f = lambda x: -np.cos(x)

assert np.max(abs(pd(p.linspace()) - f(p.linspace()))) < 1e-12

# %%%%%%%%%%%%%%%% Using LegendreP basis in the domain [-1,1] %%%%%%%%%%%%%%%%#
p = tau.Polynomial(np.cos, {"basis": "LegendreP"})

# Derivative of fractional order in the Caputo sense
pd = p.diff(2.7)
pdev = pd(p.linspace(10))

assert np.allclose(pdev, test1c)

# %%%%%%%%%%%%%%%% Using LegendreP basis in the domain [-1,1] %%%%%%%%%%%%%%%%#
p = tau.Polynomial(np.cos, {"basis": "LegendreP"})

# Derivative of fractional order in the Riemann-Liuville sense
pd = p.diff(2.7, "rl")
pdev = pd(p.linspace(10))

assert np.allclose(pdev, test1r)


# %%%%%%%%%%%%%%%% Using LegendreP basis in the domain [0,1] %%%%%%%%%%%%%%%%#
p = tau.Polynomial(np.cos, {"basis": "LegendreP", "domain": [0, 1]})

# Derivative of integer order
pd = p.diff(2)
f = lambda x: -np.cos(x)

assert np.max(abs(pd(p.linspace()) - f(p.linspace()))) < 1e-12

# %%%%%%%%%%%%%%%% Using LegendreP basis in the domain [0,1] %%%%%%%%%%%%%%%%#
p = tau.Polynomial(np.cos, {"basis": "LegendreP", "domain": [0, 1]})

# Derivative of fractional order in the Caputo sense
pd = p.diff(2.7)
pdev = pd(p.linspace(10))

assert np.allclose(pdev, test2c)

# %%%%%%%%%%%%%%%% Using LegendreP basis in the domain [0,1] %%%%%%%%%%%%%%%%#
p = tau.Polynomial(np.cos, {"basis": "LegendreP", "domain": [0, 1]})

# Derivative of fractional order in the Riemann-Liuville sense
pd = p.diff(2.7, "rl")
pdev = pd(p.linspace(10))
assert np.allclose(pdev, test2r)

#### ChebyshevU basis #####
# %%%%%%%%%%%%%%%% Using ChebyshevU basis in the domain [-1,1] %%%%%%%%%%%%%%%%#
p = tau.Polynomial(np.cos, {"basis": "ChebyshevU"})

# Derivative of integer order
pd = p.diff(2)
f = lambda x: -np.cos(x)

assert np.max(abs(pd(p.linspace()) - f(p.linspace()))) < 1e-12

# %%%%%%%%%%%%%%%% Using ChebyshevU basis in the domain [-1,1] %%%%%%%%%%%%%%%%#
p = tau.Polynomial(np.cos, {"basis": "ChebyshevU"})

# Derivative of fractional order in the Caputo sense
pd = p.diff(2.7)
pdev = pd(p.linspace(10))

assert np.allclose(pdev, test1c)

# %%%%%%%%%%%%%%%% Using ChebyshevU basis in the domain [-1,1] %%%%%%%%%%%%%%%%#
p = tau.Polynomial(np.cos, {"basis": "ChebyshevU"})

# Derivative of fractional order in the Riemann-Liuville sense
pd = p.diff(2.7, "rl")
pdev = pd(p.linspace(10))

assert np.allclose(pdev, test1r)


# %%%%%%%%%%%%%%%% Using ChebyshevU basis in the domain [0,1] %%%%%%%%%%%%%%%%#
p = tau.Polynomial(np.cos, {"basis": "ChebyshevU", "domain": [0, 1]})

# Derivative of integer order
pd = p.diff(2)
f = lambda x: -np.cos(x)

assert np.max(abs(pd(p.linspace()) - f(p.linspace()))) < 1e-10

# %%%%%%%%%%%%%%%% Using ChebyshevU basis in the domain [0,1] %%%%%%%%%%%%%%%%#
p = tau.Polynomial(np.cos, {"basis": "ChebyshevU", "domain": [0, 1]})

# Derivative of fractional order in the Caputo sense
pd = p.diff(2.7)
pdev = pd(p.linspace(10))

assert np.allclose(pdev, test2c)

# %%%%%%%%%%%%%%%% Using ChebyshevU basis in the domain [0,1] %%%%%%%%%%%%%%%%#
p = tau.Polynomial(np.cos, {"basis": "ChebyshevU", "domain": [0, 1]})

# Derivative of fractional order in the Riemann-Liuville sense
pd = p.diff(2.7, "rl")
pdev = pd(p.linspace(10))
assert np.allclose(pdev, test2r)

#### GegenbauerC basis with alpha=0.6 #####
# %%%%%%%%%%%%%%%% Using GegenbauerC in the domain [-1,1] %%%%%%%%%%%%%%%%#
p = tau.Polynomial(np.cos, {"basis": "GegenbauerC", "alpha": 0.6})

# Derivative of integer order
pd = p.diff(2)
f = lambda x: -np.cos(x)

assert np.max(abs(pd(p.linspace()) - f(p.linspace()))) < 1e-12

# %%%%%%%%%%%%%%%% Using GegenbauerC in the domain [-1,1] %%%%%%%%%%%%%%%%#
p = tau.Polynomial(np.cos, {"basis": "GegenbauerC", "alpha": 0.6})

# Derivative of fractional order in the Caputo sense
pd = p.diff(2.7)
pdev = pd(p.linspace(10))

assert np.allclose(pdev, test1c)

# %%%%%%%%%%%%%%%% Using GegenbauerC in the domain [-1,1] %%%%%%%%%%%%%%%%#
p = tau.Polynomial(np.cos, {"basis": "GegenbauerC", "alpha": 0.6})

# Derivative of fractional order in the Riemann-Liuville sense
pd = p.diff(2.7, "rl")
pdev = pd(p.linspace(10))

assert np.allclose(pdev, test1r)


# %%%%%%%%%%%%%%%% Using GegenbauerC basis in the domain [0,1] %%%%%%%%%%%%%%%%#
opt = {"basis": "GegenbauerC", "alpha": 0.6, "domain": [0, 1]}
p = tau.Polynomial(np.cos, opt)

# Derivative of integer order
pd = p.diff(2)
f = lambda x: -np.cos(x)

assert np.max(abs(pd(p.linspace()) - f(p.linspace()))) < 1e-10

# %%%%%%%%%%%%%%%% Using GegenbauerC basis in the domain [0,1] %%%%%%%%%%%%%%%%#
opt = {"basis": "GegenbauerC", "alpha": 0.6, "domain": [0, 1]}
p = tau.Polynomial(np.cos, opt)
# Derivative of fractional order in the Caputo sense
pd = p.diff(2.7)
pdev = pd(p.linspace(10))

assert np.allclose(pdev, test2c)

# %%%%%%%%%%%%%%%% Using GegenbauerC basis in the domain [0,1] %%%%%%%%%%%%%%%%#
opt = {"basis": "GegenbauerC", "alpha": 0.6, "domain": [0, 1]}
p = tau.Polynomial(np.cos, opt)

# Derivative of fractional order in the Riemann-Liuville sense
pd = p.diff(2.7, "rl")
pdev = pd(p.linspace(10))
assert np.allclose(pdev, test2r)
