# Tau Toolbox

   AUTHORS:

       Paulo B. Vasconcelos
       Centro de Matemática - Universidade do Porto
       Email: pjv@fep.up.pt

       José M. A. Matos
       Centro de Matemática - Universidade do Porto
       Email: jma@isep.ipp.pt

       José A. O. Matos
       Centro de Matemática - Universidade do Porto
       Email: jamatos@fep.up.pt

       Nilson Lima

   REFERENCE:

       Solving differential eigenproblems via the spectral Tau method
       NUMERICAL ALGORITHMS
       DOI: https://doi.org/10.1007/s11075-022-01366-z

   SOFTWARE REVISION DATE:

       Version 0.21 : 2024-02-01

   SOFTWARE LANGUAGE:

       MATLAB 9.12.0 (R2022a)
       Octave 8.1.0


Tau Toolbox is a Python library for the solution of integro-differential
problems based on the Lanczos' Tau method.

Tau Toolbox is free software released under the GNU Lesser General Public
License version 3 (LGPLv3). A copy of the License is enclosed in the project.

## Installation

Just clone the Tau Toolbox repository following
`git clone https://bitbucket.org/tautoolbox/taupy.git`.

Tau Toolbox can also be obtained at https://cmup.fc.up.pt/tautoolbox/.

`Tautoolbox` requires Python 3.6 together with the following modules:

 * `dataclasses` (part of the standard library starting from python 3.7)
 * `numpy`
 * `matplotlib`

### Help

A Tau Toolbox User Guide will be made available soon, along with a set of
Technical Reports.

### Support

The project is very recent. Nevertheless it has been thoroughly tested.
The project supports the tool in the sense that reports of errors or
poor performance will gain immediate attention from the developers.

## Getting started

The best way to get started is by using the many
[examples](https://bitbucket.org/tautoolbox/taupy/src/main/examples/)
provided.

Tackle your problems with ease:

* To solve the ordinary differential problem
$y''(x)+y(x)= 0$ in $[0,2\pi]$ with $y(0)=1$ and $y'(2\pi)=0$,
just type:

```python
import tautoolbox as tau
import numpy as np
from tautoolbox.functions import diff, linspace
import matplotlib.pyplot as plt

equation = lambda x, y: diff(y, 2) + y
domain = [0, 2 * np.pi]
conditions = lambda y: [y(0) - 1, y.diff(1, 2 * np.pi)]
problem = tau.problem(equation, domain, conditions)
yn = tau.solve(problem)[0]

x = linspace(yn)
plt.plot(x, yn(x))
```

* To solve the Fredholm integral equation of the first kind
$\int_0^\pi ke(s,t) f(t) dt = g(s)$, $s \in [0,\frac{\pi}{2}]$,  $t \in [0,\pi]$
with kernel $ke(s,t) = exp(s*cos(t))$, solution $f(t) = f_0 + sin(t)$ and 
rhs: $g(s) = 2\frac{sinh(s)}{s}$ if $f_0=0$

[M. L. Baart, The use of auto-correlation for pseudo-rank 
determination in noisy ill-conditioned linear least-squares
problems, IMA J. Numer. Anal. 2 (1982), 241-247. Example 4.2]

just type:

```python
import tautoolbox as tau
import matplotlib.pyplot as plt
import numpy as np

# set options and the problem
opts = {"basis": "ChebyshevT", "domain": [0, np.pi / 2]}
optt = {"basis": "ChebyshevT", "domain": [0, np.pi]}
ke = tau.Polynomial2(lambda s, t: np.exp(s * np.cos(t)), options=(opts, optt))
g  = tau.Polynomial (lambda s   : 2 * np.sinh(s) / s   , opts)
f  = tau.Polynomial (lambda t   : np.sin(t)            , optt)

# Solve with TSVE and compute the relative error
p1 = ke.fredholm1(g)
rel_error_TSVE = (p1 - f).norm() / f.norm()

# Solve using Tikhonov regularization and compute the relative error
p2 = ke.fredholm1(g, method="tr")
rel_error_tikhonov = (p2 - f).norm() / f.norm()

plt.figure()
f.plot(), p1.plot(), p2.plot()
plt.legend(["exact", "TSVE", "Tikhonov"])
plt.title("Baart problem")
plt.show()
```