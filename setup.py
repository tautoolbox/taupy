import setuptools
from setuptools.config import read_configuration

with open("README.rst", "r") as fh:
    long_description = fh.read()

conf_dict = read_configuration("setup.cfg")

setuptools.setup(
    name="tautoolbox",  # Replace with your own username
    version="0.21",
    author="Tau Toolbox authors",
    author_email="tautoolbox@fc.up.pt",
    description="A package that implements Lanczos' Tau method to solve differential problems",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://cmup.fc.up.pt/tautoolbox",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: GNU Lesser General Public License v3 (LGPLv3)",
        "Operating System :: OS Independent",
    ],
    python_requires=">=3.6",
)
